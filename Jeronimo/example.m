X=[0 1 0 0
   0 0 1 0
   0 0 0 1]
Y=[0.1 1.2 1
   -0.1 0   1  
   0 0   0]

clf;
PlotTetrahedron(X,'b');
view(-37.5,30)
PlotTriangle(Y,'r');
[P,nc,s]=TetrahedraTriangleIntersection(X,Y)

for i=1:1000
  X=rand(3,4);  
  %Y=rand(3,3);
  %  X(:,1)=zeros(3,1);
  % Y(:,1)=zeros(3,1);
  % X(:,2)=ones(3,1);
  % Y(:,2)=ones(3,1)/2;
  % X(:,3)=[ones(2,1);0];
  % Y(:,3)=[ones(2,1);0]/2;
  Y(:,1)=1/2*X(:,1)+1/3*X(:,2)+1/6*X(:,3);
  Y(:,2)=1/3*X(:,1)+1/2*X(:,2)+1/6*X(:,3);
  Y(:,3)=1/6*X(:,1)+1/3*X(:,2)+1/2*X(:,3);
  
  clf;
  PlotTetrahedron(X,'b');
  PlotTriangle(Y,'r');
  view(-37.5,30)
  [P,nc,s]=TetrahedraTriangleIntersection(X,Y)
  grid on
  pause
end;

% a numerically difficult case:

X=[0 1 0 0
   0 0 1 0
   0 0 0 1]
Y=[0 1 0
   0 0 1  
   0 0 0]

clf;
PlotTetrahedron(X,'b');
view(-37.5,30)
PlotTriangle(Y,'r');
[P,nc,s]=TetrahedraTriangleIntersection(X,Y)

X=[1 1 0 
   1 0 1
   1 0 0]
Y=[1 1 1/2
   1 0 1  
   1 0 1/2]

clf;
PlotTriangle(X,'blue',1);
PlotTriangle(Y,'red',1);

[P,nc,s]=CheckForColinearity(X,Y);


% take a tetrahedra mesh and a 3d triangular mesh:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

d=0.0;
[N1,T1]=NewMesh3d(1);
[N1,T1]=RefineMesh3d(N1,T1);
[N1,T1]=RefineMesh3d(N1,T1);
%for i=1:size(N1,2)
%  for j=1:3
%    if N1(j,i)>0 & N1(j,i)<1
%      N1(j,i)=N1(j,i)+d*(rand(1,1)-0.5);
%    end
%  end
%end

[N2,T2]=NewMesh(1);
[N2,T2]=RefineMesh(N2,T2);
[N2,T2]=RefineMesh(N2,T2);
[N2,T2]=RefineMesh(N2,T2);
%[N2,T2]=RefineMesh(N2,T2);
%f=@(x,y) (x+y)/2;
%f=@(x,y) sin(x).*sin(y);
% f=@(x,y) 0.25
%N2=Convert2dMeshTo3d(N2,f);
N2=Convert2dMeshTo3d(N2,'ff');

dd=1e-14;
dd=0;
for i=1:size(N2,2)
  for j=1:3
    if N2(j,i)>0 & N2(j,i)<1
      N2(j,i)=N2(j,i)+dd*(rand(1,1)-0.5);
      %      N2(j,i)=N2(j,i)+0.1
    end
  end
end

% pretreatement to round grids to single precision:

N1=round(N1*1/sqrt(eps))*sqrt(eps);
N2=round(N2*1/sqrt(eps))*sqrt(eps);

clf
PlotMesh3d(N1,T1,'b');
Plot2dInterface3d(N2,T2,'r');
S=ProjectionMatrix2dWith3d(N1,T1,N2,T2)

S=ProjectionMatrix2dWith3dOld(N2,T2,N1,T1)
S=ProjectionMatrix2dWith3dNum(N2,T2,N1,T1)



% cas difficile de Jeronimo

d=0.0;
[N1,T1]=NewMesh3d(1);
[N1,T1]=RefineMesh3d(N1,T1);
[N1,T1]=RefineMesh3d(N1,T1);
for i=1:size(N1,2)
  for j=1:3
    if N1(j,i)>0 & N1(j,i)<1
      N1(j,i)=N1(j,i)+d*(rand(1,1)-0.5);
    end
  end
end

[N2,T2]=NewMesh(1);
[N2,T2]=RefineMesh(N2,T2);
[N2,T2]=RefineMesh(N2,T2);
[N2,T2]=RefineMesh(N2,T2);
%[N2,T2]=RefineMesh(N2,T2);
%f=@(x,y) (x+y)/2;
% f=@(x,y) sin(x).*sin(y);
% f=@(x,y) 0.5
% N2=Convert2dMeshTo3d(N2,f);
N2=Convert2dMeshTo3d(N2,'ff');
% N2(3,:) = 0.25;
% N2(3,1) = 0.0;

ep=1.d-15;   %% Tester avec 1.d-8, 1.d-9, 1.d-10, 1.d-11, etc et 0.
ep=0
for i=1:size(N2,2)
%       N2(3,i)=(N2(1,i)+N2(2,i))/2.0;
      N2(3,i)=min((N2(1,i)+(1-N2(2,i)))/2.0*2.0 + (1-rand())*ep,0.9+ (1-rand())*ep);
end


area_tot = 0;
for k=1:size(T2,1)
    area_tot = area_tot+norm(cross( N2(:,T2(k,2)) - N2(:,T2(k,1)) , ...
                                    N2(:,T2(k,3)) - N2(:,T2(k,1))),2)/2.0;
end

clf
PlotMesh3d(N1,T1,'b');
Plot2dInterface3d(N2,T2,'r');

S=ProjectionMatrix2dWith3d(N1,T1,N2,T2);
fprintf('Area de la vela: %e \n', area_tot);
fprintf('Area de las intersecciones: %e \n', S);
fprintf('Error en el calculo de areas: %e \n', S - area_tot);

