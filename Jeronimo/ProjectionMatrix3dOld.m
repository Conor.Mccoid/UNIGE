function V=ProjectionMatrixOld(Na,Ta,Nb,Tb);
% PROJECTIONMATRIX3D projection matrix for nonmatching tetrahedra grids
%   M=ProjectionMatrix3D(Na,Ta,Nb,Tb); takes two tetrahedra
%   meshes Ta and Tb with associated nodal coordinates in Na and Nb
%   and computes the projection matrix M.

%aviobj = avifile('example3dfa.avi','fps',400)
%frame = getframe(gca);
%aviobj = addframe(aviobj,frame);
%pause(0)

ep=10*eps;
na=size(Ta,1);
nb=size(Tb,1);
for i=1:na                           % compute volume of each tetrahedra
  sa(i)=PyramidVolume(Na(:,Ta(i,1:3)),Na(:,Ta(i,4)));
end

al=[1];               % a list: list of tetrahedra of Ta to treat
bl=[1];               % b list: list of tetrahedra of Tb to treat
ad=zeros(na+1,1);     % a done: flag indicating tetrahedras in Ta treated
bd=zeros(nb+1,1);     % b done: flag indicating tetrahedras in Tb treated 
ad(end)=1;            % guard, to treat boundaries
bd(end)=1;
ad(1)=1;
bd(1)=1;
V=0;
while length(bl)>0
  anl=[];             % a new list: next list of triangles of Ta 
  bc=bl(1);           % b current: current tetrahedra of Tb 
  bl=bl(2:end);       
  ibc=0;              % integral of bc
  while length(al)>0
    ac=al(1);
    al=al(2:end);
    [s,P]=IntersectionOld(Na(:,Ta(ac,1:4)),Nb(:,Tb(bc,1:4)));
%    frame = getframe(gca);
%    aviobj = addframe(aviobj,frame);
%    pause(0)
    V=V+s;
    if s>0
      ibc=ibc+s;
      sa(ac)=sa(ac)-s;
      t=Ta(ac,4+find(ad(Ta(ac,5:8))==0)); % neighbors to add
      al=[al t];
      ad(t)=1;
    end
    if abs(sa(ac))>ep
      anl=[anl ac];
    end
  end
  t=Tb(bc,4+find(bd(Tb(bc,5:8))==0));
  bl=[bl t];
  bd(t)=1;
  al=anl;
end
%aviobj = close(aviobj);

      
      
      
      
      
      
      
