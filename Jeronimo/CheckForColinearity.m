function [P,nc,s]=CheckForColinearity(X,Y);
% CHECKFORCOLINEARITY check if two triangles are colinear
%   [P,nc,s]=CheckForColinearity(X,Y); checks if two triangles X and Y are
%   almost colinear. Returns P=1 if they are, and P=[] otherwise. 
%   The arguments nc and s are just for compatibility purposes so
%   one can also use [P,nc,s]=CheckForColinearityAndCompute(X,Y);


mc=10;                               % numerical tolerance for colinearity
e1=Y(:,2)-Y(:,1);
e2=Y(:,3)-Y(:,1);
if rank([X(:,2)-X(:,1) X(:,3)-X(:,1) e1 e2])<3 ...
        & abs(det([X(:,1) Y;ones(1,4)]))<mc*eps       
  P=1;nc=[];s=0;  
else
  P=[];nc=[];s=0;  
end;

    