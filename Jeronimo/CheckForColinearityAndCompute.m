function [P,nc,s]=CheckForColinearityAndCompute(X,Y);
% CHECKFORCOLINEARITYANDCOMPUTE check if two triangles are colinear
%   [P,nc,s]=CheckForColinearityAndCompute(X,Y); checks if two
%   triangles X and Y are almost colinear, and if they are, compute an
%   accurate intersection by projection. Also return the intersection
%   area in s, and in nc for which neighbors of X the triangle Y also
%   intersects.

mc=10;                               % numerical tolerance for colinearity
e1=Y(:,2)-Y(:,1);
e2=Y(:,3)-Y(:,1);
if rank([X(:,2)-X(:,1) X(:,3)-X(:,1) e1 e2])<3 ...
        & abs(det([X(:,1) Y;ones(1,4)]))<mc*eps       
  [Q,dum]=qr([e1,e2]);
  Xp=Q(:,1:2)'*X; Yp=Q(:,1:2)'*Y;                 % project into the same plane
  [P2d,nc,s]=TriangleTriangleIntersection(Xp,Yp); % use triangle intersection
  if isempty(P2d)
    P=[];
  else
    P=Q(:,1:2)*P2d+repmat((Y(:,1)'*Q(:,3))*Q(:,3),1,size(P2d,2));
  end
else
  P=[];nc=[];s=0;  
end;

    