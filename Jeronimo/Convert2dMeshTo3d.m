function N=Convert2dMeshTo3d(N,f)
% CONVERT@DMESHTO3D convert a 2d triangular mesh to 3d    
%   N3=Convert2dMeshTo3d(N,f) converts the 2d mesh given by the
%   nodal points in the matrix N, stored columwise, by adding a
%   third coordinate to each point, with the value of the function
%   f evaluated at the node.
    
 N(3,:)=feval(f,N(1,:),N(2,:));
%N(3,:)=f(N(1,:),N(2,:));
%N(3,:)=0;
