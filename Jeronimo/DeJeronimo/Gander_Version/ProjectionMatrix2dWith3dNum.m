function S=ProjectionMatrix2dWith3dNum(Na,Ta,Nb,Tb);
% PROJECTIONMATRIX2DWITH3D projection for triangular grid in 3d tetrahedras
%   M=ProjectionMatrix2dWith3dNum(Na,Ta,Nb,Tb); takes a triangular mesh
%   Ta in 3d and a tetrahedra mesh Tb with associated nodal
%   coordinates in Na and Nb and computes the projection matrix M. As
%   a precondition, the first triangle in Ta and the first tetrahedra
%   in Tb need to intersect.
%   Attention: in this numerical routine, mesh nodes are perturbed
%   to avoid pathological situations

S=0;                           % total area of the intersection
bl=[1];                        % bl: list of tetrahedra of Tb to treat
bil=[1];                       % bil: list of triangles Ta to start with
bd=zeros(size(Tb,1)+1,1);      % bd: flag for tetrahedra in Tb treated 
bd(end)=1;                     % guard, to treat boundaries
bd(1)=1;                       % mark first tetrahedra in b list.
while length(bl)>0
  bc=bl(1); bl=bl(2:end);      % bc: current tetrahedra of Tb 
  al=bil(1); bil=bil(2:end);   % triangle of Ta to start with
  ad=zeros(size(Ta,1)+1,1);    % same as for bd
  ad(end)=1;
  ad(al)=1; 
  n=[0 0 0 0];                 % tetrahedra intersecting with neighbors
  while length(al)>0
    ac=al(1); al=al(2:end);    % take next candidate
    [P,nc,s,Nb(:,Tb(bc,1:4)),Na(:,Ta(ac,1:3))]=TetrahedraTriangleIntersectionNum(Nb(:,Tb(bc,1:4)),Na(:,Ta(ac,1:3)));
    if ~isempty(P)             % intersection found
      t=Ta(ac,3+find(ad(Ta(ac,4:6))==0));
      al=[al t];               % add neighbors
      ad(t)=1;
      n(find(nc>0))=ac;        % ac is starting candidate for neighbor  
      S=S+s;
    end
  end
  tmp=find(bd(Tb(bc,5:8))==0); % find non-treated neighbors
  idx=find(n(tmp)>0);          % only if intersecting with Ta
  t=Tb(bc,4+tmp(idx));
  bl=[bl t];                   % and add them
  bil=[bil n(tmp(idx))];       % with starting candidates Ta
  bd(t)=1;
end
