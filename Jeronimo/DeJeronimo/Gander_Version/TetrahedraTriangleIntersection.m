function [P,nc,s,cp]=TetrahedraTriangleIntersection(X,Y);
% TETRAHEDRATRIANGLENTERSECTION intersection of tetrahedron with triangle
%   [P,nc,s]=TetrahedraTriangleIntersection(X,Y); computes for the
%   given tetrahedron X and triangle Y (point coordinates are stored
%   column-wise) the points P where they intersect, in nc the bolean
%   information of which neighbors triangle Y are also intersecting with X,
%   and in s the surface of the intersection. If the intersection
%   was found to be coplanar with one of the faces of the
%   tetrahedron, then cp is set to the number which corresponds to
%   the neighboring tetrahedra, that shared the same coplanar face,
%   otherwise cp is 0

l1=[1 2 3 4 1 2];                      % enumeration of lines
l2=[2 3 4 1 3 4];
l1t=[1 2 3];
l2t=[2 3 1];  

cp=0;                                     % colinear cases, must exclude
[P,nc,s]=CheckForColinearity(Y,X(:,1:3)); % neighbor in cp for future
if ~isempty(P)                            % intersection to avoid getting
  cp=1;                                   % doubles
else
  [P,nc,s]=CheckForColinearity(Y,X(:,[1 3:4]));
  if ~isempty(P)
    cp=3;
  else
    [P,nc,s]=CheckForColinearity(Y,X(:,[1:2 4]));
    if ~isempty(P)
      cp=4;
    else
      [P,nc,s]=CheckForColinearity(Y,X(:,2:4));
      if ~isempty(P)
        cp=2;
      end
    end
  end
end

if cp==0
  P=[];nc=[0 0 0]; 
  for i=1:6  % find intersections of edges of thetrahedron X with triangle Y
    p=TriangleLineIntersection([X(:,l1(i)) X(:,l2(i))],Y);
    if ~isempty(p)
      [k,P]=InsertPoint(p,P);            % insert point if new
    end; 
  end;
  for i=1:3                              % find intersections of edges of
    for j=1:4                            % triangle Y with surfaces of X
      p=TriangleLineIntersection([Y(:,l1t(i)) Y(:,l2t(i))],...
        [X(:,j) X(:,mod(j,4)+1) X(:,mod(j+1,4)+1)]);
      if ~isempty(p)      [k,P]=InsertPoint(p,P); 
        nc(i)=1;
      end; 
    end;
  end;
  fn=[0 0 0];
  for i=1:3                              % find interior points of triangle Y
    if PointInTetrahedra(Y(:,i),X)       % in tetrahedron X
      [k,P]=InsertPoint(Y(:,i),P);
      fn(i)=1;
    end;
  end;
  if sum(fn)>1                           % more than one point inside
    nc=[1 1 1];                          % means all neighbors intersect
  end;
  n=cross(Y(:,2)-Y(:,1),Y(:,3)-Y(:,1));
  n=n/norm(n);
  P=SortAndRemoveDoubles(P,n);
  s=0;
  for j=2:size(P,2)-1                    % compute area of polygon 
    s=s+norm(cross(P(:,j)-P(:,1),P(:,j+1)-P(:,1)))/2;
  end;
end;
color = ['m','b','y','g','k','r'];
if size(P,2)>=3
%        patch(P(1,:),P(2,:),P(3,:),color(myrandint(1,1,[1,2,3,4,5,6])));
% %    patch(P(1,:),P(2,:),P(3,:),P(1,:),...
% %          'FaceLighting','gouraud',...
% %          'EdgeColor','black',...
% %          'EdgeLighting','gouraud',...
% %          'LineStyle','-')
  if cp ~= 0
      RedBlue = 'b';
  else
      RedBlue = 'r';
  end
  patch(P(1,:),P(2,:),P(3,:),RedBlue,...
    'FaceLighting','phong',...
    'EdgeColor','none',...
    'EdgeLighting','phong',...
    'LineStyle','-')
end  
drawnow
