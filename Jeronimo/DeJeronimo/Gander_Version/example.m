X=[0 1 0 0
   0 0 1 0
   0 0 0 1]
Y=[0.1 1.2 1
   -0.1 0   1  
   0 0   0]

clf;
PlotTetrahedron(X,'b');
view(-37.5,30)
PlotTriangle(Y,'r');
[P,nc,s]=TetrahedraTriangleIntersection(X,Y)

for i=1:1000
  X=rand(3,4);  
  %Y=rand(3,3);
  %  X(:,1)=zeros(3,1);
  % Y(:,1)=zeros(3,1);
  % X(:,2)=ones(3,1);
  % Y(:,2)=ones(3,1)/2;
  % X(:,3)=[ones(2,1);0];
  % Y(:,3)=[ones(2,1);0]/2;
  Y(:,1)=1/2*X(:,1)+1/3*X(:,2)+1/6*X(:,3);
  Y(:,2)=1/3*X(:,1)+1/2*X(:,2)+1/6*X(:,3);
  Y(:,3)=1/6*X(:,1)+1/3*X(:,2)+1/2*X(:,3);
  
  clf;
  PlotTetrahedron(X,'b');
  PlotTriangle(Y,'r');
  view(-37.5,30)
  [P,nc,s]=TetrahedraTriangleIntersection(X,Y)
  grid on
  pause
end;

% a numerically difficult case:

X=[0 1 0 0
   0 0 1 0
   0 0 0 1]
Y=[0 1 0
   0 0 1  
   0 0 0]

clf;
PlotTetrahedron(X,'b');
view(-37.5,30)
PlotTriangle(Y,'r');
[P,nc,s]=TetrahedraTriangleIntersection(X,Y)

X=[1 1 0 
   1 0 1
   1 0 0]
Y=[1 1 1/2
   1 0 1  
   1 0 1/2]

clf;
PlotTriangle(X,'blue',1);
PlotTriangle(Y,'red',1);

[P,nc,s]=CheckForColinearity(X,Y);


% take a tetrahedra mesh and a 3d triangular mesh:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

d=0.1;
[N1,T1]=NewMesh3d(1);
[N1,T1]=RefineMesh3d(N1,T1);
[N1,T1]=RefineMesh3d(N1,T1);
%for i=1:size(N1,2)
%  for j=1:3
%    if N1(j,i)>0 & N1(j,i)<1
%      N1(j,i)=N1(j,i)+d*(rand(1,1)-0.5);
%    end
%  end
%end

[N2,T2]=NewMesh(1);
[N2,T2]=RefineMesh(N2,T2);
[N2,T2]=RefineMesh(N2,T2);
[N2,T2]=RefineMesh(N2,T2);
%[N2,T2]=RefineMesh(N2,T2);
%f=@(x,y) (x+y)/2;
%f=@(x,y) sin(x).*sin(y);
% f=@(x,y) 0.25
%N2=Convert2dMeshTo3d(N2,f);
N2=Convert2dMeshTo3d(N2,'ff');

%for i=1:size(N2,2)
%  for j=1:3
%    if N2(j,i)>0 & N2(j,i)<1
%      N2(j,i)=N2(j,i)+0.1*(rand(1,1)-0.5);
%      %      N2(j,i)=N2(j,i)+0.1
%    end
%  end
%end

clf
PlotMesh3d(N1,T1,'b');
Plot2dInterface3d(N2,T2,'r');
S=ProjectionMatrix2dWith3d(N1,T1,N2,T2)






S=ProjectionMatrix2dWith3dOld(N2,T2,N1,T1)


S=ProjectionMatrix2dWith3dNum(N2,T2,N1,T1)















fig=figure(1);
clf
set(fig,'DoubleBuffer','on');
set(gca,...% 'xlim',[-80 80],'ylim',[-80 80],...
         'nextplot','replace'); %,'Visible','off')

ProjectionMatrix2dWith3d(Na,Ta,Nb,Tb);










































tb=0;tn=0;
for i=1:5
  [N1,T1]=NewMesh3d(1);
  [N1,T1]=RefineMesh3d(N1,T1);
  [N1,T1]=RefineMesh3d(N1,T1);
  [N2,T2]=NewMesh3d(1);
  N2=(N2/1.5);
  [N2,T2]=RefineMesh3d(N2,T2);
  [N2,T2]=RefineMesh3d(N2,T2);
  N2(:,1:end)=N2(:,1:end)+(0.02*rand(size(N2(:,1:end))));

%  clf;
%  PlotMesh3d(N1,T1,'b');
%  PlotMesh3d(N2,T2,'r');
t0=clock;
  ProjectionMatrix3d(N1,T1,N2,T2);
tn=tn+etime(clock,t0)
t0=clock;
  ProjectionMatrix3dBruteForce(N1,T1,N2,T2);
tb=tb+etime(clock,t0)

end;

% scaling: 

s=[5 40 320];
tn=[2.7622  60.4931 665.5757] 
tb=[2.7524 157.6744 11120.55]


set(axes,'FontSize',18);
loglog(s,tb,'--o',s,tn,'--*',s,0.2*s.^2,'-o',s,0.5*s,'-*');
axis([4 500 1 10^5]);
legend('Brute Force','New Method','O(n2)','O(n)',2);
xlabel('number of triangles');
ylabel('time for 5 projections');
print -depsc ComplexityMeasurements3d.eps
