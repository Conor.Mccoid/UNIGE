d=0.0;
[N1,T1]=NewMesh3d(1);
[N1,T1]=RefineMesh3d(N1,T1);
[N1,T1]=RefineMesh3d(N1,T1);
for i=1:size(N1,2)
  for j=1:3
    if N1(j,i)>0 & N1(j,i)<1
      N1(j,i)=N1(j,i)+d*(rand(1,1)-0.5);
    end
  end
end

[N2,T2]=NewMesh(1);
[N2,T2]=RefineMesh(N2,T2);
[N2,T2]=RefineMesh(N2,T2);
[N2,T2]=RefineMesh(N2,T2);
%[N2,T2]=RefineMesh(N2,T2);
%f=@(x,y) (x+y)/2;
%f=@(x,y) sin(x).*sin(y);
% f=@(x,y) 0.25
%N2=Convert2dMeshTo3d(N2,f);
N2=Convert2dMeshTo3d(N2,'ff');

% dd=1.d-12;       % With this settings we get an error of 1.d-7.
% dd=1.d-13;       % Sometimes provides errors on 1.d-7.
 dd=1.d-16;       % Provides holes or errors on 1.d-6.
% dd=1.d-15;       % Provides holes and errors on 1.d-3.
% dd=1.d-16;         % Perfect.

for i=1:size(N2,2)
  for j=1:3
    if N2(j,i)>0 & N2(j,i)<1
      N2(j,i)=N2(j,i)+dd*(rand(1,1)-0.5);
      %      N2(j,i)=N2(j,i)+0.1
    end
  end
end

% dd=1.d-1;        % In three simulations of ten or so... a hole was founded. Error on 1.d-3.
% dd=1.d-12;       % Apparently good.
% dd=1.d-13;       % Apparently good.
% dd=1.d-14;       % Apparently good.
% dd=1.d-15;       % Provides holes and errors on 1.d-1 and bad conditioning.
% dd=1.d-16;       % Error in conexions. Holes.

% for i=1:size(N2,2)
%       N2(3,i)=min(min((N2(1,i)+(1-N2(2,i)))/2.0*2.0 + (1-rand())*dd,...
%                       (N2(2,i)+(1-N2(1,i)))/2.0*2.0 + (1-rand())*dd),...
%                    0.75+ (1-rand())*dd);
% end

area_tot = 0;
for k=1:size(T2,1)
    area_tot = area_tot+norm(cross( N2(:,T2(k,2)) - N2(:,T2(k,1)) , ...
                                    N2(:,T2(k,3)) - N2(:,T2(k,1))),2)/2.0;
end

figure(1)
clf
axis 'equal'
view(20,20)
% camlight('headlight')
camlight('right')
% camlight('left')
PlotMesh3d(N1,T1,'b');
Plot2dInterface3d(N2,T2,'r');
S=ProjectionMatrix2dWith3d(N1,T1,N2,T2)

fprintf('Area de la vela: %e \n', area_tot);
fprintf('Area de las intersecciones: %e \n', S);
fprintf('Error en el calculo de areas: %e \n', S - area_tot);

