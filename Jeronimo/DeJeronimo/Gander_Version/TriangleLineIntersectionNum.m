function [p,x1,x2]=TriangleLineIntersectionNum(X,Y);
% TRIANGLELINEINTERSECTION intersection of a line and a triangle
%   [p,x]=TriangeLineIntersection(X,Y); computes for a given line X in 3d,
%   and a triangle Y in 3d, the point p of intersection in the
%   triangle, and otherwise returns p=[];
%   Attention: this numerical routine modifies the first node of
%   the line segment X if the line and the triangle are colinear,
%   and returns the modified node in x

p=[]; 
b=Y(:,1)-X(:,1);               
A=[X(:,2)-X(:,1) Y(:,1)-Y(:,2) Y(:,1)-Y(:,3)];
if rank(A)==3                               % edge and triangle not parallel
  r=A\b;
  if r(1)>=0 & r(1)<=1 & r(2)>=0 & r(3)>=0 & r(2)+r(3)<=1
    p=X(:,1)+r(1)*(X(:,2)-X(:,1));
  end;
else
  if abs(det([X(:,1) Y;ones(1,4)]))<10*eps   % permanently perturb node if
    X=X+[0;0;10]*[1 1]*eps;                  % triangle and line are 
    warning('Nodes were moved in the mesh!');% coplanar to force intersection
  end;                                       % later  
end;                                    
x1=X(:,1);
x2=X(:,2);
