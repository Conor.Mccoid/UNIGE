function PlotTriangle(X,c,lw)
% PLOTTRiANGLE plots a triangle in 3d
%   PlotTriangle(X) plots the triangle given by the three
%   points in the matrix X, stored column-wise, with the color c

if nargin<3
  lw=1;
end;

line(X(1,:),X(2,:),X(3,:),'Color',c,'LineWidth',lw)
line(X(1,[3 1]),X(2,[3 1]),X(3,[3 1]),'Color',c,'LineWidth',lw)


