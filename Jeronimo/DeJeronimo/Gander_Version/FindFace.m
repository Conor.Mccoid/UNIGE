function f=FindFace(X,Y)
% FINDFACE find face in tetrahedra 
%   f=FindFace(X,Y) finds face X in thetrahedra Y, where the face and
%   tetrahedra are given by node indices.

f(1)=find(Y==X(1));
f(2)=find(Y==X(2));
f(3)=find(Y==X(3));
switch sum(f)
 case 6                 % face 1 2 3
  f(4)=5;
 case 9                 % face 2 3 4
  f(4)=8;
 case 8                 % face 3 4 1
  f(4)=7; 
 case 7                % face 4 1 2
  f(4)=6;
end; 
f=f';
