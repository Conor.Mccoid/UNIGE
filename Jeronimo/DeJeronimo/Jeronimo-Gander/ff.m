function z=ff(xx,yy)
% FF special ramp function for 2d 3d mesh intersection testing

for i=1:length(xx)    
  x=xx(i);
  y=yy(i);
  r=sqrt(x^2+y^2);    
  if r<1/2
    z(i)=r;
  else
    z(i)=1/2;
  end;
end;