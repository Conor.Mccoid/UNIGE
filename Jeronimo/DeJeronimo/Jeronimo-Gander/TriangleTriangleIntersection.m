function [P,n,s]=TriangleTriangleIntersection(X,Y);
% TRIANGLETRIANGLEINTERSECTION area of the intersection of two triangles 
%   [P,n,s]=TriangleTriangleIntersection(X,Y); computes for the two
%   given triangles X and Y (point coordinates are stored column-wise,
%   in counter clock order) the points P where they intersect, in n
%   the indices of which neighbors of X are also intersecting with Y,
%   and the intersection area s 

[P,n]=EdgeIntersections2d(X,Y);
P1=PointsOfXInY(X,Y);
if size(P1,2)>1                      % if two or more interior points
  n=[1 1 1];                         % the triangle is candidate for all 
end                                  % neighbors
P=[P P1];
P=[P PointsOfXInY(Y,X)];
P=SortAndRemoveDoubles2d(P);         % sort counter clock wise
s=0;
if size(P,2)>0
  for j=2:size(P,2)-1                % compute area of intersection
    s=s+det([P(:,j)-P(:,1) P(:,j+1)-P(:,1)])/2;
  end;
end;



