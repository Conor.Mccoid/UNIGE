function PlotMesh3d(N,T,col);
% PLOTMESH3D plots a tetrahedra mesh 
%   PlotMesh3d(N,T,col); plots the mesh given by the nodes N and
%   tetrahedra T in color col. For small meshes the node numbers are
%   added as well.

% clf;
axis('equal');
view(-37.5,30);
for i=1:size(T,1),
  PlotTetrahedron(N(:,T(i,1:4)),col);
  % c=sum(N(:,T(i,1:4))')'/4;
  % text(c(1),c(2),c(3),num2str(i),'FontSize',15,'Color',col);
  % ct=(c*4-0.8*N(:,T(i,4)))/3.2;
  % text(ct(1),ct(2),ct(3),num2str(T(i,5)),'Color','m');
  % ct=(c*4-0.8*N(:,T(i,1)))/3.2;
  % text(ct(1),ct(2),ct(3),num2str(T(i,6)),'Color','m');
  % ct=(c*4-0.8*N(:,T(i,2)))/3.2;
  % text(ct(1),ct(2),ct(3),num2str(T(i,7)),'Color','m');
  % ct=(c*4-0.8*N(:,T(i,3)))/3.2;
  % text(ct(1),ct(2),ct(3),num2str(T(i,8)),'Color','m');
%  pause
end;
m=size(N,2);
de=0.05;
%if m<100,
%  for i=1:m,
%    text(N(1,i)+de,N(2,i)+de,N(3,i)+de,num2str(i),'Color','b');
%  end;
% end;




