function [P,nc,s,X,Y]=TetrahedraTriangleIntersectionNum(X,Y);
% TETRAHEDRATRIANGLENTERSECTION intersection of triangle with tetrahedron
%   [P,nc,H,v]=TetrahedraTriangleIntersection(X,Y); computes for the
%   two given tetrahedra X and Y (point coordinates are stored
%   column-wise) the points P where they intersect, in nc the bolean
%   information of which neighbors of X are also intersecting with Y,
%   in H stored row-wise the polygons of the faces of the intersection
%   polyhedron, stored counter-clock-wise looking from the outside,
%   and in v the volume of the intersection. The numerical challenges
%   are handled by including points on the boundary and removing
%   duplicates at the end.
%   Attention: this numerical routine modifies nodes in the
%   tetrahedra Y and the triangle X for numerical purposes, if
%   elements are detected to be coplanar!
 
P=[];nc=[0 0 0 0]; 
l1=[1 2 3 4 1 2];                      % enumeration of lines
l2=[2 3 4 1 3 4];
l1t=[1 2 3];
l2t=[2 3 1];  
s1=[1 1 2 3 1 2];                      % faces touching each line
s2=[4 2 3 4 3 4];
for i=1:6  % find intersections of edges of thetrahedron X with triangle Y
    [p,X(:,l1(i)),X(:,l2(i))]=TriangleLineIntersectionNum([X(:,l1(i)) X(:,l2(i))],Y);
    if ~isempty(p)
      [k,P]=InsertPoint(p,P);          % insert point if new
      nc(s1(i))=1;nc(s2(i))=1;        
      % disp('edge intersects triangle')
      % text(p(1),p(2),p(3),num2str(k))
      % pause
    end; 
end;
for i=1:3                              % find intersections of edges of
  for j=1:4                            % triangle Y with surfaces of X
    [p,Y(:,l1t(i)),Y(:,l2t(i))]=TriangleLineIntersectionNum([Y(:,l1t(i)) Y(:,l2t(i))],...
      [X(:,j) X(:,mod(j,4)+1) X(:,mod(j+1,4)+1)]);
    if ~isempty(p)      [k,P]=InsertPoint(p,P); 
      nc(j)=1;
      % disp('edge intersects tetrahedron')
      % text(p(1),p(2),p(3),num2str(k))
      % pause
    end; 
  end;
end;
for i=1:3                              % find interior points of triangle Y
  if PointInTetrahedra(Y(:,i),X)       % in tetrahedron X
    [k,P]=InsertPoint(Y(:,i),P);
    p=Y(:,i);
    % disp('triangle node in tetrahedra')
    % text(p(1),p(2),p(3),num2str(k))
    % pause
  end;
end;
n=cross(Y(:,2)-Y(:,1),Y(:,3)-Y(:,1));
n=n/norm(n);
P=SortAndRemoveDoubles(P,n);
s=0;
for j=2:size(P,2)-1                    % compute area of polygon 
  s=s+norm(cross(P(:,j)-P(:,1),P(:,j+1)-P(:,1)))/2;
end;
if size(P,2)>=3
  patch(P(1,:),P(2,:),P(3,:),'m')
end  
pause