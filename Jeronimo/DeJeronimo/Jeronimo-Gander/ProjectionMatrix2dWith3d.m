function S=ProjectionMatrix2dWith3d(Na,Ta,Nb,Tb);
% PROJECTIONMATRIX2DWITH3D projection for triangular grid in 3d tetrahedras
%   M=ProjectionMatrix2dWith3d(Na,Ta,Nb,Tb); takes a tetrahedral mesh
%   Ta and a triangular mesh Tb in 3d with associated nodal
%   coordinates in Na and Nb and computes the projection matrix M. As
%   a precondition, the first tetrahedra in Ta and the first triangle
%   in Tb need to intersect.
S=0;                           % total area of the intersection
bl=[1];                        % bl: list of triangles of Tb to treat
% bil=[1];                       % bil: list of tetrahedra Ta to start with
% Modification allowing to start with an arbitrary triangle.
for ii = 1:size(Ta,1)
    bc = bl(1);
    ac = ii;
    [P,nc,s,cp]=TetrahedraTriangleIntersection(Na(:,Ta(ac,1:4)),Nb(:,Tb(bc,1:3)));
    if s > 1.d-6;
        bil = [ac];
        break;
    end
end
bd=zeros(size(Tb,1)+1,1);      % bd: flag for triangles in Tb treated 
bd(end)=1;                     % guard, to treat boundaries
bd(bc)=1;                       % mark first triangle in b list.
% S=0;                           % total area of the intersection
% bl=[1];                        % bl: list of triangles of Tb to treat
% bil=[1];                       % bil: list of tetrahedra Ta to start with
% bd=zeros(size(Tb,1)+1,1);      % bd: flag for triangles in Tb treated 
% bd(end)=1;                     % guard, to treat boundaries
% bd(1)=1;                       % mark first triangle in b list.
while length(bl)>0
% clf
% PlotMesh3d(Na,Ta,'b');
% Plot2dInterface3d(Nb,Tb,'r');
% bl
  bc=bl(1); bl=bl(2:end);      % bc: current triangle of Tb 
  area_triangulo2 = norm(cross( Nb(:,Tb(bc,2)) - Nb(:,Tb(bc,1)) , ...
                               Nb(:,Tb(bc,3)) - Nb(:,Tb(bc,1))),2)/2.0;
  area_triangulo = 0.0;
% PlotTriangle(Nb(:,Tb(bc,1:3)),'k',3);
% bc
% pause
  al=bil(1); bil=bil(2:end);   % tetrahedra of Ta to start with
  ad=zeros(size(Ta,1)+1,1);    % same as for bd
  ad(end)=1;
  ad(al)=1; 
  n=[0 0 0];                   % tetrahedra intersects neighboing triangles
  while length(al)>0
    ac=al(1); al=al(2:end);    % take next candidate
% PlotTetrahedron(Na(:,Ta(ac,1:4)),'g',2);
% ac
% pause
% PlotTetrahedron(Na(:,Ta(ac,1:4)),'white',2);
% PlotTetrahedron(Na(:,Ta(ac,1:4)),'blue',1);
    [P,nc,s,cp]=TetrahedraTriangleIntersection(Na(:,Ta(ac,1:4)),Nb(:,Tb(bc,1:3)));
    if ~isempty(P)             % intersection found
      if cp                    % coplanar intersection found
        ad(Ta(ac,4+cp))=1;     % do not treat that neighbor any more
      end;
      t=Ta(ac,4+find(ad(Ta(ac,5:8))==0));
      al=[al t];               % add neighbors
      ad(t)=1;
      n(find(nc>0))=ac;        % ac is starting candidate for neighbor  
%       n
      S=S+s;
      area_triangulo = area_triangulo + s;
    end
  end
  if(abs(area_triangulo-area_triangulo2)>1.e-8)
     fprintf('Suma Areas %e AreaEx %e  Dif %e Tri %i Alguno colineal %i \n', area_triangulo, area_triangulo2,area_triangulo-area_triangulo2,bc, cp);
  end
  tmp=find(bd(Tb(bc,4:6))==0); % find non-treated neighbors
  idx=find(n(tmp)>0);          % only if intersecting with Ta
  t=Tb(bc,3+tmp(idx));
  bl=[bl t];                  % and add them
  bil=[bil n(tmp(idx))];      % with starting candidates Ta
% tt=n(tmp(idx));
% for i=1:length(t)
%   PlotTetrahedron(Na(:,Ta(tt(i),1:4)),'c',5);
%   PlotTriangle(Nb(:,Tb(t(i),1:3)),'m',5);
%   pause
% end    
  bd(t)=1;
end
