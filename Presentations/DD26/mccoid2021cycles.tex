\documentclass[graybox]{svmult}

%--For SVMult class
%\usepackage{newtxtext}
%\usepackage{newtxmath}
\usepackage{mathptmx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{makeidx}
\usepackage{graphicx}
\usepackage{multicol}
\usepackage[bottom]{footmisc}

\smartqed

%--Preamble (some packages in preamble.sty conflict with the svmult class)
% cannot use the subcaption package as it interferes with the svmult class caption settings

%\newcommand{\dxdy}[2]{\frac{d #1}{d #2}}
%\newcommand{\dxdyk}[3]{\frac{d^{#3} #1}{d {#2}^{#3}}}
\newcommand{\pdxdy}[2]{\frac{\partial #1}{\partial #2}}
%\newcommand{\liminfty}[1]{\lim_{#1 \to \infty}}
%\newcommand{\limab}[2]{\lim_{#1 \to #2}}
%
\newcommand{\abs}[1]{\left \vert #1 \right \vert}
%\newcommand{\norm}[1]{\left \Vert #1 \right \Vert}
%\newcommand{\order}[1]{\mathcal{O} \left ( #1 \right )}
%\newcommand{\set}[1]{\left \{ #1 \right \}}
%\newcommand{\Set}[2]{\left \{ #1 \ \middle \vert \ #2 \right \}}
%\newcommand{\vmat}[1]{\begin{vmatrix} #1 \end{vmatrix}}
\DeclareMathOperator{\sign}{sign}
%\newcommand{\sign}{\text{sign}}
%
%\newcommand{\bbn}{\mathbb{N}}
%\newcommand{\bbz}{\mathbb{Z}}
%\newcommand{\bbq}{\mathbb{Q}}
\newcommand{\bbr}{\mathbb{R}}
%\newcommand{\bbc}{\mathbb{C}}
%\newcommand{\bbf}{\mathbb{F}}
%--End Preamble

\definecolor{darkgreen}{rgb}{0,0.6,0.1}

\usepackage{tikz}
\usetikzlibrary{decorations.pathreplacing,positioning,calc}

\title{Cycles in Newton-Raphson preconditioned by Schwarz (ASPIN and its cousins)}
\author{Conor McCoid \and Martin J. Gander}
\institute{Conor McCoid
	\at University of Geneva, \email{conor.mccoid@unige.ch}
	\and
	Martin J. Gander
	\at University of Geneva, \email{martin.gander@unige.ch}
	}

\begin{document}

\maketitle
\abstract*{Newton-Raphson preconditioned by Schwarz methods does not have sufficient convergence criteria.
We explore an alternating Schwarz method accelerated by Newton-Raphson to find an example where the underlying Schwarz method converges but the Newton-Raphson acceleration fails.
Alternating Schwarz is posed as a fixed point iteration to make use of theory for generic root-finding methods.
An algorithm is proposed combining several aspects of this theory and others to guarantee convergence.}

\section{Introduction}
\label{sec:intro}

ASPIN \cite{Cai2002}, RASPEN \cite{Dolean2016}, and MSPIN \cite{liu2015field} rely on various Schwarz methods to precondition either Newton-Raphson or inexact Newton.
While \textit{a priori} convergence criteria have been found for the underlying Schwarz methods, so far none exist for their combination with Newton-Raphson.

Like in the linear case when combining a Krylov method and a Schwarz method,
there is an equivalence between preconditioning Newton-Raphson with a Schwarz method and accelerating that same Schwarz method with Newton-Raphson \cite{Gander2017}:
A domain is first subdivided into subdomains, the problem solved on each subdomain, and the resulting formulation iterated through Krylov and Newton-Raphson, respectively.

We examine cycling behaviour in alternating Schwarz in one dimension that has been accelerated by applying Newton-Raphson.
We begin by presenting the algorithm for alternating Schwarz and how it is accelerated by Newton-Raphson.
Suppose we seek to solve the boundary value problem
\begin{equation*}
F(x,u,u',u'')=0, \quad x \in [a,b], \quad u(a)=A, \quad u(b)=B
\end{equation*}
for some function $F(x,u,v,w)$.
Then an iteration of alternating Schwarz with subdomains $(a, \beta)$ and $(\alpha, b)$, $\alpha < \beta$, is comprised of the following three steps:
\begin{align*}
(1) && & F(x,u_1,u_1',u_1'') = 0, && u_1(a) = A, && u_1(\beta) = \gamma_n, \\
(2) && & F(x,u_2,u_2',u_2'') = 0, && u_2(\alpha) = u_1(\alpha), && u_2(b) = B, \\
(3) && & \gamma_{n+1} = u_2(\beta) = G(\gamma_n).
\end{align*}

The function $G(\gamma)$ thus represents one iteration of alternating Schwarz in substructured form.
The process is repeated until convergence, ie.
\begin{equation*}
\left ( G \circ G \circ \dots \circ G \right ) (\gamma) = G^n(\gamma) \approx G^{n+1}(\gamma) = \left ( G \circ G^n \right ) (\gamma).
\end{equation*}
This is naturally a fixed point iteration applied to the function $G(\gamma)$.

To accelerate the method one applies Newton-Raphson to the function $f(\gamma) = G(\gamma) - \gamma$, which has a root at the fixed point.
If the fixed point is unique, this is the only root of $f(\gamma)$.
To apply Newton-Raphson, one needs to know the value of $G'(\gamma)$, which may be found by adding two new steps, (1') and (2'), to alternating Schwarz:
\begin{align*}
(1) && F(x,u_1,u_1',u_1'') = 0, && & u_1(a) = A, && u_1(\beta) = \gamma_n, \\
(1') && J(u_1) \cdot (v_1,v_1',v_1'') = 0, && & v_1(a) = 0, && v_1(\beta) = 1, \\
(2) && F(x,u_2,u_2',u_2'') = 0, && & u_2(\alpha) = u_1(\alpha), && u_2(b) = B, \\
(2') && J(u_2) \cdot (v_2,v_2',v_2'') = 0, && & v_2(\alpha) = 1, && v_2(b) = 0, \\
(3) && \gamma_{n+1} = \gamma_n - \frac{ u_2(\beta) - \gamma_n }{v_1(\alpha) v_2(\beta) - 1}= && & \gamma_n - \frac{G(\gamma_n) - \gamma_n}{G'(\gamma_n) - 1},
\end{align*}
where $v_i(x)=\partial u_i(x) / \partial \gamma$ and $J(u_i)$ is the Jacobian of $F(x,u_i,u_i',u_i'')$.

\section{Convergence of generic fixed point iterations and Newton-Raphson}

A generic fixed point iteration $x_{n+1}=g(x_n)$ converges when $\abs{g(x_n)-x^*} < \abs{x_n-x^*}$, where $x^*$ is the fixed point.
This occurs when $g(x)$ lies between $x$ and $2x^*-x$.
The convergence or divergence of the fixed point iteration is monotonic if $\sign(g(x)-x^*) = \sign(x - x^*)$ and oscillatory otherwise.
This creates four lines, $y=x$, $y=2x^*-x$, $y=x^*$ and $x=x^*$, that divide the plane into octants.
The four pairs of opposite octants form four regions with distinct
behaviour of the fixed point iteration, see left of Figure \ref{fig:FP} or Figure 5.7 from \cite{gander2014scientific}:
\begin{description}
\item[\textcolor{red}{1}, $g(x) < x < x^*$ or $g(x) > x > x^*$:] monotonic divergence;
\item[\textcolor{blue}{2}, $x < g(x) < x^*$ or $x > g(x) > x^*$:] monotonic convergence;
\item[\textcolor{darkgreen}{3}, $x < x^* < g(x) < 2 x^* - x$ or $x > x^* > g(x) > 2 x^* - x$:] convergent oscillations;
\item[\textcolor{orange}{4}, $x < x^* < 2 x^* - x < g(x)$ or $x > x^* > 2 x^* - x > g(x)$:] divergent oscillations.
\end{description}
\begin{figure}
%\includegraphics{TIKZ_cycles_FPRegions_20201223.pdf}
	\centering
        \begin{tikzpicture}
        	\matrix[column sep=1em, row sep=1em, every cell/.style={scale=0.5}]{
            \draw[black,thick,->] (-4,0) -- (4,0) node[above] {$x$};
            \draw[black,thick,->] (0,-4) -- (0,4) node[right] {$g(x)$};
            \draw[black,dashed] (-4,-4) -- (4,4);
            \draw[black,dashed] (-4,4) -- (4,-4);
            
            \filldraw[red,opacity=0.1] (-4,-4) -- (0,-4) -- (0,0) -- cycle; \filldraw[red,opacity=0.1] (4,4) -- (0,4) -- (0,0) -- cycle;
            \filldraw[blue,opacity=0.1] (-4,-4) -- (-4,0) -- (0,0) -- cycle; \filldraw[blue,opacity=0.1] (4,4) -- (4,0) -- (0,0) -- cycle;
            \filldraw[darkgreen,opacity=0.1] (-4,4) -- (-4,0) -- (0,0) -- cycle; \filldraw[darkgreen,opacity=0.1] (4,-4) -- (4,0) -- (0,0) -- cycle;
            \filldraw[orange,opacity=0.1] (-4,4) -- (0,4) -- (0,0) -- cycle; \filldraw[orange,opacity=0.1] (4,-4) -- (0,-4) -- (0,0) -- cycle;
            
            \draw[red,thick,opacity=0.8] plot[smooth] coordinates {(0.25,1) (0,0) (-0.5,-1.5) (-1,-2) (-2,-3) (-2.25,-4)};
            \draw[blue,thick,opacity=0.8] plot[smooth] coordinates {(-3,-2.5) (-2,-1.5) (-1.5,-1) (0,0) (0.5,0.25)};
            \draw[darkgreen,thick,opacity=0.8] plot[smooth] coordinates {(-3,1) (-2,1) (0,0) (1,-0.5) (2.5,-1)};
            \draw[orange,thick,opacity=0.8] plot[smooth] coordinates {(-1.5,3) (-1,2) (0,0) (2,-2.5) (3,-3.5)};
            
            \draw[red,->,thick,dashed] (-1,-2) -- (-2,-2) -- (-2,-3)--(-3,-3)--(-3,-4); % 1
            \filldraw[black] (-1,-2) circle (3pt); \filldraw[black] (-2,-3) circle (3pt);
            \draw[blue,->,thick,dashed] (-2,-1.5) -- (-1.5,-1.5) -- (-1.5,-1)--(-1,-1)--(-1,-0.5); % 2
            \filldraw[black] (-2,-1.5) circle (3pt); \filldraw[black] (-1.5,-1) circle (3pt);
            \draw[darkgreen,->,thick,dashed] (-2,1) -- (1,1) -- (1,-0.5)--(-0.5,-0.5)--(-0.5,0.25); % 3
            \filldraw[black] (-2,1) circle (3pt); \filldraw[black] (1,-0.5) circle (3pt);
            \draw[orange,->,thick,dashed] (-1,2) -- (2,2) -- (2,-2.5)--(-2.5,-2.5)--(-2.5,3); % 4
            \filldraw[black] (-1,2) circle (3pt); \filldraw[black] (2,-2.5) circle (3pt);
            
            \draw (-1,-3) node {1};
            \draw (-3,-1.5) node {2};
            \draw (-3,1.5) node {3};
            \draw (-1,3) node {4};
            \draw(1,3) node {1};
            \draw (3,1.5) node {2};
            \draw (3,-1.5) node {3};
            \draw (1,-3) node {4};
            &
            
            \filldraw[red,opacity=0.1] (-2,4) -- (4,4) -- (4,2) -- (-2,2) -- cycle; \filldraw[red,opacity=0.1] (-2,2) -- (-2,0) -- (-4,0) -- (-4,2) -- cycle;
            \filldraw[blue,opacity=0.1] (-2,4) -- (-4,4) -- (-2,2) -- cycle; \filldraw[blue,opacity=0.1] (-2,2) -- (-2,0) -- (0,0) -- cycle;
            \filldraw[darkgreen,opacity=0.1] (-4,4) -- (-4,3) -- (-2,2) -- cycle; \filldraw[darkgreen,opacity=0.1] (-2,2) -- (2,0) -- (0,0) -- cycle;
            \filldraw[orange,opacity=0.1] (-4,3) -- (-4,2) -- (-2,2) -- cycle; \filldraw[orange,opacity=0.1] (-2,2) -- (2,0) -- (4,0) -- (4,2) -- cycle;
            
                    \draw[black,thick,->] (-4,0) -- (4,0) node[above] {$x$};
                    \draw[black,thick,->] (0,-2) -- (0,4) node[right] {$f(x)$};
                    \filldraw[black] (-2,2) circle (3pt);
                    \draw[blue,thick,opacity=0.8] (0,0) parabola (-2,2) -- (-3,4);
                    \draw[black,dashed] (-4,2) -- (4,2);
                    \draw[black,dashed] (-2,4)--(-2,-1);
                    \draw[black,dashed] (-4,4)--(1,-1);
                    \draw[black,dashed] (-4,3)--(4,-1);
                    \draw[red,->,thick,dashed] (-1.5,2.5) -- (-4,0);
                    \draw[blue,->,thick,dashed] (-2,2) -- (-1,0);
                    \draw[darkgreen,->,thick,dashed] (-2.75,2.5) -- (1,0);
                    \draw[orange,->,thick,dashed] (-2.75,2.25) -- (4,0);
%                    \draw[red,->] (-4,2) -- (4,2); % 4-1
%                    \draw[red,->] (-2,2) -- (-2,0); % 1-2
%                    \draw[red,->] (-2,2) -- (0,0); % 2-3
%                    \draw[red,->] (-2,2) -- (2,0); % 3-4
                    
%                    \filldraw[red,opacity=0.25] (-4,2)--(-2,2)--(-2,0)--(-4,0)--cycle;
%                    \filldraw[blue,opacity=0.25] (-2,2)--(-2,0)--(0,0)--cycle;
%                    \filldraw[purple,opacity=0.25] (-2,2)--(0,0)--(2,0)--cycle;
%                    \filldraw[orange,opacity=0.25] (-2,2)--(2,0)--(4,0)--(4,2)--cycle;
                    
                    \draw (-1.25,-0.5) node {2};
                    \draw (-3,-0.5) node {1};
                    \draw (1.5,-0.5) node {3};
                    \draw (3.5,-0.5) node {4};
            \\};
        \end{tikzpicture}
        \caption{\textbf{Left:} Behaviour of the fixed point iteration $x_{n+1}=g(x_n)$, where the origin is the fixed point, $g(0)=0$.
        \textbf{Right:} Regions of Newton-Raphson, $x_{n+1}=x_n - f(x_n)/f'(x_n)$, where the origin is the root, $f(0)=0$.
        The tangent line to $f(x)$ can be traced from $(x,f(x))$ towards the line $y=0$.
        Where it lands on this line indicates which fixed point iteration behaviour occurs.}
        \label{fig:FP}
\end{figure}

If the function $g(x)$ intersects the line $y=x$ at a point other than $x^*$ then there are additional fixed points that the method can converge towards.
If it intersects the line $y=2x^*-x$ then a stable cycle can form.
A fixed point iteration is therefore only guaranteed to converge if $g(x)$ lies entirely between the lines $y=x$ and $y=2x^*-x$, ie. within regions 2 and 3.

Newton-Raphson can make use of this analysis by considering it as a fixed point iteration:
\begin{equation*}
x_{n+1} = x_n - \frac{f(x_n)}{f'(x_n)} = g_f(x_n).
\end{equation*}
The borders between the regions no longer depend solely on the value of $f(x)$ but also $f'(x)$.
The right of Figure \ref{fig:FP} shows which type of behaviour Newton-Raphson will have based on where the tangent line points.
%\begin{figure}
%\sidecaption[t]
%\includegraphics{TIKZ_cycles_NRRegions_20201223.pdf}
%                \caption{Regions of Newton-Raphson.
%                The tangent line to $f(x)$ can be traced from $(x,f(x))$ towards the line $y=0$.
%                Where it lands on this line indicates which fixed point iteration behaviour occurs.
%                The origin is the root.}
%                \label{fig:NR1}
%\end{figure}

As stated, if $g_f(x)$ intersects the line $y=x$ there are additional fixed points, and if it intersects $y=2x^*-x$ there may be stable cycles.
For guaranteed convergence $g_f(x)$ must lie between these lines.
Intersections of $g_f(x)$ with $y=x$ occur only if $f(x)=0$ and $f(x)$ has additional roots or $f'(x)=\infty$.
Both circumstances are assumed not to occur.
Intersections of $g_f(x)$ with $y=2x^*-x$ may be represented as a first order ODE:
\begin{equation*}
f_C'(x) = -\frac{f_C(x)}{2 (x^*- x)}, \quad f_C(x^*) = 0.
\end{equation*}
The solution to this ODE is $f_C(x) = C \sqrt{\abs{x - x^*}}$ where $C \in \bbr$.
If a function $f(x)$ with root $x^*$ is tangential to $f_C(x)$ for any value of $C$ then $g_f(x)$ intersects the line $y=2x^*-x$.
The left of Figure \ref{fig:borders} shows the functions $f_C(x)$.

%The slopes of most of these borders are easy to see.
%If $f'(x)=0$ then $f(x)$ lies on the border between regions 1 and 4.
%If $f'(x)=\infty$ then it is between regions 1 and 2.
%If $f(x)$ is linear then it is between regions 2 and 3 and converges instantaneously.
%
%The border between regions 3 and 4 changes depending on the value of $x$.
%Recall that this border is represented by $y=2x^*-x$.
%The intersection between this line and the fixed point iteration $g_f(x) = x - f(x)/f'(x)$ 

%The borders of the regions are summarized in Figure \ref{fig:borders}.
\newcommand{\faxis}{\clip (-4,-4) rectangle (4,4);
		\draw[black,thick,->] (-4,0) -- (4,0);
		\draw[black,thick,->] (0,-4) -- (0,4);}
\newcommand{\gaxis}{\clip (-4,-4) rectangle (4,4);
			\draw[black,thick,->] (-4,-4) -- (4,4);
			\draw[black,thick,->] (0,-4) -- (0,4);}
\begin{figure}
\centering
	\begin{tikzpicture}
	\matrix[column sep=0.5cm, row sep=0.5cm, every cell/.style={scale=0.4}]
	{
%	\node at (0,-5) {\textbf{(a)}};
%	\faxis
%	\foreach \a in {0,1,2,3} {
%		\foreach \s in {-1,1} {
%			\draw[black,dashed] (\s*\a,-4) -- (\s*\a,4);
%			}
%		} &
%	\node at (0,-5) {\textbf{(b)}};
%	\faxis
%	\foreach \a in {0,0.5,1,2,4} {
%		\foreach \s in {-1,1} {
%			\draw[black,dashed,domain=-4:4,smooth,variable=\x] plot ({\x},{\s*\a*\x});
%			}
%		} &
%	\node at (0,-5) {\textbf{(c)}};
	\faxis
	\foreach \a in {1,-1,2,-2,0.5,-0.4,1.5,-1.5} {
                    \draw[domain=-4:4, smooth, samples=200, variable=\x, black, dashed] plot ({\x},{\a * sqrt(abs(\x))});
                    } &
	\gaxis
	\foreach \a in {1,-1,2,-2,0.5,-0.4,1.5,-1.5} {
                    \draw[domain=-4:4, smooth, samples=200, variable=\x, black, dashed] plot ({\x},{\a * sqrt(abs(\x)) + \x});
%                    }
%	\node at (0,-5) {\textbf{(d)}};
%	\faxis
%           \foreach \a in {0,1,2,3} {
%		\foreach \s in {-1,1} {
%			\draw[black,dashed,domain=-4:4,smooth,variable=\x] plot({\x},{\s*\a});
%			}
		} \\
	};
	\end{tikzpicture}
%\caption{Borders between respective regions, traced from tangent lines.
%\textbf{(a)} Regions 1 and 2; \textbf{(b)} 2 and 3; \textbf{(c)} 3 and 4; \textbf{(d)} 4 and 1.}
\caption{\textbf{Left:} Solutions $f_C(x)$ such that $g_f(x)$ intersects $y=2x^*-x$ for all $x$.
\textbf{Right:} Functions $g_C(x)=f_C(x)+x$ such that $g_f(x)$ for $f(x)=g_C(x)-x$ intersects $y=2x^*-x$ for all $x$.}
\label{fig:borders}
\end{figure}
A function $f(x)$ that is monotonic with respect to this geometry has guaranteed convergence under Newton-Raphson.
That is, if $f(x)$ is nowhere tangential to $f_C(x)$ in a given domain containing $x^*$ for any value of $C$ then $g_f(x)$ converges to the root for any initial guess in that domain.
Since $f_C'(x^*)=\infty$ and $f(x^*)=0$ there is always a region around the root $x^*$ where $f(x)$ crosses all of these lines monotonically.
This conforms with the theory on Newton-Raphson.

The corresponding geometry for a fixed point function accelerated by Newton-Raphson is skewed such that the line $y=0$ is aligned to $y=x$, as seen in the right of Figure \ref{fig:borders}.
The lines of this figure are the functions $g_C(x) = f_C(x) + x$.
A function $g(x)$ must be monotonic in this geometry or Newton-Raphson applied to $g(x)-x$ may exhibit cycling behaviour.
%\begin{figure}
%\centering
%	\begin{tikzpicture}
%	\matrix[column sep=0.5cm, row sep=0.5cm, every cell/.style={scale=0.24}]
%	{
%	\node at (0,-5) {\textbf{(a)}};
%	\gaxis
%	\foreach \a in {0,1,2,3} {
%		\foreach \s in {-1,1} {
%			\draw[black,dashed] (\s*\a,-4) -- (\s*\a,4);
%			}
%		} &
%	\node at (0,-5) {\textbf{(b)}};
%	\gaxis
%	\foreach \a in {0,0.5,1,2,4} {
%		\foreach \s in {-1,1} {
%			\draw[black,dashed,domain=-4:4,smooth,variable=\x] plot ({\x},{\s*\a*\x + \x});
%			}
%		} &
%	\node at (0,-5) {\textbf{(c)}};
%	\gaxis
%	\foreach \a in {1,-1,2,-2,0.5,-0.4,1.5,-1.5} {
%                    \draw[domain=-4:4, smooth, samples=200, variable=\x, black, dashed] plot ({\x},{\a * sqrt(abs(\x)) + \x});
%                    } &
%	\node at (0,-5) {\textbf{(d)}};
%	\gaxis
%           \foreach \a in {0,1,2,3} {
%		\foreach \s in {-1,1} {
%			\draw[black,dashed,domain=-4:4,smooth,variable=\x] plot({\x},{\x+\s*\a});
%			}
%		} \\
%	};
%	\end{tikzpicture}
%\caption{Borders between respective regions, traced from tangent lines.
%\textbf{(a)} Regions 1 and 2; \textbf{(b)} 2 and 3; \textbf{(c)} 3 and 4; \textbf{(d)} 4 and 1.}
%\label{fig:borders2}
%\end{figure}

If it is known in which fixed point region of the left of Figure \ref{fig:FP} $g(x)$ lies then one can find necessary and, in some cases, sufficient conditions for Newton-Raphson to have convergent behaviour based on the slopes of the lines $g_C(x)$.
For example, in region 2 the maximum of $g_C'(x)$ is 1.
%Since $g(x)$ is between $x$ and $x^*$ and $g(x^*)=x^*$ by the mean value theorem (nb: cite?) $g'(x_0) < 1$ for some $x_0$ between $x$ and $x^*$.
If $g(x)$ lies in region 2 then its slope must therefore be less than 1 everywhere or there will be a point where $g(x)$ runs tangent to $g_C(x)$ for some $C$.
Moreover, the minimum of $g_C'(x)$ is 1/2.
If $g(x)$ has a slope less than 1/2 then it cannot run tangent to $g_C(x)$ for any $C$.
The list of these conditions is summarized in Table \ref{tab:conditions}.
\begin{table}[t]
    \caption{Conditions for convergent behaviour of Newton-Raphson applied to $g(x)-x$.}
\label{tab:conditions}
\centering
    \begin{tabular}{p{2cm}p{4cm}p{4cm}}
\hline\noalign{\smallskip}
        $g(x)$ lies in & Necessary condition & Sufficient condition \\ 
\noalign{\smallskip}\svhline\noalign{\smallskip}
        1 & $g'(x) >1$ \\
        2 & $g'(x) < 1$ & $g'(x) < 1/2$ \\
        3 & $g'(x) < 1/2$ & $g'(x) < 0$ \\
        4 & $g'(x) < 0$
    \end{tabular}
\end{table}

\section{The fixed point iteration of alternating Schwarz}

We now seek to apply this theory to alternating Schwarz.
As stated earlier, we consider alternating Schwarz as a function $G(\gamma)$, taking as input the value of $u_1(\beta)$ and as output the value of $u_2(\beta)$.
Under reasonable conditions we can prove a number of useful properties of $G(\gamma)$ without prior knowledge of the fixed point $\gamma^*$.

\begin{theorem} \label{thm:mono}
If the problem
$F(x,u,u',u'') = 0$ for $x \in \Omega$, $u(x) = h(x)$ for $x \in \partial \Omega$
has a unique solution on $\Omega = [a,\alpha]$ and $\Omega = [\beta,b]$ and the continuations of these solutions are also unique, then the function $G(\gamma)$ is strictly monotonic.
\end{theorem}

\begin{proof}
It suffices to show that $G(\gamma_1) = G(\gamma_2)$ implies $\gamma_1 = \gamma_2$.
Let $u^j_1$ solve the problem on $[a,\beta]$ with $u^j_1(\beta) = \gamma_j$.
Likewise, $u^j_2$ solves the problem on $[\alpha, b]$ with $u^j_2(\alpha) = u^j_1(\alpha)$.
Suppose $u^1_2(\beta) = u^2_2(\beta)$.
Then both $u^1_2$ and $u^2_2$ solve the same problem on $[\beta,b]$.
By assumption, this must mean $u^1_2 = u^2_2$ and $u^1_1(\alpha) = u^2_1(\alpha)$.
By a similar argument, this implies $u^1_1$ and $u^2_1$ solve the same problem on $[a,\alpha]$.
Again by assumption $u^1_1 = u^2_1$ and $\gamma_1 = \gamma_2$.
\end{proof}

We can even prove that $G(\gamma)$ is restricted to region 2 with additional properties.
As an example, we reprove a result from Lui \cite{lui1999schwarz}.

\begin{theorem}[Theorem 2 from \cite{lui1999schwarz}] \label{thm:lui}
Consider the equation
$u''(x) + f(x,u,u') = 0$ for $x \in (a,b)$, $u(a) = u(b) = 0$
under the assumptions that
\begin{itemize}
\item $f \in C^1 \left ( [a,b] \times \mathbb{R} \times \mathbb{R} \right )$ ,
\item $\pdxdy{f(x,v,v')}{u} \leq 0$ for all $x \in [a,b]$ and $v \in H_0^1([a,b])$ ,
\item $\abs{f(x,v,v')} \leq C(1 + \abs{v'}^\eta)$ for all $x \in [a,b]$ and $v \in H_0^1([a,b])$ and some $C > 0$, $0 < \eta < 1$ .
\end{itemize}
The problem is solved using alternating Schwarz with two subdomains and Dirichlet transmission conditions.
Then $G(\gamma)$ for this problem lies within region 2.
\end{theorem}

\begin{proof}
It suffices to prove that the problem is well posed and $0 < G'(\gamma) < 1$ for all $\gamma \in \mathbb{R}$.
The well-posedness of the problem is guaranteed by Proposition 2 from \cite{lui1999schwarz}.
As Lui points out, this also means the problem is well posed on any subdomain.
Using Theorem \ref{thm:mono} this gives monotonicity of $G(\gamma)$.
Moreover, if $u(x)=0$ for any $x \in (a,b)$ then the problem would be well posed on the domains $[a,x]$ and $[x,b]$.
As such, $u(x)$ has the same sign as $\gamma$ and $G'(\gamma) > 0$.

Consider the problem in $g_1$:
\begin{equation*}
g_1''(x) + \pdxdy{f}{u} g_1 + \pdxdy{f}{u'} g_1' = 0, \quad x \in [a,\beta], \quad g_1(a) = 0, \quad g_1(\beta) = 1.
\end{equation*}
From the second assumption on $f$ the operator on $g_1$ satisfies a maximum principle (see, for example, \cite{lui1999schwarz}).
Therefore, $g_1(x) < 1$ for all $x \in (a,\beta)$.
By the same reasoning, $g_2(x) < g_1(\alpha) < 1$ for all $x \in (\alpha, b)$ and $G'(\gamma) < 1$.
Incidentally, the same maximum principle applies for the operator on $-g_1$ and $-g_2$, and so $G'(\gamma) > 0$ as we had before.
\end{proof}

This provides guaranteed convergence of alternating Schwarz.
However, it does not guarantee the convergence when one accelerates it through Newton-Raphson.
Using Table \ref{tab:conditions} we know that such convergence is assured if $G'(\gamma) < 1/2$ for all $\gamma$, but this is not true in all cases and cannot be determined \textit{a priori}.

Take as an example the following second order nonlinear differential equation
\begin{equation} \label{eq:sin}
u''(x) - \sin(au(x)) = 0, \quad x \in (-1,1),
\end{equation}
with homogeneous Dirichlet boundary conditions.
The problem is well posed and admits only the trivial solution $u(x)=0$.
It is easy to see that this equation satisfies the conditions of Theorem \ref{thm:lui}.
Therefore, the alternating Schwarz fixed point iteration, $G(\gamma)$, lies within region 2 and is guaranteed to converge to the fixed point.
Sadly, its Newton-Raphson acceleration will not do so for all initial conditions.
Take $a=3.6$ with an overlap of 0.4 and symmetric regions.
\begin{figure}[t]
\centering
\includegraphics[height=0.3\textwidth]{MTLB_1Dsin_NewtonGamma_v1_20210208.eps}
\hspace{1em}
\includegraphics[height=0.3\textwidth]{exp9_02.eps}
\caption{
\textbf{Left:} Results of Newton-Raphson accelerated alternating Schwarz as a function of initial condition in solving equation (\ref{eq:sin}).
The value of $a$ is 3.6 and the subdomains are $\Omega_1=(-1,0.2)$ and $\Omega_2=(-0.2,1)$.
\textbf{Middle:} $G(\gamma)$ and its Newton-Raphson acceleration.
\textbf{Right:} $G(\gamma)$ plotted with the geometry of the right of Figure \ref{fig:borders}.
}
\label{fig:ex1}
\end{figure}
The results of the Newton-Raphson acceleration are found in Figure \ref{fig:ex1} (left).
While for most initial values of $\gamma$ the method converges to the correct solution $u=0$ there are two small intervals where the method enters a stable cycle.

The function $G(\gamma)$ can be plotted numerically, along with its Newton-Raphson acceleration, see Figure \ref{fig:ex1} (middle), which shows that $G(\gamma)$ does indeed lie within region 2 as predicted by Theorem \ref{thm:lui}.
However, $G(\gamma)$ runs tangential to one of the lines $g_C(\gamma)$, see Figure \ref{fig:ex1} (right), and so its Newton-Raphson acceleration crosses into region 4.
Due to symmetry, there is a 2-cycle at each crossing.
Depending on the slope of the acceleration as it crosses into region 4 this cycle may be stable.

Where stable cycles exist so too must there be period doubling bifurcation.
Changing the value of the parameter $a$ we find that the 2-cycle found in Figure \ref{fig:ex1} (left) becomes two 2-cycles, then two 4-cycles, and so on until it devolves into chaos, see Figure \ref{fig:ex2}.
\begin{figure}
\centering
        \includegraphics[width=\textwidth]{bifurcation_all.eps}
        \caption{Period doubling bifurcation in the example caused by Newton-Raphson acceleration.}
\label{fig:ex2}
\end{figure}
With enough chaos the cycles are no longer stable and the acceleration exits into a convergent region.

% possible relationships between overlap and basin of cycling

While a change in the parameter $a$ is the most obvious way to alter the dynamics, one can also change the size of the overlap.
This has a direct effect on the basin of cycling in the spaces of both initial condition $\gamma$ and the parameter $a$.
\begin{figure}[t]
\centering
	\includegraphics[width=0.45\textwidth]{MTLB_1Dsin_ParamOverlap_v1_20210208.eps}
	\includegraphics[width=0.45\textwidth]{MTLB_1Dsin_BasinOverlap_v1_20210208.eps}
\caption{
\textbf{Left:} value of $a$ at which bifurcation starts.
\textbf{Right:} width of basin of cycling in $\gamma$ and $a$.}
\label{fig:overlap}
\end{figure}
Figure \ref{fig:overlap} (left) shows a nonlinear relationship between the first value of $a$ at which cycling is observed and the size of the overlap.
As the overlap grows the parameter $a$ must be larger and larger for cycling to occur.
Figure \ref{fig:overlap} (right) indicates that the interval of initial conditions that result in cycling shrinks as the overlap grows.
Meanwhile, the length of the bifurcation diagram increases, meaning there are more values of $a$ with stable cycling.

% section on the algorithm

\section{Accelerated alternating Schwarz with guaranteed convergence}

Given Theorem \ref{thm:lui} and the conditions of Table \ref{tab:conditions} one can construct a series of tests to see if the Newton-Raphson acceleration is suitable for a given iteration.
We present one further useful trick to strengthen convergence, a correction to Newton-Raphson due to Davidenko and Branin \cite{branin1972widely,brent1972davidenko,davidenko1953new}.
We replace step (3) in the algorithm with
\begin{equation*}
(3*) \quad \tilde{\gamma}_n = \gamma_n - \frac{G(\gamma_n) - \gamma_n}{\abs{G'(\gamma_n) - 1}} .
\end{equation*}
For $G(\gamma)$ within region 2 the Newton-Raphson acceleration will now always march in the direction of the fixed point.
It may still overshoot and cycle but the direction will always be correct.

For a problem satisfying the conditions of Theorem \ref{thm:lui} or similar that guarantees that $G(\gamma)$ lies in region 2 the algorithm proceeds as follows:
\begin{enumerate}
\item Select some $\gamma_0 \in \mathbb{R}$. Set $n=0$.
\item Calculate $G(\gamma_n)$ and $G'(\gamma_n)$.
If $G'(\gamma_n) = 1$ then set $\gamma_{n+1} = G(\gamma_n)$, increment $n$ and return to step 2.
If this is not true, proceed to step 3.
\item Perform step (3*), which is the Newton-Raphson acceleration using the Davidenko-Branin trick.
If $\abs{G'(\gamma_n) - 1} \geq 1/2$ then set $\gamma_{n+1} = \tilde{\gamma}_n$, increment $n$ and return to step 2.
If this is not true, calculate $\hat{\gamma}_n$, the average of $\gamma_n$ and $\tilde{\gamma}_n$, and proceed to step 4.
\item Calculate $G(\hat{\gamma}_n)$.
If $G(\hat{\gamma}_n)-\hat{\gamma}_n$ has the same sign as $G(\gamma_n) - \gamma_n$ then set $\gamma_{n+1} = \tilde{\gamma}_n$, increment $n$ and return to step 2.
If this is not true, set $\gamma_{n+1} = G(\gamma_n)$, increment $n$ and return to step 2.
\end{enumerate}

Each of steps 2, 3 and 4 contain a test of whether Newton-Raphson will converge.
In step 2, Newton-Raphson will not converge if the derivative of $G(\gamma)-1$ is zero.
In step 3, convergence is guaranteed if $G'(\gamma) \leq 1/2$ based on Table \ref{tab:conditions}.
The Davidenko-Branin trick strengthens this and also guarantees convergence if $G'(\gamma) \geq 3/2$.

In step 4 we test the point halfway between the starting value $\gamma_n$ and the Newton-Raphson acceleration $\tilde{\gamma}_n$, denoted $\hat{\gamma}_n$.
Since $G(\gamma)$ is in region 2 if $G(\gamma) > \gamma$ then $\gamma < \gamma^*$ and vice versa.
Therefore, we can easily determine whether $\hat{\gamma}_n$ is on the same side of the fixed point as $\gamma_n$.
If it is, then the fixed point $\gamma^*$ lies on the same side of $\hat{\gamma}_n$ as $\tilde{\gamma}_n$, and so $\tilde{\gamma}_n$ is closer to $\gamma^*$ than $\gamma_n$.
If it is not, then $\gamma^*$ lies between $\gamma_n$ and $\hat{\gamma}_n$.
Since $\tilde{\gamma}_n$ is on the other side of $\hat{\gamma}_n$ it is further from $\gamma^*$ than $\gamma_n$ and we have divergence.
In such a case, the fixed point iteration should be used.

Note that while $G(\gamma)$ represents alternating Schwarz in this context, it may be exchanged for any fixed point iteration, in particular any Schwarz method.
All that is required for the algorithm to function is for $G(\gamma)$ to be within region 2.
For Schwarz methods, this would necessitate a theorem similar to Theorem \ref{thm:lui}.

% section on 2D extensions?

\bibliographystyle{plain}
\bibliography{references}
%\bibliography{export.bib}

\end{document}