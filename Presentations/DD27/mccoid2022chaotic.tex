\documentclass[graybox]{svmult}

\usepackage{amsmath,amssymb}
%\usepackage{amsthm}
\usepackage{graphicx}
%\usepackage{hyperref}
%\usepackage{subcaption}
\usepackage{float}
\usepackage{tikz}
\usetikzlibrary{decorations.pathreplacing,positioning,calc,intersections,3d,shapes.geometric,math}%,external}
%\tikzexternalize[prefix=tikz/]
\usepackage{colortbl}
\usepackage{algorithm,algpseudocode}

\usepackage[bottom]{footmisc}
\usepackage{mathptmx}
\usepackage{makeidx}
\usepackage{multicol}
\smartqed

\newcommand{\dxdy}[2]{\frac{d #1}{d #2}}
\newcommand{\dxdyk}[3]{\frac{d^{#3} #1}{d {#2}^{#3}}}
\newcommand{\pdxdy}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\liminfty}[1]{\lim_{#1 \to \infty}}
\newcommand{\limab}[2]{\lim_{#1 \to #2}}

\newcommand{\abs}[1]{\left \vert #1 \right \vert}
\newcommand{\norm}[1]{\left \Vert #1 \right \Vert}
\newcommand{\order}[1]{\mathcal{O} \left ( #1 \right )}
\newcommand{\set}[1]{\left \{ #1 \right \}}
\newcommand{\Set}[2]{\left \{ #1 \ \middle \vert \ #2 \right \}}
\newcommand{\vmat}[1]{\begin{vmatrix} #1 \end{vmatrix}}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\diag}{diag}

\newcommand{\bbn}{\mathbb{N}}
\newcommand{\bbz}{\mathbb{Z}}
\newcommand{\bbq}{\mathbb{Q}}
\newcommand{\bbr}{\mathbb{R}}
\newcommand{\bbc}{\mathbb{C}}
\newcommand{\bbf}{\mathbb{F}}

\newtheorem{conj}{Conjecture}
%\newtheorem{theorem}{Theorem}
%\newtheorem{lemma}[theorem]{Lemma}
%\newtheorem{corollary}[theorem]{Corollary}
%\newtheorem{proposition}[theorem]{Proposition}
%\newtheorem{definition}{Definition}

\definecolor{darkgreen}{rgb}{0,0.6,0.1}

\let\vec\mathbf

\title{Chaotic examples in ASPIN and its cousins}

\author{Conor McCoid \and Martin J. Gander}
\institute{Conor McCoid
	\at University of Geneva, \email{conor.mccoid@unige.ch}
	\and
	Martin J. Gander
	\at University of Geneva, \email{martin.gander@unige.ch}
	}

\begin{document}

\maketitle

\section{Introduction}

In \cite{mccoid2021cycles} we showed the limits of using Newton's method to accelerate domain decomposition methods in 1D.
Methods such as ASPIN \cite{Cai2002}, RASPEN \cite{Dolean2016}, and MSPIN \cite{liu2015field} that use Schwarz methods to precondition exact or inexact Newton's method cannot always be relied upon to converge.
The goal of this paper is to expand the space of counterexamples, showing the extent to which chaotic and cycling behaviour can occur in these methods.

We begin by defining the particular method considered, alternating Schwarz preconditioning Newton's method (ASPN) in a general dimension:
\begin{align*}
(1) & \begin{cases} F(x,u_1,Du_1,D^2u_1) = 0, \\ u_1(\partial \Omega) = h, \\ u_1(\Gamma_1) = \gamma, \end{cases} &
(2) & \begin{cases} F(x,u_2,Du_2,D^2u_2) = 0, \\ u_2(\partial \Omega) = h, \\ u_2(\Gamma_2) = u_1(\Gamma_2), \end{cases} \\
(3) & \begin{cases} J(u_1) \cdot g_1 = 0, \\
g_1(\partial \Omega) = 0, \\
g_1(\Gamma_1) = I, \end{cases} &
(4) & \begin{cases} J(u_2) \cdot g_2 = 0, \\
g_2(\partial \Omega) = 0, \\
g_2(\Gamma_2) = I, \end{cases}
\end{align*}
\begin{align*}
(5) \ \gamma_{n+1}  = & \gamma_n - (G'(\gamma_n) - I)^{-1} (G(\gamma_n) - \gamma_n) \\
					= & \gamma_n - (g_1(\Gamma_2) g_2(\Gamma_1) - I)^{-1} (u_2(\Gamma_1) - \gamma_n),
\end{align*}
where $Du$ and $D^2u$ represent the partial derivatives of $u(x)$ of orders 1 and 2, respectively,
$J(u)$ is the Jacobian of $F$ evaluated for the function $u(x)$,
and $\Gamma_1$ and $\Gamma_2$ are the boundaries of the two subdomains where they overlap.
Steps (1) and (3) are solved in the first subdomain $\Omega_1$ while steps (2) and (4) are solved in the second, $\Omega_2$.

The function $G(\gamma) = u_2(\Gamma_1)$ is the fixed point iteration of alternating Schwarz.
It represents one iteration of alternating Schwarz using $\gamma$ as the boundary value of $u_1(\Gamma_1)$.
Step (5) applies Newton's method to $G(\gamma)$, thereby accelerating the convergence of the fixed point iteration.

As explained in \cite{mccoid2021cycles} Newton's method displays chaotic or cycling behaviour if it displays both oscillatory convergence and oscillatory divergence in a given domain.
In 1D this was found to happen when the fixed point iteration to be accelerated ran parallel to the functions
\begin{equation*}
g_C(x) = C \sign(x - x^*) \sqrt{\abs{x - x^*}} + x
\end{equation*}
where $g(x^*)=x^*$ and $C \in \bbr$.
In higher dimensions the class of functions indicating this behaviour greatly increases and becomes difficult to summarize, but of note if one applies the square root operator to each coordinate of $\abs{x-x^*}$ then the higher dimensional equivalent of $g_C(x)$ remains an indicator of this behaviour.

\section{Cycling counterexamples in 1D}

We seek a larger space of examples where cycling occurs for ASPN.
To do so, we employ optimization techniques.
First we must find a functional that takes a nonlinearity and returns a measure of the chaos that results from applying ASPN.

We consider the set of problems
\begin{equation}
u''(x) + f(u(x)) = 0, \quad x \in (-1,1)
\end{equation}
with homogeneous Dirichlet boundary conditions.
The function $f(x)$ satsifies $f(0)=0$ so that $u(x)=0$ is the solution to the ODE.

The counterexample presented in \cite{mccoid2021cycles},
\begin{equation*}
u''(x) - \sin(\mu u(x)) = 0, \ u(\pm 1) = 0,
\end{equation*}
makes use of the antisymmetry in $G(\gamma)$ to achieve its cycles.
We wish the same for all nonlinearities $f(x)$ in our space of counterexamples.

\begin{proposition} \label{aspn prop: antisymmetry}
If $f(x)$ is antisymmetric then $G(\gamma)$ is antisymmetric.
\end{proposition}

\begin{proof}
Suppose $\hat{u}_1(x)$ solves step (1) of alternating Schwarz with $u_1(\beta)=\gamma$.
Then $$-\hat{u}_1'' + f(-\hat{u}_1) = f(\hat{u}_1) - f(\hat{u}_1) = 0.$$
Thus, $-\hat{u}_1(x)$ solves step (1) of alternating Schwarz with $u_1(\beta)=-\gamma$.

By the same logic, if $\hat{u}_2(x)$ solves step (2) of alternating Schwarz with $u_2(\alpha) = u_1(\alpha)$ then
$-\hat{u}_2(x)$ solves step (2) with $u_2(\alpha) = -u_1(\alpha)$.
Thus, $-\hat{u}_2(\beta) = -G(\gamma) = G(-\gamma)$.
\end{proof}

We therefore restrict our search to nonlinearities which are antisymmetric.
It is then sufficient for the Newton-Raphson acceleration, represented by $G_{NR}(\gamma)$, to cross the line $y=-\gamma$ for cycles to exist.

Given that $f(x)$ is antisymmetric it can be decomposed into a Fourier series consisting solely of sinusoids:
\begin{equation} \label{aspn eq: f sin}
f(x) = \sum_{k=1}^N c_k \sin(\pi k x).
\end{equation}
Thus, the functional to optimize takes a set $\set{c_k}_{k=1}^N \in \bbr^N$, passes it through $f(x) \in C(-1,1)$ and $G(\gamma) \in C(\bbr)$ to arrive at a measure for the chaos of the system.
We will represent this functional as $L: \bbr^N \to \bbr.$
There are many ways to define $L(\set{c_k})$, but we shall use
\begin{equation} \label{aspn eq: optsin}
L(\set{c_k}) := \norm{G_{NR}(\gamma) + \gamma}.
\end{equation}
Thus, $L(\set{c_k}) = 0$ if and only if $G_{NR}(\gamma)=-\gamma$, and ASPN cycles between $\gamma$ and $-\gamma$ for all values of $\gamma$.

It is well-known that there always exists a region around the root of a function where Newton-Raphson converges, assuming continuity of $G''(\gamma)$ and $G'(\gamma) \neq 1$.
We expect then to find only local minimizers of $L(\set{c_k})$.
We use a gradient descent with line search to optimize the functional $L$, with restrictions $\gamma \in [-2,2]$ and $\set{c_k}_{k=1}^5 \in \bbr^N$, with starting condition $c_1=1$, $c_i=0$ for $i=2, \dots, 5$.
There is an overlap of 0.4 between the domains, which are symmetric about $x=0$.
We find an exceptional counterexample using this methodology, as presented in Figure \ref{aspn fig: optsin example}.
\begin{figure}
\centering
\includegraphics[width=\textwidth]{FIG_ASPN/MTLB_optsin_f_v1_20220704.eps}
\caption{A counterexample found through optimizing the functional $L$, equation (\ref{aspn eq: optsin}).
	(Left) ASPN falls into a stable 2-cycle; the basin of attraction of this cycle is most values within $[-2,-1) \cup (1, 2]$.
	(Right) The function $f(x)$ for this counterexample; it is the sum of five sinusoids.}
\label{aspn fig: optsin example}
\end{figure}

\section{Chaotic counterexamples in 2D}

We now seek counterexamples in 2D.
That is, we seek $f: \bbr \to \bbr$ such that using ASPN to solve
\begin{equation*}
u_{xx}(x,y) + u_{yy}(x,y) + f(u(x,y)) = 0, \quad x,y \in (-1,1)
\end{equation*}
with homogeneous Dirichlet boundary conditions results in cycling behaviour.
The two domains are split along the $x$-axis, so that the first domain is $x \in (-1,\alpha)$, $y \in (-1,1)$ and the second is $x \in (-\alpha,1)$, $y \in (-1,1)$.

As before, $f(x)$ is chosen to be antisymmetric so that $G(\vec{\gamma})$ is antisymmetric.
Proposition \ref{aspn prop: antisymmetry} applies to the higher dimensional case by replacing all relevant scalar objects ($\gamma$, $G(\gamma)$) with their corresponding vectors ($\vec{\gamma}$, $G(\vec{\gamma})$).
The decomposition of $f(x)$ into sinusoids and the definition of the functional $L(\set{c_k})$ remain unchanged.

If $\gamma$ is the discretization of a sinusoid then $G(\gamma)=c \gamma$, where $c \in \bbr$, up to numerical error.
This can be seen by transforming the solution into a Fourier series in the $y$ variable.
Suppose
\begin{equation*}
\begin{cases} \frac{\partial^2}{\partial x^2} u_{1}(x,y) +\frac{\partial^2}{\partial y^2} u_{1}(x,y) + f(u_1(x,y)) = 0, & x \in (-1,\alpha), \quad y \in (-1,1) \\
u_1(-1,y)=u_1(x,\pm 1) = 0 \\
u_1(\alpha,y) = C \sin(\pi m y) \end{cases}
\end{equation*}
where $f(x)$ is antisymmetric and can therefore be expressed in the form of equation (\ref{aspn eq: f sin}).
Use as an ansatz $u_1(x,y) = g(x) \sin (\pi m y)$.
Take the Fourier transform of the equation:
\begin{align*}
0 = & \int_{-1}^1 (g''(x) - m^2 \pi^2 g(x)) \sin(\pi m y) \sin(\pi k y) + f(g(x) \sin(\pi m y)) \sin (\pi k y) dy \\
= & (g''(x) - m^2 \pi^2 g(x) ) \delta_{m,k} C_m + \int_{-1}^1 \sum_{j=1}^N c_j \sin(\pi j g(x) \sin(\pi m y) ) \sin(\pi k y) dy \\
= & \delta_{m,k} \mathcal{L} g(x) + \sum_{j=1}^N c_j \int_{-1}^1 \sum_{n=0}^\infty \frac{(\pi j g(x))^{2n+1}}{(2n+1)!} \sin(\pi m y)^{2n+1} \sin(\pi k y) dy \\
= & \delta_{m,k} \mathcal{L} g(x) + \sum_{j=1}^N c_j \sum_{n=0}^\infty \frac{(\pi j g(x))^{2n+1}}{(2n+1)!} S(2n+1),
\end{align*}
where $S(n)$ is the integral of $\sin(\pi m y)^n \sin(\pi k y)$ over $(-1,1)$.
We can find a recursive formula for $S(2n+1)$:
\begin{align*}
S(2n+1) = & -\frac{1}{\pi k} \cos(\pi k y) \sin(\pi m y)^{2n+1} \bigg \vert_{-1}^1 \\
& + \int_{-1}^1 \frac{m}{k} (2n+1) \cos(\pi k y) \cos(\pi m y) \sin(\pi m y)^{2n} dy \\
= & (2n+1) \frac{m}{k} \bigg ( -\frac{1}{\pi k} \cos(\pi m y) \sin(\pi m y)^{2n} \sin(\pi k y) \bigg \vert_{-1}^1 \\
& + \frac{m}{k} 2n \int_{-1}^1 \sin(\pi k y) \left ( -\sin (\pi m y)^{2n+1} + \cos(m \pi y)^2 \sin (\pi m y)^{2n-1} \right ) dy \bigg ) \\
= & (2n+1) (2n) \frac{m^2}{k^2} \int_{-1}^1 \sin(\pi m y)^{2n-1} \sin(\pi k y) - 2 \sin(\pi m y)^{2n+1} \sin(\pi k y) dy \\
= & (2n+1) (2n) \frac{m^2}{k^2} \left ( S(2n-1) - 2 S(2n+1) \right ) \\
= & \frac{ (2n+1) (2n) \frac{m^2}{k^2} }{1 + 2 (2n+1) (2n) \frac{m^2}{k^2}} S(2n-1) \\
= & C(m,k,n) S(1) .
\end{align*}
The value of $S(1)$ is zero unless $k=m$.
This proves there exists a function $\tilde{f}_m(x)$ such that
\begin{equation*}
\mathcal{L} g(x) + \tilde{f}_m(g(x)) = 0 .
\end{equation*}
Thus, if $g(x)$ satisfies this ODE with boundary conditions $g(-1) = 0$ and $g(\alpha)=C$ then $u_1(x,y)$ solves the PDE.
Since $u_1(x,y)$ in this form satisfies the boundary conditions it is the unique solution to this step of alternating Schwarz.
The solution on the second domain, $u_2(x,y)$, then also has the same form by a symmetry argument, and its value at $x=-\alpha$ is a sinusoid of the same period.
Therefore, $G(\gamma) = c \gamma$.

As a direct consequence of this, starting with any single sine wave as boundary conditions provides a single parameter pathway for the function $G: \bbr^N \to \bbr^N$.
We take advantage of this fact and use $c \sin(\pi y)$ as the boundary condition for the first subdomain of ASPN, varying $c$ between -0.5 and 0.
To seed the optimization we use the previously obtained counterexample nonlinearity $f(x)$ from the 1D case.
The same optimization method is used.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{FIG_ASPN/MTLB_optsin2D_f_v1_20220705.eps}
\caption{A chaotic ASPN sequence in 2D, at three resolutions (top row, bottom left).
	The nonlinearity (bottom right) is found through optimization on the functional $L$, equation (\ref{aspn eq: optsin}).
	The ASPN sequence is seeded using a sine wave $\sin(\pi y)$ as boundary condition on the first subdomain, but quickly diverges from this pathway.}
\label{aspn fig: optsin2d}
\end{figure}
The resulting nonlinearity does not admit a stable cycle but highlights the chaos that can result from using ASPN.
Figure \ref{aspn fig: optsin2d} gives this nonlinearity and an example of a sequence generated by ASPN.
The sequence begins on the sine wave path described previously.
It then approaches a nearby stable cycle, having departed from the strict sine wave path to one that closely resembles sine waves (bottom left of figure).
However, the cycle is not numerically stable and is ultimately abandoned.
As it leaves this pathway it descends into a divergent regime, with the norm growing exponentially (top left).
It quickly ejects onto a convergent pathway (top right).
It is possible these cycles exist on saddlepoints, stable along some pathways but unstable along others.
A small numerical error will then shunt the ASPN sequence away from the cycle, either to a divergent (top left) or convergent (top right) pathway.

Figure \ref{aspn fig: optsin2d iterates} provides snapshots of the solution at each of the iterates.
\begin{figure}
\centering
\includegraphics[trim={2.9cm 1.5cm 2.3cm 1.5cm},clip,width=\textwidth]{FIG_ASPN/MTLB_optsin2D_iterates_v1_20220727.eps}
\caption{The first 24 iterations of the chaotic ASPN sequence.
	The left figure of each iteration shows the overall solution at that step combined from the two subdomains, and the right of each shows the resulting $G(\gamma)$ in blue and the ASPN result in black.
	For comparison, the sine wave $0.5 \sin(\pi y)$ is plotted for the cycling regime, with sign that alternates with each iteration.}
\label{aspn fig: optsin2d iterates}
\end{figure}
The first 24 iterations of ASPN are shown, giving the nearly cycling regime (iterations 1 to 12), the divergent regime (13 to 20) and the convergent regime (21 to 24).
The cycling regime is very nearly a cycle of sine waves, as seen by the comparable sine waves in red.
A small change from these sine waves in iteration 13 causes chaos to take over until relative stability in iteration 20.
From there, ASPN converges quickly, arriving at the solution by iteration 24.
The remaining iterates resolve this solution to higher precision.

\section{Conclusions}

These results show that acceleration cannot be used without consequences for all Schwarz algorithms.
As with standard Newton-Raphson, there exist problems for which the sequence diverges, cycles or behaves chaotically.

In 1D, we witnessed cycling behaviour with large basins of attraction.
In 2D, while none of the cycles found were numerically stable the resulting chaos hampered convergence, increasing rather than decreasing the number of iterations required.

\bibliographystyle{plain}
\bibliography{references}

\end{document}