\documentclass{beamer}

\usetheme{CambridgeUS}
\setbeamertemplate{page number in head/foot}{}
\usecolortheme{dolphin}
%\setbeameroption{hide notes}

%===Packages===%

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{subcaption}
\usepackage{tikz}
\usetikzlibrary{decorations.pathreplacing,positioning,calc,intersections,3d,shapes.geometric,math,external}
\usepackage{algorithm,algorithmicx,algpseudocode}

%===Math Operators===%

\newcommand{\dxdy}[2]{\frac{d #1}{d #2}}
\newcommand{\dxdyk}[3]{\frac{d^{#3} #1}{d {#2}^{#3}}}
\newcommand{\pdxdy}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\liminfty}[1]{\lim_{#1 \to \infty}}
\newcommand{\limab}[2]{\lim_{#1 \to #2}}

\newcommand{\abs}[1]{\left \vert #1 \right \vert}
\newcommand{\norm}[1]{\left \Vert #1 \right \Vert}
\newcommand{\order}[1]{\mathcal{O} \left ( #1 \right )}
\newcommand{\set}[1]{\left \{ #1 \right \}}
\newcommand{\Set}[2]{\left \{ #1 \ \middle \vert \ #2 \right \}}
\newcommand{\vmat}[1]{\begin{vmatrix} #1 \end{vmatrix}}
\DeclareMathOperator{\sign}{sign}

\let\vec\mathbf

%===Important Sets===%

\newcommand{\bbn}{\mathbb{N}}
\newcommand{\bbz}{\mathbb{Z}}
\newcommand{\bbq}{\mathbb{Q}}
\newcommand{\bbr}{\mathbb{R}}
\newcommand{\bbc}{\mathbb{C}}
\newcommand{\bbf}{\mathbb{F}}

%===Types of Statements===%

\newtheorem{lem}{Lemma}
\newtheorem{thm}{Theorem}
\newtheorem{cor}{Corollary}
\newtheorem{prop}{Proposition}
\newtheorem{conj}{Conjecture}
\newtheorem{defn}{Definition}

%===Specific commands for this talk===%
\newcommand{\rxi}[1]{\vec{r}(\vec{x}_{#1})}
\newcommand{\fxi}[1]{\vec{f}(\vec{x}_{#1})}
\newcommand{\Jxn}{J(\vec{x}_n)}
%===%

\usepackage{setspace}
\usepackage{animate}
\usepackage{tikz}
\usetikzlibrary{calc,intersections,positioning,tikzmark,fit,3d}

%\setbeamertemplate{footnote}{%
%  \hangpara{2em}{1}%
%  \makebox[2em][l]{\insertfootnotemark}\footnotesize\insertfootnotetext\par%
%}
\usepackage[backend=bibtex,style=authoryear]{biblatex}
\ExecuteBibliographyOptions{doi=false, eprint=false, url=false}
\addbibresource{export.bib}
\setbeamerfont{footnote}{size=\tiny}

\title{Extrapolation methods as nonlinear Krylov methods}
\author{Conor McCoid, Martin J. Gander}
\institute{Universit\'e de Gen\`eve}
\date{23.06.2022}

\begin{document}

\frame{\titlepage}

% Outline:
%	--Krylov methods
%		* how they work
%		* specific example: GMRES?
%	--Extrapolation methods
%		* how they work
%		* specific example: the GMRES equivalent -> MPE?
%	--Equivalence between GMRES and MPE
%	--Multisecant equations
%		* generalization of secant method
%		* quasi-Newton methods?
%		* how to get to Krylov and extrapolation

% To do:
%	quasi-Newton methods?
%	details on my proof of equivalence?

\begin{frame}
\frametitle{Linear vs. Nonlinear}
Linear problems:
\begin{equation*}
A \vec{x} = \vec{b}
\end{equation*}

~

Nonlinear problems:
\begin{equation*}
\vec{f} ( \vec{x} ) = \vec{b}
\end{equation*}
\end{frame}

%===Krylov methods===%

\begin{frame}
\frametitle{Krylov methods}
To solve the linear problem $A \vec{x} = \vec{b}$, consider Krylov subspaces:
\begin{equation*}
\mathcal{K}_k(A,\vec{q}) = \text{span} \set{\vec{q}, A\vec{q}, \dots, A^{k-1} \vec{q}}
\end{equation*}
We seek a solution $\vec{x}_k \in \set{\vec{x}_0} \cup \mathcal{K}_k(A,\vec{r}_0)$, where $\vec{r}_0 = A\vec{x}_0 - \vec{b}$.

~

Often, we ask that the search direction $\vec{q}_k$ be orthogonal to $\mathcal{K}_k(A,\vec{r}_0)$ with respect to some inner product.
\end{frame}

\begin{frame}
\frametitle{GMRES (general minimal residual)}
GMRES minimizes the 2-norm of the residual, $\norm{A \vec{x} - \vec{b}}$.

~

For GMRES, the search direction $\vec{q}_k$ satisfies
\begin{equation*}
\vec{q}_k^\top \vec{q}_j = 0 \quad \forall j < k, \quad \vec{q}_j \in \mathcal{K}_j(A, \vec{r}_0) .
\end{equation*}
Thus, $\vec{q}_k$ is orthogonal to $\mathcal{K}_{k-1}(A, \vec{r}_0)$.
\end{frame}

%===Extrapolation methods===%

\begin{frame}
\frametitle{Extrapolation methods}
Extrapolation methods seek to minimize the difference in iterates $\rxi{i} = \vec{x}_{i+1} - \vec{x}_i$.
Given previous search directions $\set{\vec{x}_n, \dots, \vec{x}_{n+k}}$ the next search direction lies in their column space,
\begin{equation*}
\hat{\vec{x}}_{n+1} = \sum_{i=0}^k \alpha_i \vec{x}_{n+i} .
\end{equation*}
We can choose the $\alpha_i$ such that
\begin{equation*}
\vec{x} = \lim_{n \to \infty} \vec{x}_n = \sum_{i=0}^k \alpha_i \vec{x}_{n+i} \iff \sum_{i=0}^k \alpha_i ( \vec{x}_{n+i} - \vec{x}) = 0.
\end{equation*}
\end{frame}

\begin{frame}
\frametitle{Extrapolation methods}
Some ways to satisfy this can be written as:
\begin{equation*}
\hat{\vec{x}}_{n+1} = \frac{ \vmat{
\vec{x}_n & \dots & \vec{x}_{n+k} \\
\vec{v}_1^\top \rxi{n} & \dots & \vec{v}_1^\top \rxi{n+k} \\
\vdots & & \vdots \\
\vec{v}_k^\top \rxi{n} & \dots & \vec{v}_k^\top \rxi{n+k} }}{ \vmat{
1 & \dots & 1 \\
\vec{v}_1^\top \rxi{n} & \dots & \vec{v}_1^\top \rxi{n+k} \\
\vdots & & \vdots \\
\vec{v}_k^\top \rxi{n} & \dots & \vec{v}_k^\top \rxi{n+k} }}
\end{equation*}
(note this is a generalized notion of determinant)
% more explanation needed?
\end{frame}

\begin{frame}
\frametitle{Extrapolation methods}
Using Cramer's rule, the equation for $\hat{\vec{x}}_{n+1}$ implies
\begin{equation*}
\hat{\vec{x}}_{n+1} = \begin{bmatrix} \vec{x}_n & \dots & \vec{x}_{n+k} \end{bmatrix} \vec{u}, \quad \begin{bmatrix} 1 & \dots & 1 \\ \vec{v}_1^\top \rxi{n} & \dots & \vec{v}_1^\top \rxi{n+k} \\ \vdots & & \vdots \\ \vec{v}_k^\top \rxi{n} & \dots & \vec{v}_k^\top \rxi{n+k} \end{bmatrix} \vec{u} = \begin{bmatrix} 1 \\ 0 \\ \vdots \\ 0 \end{bmatrix},
\end{equation*}
so $\sum_{i=0}^{k} u_i \rxi{n+i}$ is orthogonal to $\vec{v}_j$ for all $j$.
\end{frame}

\begin{frame}
\frametitle{MPE (minimum polynomial extrapolation)}
For MPE, the vectors $\vec{v}_i = \rxi{n+i-1} = \vec{x}_{n+i} - \vec{x}_{n+i-1}$,
and so $\sum_{i=0}^k u_i \rxi{n+i}$ is orthogonal to $\rxi{n+i-1}$ for all $i=1, \dots, k$.

~

Once the maximum number of search directions has been found ($k=d$, the dimension of $\vec{x}$) the previous directions are replaced one by one.
That is, once $\vec{x}_{n+d}$ is found the direction $\vec{x}_n$ is dropped to make room for the new direction.
\end{frame}

%===Equivalence===%

\begin{frame}
\frametitle{Equivalence between MPE and GMRES \footnote[frame]{\cite{Sidi1988,WalkerNi}}}
When MPE is applied to a linear sequence, $\vec{x}_{n+1} = (A+I) \vec{x}_n - \vec{b}$, then
\begin{align*}
\rxi{n+1} = & (A+I) \vec{x}_{n+1} - \vec{b} - (A+I) \vec{x}_n + \vec{b} \\
	= & (A+I) (\vec{x}_{n+1} - \vec{x}_n) = (A+I) \rxi{n}, \\
\rxi{n} = & (A+I) \vec{x}_{n} - \vec{b} - \vec{x}_n \\
	= & A \vec{x}_n - \vec{b}.
\end{align*}
Thus, $\norm{\rxi{n+i}} = \norm{A \vec{x}_{n+i} - \vec{b}}$ and we are solving the equation $A \vec{x} = \vec{b}$,
and $\rxi{n+k-1} \in \mathcal{K}_k(A+I,\rxi{n})$.
\end{frame}

\begin{frame}
We've seen that for MPE $\sum_{i=0}^k u_i \rxi{n+i}$ is orthogonal to all $\vec{v}_i = \rxi{n+i-1}$.

~

Moreover, $\mathcal{K}_k(A,\rxi{n}) = \mathcal{K}_k(A + I, \rxi{n})$, and so the vectors $\vec{u}$ used to find $\hat{\vec{x}}_{n+1}$ are constructed equivalently to the Arnoldi process.

~

The search directions are therefore identical to those used for GMRES. % nb: let's add some numerical results, using HH reflections etc.
\end{frame}

\begin{frame}
\frametitle{Side by side comparison}
\begin{columns}
\begin{column}{0.49\textwidth}
\begin{algorithm}[H]
\caption{GMRES}
\begin{algorithmic}
	\State $\to \vec{x}_0$
	\State $\vec{r}=A \vec{x}_0 - \vec{b}$
	\State $\vec{q}_1 = \vec{r} / \norm{\vec{r}}$
	\For{$k=1$ to $n$}
		\State
		\State $\vec{y} = A \vec{q}_k$
		\State orthogonalize $\vec{y}$ wrt $Q_{k-1}$
%		\For{$j=1$ to $k$}
%			\State $H(j,k) = \vec{q}_j^\top \vec{y}$
%			\State $\vec{y}=\vec{y}-H(j,k) \vec{q}_j$
%		\EndFor
%		\State $H(k+1,k) = \norm{\vec{y}}$
		\State $\vec{q}_{k+1} = \vec{y} / \norm{\vec{y}}$
	\EndFor
	\State minimize $\norm{H_n \vec{u} - \norm{\vec{r}} \vec{e}_1}$ \footnote[frame]{$A Q_k = Q_{k+1} H_k$}
	\State $\hat{\vec{x}}_{n+1} = Q_n \vec{u} + \vec{x}_0$
\end{algorithmic}
\end{algorithm}
\end{column}
\begin{column}{0.49\textwidth}
\begin{algorithm}[H]
\caption{MPE}
\begin{algorithmic}
	\State $\to \vec{x}_0$
	\State $\vec{r} = A \vec{x}_0 - \vec{b}$
	\State $\vec{q}_1 = \vec{r} / \norm{\vec{r}}$
	\For{$k=1$ to $n$}
		\State $\vec{x}_k = (A + I) \vec{x}_{k-1} - \vec{b}$
		\State $\vec{y} = \vec{x}_k - \vec{x}_{k-1}$
		\State orthogonalize $\vec{y}$ wrt $Q_{k-1}$
		\State $\vec{q}_{k+1} = \vec{y} / \norm{\vec{y}}$
	\EndFor
	\State minimize $\norm{H_n \vec{u}}$ st $\vec{1}^\top \vec{u} = 1$
	\State $\hat{\vec{x}}_{n+1} = \begin{bmatrix} \vec{x}_0 & \dots & \vec{x}_n \end{bmatrix} \vec{u}$
\end{algorithmic}
\end{algorithm}
\end{column}
\end{columns}
\end{frame}

%===Multisecant equations===%

\begin{frame}
\frametitle{Multisecant equations -- where they come from}
Recall the secant method for finding the roots of a function $f: \bbr \to \bbr$:
\begin{equation*}
\hat{x} = x_{n+1} -  \frac{x_{n+1} - x_n}{f(x_{n+1}) - f(x_n)}f(x_{n+1}),
\end{equation*}
which uses $(f(x_{n+1}) - f(x_n))/(x_{n+1} - x_n)$ as an approximation to the derivative.
\begin{figure}
	\begin{tikzpicture}
		 \draw[->,thick] (-0.5,0) -- (1.5,0) node[below] {$x$}; \draw[->,thick] (0,-0.5) -- (0,1.5) node[right] {$y$};
		 \draw[domain=-0.5:1.15, smooth, variable=\x, blue] plot ({\x}, {\x*\x*\x + \x/2});
		 \draw[red] (0.5,0.5^3+0.25) circle (1pt); \draw[red] (1,1.5) circle (1pt);
		 \draw[thick,red] (0.5,0.375) -- (1,1.5);
		 \draw[->,thick,red] (0.5,0.5^3+0.25) -- (0.5 - 0.1667,0);
	\end{tikzpicture}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Multisecant equations -- where they come from}
In higher dimensions, an approximation to the Jacobian $J(\vec{x}_n)$ satisfies:
\begin{equation*}
\fxi{n+i} - \fxi{n} \approx \Jxn ( \vec{x}_{n+i} - \vec{x}_n ).
\end{equation*}
Collect enough of these and you get the system
\begin{equation*}
\begin{bmatrix} \fxi{n+1} & \dots & \fxi{n+d} \end{bmatrix} - \fxi{n} \vec{1}^\top = \hat{J} \left ( \begin{bmatrix}
\vec{x}_{n+1} & \dots & \vec{x}_{n+d} \end{bmatrix} - \vec{x}_n \vec{1}^\top \right )
\end{equation*}
where $\hat{J}$ aproximates $\Jxn$.
\end{frame}

\begin{frame}
\frametitle{Multisecant equations -- where they come from}
Using this approximate Jacobian, we can find an approximate root of $\vec{f}: \bbr^d \to \bbr^d$:
\begin{equation*}
\hat{\vec{x}} = \vec{x}_n - \hat{J}^{-1} \fxi{n}.
\end{equation*}
This system is called the multisecant equations.

~

If you don't have enough values of $\fxi{k}$ then the system to find $\hat{J}$ is underdetermined.
Either pad out the values of $\fxi{k}$ or add constraints to the system.
\end{frame}

\begin{frame}
\frametitle{Multisecant equations - Newtonian form}
Let $F_{n,k} = \begin{bmatrix} \fxi{n} & \dots & \fxi{n+k} \end{bmatrix}$ and  $X_{n,k} = \begin{bmatrix} \vec{x}_n & \dots & \vec{x}_{n+k} \end{bmatrix}$.
Then the system to find the approximate of the Jacobian may be written as
\begin{equation*}
F_{n,k} \Delta_n = \hat{J} X_{n,k} \Delta_n
\end{equation*}
where
\begin{equation*}
\Delta_n = \begin{bmatrix} -1 & \dots & -1 \\ 1 \\ & \ddots \\ & & 1 \end{bmatrix}.
\end{equation*}
\end{frame}

\begin{frame}
\frametitle{Multisecant equations - Newtonian form}
Recall we are seeking the term $\hat{J}^{-1} \fxi{n}$.
Multiply the previous system by $\hat{J}^{-1}$:
\begin{equation*}
\hat{J}^{-1} F_{n,k} \Delta_n = X_{n,k} \Delta_n.
\end{equation*}
If there exists some $\tilde{\vec{u}}$ such that $F_{n,k} \Delta_n \tilde{\vec{u}} = \fxi{n}$ then $\hat{J}^{-1} \fxi{n} = X_{n,k} \Delta_n \tilde{\vec{u}}$:
\begin{equation*}
F_{n,k} \Delta_n \tilde{\vec{u}} = \fxi{n}, \quad \hat{\vec{x}} = \vec{x}_n - X_{n,k} \Delta_n \tilde{\vec{u}}.
\end{equation*}
\end{frame}

\begin{frame}
\frametitle{Multisecant equations - underdetermined Newtonian form}
If the system to find $\hat{J}$ is underdetermined then the system to find $\tilde{\vec{u}}$ is overdetermined.
To solve it, multiply by some matrix $B^\top$:
\begin{equation*}
B^\top F_{n,k} \Delta_n \tilde{\vec{u}} = B^\top \fxi{n}, \quad \hat{\vec{x}} = \vec{x}_n - X_{n,k} \Delta_n \tilde{\vec{u}}.
\end{equation*}
For example, if $B = F_{n,k} \Delta_n$ then we solve for $\tilde{\vec{u}}$ in a least-squares sense.
\end{frame}

\begin{frame}
\frametitle{Multisecant equations - intermediate form}
Let $\hat{\vec{u}} = \Delta_n \tilde{\vec{u}}$, then $\vec{1}^\top \hat{\vec{u}} = 0$ and
\begin{equation*}
\begin{bmatrix} \vec{1}^\top \\ B^\top F_{n,k} \end{bmatrix} \hat{\vec{u}} = \begin{bmatrix} 0 \\ B^\top \fxi{n} \end{bmatrix}, \quad \hat{\vec{x}} = \vec{x}_{n} - X_{n,k} \hat{\vec{u}}.
\end{equation*}
\end{frame}

\begin{frame}
\frametitle{Multisecant equations - base form}
Finally, let $\vec{u} = \vec{e}_1 - \hat{\vec{u}}$ so that
\begin{equation*}
\begin{bmatrix} \vec{1}^\top \\ B^\top F_{n,k} \end{bmatrix} \vec{u} = \begin{bmatrix} 1 \\ \vec{0} \end{bmatrix}, \quad \hat{\vec{x}} = X_{n,k} \vec{u}.
\end{equation*}

~

From this base form you can make many more methods.
For starters, if $\vec{u} = \vec{e}_i - \hat{\vec{u}}$ then the vectors $\fxi{n}$ and $\vec{x}_n$ in the Newtonian form are replaced by $\fxi{n+i}$ and $\vec{x}_{n+i}$, respectively, without changing $\Delta_n$ or $\hat{\vec{x}}$.
\end{frame}

\begin{frame}
\frametitle{How to solve the multisecant equations}
Using the base form and Cramer's rule, with the columns of $B$ equal to $\vec{v}_i$, the solution $\hat{\vec{x}}$ is equal to
\begin{equation} \label{eq:uqn}
\hat{\vec{x}}_{n+1} = \frac{ \vmat{
\vec{x}_n & \dots & \vec{x}_{n+k} \\
\vec{v}_1^\top \fxi{n} & \dots & \vec{v}_1^\top \fxi{n+k} \\
\vdots & & \vdots \\
\vec{v}_k^\top \fxi{n} & \dots & \vec{v}_k^\top \fxi{n+k} }}{ \vmat{
1 & \dots & 1 \\
\vec{v}_1^\top \fxi{n} & \dots & \vec{v}_1^\top \fxi{n+k} \\
\vdots & & \vdots \\
\vec{v}_k^\top \fxi{n} & \dots & \vec{v}_k^\top \fxi{n+k} }}.
\end{equation}
\end{frame}

\begin{frame}
\frametitle{General equivalence}
Based on the first underdetermined form we're asking $\vec{v}_i^\top F_{n,k} \vec{u} = 0$ for all $i=1,\dots,k$.
For a problem with $\fxi{n}=\rxi{n}$ this means we're asking the residual to be orthogonal to the space spanned by the columns of $B$.

~

If we also have that $\fxi{i+1} = A \fxi{i}$ for all $i$ then the columns of $F_{n,k}$ lie in $\mathcal{K}_k(A, \fxi{n})$.

~

If both are true then we have both an extrapolation method and a Krylov method.
Which ones depend on the choice of $B$.
\end{frame}

\begin{frame}
GMRES: $\hat{\vec{x}} =\vec{x}_n + Q_k \vec{y}_k$ where $Q_k$ is derived from the Arnoldi iteration on the Krylov subspace $\mathcal{K}_{k-1}(A,\fxi{n})$;
\begin{align*}
\norm{A \vec{x}_n + A Q_k \vec{y}_k - \vec{b}} = & \norm{\fxi{n} + Q_k H_{k+1} \vec{y}_k}.
\end{align*}

\pause Multisecant Newtonian form: $\hat{\vec{x}} = \vec{x}_n + X_{n,k} \Delta \tilde{\vec{u}}_k$;
\begin{align*}
\norm{A \vec{x}_n + A X_{n,k} \Delta \tilde{\vec{u}}_k - \vec{b}} = & \norm{ \fxi{n} + F_{n,k} \Delta \tilde{\vec{u}}_k}
\end{align*}

\pause MPE: $\hat{\vec{x}} = X_{n,k} \vec{u}_k$ such that $\vec{1}^\top \vec{u}_k=1$;
\begin{align*}
\norm{A X_{n,k} \vec{u}_k - \vec{b}} = & \norm{(A X_{n,k} - \vec{b} \vec{1}^\top) \vec{u}_k} \\
						     = & \norm{F_{n,k} \vec{u}_k}.
\end{align*}

%\item $\hat{\vec{x}} = \vec{x}_n + X_{n,k} \Delta \tilde{\vec{u}}_k$ where
%\begin{equation*}
%\Delta = \begin{bmatrix} -1 \\ 1 & \ddots \\ & \ddots & -1 \\ & & 1 \end{bmatrix};
%\end{equation*}
%\begin{align*}
%\norm{(A-I) \vec{x}_n + (A-I) X_{n,k} \Delta \tilde{\vec{u}}_k + \vec{b}} = & \norm{\fxi{n} + F_{n,k} \Delta \tilde{\vec{u}}_k} \\
%											 = & \norm{\fxi{n} + (A-I) F_{n,k-1} \tilde{\vec{u}}_k}.
%\end{align*}
\end{frame}

%\begin{frame}
%\frametitle{Table of equivalences}
%\begin{table}
%	\begin{tabular}{c | c | c}
%		Extrap. method & Residual is orthogonal to... & Associated Krylov \\ \hline
%		MPE & $\mathcal{K}_{k-1} (A,\fxi{n})$ (Arnoldi) & GMRES \\
%		RRE & $\mathcal{K}_{k-1} (A-I,\fxi{n})$ & GCR \\
%		MMPE & $\mathcal{K}_{k-1} (G,\vec{q}_0)$ & n/a
%	\end{tabular}
%	\caption{Connections between extrapolation methods and Krylov methods.}
%	\label{tab:KrylovExtrap}
%\end{table}
%% We discussed how MPE in fact needs to use the Krylov subspace of A-I to be equivalent to GMRES
%% This Krylov subspace is equal to that for A, so finding a basis for K(A,r) also finds a basis for K(A-I,r)
%% Hence the equivalence
%% But for RRE, the basis is found specifically for K(A-I,r)
%% The difference is in how the basis is found, but both are bases for the same spaces
%\end{frame}

\begin{frame}
\frametitle{Map of equivalences}
\begin{figure}
  \centering
  \begin{tikzpicture}
    \matrix (m) [column sep=0.8cm, row sep=1em, ampersand replacement=\&]{
%      \& \& \& \& \node[align=center] (Anderson) {Anderson\\mixing}; \\
%      \node[align=center] (multisecant) {$\begin{bmatrix} \vec{1}^\top \\ F_{n,k} \end{bmatrix} \vec{u} = \begin{bmatrix} 1 \\ 0 \end{bmatrix}$,\\ $X_{n,k} \vec{u} = \hat{\vec{x}}$}; \&
      \node (overdetermined) {$\begin{bmatrix} \vec{1}^\top \\ B^\top F_{n,k} \end{bmatrix} \vec{u} = \begin{bmatrix} 1 \\ 0 \end{bmatrix}$}; \& \&
      \node[coordinate] (preMPE) {}; \& \node (MPE) {MPE}; \\
      \& \& \& \node (GMRES) {GMRES}; \\
%      \node[align=center] (preBroyden) {$\begin{bmatrix} \vec{1}^\top \& \vec{1}^\top \\ F_{n,k} \& B \end{bmatrix} \vec{u} = \begin{bmatrix} 1 \\ 0 \end{bmatrix}$,\\ $\begin{bmatrix} X_{n,k} \& C \end{bmatrix} \vec{u} = \hat{\vec{x}}$};
      \node[coordinate] (prepreRRE) {}; \& \& \node[coordinate] (preRRE) {}; \& \node (RRE) {RRE}; \\
      \& \& \& \node (GCR) {GCR}; \\
      \node[coordinate] (prepreMMPE) {}; \& \& \node[coordinate] (preMMPE) {}; \& \node (MMPE) {MMPE}; \\
%      \& \node[coordinate] (preTEA) {}; \& \& \node[coordinate] (preBiCG) {}; \& \node (BiCG) {BiCG}; \\
%      \node (Broyden) {Generalized Broyden};
%      \& \& \& \& \node (TEA) {TEA*}; \\
      };
    \path[->,very thick]
%      (multisecant) edge node[above] {$k < d$} (overdetermined) edge node[right] {$k < d$} (preBroyden)
%      (preBroyden) edge node[right,align=left] {$(C \Delta)^\top (X_{n,k} \Delta) = 0$, \\ $B = \hat{J}_{n-1,k} C$} (Broyden)
      (overdetermined) edge node[above] {$B=F_{n,k-1}$} (preMPE)
      (preMPE) edge[red] (MPE)
      (prepreRRE) edge node[above] {$B=F_{n,k} \Delta$} (preRRE)
      (preRRE) edge[red] (RRE)
      (prepreMMPE) edge node[above] {$B=\begin{bmatrix} \vec{q}_1 & \dots & \vec{q}_k \end{bmatrix}$} (preMMPE)
      (preMMPE) edge[red] (MMPE);
%      (preTEA) edge node[above] {$B=\begin{bmatrix} \vec{q} & A^\top \vec{q} & \dots \end{bmatrix}$} (preBiCG)
%      (preBiCG) edge[blue] (BiCG)
%      (BiCG) edge[red] (TEA);
    \draw[-, very thick] (overdetermined) edge (prepreMMPE);
    \draw[->,very thick,blue] (preMPE) |- (GMRES);
    \draw[->,very thick,blue] (preRRE) |- (GCR);
%    \draw[->,very thick] (preMPE) |- node[above] {+relaxation} (Anderson);
  \end{tikzpicture}
  \caption{Interconnectivity of extrapolation and acceleration methods.
  Red arrows indicated $\fxi{n} = \vec{x}_{n+1}-\vec{x}_n$ while blue arrows indicate $\fxi{n} = A \vec{x}_n - \vec{b}$ and $\fxi{n+1} = (A+I) \fxi{n}$.}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Conclusions}
\begin{itemize}
\item Extrapolation methods and Krylov subspace methods use the same underlying multisecant equations, and so they can share techniques
\item We can use extrapolation methods to solve nonlinear sequences in place of Krylov subspace methods
\end{itemize}

\printbibliography
\end{frame}

\begin{frame}
\frametitle{Quasi-Newton methods}
Instead of adding constraints to the multisecant equations to solve for $\hat{J}$, one can pad out the values of $\fxi{k}$:
\begin{equation*}
\begin{bmatrix} 1 & \dots & 1 & 1 & \dots & 1 \\ \fxi{n} & \dots & \fxi{n+k} & \vec{v}_1 & \dots & \vec{v}_{d-k} \end{bmatrix} \vec{u} = \begin{bmatrix} 1 \\ \vec{0} \end{bmatrix}, \quad \begin{bmatrix} X_{n,k} & C \end{bmatrix} \vec{u} = \hat{\vec{x}}
\end{equation*}
for some set of vectors $\vec{v}_i$ and some matrix $C$ satisfying some conditions.

~

For example, generalized Broyden's method for solving $\fxi{}=\vec{0}$ may be written as above with
\begin{equation*}
(C \Delta)^\top (X_{n,k} \Delta) = 0, \quad B = \hat{J}_{n-1,k} C
\end{equation*}
where $B$ is the matrix with columns $\vec{v}_i$ and $\hat{J}_{n-1,k}$ is a previous estimate of $J$.
\end{frame}

\begin{frame}
\frametitle{TEA and BiCG}
\only<1>{TEA:
\begin{equation*}
\vec{\hat{x}}_{n+1} = \frac{ \vmat{
\vec{x}_n & \dots & \vec{x}_{n+k} \\
\vec{q}^\top \fxi{n} & \dots & \vec{q}^\top \fxi{n+k} \\
\vdots & & \vdots \\
\vec{q}^\top \fxi{n+k-1} & \dots & \vec{q}^\top \fxi{n+2k-1}
}}{ \vmat{
1 & \dots & 1 \\
\vec{q}^\top \fxi{n} & \dots & \vec{q}^\top \fxi{n+k} \\
\vdots & & \vdots \\
\vec{q}^\top \fxi{n+k-1} & \dots & \vec{q}^\top \fxi{n+2k-1}}}
\end{equation*}
}
\only<2>{If $\fxi{n+1} = A \fxi{n}$ then TEA becomes
\begin{equation*}
\begin{bmatrix} 1^\top \\ B^\top F_{n,k} \end{bmatrix} \vec{u} = \begin{bmatrix} 1 \\ 0 \end{bmatrix}, \quad \hat{\vec{x}} = X_{n,k} \vec{u}
\end{equation*}
with $B = \begin{bmatrix} \vec{q} & A^\top \vec{q} & \dots & (A^\top)^{k-1} \vec{q} \end{bmatrix}$.

~

The search directions now are biorthogonal and can be found through the Lanczos algorithm.
Linear TEA is then equivalent to biorthogonal conjugate gradient (BiCG).
}
\end{frame}

\end{document}