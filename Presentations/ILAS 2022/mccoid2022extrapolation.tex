\documentclass{elsarticle}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{subcaption}
\usepackage{float}
\usepackage{tikz}
\usetikzlibrary{decorations.pathreplacing,positioning,calc,intersections,3d,shapes.geometric,math}
\usepackage{colortbl}
\usepackage{algorithm,algpseudocode}

\newcommand{\dxdy}[2]{\frac{d #1}{d #2}}
\newcommand{\dxdyk}[3]{\frac{d^{#3} #1}{d {#2}^{#3}}}
\newcommand{\pdxdy}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\liminfty}[1]{\lim_{#1 \to \infty}}
\newcommand{\limab}[2]{\lim_{#1 \to #2}}

\newcommand{\abs}[1]{\left \vert #1 \right \vert}
\newcommand{\norm}[1]{\left \Vert #1 \right \Vert}
\newcommand{\order}[1]{\mathcal{O} \left ( #1 \right )}
\newcommand{\set}[1]{\left \{ #1 \right \}}
\newcommand{\Set}[2]{\left \{ #1 \ \middle \vert \ #2 \right \}}
\newcommand{\vmat}[1]{\begin{vmatrix} #1 \end{vmatrix}}
\DeclareMathOperator{\sign}{sign}

\newcommand{\bbn}{\mathbb{N}}
\newcommand{\bbz}{\mathbb{Z}}
\newcommand{\bbq}{\mathbb{Q}}
\newcommand{\bbr}{\mathbb{R}}
\newcommand{\bbc}{\mathbb{C}}
\newcommand{\bbf}{\mathbb{F}}

\newtheorem{conj}{Conjecture}
\newtheorem{proposition}{Proposition}

\definecolor{darkgreen}{rgb}{0,0.6,0.1}

\let\vec\mathbf

\title{Extrapolation methods as nonlinear Krylov subspace methods}
\author[1]{Conor McCoid}
\affiliation[1]{organization={University of Geneva},
	addressline={24 rue de G\'en\'eral-Dufour},
	postcode={1211 Gen\`eve 4},
	country={Switzerland}}

\begin{document}

% === notes ===
% equivalence is well-known, see survey paper on extrapolation methods: Vector extrapolation methods. Applications and numerical comparison, Jbilou and Sadok -- will need to cite much of this, esp. Sidi and Saad
% in above, MMPE is said to be equivalent to Hessenberg method, which I've never heard of
% 		according to cited reference on this method (The Algebraic Eigenvalue Problem, by Wilkinson) this is in fact the QR algorithm? need to work through this
% should also include some other popular extrapolation and Krylov methods in a table
%		if methods in the table do not have known equivalents then it is a good opportunity to construct more

\begin{abstract}
When applied to linear vector sequences extrapolation methods are equivalent to Krylov subspace methods.
Both types of methods can be expressed as particular cases of the multisecant equations, the secant method generalized to higher dimensions.
Through these equations, there is also equivalence with a variety of quasi-Newton methods.
This paper presents a framework to connect these various methods.
\end{abstract}

\maketitle

\newcommand{\fxi}[1]{\vec{f}(\vec{x}_{#1})}
\newcommand{\rxi}[1]{\vec{r}(\vec{x}_{#1})}
\newcommand{\Jxn}{J(\vec{x}_n)}

\section{Introduction}

The equivalence between extrapolation methods and Krylov subspace methods is well-studied \cite{Sidi1988, Sidi1991, Jbilou2000}.
These have largely focused on individual extrapolation methods and the orthogonalization processes involved in each.
Equivalence between Krylov subspace methods and quasi-Newton methods, including the multisecant equations, is less studied but still known \cite{Walker2011}.
Gragg and Stewart describe using a QR factorization to solve the multisecant equations \cite{Gragg1976};
If the function evaluations form a Krylov subspace this would be exactly GMRES \cite{Saad1986}.

Sidi \cite{Sidi1986} has developed a framework for extrapolation methods, while Fang and Saad \cite{Fang2009} have developed one for quasi-Newton methods, but neither includes consideration of the other type of methods, nor Krylov subspace methods.
While the framework presented here does not include every method considered in both of these previous frameworks, it shows all three types of methods connect fundamentally.

To show these connections in the simplest way possible, we begin by considering the multisecant equations in various forms.
From these various forms we build quasi-Newton methods, extrapolation methods, and Krylov subspace methods.

\section{Multisecant equations}
\label{multisecant sec:intro}

The multisecant equations are a generalization of the secant method into higher dimensions.
Recall that the secant method seeks the root of the function $f(x)$ by computing an approximation of the derivative $f'(x_n)$:
\begin{equation*}
\hat{x} = x_{n+1} -  \left ( x_{n+1} - x_n \right) \left ( f(x_{n+1}) - f(x_n) \right )^{-1}f(x_{n+1}) .
\end{equation*}
\begin{figure}
	\centering
	\begin{tikzpicture}
		 \draw[->,thick] (-0.5,0) -- (1.5,0) node[below] {$x$}; \draw[->,thick] (0,-0.5) -- (0,1.5) node[right] {$y$};
		 \draw[domain=-0.5:1.15, smooth, variable=\x, blue] plot ({\x}, {\x*\x*\x + \x/2});
		 \draw[red] (0.5,0.5^3+0.25) circle (1pt); \draw[red] (1,1.5) circle (1pt);
		 \draw[thick,red] (0.5,0.375) -- (1,1.5);
		 \draw[->,thick,red] (0.5,0.5^3+0.25) -- (0.5 - 0.1667,0);
	\end{tikzpicture}
	\caption{Example of the secant method.
		Two points $(x_1,f(x_1))$ and $(x_2,f(x_2))$ are used to draw a line.
		The zero of this line is then used as the next estimate of the root of $f(x)$.}
\end{figure}

In higher dimensions it is necessary to find an approximation to the Jacobian $\Jxn$.
To do so, one can expand $\vec{f}(\vec{x}_n)$ into a Taylor series:
\begin{equation*}
\fxi{n+i} = \vec{f}(\vec{x}_n) + J(\vec{x}_n) ( \vec{x}_{n+i} - \vec{x}_n ) + \frac{1}{2} ( \vec{x}_{n+i} - \vec{x}_n ) H(\vec{x}_n) ( \vec{x}_{n+i} - \vec{x}_n ) + \dots
\end{equation*}
As a first order approximation we can take the first two terms of this series, resulting in the following approximate equation \cite{Barnes1965}:
\begin{equation*}
\fxi{n+i} - \fxi{n} \approx \Jxn ( \vec{x}_{n+i} - \vec{x}_n ).
\end{equation*}
This system can be solved for $\Jxn$, though such a system would be underdetermined.
However, if one had as many $\fxi{n+i}$ as there are dimensions in the space then one could solve
\begin{equation} \label{multisecant eq: Jacobian approximation}
\begin{bmatrix} \fxi{n+1} & \dots & \fxi{n+d} \end{bmatrix} - \fxi{n} \vec{1}^\top = \hat{J} \left ( \begin{bmatrix}
\vec{x}_{n+1} & \dots & \vec{x}_{n+d} \end{bmatrix} - \vec{x}_n \vec{1}^\top \right )
\end{equation}
where $\hat{J}$ approximates $\Jxn$.
This system is nonsingular given sufficient conditions on the choice of the $\vec{x}_{n+i}$.

Now that an approximation $\hat{J}$ has been found for the Jacobian, an approximate root may be calculated:
\begin{equation*}
\hat{\vec{x}} = \vec{x}_n - \hat{J}^{-1} \fxi{n}.
\end{equation*}
This system is referred to as the multisecant equations.
They can be further generalized by allowing $\hat{J}$ to be the solution of an underdetermined system, such as by multiplying both sides of equation (\ref{multisecant eq: Jacobian approximation}) by a matrix $B^\top$.

The multisecant equations are but one instance of several equivalent methods.
We begin with a specific form and prove the general form.
Let
\begin{align*}
F_{n,k} = & \begin{bmatrix} \fxi{n} & \dots & \fxi{n+k} \end{bmatrix}, \\
X_{n,k} = & \begin{bmatrix} \vec{x}_n & \dots & \vec{x}_{n+k} \end{bmatrix}, \\
\Delta_n = & \begin{bmatrix} -1 & \dots & -1 \\ 1 \\ & \ddots \\ & & 1 \end{bmatrix}
\end{align*}
then equation (\ref{multisecant eq: Jacobian approximation}) may be rewritten as
\begin{equation*}
\hat{J}^{-1} F_{n,k} \Delta_n = X_{n,k} \Delta_n.
\end{equation*}

We require the vector $\hat{J}^{-1} \fxi{n}$.
If there exists a vector $\tilde{\vec{u}}$ such that $F_{n,k} \Delta_n \tilde{\vec{u}} = \fxi{n}$ then $\hat{J}^{-1} \fxi{n} = X_{n,k} \Delta_n \tilde{\vec{u}}$.
Thus, the multisecant equations can be represented in the following compact form:
\begin{equation} \label{multisecant eq: Newtonian form}
F_{n,k} \Delta_n \tilde{\vec{u}} = \fxi{n}, \quad \hat{\vec{x}} = \vec{x}_n - X_{n,k} \Delta_n \tilde{\vec{u}}.
\end{equation}

If there are fewer function evaluations than the dimension of the space, $k < d$, the equation to solve $\hat{J}$ is underdetermined
and the first system of equation (\ref{multisecant eq: Newtonian form}) is overdetermined.
There are two options to then solve this system:
either to pad out the matrix $F_{n,k}$ with additional vectors,
or add constraints.
We will focus on the latter, but will discuss the former in Section \ref{multisecant sec: rootfinding}.
Adding constraints results in the underdetermined Newtonian form of the multisecant equations:
\begin{equation} \label{multisecant eq: underdetermined Newtonian form}
B^\top F_{n,k} \Delta_n \tilde{\vec{u}} = B^\top \fxi{n}, \quad \hat{\vec{x}} = \vec{x}_n - X_{n,k} \Delta_n \tilde{\vec{u}}.
\end{equation}

One can replace $\Delta_n \tilde{\vec{u}}$ with $\hat{\vec{u}}$, transforming equation (\ref{multisecant eq: underdetermined Newtonian form}) into
\begin{equation} \label{multisecant eq: second form}
\begin{bmatrix} \vec{1}^\top \\ B^\top F_{n,k} \end{bmatrix} \hat{\vec{u}} = \begin{bmatrix} 0 \\ B^\top \fxi{n} \end{bmatrix}, \quad \hat{\vec{x}} = \vec{x}_n - X_{n,k} \hat{\vec{u}}.
\end{equation}
The additional constraint $\vec{1}^\top \hat{\vec{u}} = 0$, that the elements sum to 0, ensures a bijection between $\tilde{\vec{u}}$ and $\hat{\vec{u}}$.

\begin{proposition}
For all $\hat{\vec{u}} \in \bbr^k$ there exists a unique $\tilde{\vec{u}} \in \bbr^{k+1}$ such that $\hat{\vec{u}} = \Delta \tilde{\vec{u}}$ where $\vec{1}^\top \hat{\vec{u}} = 0$, the columns of $\Delta$ sum to zero, and its first $k$ rows form an invertible matrix.
\end{proposition}

\begin{proof}
Using the constraint $\vec{1}^\top \hat{\vec{u}} = 0$ one can write $\hat{\vec{u}}_{k+1} = - \vec{1}^\top R_k \hat{\vec{u}}$, where $R_k$ is the restriction operator that takes the first $k$ elements of a vector of length $k+1$.
Then $\tilde{\vec{u}}$ can be found by solving the system
$
R_k \Delta \tilde{\vec{u}} = R_k \hat{\vec{u}}.
$
The matrix $R_k \Delta$ is square and invertible, meaning $R_k \hat{\vec{u}}$ uniquely determines $\tilde{\vec{u}}$.
Since $R_k \hat{\vec{u}}$ also uniquely determines $\hat{\vec{u}}_{k+1}$, there exists exactly one $\tilde{\vec{u}}$ for any given $\hat{\vec{u}}$.
\end{proof}

Since $\vec{x}_n$ is the first column of $X_{n,k}$ the equation for $\hat{\vec{x}}$ can be written as
$$\hat{\vec{x}} = \vec{x}_n - X_{n,k} \hat{\vec{u}} = X_{n,k} \left ( \vec{e}_1 - \hat{\vec{u}} \right ) = X_{n,k} \vec{u}.$$
We can then find a system for the vector $\vec{u} = \vec{e}_1 - \hat{\vec{u}}$:
\begin{align*}
\begin{bmatrix} \vec{1}^\top \\ B^\top F_{n,k} \end{bmatrix} \vec{u} = & \begin{bmatrix} \vec{1}^\top \\ B^\top F_{n,k} \end{bmatrix} \left ( \vec{e}_1 - \hat{\vec{u}} \right ) \\
 = & \begin{bmatrix} 1 \\ B^\top \fxi{n} \end{bmatrix} - \begin{bmatrix} 0 \\ B^\top \fxi{n} \end{bmatrix} = \begin{bmatrix} 1 \\ \vec{0} \end{bmatrix}.
 \end{align*}
This leaves us with the base form of the multisecant equations \cite{Wolfe1959}:
\begin{equation} \label{multisecant eq: first form}
\begin{bmatrix} \vec{1}^\top \\ B^\top F_{n,k} \end{bmatrix} \vec{u} = \begin{bmatrix} 1 \\ \vec{0} \end{bmatrix}, \quad \hat{\vec{x}} = X_{n,k} \vec{u}.
\end{equation}

From this base form one can transform into several equivalent forms.
One interesting example is to use the transform $\vec{u} = \hat{\vec{u}} - \vec{e}_i$, giving
\begin{equation*}
\begin{bmatrix} \vec{1}^\top \\ B^\top F_{n,k} \end{bmatrix} \hat{\vec{u}} = \begin{bmatrix} 0 \\ B^\top \fxi{n+i} \end{bmatrix}, \quad \hat{\vec{x}} = \vec{x}_{n+i} - X_{n,k} \hat{\vec{u}}.
\end{equation*}
One can then replace $\hat{\vec{u}}$ with $\Delta \tilde{\vec{u}}$ for any $\Delta \in \bbr^{k+1 \times k}$ such that its columns sum to zero:
\begin{equation*}
F_{n,k} \Delta \tilde{\vec{u}} = \fxi{n+i}, \quad \hat{\vec{x}} = \vec{x}_{n+i} - X_{n,k} \Delta \tilde{\vec{u}}.
\end{equation*}
Our original $\Delta_n$ gives a centered finite difference stencil, but one can also use a path-based stencil:
\begin{equation*}
\Delta_s = \begin{bmatrix} -1 \\ 1 & \ddots \\ & \ddots & -1 \\ & & 1 \end{bmatrix}.
\end{equation*}

Since all three forms are equivalent for all choices of $i$ and valid $\Delta$ and the multisecant equations represent a specific choice of $i$ and $\Delta$, all three forms provide the same approximation to the root of $\vec{f}(\vec{x})$ as the multisecant equations.
Wherever the multisecant equations are used one may replace them with any of the forms presented here.

The multisecant equations are an example of a quasi-Newton method.
A quasi-Newton method is any method of the form
\begin{equation} \label{eq:quasiNewton}
\hat{\vec{x}}_{n+1} = \vec{x}_n - \vec{u}_n
\end{equation}
where $\vec{u}_n$ is an approximate solution to the equation
\begin{equation} \label{eq:Newtondirection}
J(\vec{x}_n)  \vec{u} = \vec{f}(\vec{x}_n)
\end{equation}
where $J(\vec{x})$ is the Jacobian of $\vec{f}(\vec{x})$ evaluated at $\vec{x}_n$.
In particular, one can use the multisecant equations in any of their forms to provide such an approximation.

Consider, for example, equation (\ref{multisecant eq: Newtonian form}) with $k=d$.
The solution $\tilde{\vec{u}}$ may be found elementwise by Cramer's rule:
\begin{align*}
\tilde{\vec{u}}_i = & \frac{ \vmat{
\dots & \fxi{n+i-1} - \fxi{n} & \fxi{n} & \fxi{n+i+1}-\fxi{n} & \dots
}
}{ \vmat{
\fxi{n+1}-\fxi{n} & \dots & \fxi{n+d}-\fxi{n}
}
} \\
= & (-1)^i \frac{ \vmat{
\fxi{n} & \dots & \fxi{n+i-1} & \fxi{n+i+1} & \fxi{n+d}
}
}{ \vmat{
1 & \dots & 1 \\ \fxi{n} & \dots & \fxi{n+d}
}}.
\end{align*}
The quasi-Newton method defined above may then be expressed as
\begin{align*}
\hat{\vec{x}}_{n+1} = & \vec{x}_n - \frac{\vmat{
0 & \vec{x}_{n+1} - \vec{x}_n & \dots & \vec{x}_{n+d} - \vec{x}_n \\
\fxi{n} & \fxi{n+1} & \dots & \fxi{n+d} }
}{ \vmat{
1 & \dots & 1 \\ \fxi{n} & \dots & \fxi{n+d} }} \\
= & \frac{ \vmat{
\vec{x}_n & \dots & \vec{x}_{n+d} \\ \fxi{n} & \dots & \fxi{n+d} }
}{ \vmat{
1 & \dots & 1 \\ \fxi{n} & \dots & \fxi{n+d} }}
\end{align*}
where one must expand the determinant along the top row to maintain the correct dimensions.

Suppose that we do not have enough values of $\fxi{n+i}$ to fully determine $\hat{J}$, ie. $k<d$.
We apply the solution found above to the underdetermined Newtonian form of the equations, equation (\ref{multisecant eq: underdetermined Newtonian form}).
The quasi-Newton method that results from this may be written as
\begin{equation} \label{eq:uqn}
\hat{\vec{x}}_{n+1} = \frac{ \vmat{
\vec{x}_n & \dots & \vec{x}_{n+k} \\
\vec{v}_1^\top \fxi{n} & \dots & \vec{v}_1^\top \fxi{n+k} \\
\vdots & & \vdots \\
\vec{v}_k^\top \fxi{n} & \dots & \vec{v}_k^\top \fxi{n+k} }}{ \vmat{
1 & \dots & 1 \\
\vec{v}_1^\top \fxi{n} & \dots & \vec{v}_1^\top \fxi{n+k} \\
\vdots & & \vdots \\
\vec{v}_k^\top \fxi{n} & \dots & \vec{v}_k^\top \fxi{n+k} }}
\end{equation}
where $\vec{v}_i$ is the $i$--th column of $B$.

\section{Connection to root-finding methods}
\label{multisecant sec: rootfinding}

As mentioned in the previous section, as an alternative to adding constraints to equation (\ref{multisecant eq: Jacobian approximation}) one can add vectors to the matrix $F_{n,k}$.
For example, the update for Broyden's method \cite{Broyden1965} is chosen such that
\begin{equation*}
\hat{J}_{n+1} \begin{bmatrix} X_{n,1} \Delta & Q \end{bmatrix} = \begin{bmatrix} F_{n,1} \Delta & \hat{J}_n Q \end{bmatrix}
\end{equation*}
where $Q^\top X_{n,1} \Delta = 0$.
The columns of $Q$ are additional search directions, and the product $\hat{J}_n Q$ an approximation to function evaluations in these search directions.
In the generalized Broyden's method \cite{Vanderbilt1984, Eyert1996} $X_{n,1}$ and $F_{n,1}$ are replaced by $X_{n,k}$ and $F_{n,k}$ and $Q$ reduced in size by $k$ columns.

Broyden's family of methods \cite{Eyert1996, Fang2009} may be written as
\begin{equation*}
\hat{J}_{n+1} = \hat{J}_n + \fxi{n+1} \vec{v}_n^\top
\end{equation*}
where $\vec{v}_n^\top (\vec{x}_{n+1} - \vec{x}_n) = 1$.
If $\vec{v}_n$ is chosen such that
\begin{equation*}
\vec{v}_n^\top X_{n-k,k} \Delta = \begin{bmatrix} 0 & \dots & 0 & 1 \end{bmatrix}
\end{equation*}
with possibly other constraints then $\hat{J}_{n+1} X_{n-k,k} \Delta = F_{n-k,k} \Delta$.

Anderson mixing \cite{Anderson1965, Eyert1996, Walker2011} solves $F_{n,k} \Delta \vec{u} = \fxi{n+k}$ in a least-squares sense then uses the step
\begin{equation*}
\hat{\vec{x}}_{n+k+1} = \vec{x}_{n+k} - X_{n,k} \Delta \vec{u} + \beta \left ( \fxi{n+k} - F_{n,k} \Delta \vec{u} \right ).
\end{equation*}
For $\beta=0$ this is exactly the multisecant equations.
Anderson mixing is then the multisecant equations with relaxation.

\section{Connection to extrapolation methods}

Extrapolation methods seek to accelerate the convergence of sequences.
In general, these sequences are nonlinear and can lie within a vector space.
If one has $k+1$ iterates of the sequence, $\set{\vec{x}_n, \dots, \vec{x}_{n+k}}$, then the next element in the accelerated sequence is
\begin{equation*}
\hat{\vec{x}}_{n+1} = \sum_{i=0}^k \vec{u}_i \vec{x}_{n+i} = X_{n,k} \vec{u}
\end{equation*}
where $\vec{1}^\top \vec{u} = 1$.
We choose $\vec{u}$ such that
\begin{equation*}
\lim_{n \to \infty} \vec{x}_n = \vec{x} = X_{n,k} \vec{u} \iff (X_{n,k} - \vec{x} \vec{1}^\top) \vec{u} = 0.
\end{equation*}
The iterate $\hat{\vec{x}}_{n+1}$ may be expressed as
\begin{equation*}
\hat{\vec{x}}_{n+1} = \frac{ \vmat{
\vec{x}_n & \dots & \vec{x}_{n+k} \\
\vec{v}_1^\top \rxi{n} & \dots & \vec{v}_1^\top \rxi{n+k} \\
\vdots & & \vdots \\
\vec{v}_k^\top \rxi{n} & \dots & \vec{v}_k^\top \rxi{n+k} }}{ \vmat{
1 & \dots & 1 \\
\vec{v}_1^\top \rxi{n} & \dots & \vec{v}_1^\top \rxi{n+k} \\
\vdots & & \vdots \\
\vec{v}_k^\top \rxi{n} & \dots & \vec{v}_k^\top \rxi{n+k} }}
\end{equation*}
where $\rxi{n+i} = \vec{x}_{n+i+1} - \vec{x}_{n+i}$ and $\set{\vec{v}_i}$ is a linearly independent set of vectors.
Note this is exactly equation (\ref{eq:uqn}) replacing $\fxi{}$ with $\rxi{}$.
Thus, extrapolation methods of this form are identical to the multisecant equations acting on $\fxi{}=\rxi{}$ using $\set{\vec{x}_{n+i}}$ as search directions.
They can therefore be expressed in the form of equation (\ref{multisecant eq: first form}).

Methods of this form are called polynomial extrapolation algorithms \cite{Sidi1986, Jbilou2000}.
Several extrapolation methods fall in this category:
\begin{itemize}
\item minimum polynomial extrapolation (MPE) \cite{Cabay1976}, with $\vec{v}_i = \rxi{n+i-1}$;
\item modified minimum polynomial extrapolation (MMPE) \cite{Sidi1986}, with $\vec{v}_i$ some fixed vector;
\item reduced rank extrapolation (RRE) \cite{Eddy1979,Mesina1977,Smith1987}, with $\vec{v}_i = \rxi{n+i} - \rxi{n+i-1}$.
\end{itemize}

There is another category of methods known as $\epsilon$-algorithms \cite{Wynn1956, Gekeler1972, Brezinski1975, Jbilou2000}.
They may be connected to the multisecant equations as well, though not in the same manner as the others.
Recall equation (\ref{multisecant eq: Jacobian approximation}).
One can take several such equations and arrive at several approximations of the Jacobian:
\begin{equation*}
F_{n+j,k} \Delta_n = \hat{J}_{n+j} X_{n+j,k} \Delta_n \quad \forall \ j.
\end{equation*}
As before, we seek to solve
\begin{equation*}
F_{n+j,k} \Delta \tilde{\vec{u}}_j = \fxi{n+j}, \quad \hat{\vec{x}}_{n+1} = \vec{x}_{n+j} - X_{n+j,k} \Delta \tilde{\vec{u}}_j \quad \forall \ j.
\end{equation*}
This gives several estimates of $\hat{\vec{x}}_{n+1}$.
Each of these systems can be reduced to a single equation by taking the inner product with a given vector $\vec{v}$.
There are then as many equations as there are values of $j$.
If one assumes all $\tilde{\vec{u}}_j$ are equal then one can summarize these equations in the following system:
\begin{equation*}
\begin{bmatrix} \vec{v}^\top F_{n,k} \\ \vdots \\ \vec{v}^\top F_{n+k-1,k} \end{bmatrix} \Delta \tilde{\vec{u}} = \begin{bmatrix} \vec{v}^\top \fxi{n} \\ \vdots \\ \vec{v}^\top \fxi{n+k-1} \end{bmatrix}, \quad \hat{\vec{x}}_{n+1} = \vec{x}_n - X_{n,k} \Delta \tilde{\vec{u}}.
\end{equation*}
Following the same work as in Section \ref{multisecant sec:intro} one arrives at the solution
\begin{equation*}
\vec{\hat{x}}_{n+1} = \frac{ \vmat{
\vec{x}_n & \dots & \vec{x}_{n+k} \\
\vec{v}^\top \fxi{n} & \dots & \vec{v}^\top \fxi{n+k} \\
\vdots & & \vdots \\
\vec{v}^\top \fxi{n+k-1} & \dots & \vec{v}^\top \fxi{n+2k-1}
}}{ \vmat{
1 & \dots & 1 \\
\vec{v}^\top \fxi{n} & \dots & \vec{v}^\top \fxi{n+k} \\
\vdots & & \vdots \\
\vec{v}^\top \fxi{n+k-1} & \dots & \vec{v}^\top \fxi{n+2k-1}}} .
\end{equation*}
Replacing $\fxi{}$ with $\rxi{}$ gives the topological $\epsilon$-algorithm (TEA) \cite{Brezinski1977}.

\section{Connection to Krylov methods}

Suppose the extrapolation methods of the previous section are applied to the linear vector sequence $\vec{x}_{n+1} = (A + I)\vec{x}_n - \vec{b}$.
The limit $\vec{x}$ of this sequence is then the solution to $A \vec{x} = \vec{b}$.
The functions $\rxi{n+i}$ are then
\begin{equation*}
\rxi{n+i} = (A+I) \vec{x}_{n+i} - \vec{b} - \vec{x}_{n+i} = A \vec{x}_{n+i} - \vec{b}
\end{equation*}
and they satisfy
\begin{equation*}
\rxi{n+i} = (A+I) \vec{x}_{n+i} - \vec{b} - (A+I) \vec{x}_{n+i-1} + \vec{b} = (A+I) \rxi{n+i-1}.
\end{equation*}
The vectors $\rxi{n+i}$ then form a Krylov subspace, such that $\rxi{n+i} \in \mathcal{K}_{i} (A+I, \rxi{n})$.

Under these conditions the extrapolation methods become Krylov subspace methods.
Most notably, since MPE uses the Arnoldi iteration to produce orthogonal search directions, it is identical to GMRES when applied to this linear sequence \cite{Walker2011, Sidi1988, Jbilou2000}.
These algorithms are presented as Algorithms \ref{multisecant alg: GMRES} and \ref{multisecant alg: MPE}.
The connections to the linear case are noted in the latter.
Since $\mathcal{K}_k(A+I,\vec{v}) = \mathcal{K}_k(A,\vec{v})$ the search directions for both algorithms are identical.
\begin{algorithm}
\begin{algorithmic}
	\State
	\State $\vec{q}_1 = \vec{b} / \norm{\vec{b}}$
	\For{$k=1$ to $n$}
		\State $\vec{y} = A \vec{q}_k$ ($\in \mathcal{K}_k(A,\vec{b})$)
		\State orthogonalize $\vec{y}$ with respect to $\mathcal{K}_{k-1}(A,\vec{b})$
		\State $\vec{q}_{k+1} = \vec{y} / \norm{\vec{y}}$
	\EndFor
	\State minimize $\norm{H_n \vec{u} - \norm{\vec{b}} \vec{e}_1}$
	\State $\hat{\vec{x}}_{n+1} = Q_n \vec{u} + \vec{x}_0$
\end{algorithmic}
\caption{GMRES} \label{multisecant alg: GMRES}
\end{algorithm}

\begin{algorithm}
\begin{algorithmic}
	\State
	\State $\vec{q}_1 = \rxi{n+1} / \norm{\rxi{n+1}}$ ($\vec{b} / \norm{\vec{b}}$ in linear case)
	\For{$k=1$ to $n$}
		\State $\vec{y} = \rxi{n+k}$ ($\in \mathcal{K}_k(A+I,\vec{b})$ in linear case)
		\State orthogonalize $\vec{y}$ with respect to $\set{\rxi{n}, \dots, \rxi{n+k-1}}$ ($\mathcal{K}_{k-1}(A+I,\vec{b})$ in linear case)
		\State $\vec{q}_{k+1} = \vec{y} / \norm{\vec{y}}$
	\EndFor
	\State minimize $\norm{H_n \vec{u}}$ such that $\vec{1}^\top \vec{u} = 1$
	\State $\hat{\vec{x}}_{n+1} = \begin{bmatrix} \vec{x}_0 & \dots & \vec{x}_n \end{bmatrix} \vec{u}$
\end{algorithmic}
\caption{MPE} \label{multisecant alg: MPE}
\end{algorithm}
The minimization steps of the two algorithms can be shown to be equivalent as well.
Consider the solution found using GMRES:
$\hat{\vec{x}} =Q_k \vec{y}_k$ where $Q_k$ is derived from the Arnoldi iteration on $\mathcal{K}_{k-1}(A+I, \vec{b})$.
Then we seek
\begin{align*}
\min \norm{A \hat{\vec{x}} - \vec{b}}
	= & \min \norm{A Q_k \vec{y}_k - \vec{b}} \\
	= & \min \norm{F_{n,k} \Delta \tilde{\vec{u}}_k - \vec{b}}
\end{align*}
since the column space of $F_{n,k} \Delta$ is equal to the Krylov subspace $\mathcal{K}_{k-1}(A,\vec{b})$.
Recall in the linear case that $\fxi{n} = \vec{b}$, and so this minimization is equivalent to solving equation (\ref{multisecant eq: underdetermined Newtonian form}) with $B=F_{n,k}$.
We've shown in Section \ref{multisecant sec:intro} that this is equivalent to minimizing
$
\norm{F_{n,k} \vec{u}_k}
$
under the constraint $\vec{1}^\top \vec{u}_k = 1$.
This is exactly the minimization step in MPE.

For the linear case of TEA, the term $\vec{v}^\top \fxi{n+i}$ may now be written as $\vec{v}^\top (A+I)^{i-j} \fxi{n+j}$.
This means TEA can now be expressed in the form of equation (\ref{multisecant eq: first form}) with $\vec{v}_i = (A^\top+I)^i \vec{v}$.
TEA then uses the Lanczos biorthogonalization process, making it equivalent in the linear case to biorthogonal conjugate gradient (BiCG).

%nb: needs fixing or removing
Table \ref{tab:KrylovExtrap} gives the orthogonalization conditions and corresponding Krylov subspace methods for these four extrapolation methods when they are applied to linear vector sequences.
\begin{table}
	\centering
	\begin{tabular}{c | c | c}
		Extrapolation & $\vec{q}_{k+1} \bot$ & Krylov \\ \hline
		MPE & $\mathcal{K}_k (A+I,\fxi{n})$ & GMRES \\
		RRE & $\mathcal{K}_k (A,\fxi{n})$ & GMRES \\
		MMPE & $\mathcal{K}_k (G,\vec{q}_0)$ & n/a \\
		TEA & $\mathcal{K}_k (A^\top, \vec{q})$ & BiCG
	\end{tabular}
	\caption{Connections between extrapolation methods and Krylov methods.
	The extrapolation methods are applied to the sequence $\vec{x}_{n+1} = (A+I) \vec{x}_n - \vec{b}$.}
	\label{tab:KrylovExtrap}
\end{table}

\begin{figure}
  \centering
  \begin{tikzpicture}
    \matrix (m) [column sep=0.8cm, row sep=1em]{
      & & & & \node[align=center] (Anderson) {Anderson\\mixing}; \\
      \node[align=center] (multisecant) {$\begin{bmatrix} \vec{1}^\top \\ F_{n,k} \end{bmatrix} \vec{u} = \begin{bmatrix} 1 \\ 0 \end{bmatrix}$,\\ $X_{n,k} \vec{u} = \hat{\vec{x}}$}; &
      \node (overdetermined) {$\begin{bmatrix} \vec{1}^\top \\ B^\top F_{n,k} \end{bmatrix} \vec{u} = \begin{bmatrix} 1 \\ 0 \end{bmatrix}$}; & &
      \node[coordinate] (preMPE) {}; & \node (MPE) {MPE}; \\
      & & & & \node (GMRES) {GMRES}; \\
      \node[align=center] (preBroyden) {$\begin{bmatrix} \vec{1}^\top & \vec{1}^\top \\ F_{n,k} & B \end{bmatrix} \vec{u} = \begin{bmatrix} 1 \\ 0 \end{bmatrix}$,\\ $\begin{bmatrix} X_{n,k} & C \end{bmatrix} \vec{u} = \hat{\vec{x}}$};
      & \node[coordinate] (prepreRRE) {}; & & \node[coordinate] (preRRE) {}; & \node (RRE) {RRE}; \\
%      & & & & \node (GCR) {}; \\
      & \node[coordinate] (prepreMMPE) {}; & & \node[coordinate] (preMMPE) {}; & \node (MMPE) {MMPE}; \\
      \\
      & \node[coordinate] (preTEA) {}; & & \node[coordinate] (preBiCG) {}; & \node (BiCG) {BiCG}; \\
      \node (Broyden) {Generalized Broyden};
      & & & & \node (TEA) {TEA*}; \\
      };
    \path[->,very thick]
      (multisecant) edge node[above] {$k < d$} (overdetermined) edge node[right] {$k < d$} (preBroyden)
      (preBroyden) edge node[right,align=left] {$(C \Delta)^\top (X_{n,k} \Delta) = 0$, \\ $B = \hat{J}_{n-1,k} C$} (Broyden)
      (overdetermined) edge node[above] {$B=F_{n,k-1}$} (preMPE)
      (preMPE) edge[red] (MPE)
      (prepreRRE) edge node[above] {$B=F_{n,k} \Delta$} (preRRE)
      (preRRE) edge[red] (RRE)
      (prepreMMPE) edge node[above] {$B=\begin{bmatrix} \vec{q}_1 & \dots & \vec{q}_k \end{bmatrix}$} (preMMPE)
      (preMMPE) edge[red] (MMPE)
      (preTEA) edge node[above] {$B=\begin{bmatrix} \vec{q} & A^\top \vec{q} & \dots \end{bmatrix}$} (preBiCG)
      (preBiCG) edge[blue] (BiCG)
      (BiCG) edge[red] (TEA);
    \draw[-, very thick] (overdetermined) edge (preTEA);
    \draw[->,very thick,blue] (preMPE) |- (GMRES);
    \draw[->,very thick,blue] (preRRE) |- (GMRES);
    \draw[->,very thick] (preMPE) |- node[above] {+relaxation} (Anderson);
  \end{tikzpicture}
  \caption{Interconnectivity of extrapolation, acceleration and quasi-Newton methods.
  Red arrows indicate $\fxi{n} = \vec{x}_{n+1}-\vec{x}_n$ while blue arrows indicate $\fxi{n} = A \vec{x}_n - \vec{b}$ and $\fxi{n+1} = (A+I) \fxi{n}$.
  Note that TEA* is the linear version of the method; general TEA derives directly from the multisecant equations.}
\end{figure}

\section{Conclusions}

The multisecant equations form the basis of numerical methods for root-finding in higher dimensions.
They themselves are extensions of the secant method in 1D.

When underdetermined they can be connected to extrapolation methods.
Which particular extrapolation method depends on the additional constraints imposed on the underdetermined systems.

If these methods are applied to linear problems with vector sequences that lie within Krylov subspaces, then they become Krylov subspace methods.
This is due to the orthogonalization steps in these methods.

The three methods are then intrinsically linked.
As a direct consequence, extrapolation methods can be thought of as nonlinear Krylov subspace methods, as they use the same orthogonalization processes but applied to nonlinear problems.
Moreover, this indicates the advanced techniques employed in Krylov subspace methods can be used to improve extrapolation methods.
It also suggests that any Krylov subspace method has an extrapolation method counterpart, and vice versa.

\bibliographystyle{plain}
\bibliography{export_extrap}

\end{document}