\documentclass{beamer}

\usetheme{Berlin}
\usecolortheme{dolphin}
\setbeameroption{hide notes}

%===Packages===%

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{subcaption}

%===Math Operators===%

\newcommand{\dxdy}[2]{\frac{d #1}{d #2}}
\newcommand{\dxdyk}[3]{\frac{d^{#3} #1}{d {#2}^{#3}}}
\newcommand{\pdxdy}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\liminfty}[1]{\lim_{#1 \to \infty}}
\newcommand{\limab}[2]{\lim_{#1 \to #2}}

\newcommand{\abs}[1]{\left \vert #1 \right \vert}
\newcommand{\norm}[1]{\left \Vert #1 \right \Vert}
\newcommand{\order}[1]{\mathcal{O} \left ( #1 \right )}
\newcommand{\set}[1]{\left \{ #1 \right \}}
\newcommand{\Set}[2]{\left \{ #1 \ \middle \vert \ #2 \right \}}
\newcommand{\vmat}[1]{\begin{vmatrix} #1 \end{vmatrix}}
\DeclareMathOperator{\sign}{sign}

%===Important Sets===%

\newcommand{\bbn}{\mathbb{N}}
\newcommand{\bbz}{\mathbb{Z}}
\newcommand{\bbq}{\mathbb{Q}}
\newcommand{\bbr}{\mathbb{R}}
\newcommand{\bbc}{\mathbb{C}}
\newcommand{\bbf}{\mathbb{F}}

%===Types of Statements===%

\newtheorem{lem}{Lemma}
\newtheorem{thm}{Theorem}
\newtheorem{cor}{Corollary}
\newtheorem{prop}{Proposition}
\newtheorem{conj}{Conjecture}
\newtheorem{defn}{Definition}

\usepackage{setspace}
\usepackage{animate}
\usepackage{tikz}
\usetikzlibrary{calc,intersections,positioning,tikzmark,fit,3d}
\newcommand{\tetra}[2]{
    \filldraw[red] \foreach \x in {#1} {(\x) circle (1.5pt)};
    \filldraw[blue,fill opacity=0.2] (S1) -- (S2) -- (S3) -- (S4) -- cycle;
    \filldraw[red] \foreach \x in {#2} {(\x) circle (1.5pt)};
    }
\newcommand{\blueplane}[4]{
    \filldraw[blue,fill opacity=0.4] #1 -- #2 -- #3 -- #4 -- cycle;
    }
\newcommand{\redplanes}[4]{
    \filldraw[red,fill opacity=0.2] #1 -- #2 -- #3 -- cycle;
    \filldraw[red,fill opacity=0.2] #1 -- #2 -- #4 -- cycle;
    \filldraw[red,fill opacity=0.2] #1 -- #3 -- #4 -- cycle;
    }
\newcommand{\tikzint}[4]{{(#1*#4-#3*#2)/(#4-#2)}}
\newcommand{\Tetra}[9]{
	\coordinate (X1) at (#1,#2,#3); \coordinate (X2) at (#4,#5,#6); \coordinate (X3) at (-2,-2,-2); \coordinate (X4) at (#7,#8,#9);
		\coordinate (z1) at (\tikzint{#4}{#6}{#1}{#3},\tikzint{#5}{#6}{#2}{#3},0);
		\coordinate (z2) at (\tikzint{#4}{#6}{-2}{-2},\tikzint{#5}{#6}{-2}{-2},0);
		\coordinate (z3) at (\tikzint{#4}{#6}{#7}{#9},\tikzint{#5}{#6}{#8}{#9},0);
		\coordinate (z4) at (\tikzint{#7}{#9}{-2}{-2},\tikzint{#8}{#9}{-2}{-2},0);
		\coordinate (z5) at (\tikzint{#1}{#3}{-2}{-2},\tikzint{#2}{#3}{-2}{-2},0);
		\coordinate (z6) at (\tikzint{#7}{#9}{#1}{#3},\tikzint{#8}{#9}{#2}{#3},0);
		
		\coordinate (x1) at (0,\tikzint{#2}{#1}{#5}{#4},\tikzint{#3}{#1}{#6}{#4});
		\coordinate (x2) at (0,\tikzint{#2}{#1}{-2}{-2},\tikzint{#3}{#1}{-2}{-2});
		\coordinate (x3) at (0,\tikzint{#2}{#1}{#8}{#7},\tikzint{#3}{#1}{#9}{#7});
		\coordinate (x4) at (0,\tikzint{#8}{#7}{-2}{-2},\tikzint{#9}{#7}{-2}{-2});
		\coordinate (x5) at (0,\tikzint{-2}{-2}{#5}{#4},\tikzint{-2}{-2}{#6}{#4});
		\coordinate (x6) at (0,\tikzint{#8}{#7}{#5}{#4},\tikzint{#9}{#7}{#6}{#4});
	
	\draw[black] (0,-2,0) -- (0,2,0);
	\filldraw[blue,fill opacity=0.2] (2,-2,0) -- (2,2,0) -- (0,2,0) -- (0,2,-2) -- (0,-2,-2) -- (0,-2,0) -- cycle;
	\filldraw[red] \foreach \x in {(X1),(X2),(X3),(X4)} {\x circle (1.5pt)};
	\filldraw[red,fill opacity=0.2] (X1) -- (X2) -- (X3) -- cycle;
	\filldraw[red,fill opacity=0.2] (X1) -- (X2) -- (X4) -- cycle;
	\filldraw[red,fill opacity=0.2] (X4) -- (X2) -- (X3) -- cycle;
	\filldraw[red,fill opacity=0.2] (X1) -- (X4) -- (X3) -- cycle;
	\filldraw[blue,fill opacity=0.2] (-2,-2,0) -- (-2,2,0) -- (0,2,0) -- (0,2,2) -- (0,-2,2) -- (0,-2,0) -- cycle;
}

\title{Robust algorithms for the intersection of simplices}
\author{Conor McCoid, Martin J. Gander}
\institute{Universit\'e de Gen\`eve}
\date{13.09.2021}

\begin{document}

\frame{\titlepage}

%---Motivation---%

\begin{frame}
\note{
Clipping algorithms are ubiquitous and numerous, so it's no surprise when some of them fail.
Here we see two examples, one in 2D and one in 3D.
Clearly these triangles overlap, but when fed to a particular intersection algorithm no intersection is found.
These two icosahedra have several overlapping tetrahedra within them, but only some are found to intersect.
While this kind of failure might be acceptable for some applications, other times it is catastrophic.}

\frametitle{Failures of clipping algorithms}
  \begin{figure}
    \includegraphics[width=0.49\textwidth]{Jeronimo_fail.png}
    \includegraphics[width=0.49\textwidth]{Hecht3D_fail.eps}
  \end{figure}
\end{frame}

\begin{frame}
\frametitle{Definitions}
\note{
How do we avoid these problems? By building robust algorithms.
A robust algorithm is one that does not change the result significantly when error is introduced.

How do we build robust algorithms? By building them with parsimony.
This tells us we must use every aspect of a calculation rather than find the same information again, even if extracting that information is more expensive than recalculating it.
What we want is to never have two pieces of contradictory data.}

\textbf{Robust:} resistant to failure.

~

\textbf{Parsimony:} the principle of using the fewest resources to solve a problem.
\end{frame}

%---Reference simplices---%

\begin{frame}
\note{
Before we dig in, let's introduce the objects we'll be working with.
We are concerned with intersecting two n-dimensional simplices,
 and we're going to assume that one of them is the reference simplex with edges aligning with the coordinate axes.
For example, the reference triangle has vertices at the origin, (1,0) and (0,1).
The reference simplex will always be refered to as Y, whereas the non-reference simplex will be X.

If neither of the simplices to be intersected meet this criteria we need to perform a change of coordinates.
Suffice it to say that, as long as the simplices are not dramatically different in size and shape, this can be done robustly.}

\frametitle{Reference simplices}
\begin{figure}
  \begin{tikzpicture}[scale=2]
      \draw[->,thick] (-0.5,0) -- (1.5,0) node[above] {$x$}; \draw[->,thick] (0,-0.5) -- (0,1.5) node[right] {$y$};
      \filldraw[blue,opacity=0.5] (1,0) -- (0,1) -- (0,0) -- cycle;
  \end{tikzpicture}
  \begin{tikzpicture}[scale=2,rotate around y=0]
      \draw[->,thick] (-0.5,0,0) -- (1.5,0,0) node[below] {$x$}; \draw[->,thick] (0,-0.5,0) -- (0,1.5,0) node[right] {$z$}; \draw[->,thick] (0,0,-0.5) -- (0,0,1.5) node[right] {$y$};
      \filldraw[blue,opacity=0.5] (1,0,0) -- (0,1,0) -- (0,0,1);
  \end{tikzpicture}
\end{figure}
\end{frame}

%---Triangles---%

\begin{frame}
\note{
Let's now look at the case of two intersecting triangles.
There are 3 types of points that make up the intersection:
- vertices of X inside Y
- vertices of Y inside X and
- intersections between the edges of the triangles}

\frametitle{Triangle intersections}

\begin{columns}
\begin{column}{0.6\textwidth}
\textbf{3 types of points in intersection:}
\begin{itemize}
\item \textcolor{red}{vertices of X in Y}
\item vertices of Y in X
\item \textcolor{blue}{intersections between edges}
\end{itemize}
\end{column}
\begin{column}{0.4\textwidth}
\begin{figure}
  \centering
  \begin{tikzpicture}
            \coordinate (x1) at (0.5,1.5);
            \coordinate (x2) at (0.5,3.5);
            \coordinate (x3) at (-1,3);
            \coordinate (y1) at (0,0);
            \coordinate (y2) at (3,0);
            \coordinate (y3) at (0,3);
            \filldraw[blue,opacity=0.5] (y1) -- (y2) -- (y3) -- cycle;
            \filldraw[red,opacity=0.5] (x1) -- (x2) -- (x3) -- cycle;
            \filldraw[blue] (intersection of x1--x2 and y2--y3) circle (3pt);
            \filldraw[blue] (intersection of x1--x3 and y1--y3) circle (3pt);
            \filldraw[red] (x1) circle (3pt);
            \filldraw[black] (y3) circle (3pt);
  \end{tikzpicture}
\end{figure}
\end{column}
\end{columns}
\end{frame}

\begin{frame}
\frametitle{X in Y}
\note{
Pick an edge of Y; this edge divides the plane in two, one side containing Y the reference triangle and one side without Y.
We can reparametrize the coordinates of the vertices of X to be in line with this edge:
  $p_i$ is the distance the i-th vertex of X is away from the edge, with some other coordinate $q_i$.
If $p_i$ is positive then the vertex of X lies on the same side of the edge as Y.
If we repeat this for each of the edges of Y and $p_i$ is positive for all of the edges, then the vertex of X must lie inside Y.

If some $p_i$ are positive and some negative then there are intersections between this edge of Y and those of X.
Therefore, by discovering which vertices of X lie inside Y we have determined which edges of X intersect which edges of Y.}

\begin{figure}
  \begin{tikzpicture}[scale=2]
    \filldraw[blue,opacity=0.25] (0,0) -- (0,1.5) -- (1.5,1.5) -- (1.5,0) -- cycle;
    \draw[->,thick] (-0.5,0) -- (1.5,0) node[below] {$q$}; \draw[->,thick] (0,-0.5) -- (0,1.5) node[left] {$p$};
    \coordinate (x1) at (0.5,1); \coordinate (x2) at (1,0.5); \coordinate (x3) at (0.75,-0.5); \coordinate (y1) at (0,0); \coordinate (y2) at (1,0);
    \filldraw[red] \foreach \x in {1,2,3} {(x\x) circle (1pt)}; \draw[red,thick] (x1)--(x2)--(x3)--cycle; \node[above] at (x1) {+ve}; \node[above] at (x2) {+ve}; \node[below] at (x3) {-ve};
    \draw[blue] (intersection of x1--x3 and y1--y2) circle (2pt); \draw[blue] (intersection of x2--x3 and y1--y2) circle (2pt);
  \end{tikzpicture}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Edge intersections}
\note{
The location along an edge of Y of an intersection can be written as a quotient.
There are two vertices of Y along this edge.
We need to know the intersection's distance from both vertices.

Each of the numerators in this table is repeated twice.
This pairing is over an edge of X.
This means that the position of an edge of X with respect to a vertex of Y is determined by the sign of this numerator.
As long as we only calculate it once the result is consistent.
Edge intersection calculations then determine which vertices of Y lie inside X.}

\begin{table}
  \begin{tabular}{c|c|c}
    Edge/Line & Distance from 1st vertex & Distance from 2nd vertex \\ \hline
    $y=0$ & $\frac{x_i y_j - x_j y_i}{y_j - y_i}$ & $\frac{(1-x_i)y_j - (1-x_j)y_i}{y_j-y_i}$ \\
    $x=0$ & $\frac{x_i y_j - x_j y_i}{x_i - x_j}$ & $\frac{(1-y_i)x_j - (1-y_j)x_i}{x_j-x_i}$ \\
    $x+y=1$ & $\frac{(1-x_i)y_j - (1-x_j)y_i}{x_i + y_i-x_j -y_j}$ & $\frac{(1-y_i)x_j - (1-y_j)x_i}{x_j + y_j-x_i -y_i}$
  \end{tabular}
\end{table}
\end{frame}

\begin{frame}
\note{
We've calculated the numerator of both of these intersections as being equal, meaning they have the same sign.
Depending on their respective denominators the intersections will end up positive or negative, but importantly they'll be consistent with each other.
If the sign of one should flip then so will the other.
The edge will end up on the other side of the vertex.}

\frametitle{Y in X}
\begin{figure}
  \begin{tikzpicture}[scale=2]
    \filldraw[blue,opacity=0.25] (0,0) -- (0,1.5) -- (1.5,1.5) -- (1.5,0) -- cycle;
    \draw[->,thick] (-0.5,0) -- (1.5,0); \draw[->,thick] (0,-0.5) -- (0,1.5);
    \coordinate (x1) at (-0.5,1); \coordinate (x2) at (1,-0.5); \coordinate (y1) at (0,0); \coordinate (y2) at (0,1); \coordinate (y3) at (1,0);
    \filldraw[red] \foreach \x in {1,2} {(x\x) circle (1.5pt)}; \filldraw[black] (y1) circle (1.5pt); \draw[red] (x1)--(x2);
    \draw[blue] (intersection of x1--x2 and y1--y2) circle (2pt); \draw[blue] (intersection of x1--x2 and y1--y3) circle (2pt);
  \end{tikzpicture}
\end{figure}
\end{frame}

%---Tetrahedra---%

\begin{frame}
\frametitle{Tetrahedral intersections}
\note{
We can now move on to the tetrahedral case.
We now have four types of points in the intersection:
  the vertices of the tetrahedra inside each other and
  the intersection between the edges of one and the faces of the other.}

\textbf{4 types of points in intersection:}
\begin{itemize}
\item vertices of X in Y
\item edges of X intersecting faces of Y
\item edges of Y intersecting faces of X
\item vertices of Y in X
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{X in Y}
\note{
The vertices that are in the intersection are found in nearly identical fashion as the 2D case.
However, there is now a plane that separates the vertices of X, rather than just a line.
This blue plane contains one of the faces of Y.
There are three vertices of X on one side and one on the other.
Using the same logic as in the 2D case, this gives us three intersections in this plane, creating a triangle that may or may not intersect the face of Y.}

\begin{figure}
  \centering
  \begin{tikzpicture}
        \coordinate (A1) at (-1,-1);
        \coordinate (B1) at (0,-0.75);
        \coordinate (C1) at (-0.5,1);
        \coordinate (D2) at (2,-1);
        \coordinate (S1) at (0,-2);
        \coordinate (S2) at (0,2);
        \coordinate (S3) at (2,1);
        \coordinate (S4) at (2,-1);
        \coordinate (int1) at ($.5*(A1)+.5*(D2)$);
        \coordinate (int2) at ($.5*(B1)+.5*(D2)$);
        \coordinate (int3) at ($.5*(C1)+.5*(D2)$);
                \tetra{A1,B1,C1}{D2} \redplanes{(A1)}{(B1)}{(C1)}{(D2)} \blueplane{(int1)}{(int2)}{(int3)}{(int3)};
  \end{tikzpicture}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{X with Y}
\note{
The two types of intersections we now calculate are treated very differently.
For the first type, edges of X intersecting with faces of Y, we need to know their distance from the three lines that make up the face of Y.
This leads to three quotients for each intersection.
For a given edge of X this produces 12 possible quotients.
This table presents the numerators of these 12 quotients and, as in the 2D case, they appear in pairs.}

\begin{table}
	\centering
	\scalebox{0.9}{
	\begin{tabular}{c||c|c|c}
	Plane $P$ & Num. of $q_\gamma^{ij}$ & Num. of $r_\gamma^{ij}$ & Num. of $1-q_\gamma^{ij}-r_\gamma^{ij}$ \\ \hline \hline
	$x=0$ & \tikzmark{start1}$\begin{vmatrix} x_i & y_i \\ x_j & y_j \end{vmatrix}$\tikzmark{end1} & $\begin{vmatrix} x_i & z_i \\ x_j & z_j \end{vmatrix}$ & $\begin{vmatrix} x_i & 1-y_i-z_i \\ x_j & 1- y_j-z_j \end{vmatrix}$ \\ \hline
	$y=0$ & $\begin{vmatrix} y_i & z_i \\ y_j & z_j \end{vmatrix}$ & \tikzmark{start2}$\begin{vmatrix} y_i & x_i \\ y_j & x_j \end{vmatrix}$\tikzmark{end2} & $\begin{vmatrix} y_i & 1-x_i-z_i \\ y_j & 1-x_j-z_j \end{vmatrix}$ \\ \hline
	$z=0$ & $\begin{vmatrix} z_i & x_i \\ z_j & x_j \end{vmatrix}$ & $\begin{vmatrix} z_i & y_i \\ z_j & y_j \end{vmatrix}$ & $\begin{vmatrix} z_i & 1-x_i-y_i \\ z_j & 1-x_j-y_j \end{vmatrix}$ \\ \hline
	$x+y+z=1$ & $\begin{vmatrix} 1-y_i-z_i & x_i \\ 1-y_j-z_j & x_j \end{vmatrix}$ & $\begin{vmatrix} 1-x_i-z_i & y_i \\ 1-x_j-z_j & y_j \end{vmatrix}$ & $\begin{vmatrix} 1-x_i-y_i & z_i \\ 1-x_j-y_j & z_j \end{vmatrix}$
	\end{tabular}
\begin{tikzpicture}[remember picture, overlay]
  \foreach \x in {1,2} {
  \node[draw,line width=2pt,red,rectangle,inner ysep=15pt,fit={(pic cs:start\x) (pic cs:end\x)}] {};
  };
\end{tikzpicture}
	}
\end{table}
\end{frame}

\begin{frame}
\frametitle{Y with X}
\note{
The other type of intersections now only needs two quotients, telling the distance from the two vertices of Y that lie along the edge that intersects a given face of X.
These numerators appear in triples.}

\newcommand{\detijk}{\begin{vmatrix} x_i & y_i & z_i \\ x_j & y_j & z_j \\ x_k & y_k & z_k \end{vmatrix}}
\begin{table}
  \centering
  \scalebox{0.8}{
  \begin{tabular}{c||c|c|c|c}
  Edge of $Y$ & $y,z$ & $x,z$ & $x,y$ & \dots \\ \hline \hline
  Num. of $t^{ijk}$ & \tikzmark{start3}$\detijk$ & $\detijk$ & $\detijk$\tikzmark{end3} \\ \hline
  Num. of $1-t^{ijk}$ & $\begin{vmatrix} 1-x_i & y_i & z_i \\ 1-x_j & y_j & z_j \\ 1-x_k & y_k & z_k \end{vmatrix}$ & $\begin{vmatrix} x_i & 1-y_i & z_i \\ x_j & 1-y_j & z_j \\ x_k & 1-y_k & z_k \end{vmatrix}$ & $\begin{vmatrix} x_i & y_i & 1-z_i \\ x_j & y_j & 1-z_j \\ x_k & y_k & 1-z_k \end{vmatrix}$
  \end{tabular}
  \begin{tikzpicture}[remember picture, overlay]
  \node[draw,line width=2pt,red,rectangle,inner ysep=23pt,fit={(pic cs:start3) (pic cs:end3)}] {};
  \end{tikzpicture}
  }
\end{table}
\end{frame}

\begin{frame}
\frametitle{Pairings}
\note{
So what does this mean?
In the 2D case, pairing the intersections together meant the edge of X moved as a unit.
The same is true in the 3D case.
The sign of the numerator informs us on which side of the edge of Y the edge of X falls.}

\begin{figure}
  \centering
  \begin{tikzpicture}[rotate around y=60]
    \matrix[row sep=1em, column sep=1em, every cell/.style={scale=0.75},ampersand replacement=\ampersand]{
			\Tetra{-1}{0}{0.5}{0.5}{0}{-1}{1.5}{-2}{2.5}
			\draw[blue] (x1) -- (x3) -- (x4) -- (x5) -- cycle; \draw[blue] (z1) -- (z3) -- (z4) -- (z5) -- cycle;
			\filldraw[blue] (x1) circle (1.5pt); \filldraw[blue] (z1) circle (1.5pt);
			\filldraw[black] (intersection of z1--z3 and x1--x3) circle (1.5pt);
			\draw[black] (intersection of z3--z4 and x4--x5) circle (2pt);
			\ampersand
			\draw[<->,rotate around y=-60, very thick] (0,0,0) -- (1,0,0);
			\ampersand
			\Tetra{1}{0}{-0.5}{-0.5}{0}{1}{1.5}{-2}{2.5}
			\draw[blue] (x1) -- (x2) -- (x4) -- (x6) -- cycle; \draw[blue] (z1) -- (z2) -- (z4) -- (z6) -- cycle;
			\filldraw[blue] (x1) circle (1.5pt); \filldraw[blue] (z1) circle (1.5pt);
			\filldraw[black] (intersection of z1--z2 and x1--x2) circle (1.5pt);
			\draw[black] (intersection of z6--z4 and x4--x2) circle (2pt);
			\\};
  \end{tikzpicture}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Triplings}
\note{
The tripling of the Y with X intersections collects the intersections found on a given face of X.
Now the sign of the numerator defines which side of the vertex of Y the face of X falls on, and by extension whether the vertex of Y is in X or not.}

\begin{figure}
  \centering
  \begin{tikzpicture}[rotate around y=-20]
    \matrix[row sep=1em, column sep=1em,ampersand replacement=\ampersand]{
		\draw[black] (-2,0,0) -- (2,0,0); \draw[black] (0,-2,0) -- (0,2,0); \draw[black] (0,0,-2) -- (0,0,2);
		\filldraw[blue,opacity=0.2] (0,0,0) -- (-2,0,0) -- (-2,2,0) -- (0,2,0) -- cycle;
		\filldraw[blue,opacity=0.2] (0,0,0) -- (-2,0,0) -- (-2,0,2) -- (0,0,2) -- cycle;
		\filldraw[red,opacity=0.5] (-1,0,0) -- (0,1,0) -- (0,0,1) -- cycle;
		\filldraw[blue,opacity=0.2] (0,0,0) -- (0,0,2) -- (0,2,2) -- (0,2,0) -- cycle;
		\filldraw[blue] (-1,0,0) circle (1.5pt); \filldraw[blue] (0,1,0) circle (1.5pt); \filldraw[blue] (0,0,1) circle (1.5pt);
		\filldraw[black] (0,0,0) circle (1.5pt);
		\ampersand
			\draw[<->,rotate around y=20, very thick] (0,0,0) -- (1,0,0);
		\ampersand
		\draw[black] (-2,0,0) -- (2,0,0); \draw[black] (0,-2,0) -- (0,2,0); \draw[black] (0,0,-2) -- (0,0,2);
		\filldraw[blue,opacity=0.2] (0,0,0) -- (-2,0,0) -- (-2,2,0) -- (0,2,0) -- cycle;
		\filldraw[blue,opacity=0.2] (0,0,0) -- (-2,0,0) -- (-2,0,2) -- (0,0,2) -- cycle;
		\filldraw[red,opacity=0.5] (1,0,0) -- (0,-1,0) -- (0,0,-1) -- cycle;
		\filldraw[blue,opacity=0.2] (0,0,0) -- (0,0,2) -- (0,2,2) -- (0,2,0) -- cycle;
		\filldraw[blue] (1,0,0) circle (1.5pt); \filldraw[blue] (0,-1,0) circle (1.5pt); \filldraw[blue] (0,0,-1) circle (1.5pt);
		\filldraw[black] (0,0,0) circle (1.5pt);
			\\};
  \end{tikzpicture}
\end{figure}
\end{frame}

%---Simplices---%

\begin{frame}
\frametitle{Arbitrary dimension}
\note{
How can we apply this to higher dimensions?
Suppose we now have the n-simplex with unit edges that align with the coordinate axes, which denote by vectors e eta.
Suppose we intersect this with another n-simplex with vertices at the points $x_i$ for n+1 values of i.
At a given step in the algorithm we need to know the intersection between an m-dimensional `face' of X and m hyperplanes of Y.
This intersection can be written as this quotient.
The numerator of this quotient is shared over m of these intersections, keeping the given m-face of X constant but changing the m hyperplanes.
The position of the m-face is then determined by this numerator alone, and moves as a single object under the effects of error.}

Intersection between an $m$-face of $X$ and $m$ hyperplanes of $Y$:
\begin{equation*}
\vec{h}(J | \Gamma) \cdot \vec{e}_\eta = \frac{\begin{vmatrix} \vec{x}_{i_0} \cdot \vec{e}_\eta & \vec{x}_{i_0} \cdot \vec{e}_{\gamma_1} & \dots & \vec{x}_{i_0} \cdot \vec{e}_{\gamma_m} \\ \vdots & \vdots & & \vdots \\ \vec{x}_{i_m} \cdot \vec{e}_\eta & \vec{x}_{i_m} \cdot \vec{e}_{\gamma_1} & \dots & \vec{x}_{i_m} \cdot \vec{e}_{\gamma_m} \end{vmatrix}}{\begin{vmatrix} 1 & \vec{x}_{i_0} \cdot \vec{e}_{\gamma_1} & \dots & \vec{x}_{i_0} \cdot \vec{e}_{\gamma_m} \\ \vdots & \vdots & & \vdots \\ 1 & \vec{x}_{i_m} \cdot \vec{e}_{\gamma_1} & \dots & \vec{x}_{i_m} \cdot \vec{e}_{\gamma_m} \end{vmatrix}}
\end{equation*}
\end{frame}

\begin{frame}
\frametitle{Final remarks}
\note{
I'd like to end with a few ongoing questions.

We've seen that these algorithms use parsimony by not producing contradictory internal results, which makes them more stable.
But does this occur at the cost of accuracy? or efficiency?
Can the same tricks that have produced accurate and fast algorithms in the past be applied here?

We've also seen that this type of algorithm breaks symmetry between the two simplices being intersected.
Does this have any disadvantages in practical applications?}

\begin{itemize}
\item More stable
\item Less accurate?
\item Less efficient?
\item Breaks symmetry
\end{itemize}
\end{frame}

\end{document}