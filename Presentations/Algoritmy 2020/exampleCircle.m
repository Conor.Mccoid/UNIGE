[N,T,Np]=CircleMesh(20,eps);
close all
%%
figure(1)
clf
PlotMesh(N,T,'b')
PlotMesh(Np,T,'r')
M = InterfaceMatrix(N,T,Np,T);
pause(1)

%%
figure(2)
clf
PlotMesh(N,T,'b')
PlotMesh(Np,T,'r')
M = InterfaceMatrixNew(N,T,Np,T);