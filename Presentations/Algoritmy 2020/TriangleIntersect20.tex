\documentclass{beamer}

\usetheme{Berlin}

\usecolortheme{dolphin}

\usepackage{preamble}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{setspace}
\usepackage{animate}
\usepackage{tikz}
\usetikzlibrary{calc,intersections,positioning}

\usepackage{pgfpages}
\setbeameroption{hide notes}

\title{A provably robust algorithm for triangle intersections}
\author{Conor McCoid, Martin Gander}
\institute{University of Geneva}
\date{12.09.20}

\begin{document}

\frame{\titlepage}

%--------Intersecting Triangles

\begin{frame}{Intersecting triangles}

\begin{block}{Goal}
Transfer information between two nonmatching grids (AKA the grid transfer problem).
\end{block}

\begin{block}{Method}
Calculate the overlap of the triangulations.
\end{block}

\begin{block}{New goal}
Calculate the intersection of 2D triangles
robustly in floating point arithmetic.
\end{block}
\note{
 First, a description of the problem:
 Suppose you have two grids, one with information on it and the other you want to transfer this information to.
 But the grids don't overlap.
 How do you transfer the information?

 One way is to calculate the overlap of the respective triangulations.
 For each triangle in the first grid, which triangles overlap with it and by how much?
 Our new problem to solve is the intersection of two triangles.
 A simple problem, no? One with many solutions already available.
}
\end{frame}

\begin{frame}
\frametitle{The trouble clipping polygons}
\note{
 The trouble is that not all of the algorithms for calculating these intersections are robust against all errors.
 Take this pair of triangles on the left here.
 Their intersection was calculated with a particular algorithm, the precursor to the one I'll be presenting.
 Clearly they overlap, but no intersection was found. Why?

 This particular algorithm used two different subroutines to find the vertices of the polygon of intersection.
 First, it looked for the vertices of one triangle inside the other and found two (point to the bottom vertices of the blue triangle).
 Then, it calculated the intersections of the edges and found none.
 This second subroutine thought that the blue triangle was contained entirely within the red triangle.
 The two subroutines were inconsistent as to the number of vertices of the blue triangle that lay inside the red triangle.
 A small perturbation fixed the problem (point to the right).
}
\begin{figure}
    \centering
    \mbox{\includegraphics[width=0.49\textwidth]{Jeronimo_fail.png}
    \includegraphics[width=0.49\textwidth]{Jeronimo_success.png}}
\end{figure}
\end{frame}

%--------The Algorithm

\begin{frame}
\frametitle{The algorithm}
\note{
 So we know we need to be consistent over our subroutines.
 How are we going to introduce consistency into the algorithm?

 Let's start by writing out what we need each subroutine to do.
 First, we'll use a change of coordinates to turn one of the triangles into a right-angled reference triangle with unit length on two sides.
 Second, we'll calculate the intersections between the edges.
 Third, we'll decide which vertices are inside or outside and we'll do so in a manner that is consistent with the second step.
 }
\begin{enumerate}
\item Change of coordinates $\to$ reference triangle
\item Calculate intersections between edges
\item Determine if vertices are inside or outside
\end{enumerate}
\end{frame}

\begin{frame}
\frametitle{1. Change of coordinates}
\note{
 The change of coordinates is arguably not necessary, but it simplifies a number of calculations.
 It also provides consistency across multiple calculations of the same triangle.
 However, it does break the symmetry of the problem:
 One of the triangles now gets preferential treatement.
 }
\begin{enumerate}
\item Choose one of the two triangles
\item Transform this triangle into a reference triangle, ie. $\set{(0,0),(0,1),(1,0)}$
\item Use the same transform on the second triangle
\end{enumerate}
Call the reference triangle $Y$ and the other triangle $X$

\begin{figure}
    \centering
    \begin{tikzpicture}[scale=0.5]
        \draw[gray,thick] (-8,0) -- (-2,0);
        \draw[gray,thick] (-6,-2) -- (-6,4);
        \draw[->,red] (-6,0) -- (-5,1);
        \draw[->,red] (-5,1) -- (-4,3);
        \draw[->,red] (-5,1) -- (-3,2);
        \draw[red,dashed] (-3,2) -- (-4,3);
        
        \draw (-5,0.25) node {$\mathbf{v}_0$};
        \draw (-5,2) node {$\mathbf{v}_2$};
        \draw (-3.5,1) node {$\mathbf{v}_1$};
        
        \draw[->,thick] (-1,0) -- (1,0);
        
        \draw[gray,thick] (8,0) -- (2,0);
        \draw[gray,thick] (4,-2) -- (4,4);
        \draw[red,dashed] (4,3) -- (7,0);
        \draw[red] (4,0) -- (4,3);
        \draw[red] (4,0) -- (7,0);
        
        \draw (3,-0.5) node {(0,0)};
        \draw (3,3) node {(0,1)};
        \draw (7,-.5) node {(1,0)};
    \end{tikzpicture}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{2. Edge parametrization}
\note{
 Now we calculate the edge intersections.
 Pick an edge, any edge, of the reference triangle.
 We choose a coordinate system $(q,p)$ such that the edge lies along p=0 with q in the interval between 0 and 1.
 Since we're using a reference triangle for two of the edges this is just (x,y) or (y,x).
 For the third edge there's a little more work but nothing too challenging.

 Now we take a look at an edge of the other triangle, which we're calling X.
 We compare the p values of the vertices of this edge.
 If they have different signs then one vertex is below the line p=0 and the other is above it, and so there's an intersection which we can calculate.
 The result, (q0,0), is a vertex of the polygon of intersection if q0 lies between 0 and 1.
 If that's the case, we transform it back into the starting coordinates.
 Otherwise, we'll keep the value q0 for the next step.
}
\begin{enumerate}
\item Choose an edge of $Y$, the reference triangle
\item Parametrize the coordinates $(q,p)$ such that this edge lies on $p=0$, $q \in [0,1]$ and $Y$ lies on the positive side of $p=0$; let $(q_i, p_i)$ be the coordinates of the $i$--th vertex of $X$
\item For the three edges of $X$, check if $p_i$ and $p_j$ have different signs;
calculate the value of $q_0$ where this edge crosses $p=0$
\item If $q_0 \in [0,1]$ then $(q_0,0)$ is an intersection between an edge of $X$ and an edge of $Y$; transform $(q_0,0)$ into the original coordinates
\end{enumerate}

\begin{figure}
	\centering
	\begin{tikzpicture}[scale=1]
		\coordinate (0) at (0,0);
		\coordinate (1) at (4,0);
		\coordinate (x1) at (3,0.5);
		\coordinate (x2) at (1,-0.5);

		\draw[->, black,thick] ($(0) -(0.5,0)$) -- ($(1) + (0.5,0)$) node[above] {q};
		\draw[->, black,thick] (0,-1)--(0,1) node[left] {p};

		\filldraw[red] (0) circle (3pt);
		\filldraw[red] (1) circle (3pt);
		\draw[thick,red,name path=01] (0) -- (1);

		\draw[thick,red,name path=x] (x1)--(x2);
		\filldraw[red] (x1) circle (3pt) node[above right,black] {$(q_i,p_i>0)$};
		\filldraw[red] (x2) circle (3pt) node[below right,black] {$(q_j,p_j<0)$};

		\path[name intersections={of=01 and x,by=q0}];
		\filldraw[blue] (q0) circle (3pt) node[above left,black] {$(q_0,0)$};
	\end{tikzpicture}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{3. Vertices}
\note{
 This is the step that gives us the consistency we need.
 After the previous step we have up to 6 values of q0, two for each edge of the reference triangle.
 On each edge, the space between the two values lies entirely within X and nothing outside this space lies within X.
 So, if a vertex of Y lies between the two values, ie. 0 or 1 is between the two values of q0, then that vertex lies within X.
 The vertices of Y in X are now directly related to the calculated intersections.
 }
Vertices of $Y$ in $X$:
	\begin{enumerate}
	\item Choose an edge of $Y$ and consider the two values of $q_0$ for this edge (call them $q_0^1$ and $q_0^2$)
	\item If $0, 1 \in [q_0^1, q_0^2]$ then the corresponding vertices of $Y$ lie in $X$
	\end{enumerate}
\begin{figure}
    \centering
    \begin{tikzpicture}[scale=0.75]
        \filldraw[red] (0,0) circle (3pt);
        \filldraw[blue] (0,1) circle (3pt);
        \filldraw[blue] (2,0) circle (3pt);
        \filldraw[blue] (0,-2) circle (3pt);
        \filldraw[blue] (-2,0) circle (3pt);
        
        \draw[gray,thick] (0,0) -- (0,3);
        \draw[gray,thick] (0,0) -- (3,0);
        
        \draw[gray,dashed] (0,0) -- (0,-3);
        \draw[gray,dashed] (0,0) -- (-3,0);
        
        \draw[gray,thick] (-3,-0.5) -- (3,2.5);
        \draw[gray,thick] (-1,-3) -- (3,1);
    \end{tikzpicture}
\end{figure}
\end{frame}

\begin{frame}
\note{
 For the other type of vertices, X in Y, we take the values of p from each edge parametrization.
 If p is positive for every edge for a given vertex then that vertex lies inside Y, since we parametrized the coordinates so that Y is always on the positive side of p=0.
 Again, we have consistency:
 We used the sign of p to determine if we would calculate an intersection.
 Now we're using the same sign of p to determine if the vertex of X lies inside Y.
}
Vertices of $X$ in $Y$:
	\begin{enumerate}
	\item Choose a vertex of $X$ and consider the three values of $p_i$ for this vertex (call them $p_i^j$ where $j=1,2,3$)
	\item If $p_i^j$ is positive for all $j$ then this vertex lies in $Y$
	\end{enumerate}
\end{frame}

%--------The Proof

\begin{frame}
\frametitle{Proof of robustness}
\note{
 So we have the algorithm, how do we prove it's robust?
 First, we need to classify the errors that can occur.
 Then, we need to show these errors don't cause any catastrophic changes to the polygon of intersection, like delete it.
 }
\begin{enumerate}
\item There are only a limited number of errors that can occur with this algorithm
\item These errors can change the size and shape of the polygon of intersection, but only on the order of machine epsilon
\end{enumerate}
\end{frame}

\begin{frame}
\frametitle{Types of errors}
\note{ Since there are three types of points in the polygon of intersection, there are three types of errors (read them as written).}
\begin{description}
\item[$X$--in--$Y$ errors:] The algorithm incorrectly places a vertex of $X$ outside/inside $Y$
\item[Intersection errors:] The algorithm miscalculates an intersection to lie on/off an edge of $Y$
\item[$Y$--in--$X$ errors:] The algorithm incorrectly determines a vertex of $Y$ to be outside/inside $X$
\end{description}
\end{frame}

\begin{frame}{$X$--in--$Y$ errors}
\note{
 How does a vertex of X get inside Y when it's meant to be outside?
 The sign of pij must have flipped for at least one combination of i and j.
 This can only happen if the vertex has drifted over the line p=0, at least in floating point arithmetic.
 What can happen as a result? (read off the possible options, pointing to the figures)
 }
This error is caused by a sign flip of a value of $p_i^j$, for some $i$ and $j$.
As a result, one of the following occurs:
\begin{itemize}
\item two intersections have been created;
\item an intersection has been moved;
\item two intersections have been deleted.
\end{itemize}
\begin{figure}
\begin{subfigure}{0.3\textwidth}
\centering
\begin{tikzpicture}[scale=0.4]
            \filldraw[red] (-2,2) circle (3pt);
            \filldraw[red] (-2,-2) circle (3pt);
            \draw[gray,thick] (-2,2)--(-2,-2);
            
            \filldraw[red] (-3,0) circle (3pt);
            \draw[gray, thick] (-3,0) -- (-4,1);
            \draw[gray, thick] (-3,0) -- (-4,-1);
            
            \draw[->, thick] (-1,0) -- (1,0);
            
            \filldraw[red] (3,2) circle (3pt);
            \filldraw[red] (3,-2) circle (3pt);
            \draw[gray,thick] (3,2) -- (3,-2);
            \filldraw[blue] (3,1) circle (3pt);
            \filldraw[blue] (3,-1) circle (3pt);
            
            \filldraw[red] (4,0) circle (3pt);
            \draw[gray,thick] (4,0) -- (2,2);
            \draw[gray, thick] (4,0) -- (2,-2);
        \end{tikzpicture}
\end{subfigure}
\begin{subfigure}{0.3\textwidth}
    \centering
        \begin{tikzpicture}[scale=0.4]
            \filldraw[red] (-2,2) circle (3pt);
            \filldraw[red] (-2,-2) circle (3pt);
            \draw[gray,thick] (-2,2)--(-2,-2);
            
            \filldraw[red] (-3,0) circle (3pt);
            \draw[gray, thick] (-3,0) -- (-4,1);
            \draw[gray, thick] (-3,0) -- (-1,-2);
            \filldraw[blue] (-2,-1) circle (3pt);
            
            \draw[->, thick] (-1,0) -- (1,0);
            
            \filldraw[red] (3,2) circle (3pt);
            \filldraw[red] (3,-2) circle (3pt);
            \draw[gray,thick] (3,2) -- (3,-2);
            \filldraw[blue] (3,1) circle (3pt);

            \filldraw[red] (4,0) circle (3pt);
            \draw[gray,thick] (4,0) -- (2,2);
            \draw[gray, thick] (4,0) -- (5,-1);
        \end{tikzpicture}
    \end{subfigure}
    \begin{subfigure}{0.3\textwidth}
    \centering
        \begin{tikzpicture}[scale=0.4]
            \filldraw[red] (-2,2) circle (3pt);
            \filldraw[red] (-2,-2) circle (3pt);
            \draw[gray,thick] (-2,2)--(-2,-2);
            
            \filldraw[red] (-3,0) circle (3pt);
            \draw[gray, thick] (-3,0) -- (-1,-2);
            \draw[gray, thick] (-3,0) -- (-1,2);
            \filldraw[blue] (-2,-1) circle (3pt);
            \filldraw[blue] (-2,1) circle (3pt);
            
            \draw[->, thick] (-1,0) -- (1,0);
            
            \filldraw[red] (2,2) circle (3pt);
            \filldraw[red] (2,-2) circle (3pt);
            \draw[gray,thick] (2,2) -- (2,-2);
            
            \filldraw[red] (3,0) circle (3pt);
            \draw[gray,thick] (3,0) -- (4,1);
            \draw[gray,thick] (3,0) -- (4,-1);
        \end{tikzpicture}
    \end{subfigure}
\end{figure}
\end{frame}

\begin{frame}{Intersection errors and $Y$--in--$X$ errors}
\note{
 An intersection can be misplaced along the line p=0.
 This wouldn't cause any major change to the shape of the polygon of intersection unless it fell out of (or in to) the interval 0 to 1.
 In that case, we'd stop thinking of it as an intersection.
 Moreover, it would change our determination of the vertex of Y.
 It turns out, however, that either this doesn't affect the shape of the polygon of intersection or it looks exactly like another error we've talked about on the previous slide.
}
If an intersection moves over the line $q=0$ or $q=1$ then either:
\begin{itemize}
\item there is no change in the shape or size of the polygon of intersection or;
\item the change is equivalent to an $X$--in--$Y$ error where an intersection has been moved.
\end{itemize}
$Y$--in--$X$ errors occur only as a result of the other types of errors.
They help ensure consistency.
\end{frame}

\begin{frame}
\note{
 Here are the four ways an intersection error can occur.
 a and b are where the intersections should be.
 $\tilde{a}$ is where a has been calculated to be.
 The upper right corner of each graph is the triangle Y, and the little red dot is its vertex at (0,0) in (q,p) coordinates.

 In the first of these, a new intersection point is introduced into the triangle, but the vertex of Y is still found to be inside X because of the second set of intersections along the other edge of this vertex.
 This vertex, $\tilde{a}$ and b form a line.
 This means $\tilde{a}$ isn't a corner of the polygon of intersection, but a midpoint on one of its edges.
 The shape of the polygon doesn't change.

 In the second figure, we've lost an intersection but gained a vertex.
 The number of corners of the polygon again hasn't changed.

 The last two are equivalent to the X in Y error which results in an intersection being moved (return to slide and point).
 In (c), the vertex moves into the triangle, while (d) is equivalent to the vertex moving out of the triangle.
}
\begin{figure}
    \centering
    \begin{subfigure}{0.45\textwidth}
    \centering
        \begin{tikzpicture}[scale=0.3]
            \filldraw[red] (-5,0) circle (5pt);
            \draw[gray,thick] (-5,0) -- (-5,2);
            \draw[gray,thick] (-5,0) -- (-2,0);
            \draw[gray,dashed,thick] (-5,0) -- (-5,-2);
            \draw[gray,dashed,thick] (-5,0) -- (-8,0);
            \draw[->] (-1,0) -- (1,0);
            \filldraw[red] (5,0) circle (5pt);
            \draw[gray,thick] (5,0) -- (5,2);
            \draw[gray,thick] (5,0) -- (8,0);
            \draw[gray,dashed,thick] (5,0) -- (5,-2);
            \draw[gray,dashed,thick] (5,0) -- (2,0);
            
            \filldraw[blue] (-3,0) circle (5pt);
            \draw (-3,1) node {$b$};
            \filldraw[blue] (-6,0) circle (5pt);
            \draw (-6,1) node {$a$};
            \filldraw[blue] (7,0) circle (5pt);
            \draw (7,1) node {$b$};
            \filldraw[blue] (6,0) circle (5pt);
            \draw (6,1) node {$\tilde{a}$};
            
            \filldraw[blue] (-5,1) circle (5pt);
            \filldraw[blue] (-5,-1) circle (5pt);
            \filldraw[blue] (5,1) circle (5pt);
            \filldraw[blue] (5,-1) circle (5pt);
        \end{tikzpicture}
        \caption{New point of intersection.}
        \label{subfig:intersect a}
    \end{subfigure}
    \begin{subfigure}{0.45\textwidth}
    \centering
        \begin{tikzpicture}[scale=0.3]
            \filldraw[red] (-5,0) circle (5pt);
            \draw[gray,thick] (-5,0) -- (-5,2);
            \draw[gray,thick] (-5,0) -- (-2,0);
            \draw[gray,dashed,thick] (-5,0) -- (-5,-2);
            \draw[gray,dashed,thick] (-5,0) -- (-8,0);
            \draw[->] (-1,0) -- (1,0);
            \filldraw[red] (5,0) circle (5pt);
            \draw[gray,thick] (5,0) -- (5,2);
            \draw[gray,thick] (5,0) -- (8,0);
            \draw[gray,dashed,thick] (5,0) -- (5,-2);
            \draw[gray,dashed,thick] (5,0) -- (2,0);
            
            \filldraw[blue] (-3,0) circle (5pt);
            \draw (-3,1) node {$b$};
            \filldraw[blue] (-4,0) circle (5pt);
            \draw (-4,1) node {$a$};
            \filldraw[blue] (7,0) circle (5pt);
            \draw (7,1) node {$b$};
            \filldraw[blue] (4,0) circle (5pt);
            \draw (4,1) node {$\tilde{a}$};
        \end{tikzpicture}
        \caption{Loss of intersection, new $Y$-in-$X$ point.}
        \label{subfig:intersect b}
    \end{subfigure}
    \begin{subfigure}{0.45\textwidth}
    \centering
        \begin{tikzpicture}[scale=0.3]
            \filldraw[red] (-5,0) circle (5pt);
            \draw[gray,thick] (-5,0) -- (-5,2);
            \draw[gray,thick] (-5,0) -- (-2,0);
            \draw[gray,dashed,thick] (-5,0) -- (-5,-2);
            \draw[gray,dashed,thick] (-5,0) -- (-8,0);
            \draw[->] (-1,0) -- (1,0);
            \filldraw[red] (5,0) circle (5pt);
            \draw[gray,thick] (5,0) -- (5,2);
            \draw[gray,thick] (5,0) -- (8,0);
            \draw[gray,dashed,thick] (5,0) -- (5,-2);
            \draw[gray,dashed,thick] (5,0) -- (2,0);
            
            \filldraw[blue] (-7,0) circle (5pt);
            \draw (-7,1) node {$b$};
            \filldraw[blue] (-6,0) circle (5pt);
            \draw (-6,1) node {$a$};
            \filldraw[blue] (3,0) circle (5pt);
            \draw (3,1) node {$b$};
            \filldraw[blue] (6,0) circle (5pt);
            \draw (6,1) node {$\tilde{a}$};
        \end{tikzpicture}
        \caption{New intersection and $Y$-in-$X$ point.}
        \label{subfig:intersect c}
    \end{subfigure}
    \begin{subfigure}{0.45\textwidth}
    \centering
        \begin{tikzpicture}[scale=0.3]
            \filldraw[red] (-5,0) circle (5pt);
            \draw[gray,thick] (-5,0) -- (-5,2);
            \draw[gray,thick] (-5,0) -- (-2,0);
            \draw[gray,dashed,thick] (-5,0) -- (-5,-2);
            \draw[gray,dashed,thick] (-5,0) -- (-8,0);
            \draw[->] (-1,0) -- (1,0);
            \filldraw[red] (5,0) circle (5pt);
            \draw[gray,thick] (5,0) -- (5,2);
            \draw[gray,thick] (5,0) -- (8,0);
            \draw[gray,dashed,thick] (5,0) -- (5,-2);
            \draw[gray,dashed,thick] (5,0) -- (2,0);
            
            \filldraw[blue] (-7,0) circle (5pt);
            \draw (-7,1) node {$b$};
            \filldraw[blue] (-4,0) circle (5pt);
            \draw (-4,1) node {$a$};
            \filldraw[blue] (3,0) circle (5pt);
            \draw (3,1) node {$b$};
            \filldraw[blue] (4,0) circle (5pt);
            \draw (4,1) node {$\tilde{a}$};
            
            \filldraw[blue] (-5,1) circle (5pt);
            \filldraw[blue] (-5,-1) circle (5pt);
            \filldraw[blue] (5,1) circle (5pt);
            \filldraw[blue] (5,-1) circle (5pt);
        \end{tikzpicture}
        \caption{Loss of intersection.}
        \label{subfig:intersect d}
    \end{subfigure}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Triangle graphs}
\note{
 We now need to codify the possible intersections as graphs and study how the errors change these graphs.
 The graphs that represent the possible intersections of this algorithm have four conditions.
 Firstly, they must have 6 vertices with two neighbours, representing the vertices of the triangles.
 Secondly, they must have 0, 2, 4 or 6 vertices with four neighbours, representing the intersections of the edges.
}
\begin{columns}
\begin{column}{0.6\textwidth}
Triangle-triangle intersections can be represented by graphs with the following conditions:
\begin{description}
\item[Vertex condition:] the graph has exactly 6 red vertices with two neighbours;
\item[Intersection condition:] the graph has 0, 2, 4 or 6 blue vertices with four neighbours;
\end{description}
\end{column}

\begin{column}{0.4\textwidth}
\begin{figure}
\begin{tikzpicture}[scale=0.75]
            \filldraw[red] (0,0) circle (3pt);
            \filldraw[red] (3,0) circle (3pt);
            \filldraw[red] (0,3) circle (3pt);
            \draw[red,thick] (0,0) -- (3,0) -- (0,3) -- cycle;
            
            \draw[blue,thick] (1,1) -- (2,1) -- (3,2) -- (2,3) -- (1,2) -- cycle;
            \filldraw[red] (1,1) circle (3pt);
            \filldraw[red] (3,2) circle (3pt);
            \filldraw[red] (2,3) circle (3pt);
            \filldraw[blue] (2,1) circle (3pt);
            \filldraw[blue] (1,2) circle (3pt);
        \end{tikzpicture}
\end{figure}
\end{column}
\end{columns}
\end{frame}

\begin{frame}
\note{
 Thirdly, the edges and vertices of the graphs must form two cycles, each containing three red vertices and meeting only at blue vertices.
 Finally, edges between two blue vertices do not touch the exterior of the graph.
 For those familiar with graph theory, this means there is no edge in the dual graph between the exterior and the polygon of intersection.
}
\begin{columns}
\begin{column}{0.6\textwidth}
\begin{description}
\item[Triangle condition:] there are exactly two cycles each containing three red vertices not shared between these cycles such that the cycles meet at the blue vertices;
\item[Sheltered polygon property:] no edge between two blue vertices touches the exterior of the graph.
\end{description}
\end{column}

\begin{column}{0.4\textwidth}
\begin{figure}
\begin{tikzpicture}[scale=0.75]
            \filldraw[red] (0,0) circle (3pt);
            \filldraw[red] (3,0) circle (3pt);
            \filldraw[red] (0,3) circle (3pt);
            \draw[red,thick] (0,0) -- (3,0) -- (0,3) -- cycle;
            
            \draw[blue,thick] (1,1) -- (2,1) -- (3,2) -- (2,3) -- (1,2) -- cycle;
            \filldraw[red] (1,1) circle (3pt);
            \filldraw[red] (3,2) circle (3pt);
            \filldraw[red] (2,3) circle (3pt);
            \filldraw[blue] (2,1) circle (3pt);
            \filldraw[blue] (1,2) circle (3pt);
            
            \draw[black,thick] (0.5,1.5) -- (1.25,1.25) -- (2,2) -- (1,3) -- cycle;
            \draw[red,thick] (0.5,1.5) circle (3pt);
            \draw[blue,thick] (1.25,1.25) circle (3pt);
            \draw[red,thick] (2,2) circle (3pt);
            \draw[black,thick] (1,3) circle (3pt);
        \end{tikzpicture}
\end{figure}
\end{column}
\end{columns}
\end{frame}

\begin{frame}
\note{
 It turns out there are only 10 graphs that meet these criteria.
 Degenerate cases, where vertices appear on edges or overlap with each other, are treated separately.
}
\begin{figure}
\centering
\begin{subfigure}{0.3\textwidth}
% config A
\centering
    \begin{tikzpicture}[scale=0.5]
    \draw[gray, thick] (0,0) -- (0,3);
    \draw[gray, thick] (0,3) -- (3,0);
    \draw[gray, thick] (3,0) -- (0,0);
    \filldraw[red] (0,0) circle (3pt);
    \filldraw[red] (0,3) circle (3pt);
    \filldraw[red] (3,0) circle (3pt);
    
    \draw[gray, thick] (0.5,0.5) -- (1.5,0.5);
    \draw[gray, thick] (0.5,1.5) -- (1.5,0.5);
    \draw[gray, thick] (0.5,0.5) -- (0.5,1.5);
    \filldraw[red] (0.5,0.5) circle (3pt);
    \filldraw[red] (1.5,0.5) circle (3pt);
    \filldraw[red] (0.5,1.5) circle (3pt);
    \end{tikzpicture}
\caption{A}
\end{subfigure}
\begin{subfigure}{0.3\textwidth}
% config B
    \centering
    \begin{tikzpicture}[scale=0.5]
    \draw[gray, thick] (0,0) -- (0,3);
    \draw[gray, thick] (0,3) -- (3,0);
    \draw[gray, thick] (3,0) -- (0,0);
    \filldraw[red] (0,0) circle (3pt);
    \filldraw[red] (0,3) circle (3pt);
    \filldraw[red] (3,0) circle (3pt);
    
    \draw[gray, thick] (0.5,1.5) -- (1.5,0.5);
    \draw[gray, thick] (0.5,1.5) -- (1,2);
    \draw[gray, thick] (1.5,0.5) -- (2,1);
    \filldraw[blue] (2,1) circle (3pt);
    \filldraw[blue] (1,2) circle (3pt);
    \draw[gray, thick] (1,2) -- (2,2);
    \draw[gray, thick] (2,1) -- (2,2);
    \filldraw[red] (0.5,1.5) circle (3pt);
    \filldraw[red] (1.5,0.5) circle (3pt);
    \filldraw[red] (2,2) circle (3pt);
    \end{tikzpicture}
\caption{B}
\end{subfigure}
\begin{subfigure}{0.3\textwidth}
% config C
    \centering
    \begin{tikzpicture}[scale=0.5]
        \draw[gray, thick] (0,0) -- (0,3);
        \draw[gray, thick] (0,3) -- (3,0);
        \draw[gray, thick] (3,0) -- (0,0);
        \filldraw[red] (0,0) circle (3pt);
        \filldraw[red] (0,3) circle (3pt);
        \filldraw[red] (3,0) circle (3pt);
        
        \filldraw[red] (-1,2) circle (3pt);
        \filldraw[red] (2,2) circle (3pt);
        \filldraw[red] (1,0.5) circle (3pt);
        \draw[gray, thick] (-1,2) -- (2,2);
        \filldraw[blue] (0,1) circle (3pt);
        \filldraw[blue] (0,2) circle (3pt);
        \filldraw[blue] (2,1) circle (3pt);
        \filldraw[blue] (1,2) circle (3pt);
        \draw[gray, thick] (-1,2) -- (0,1);
        \draw[gray, thick] (0,1) -- (1,0.5);
        \draw[gray, thick] (1,0.5) -- (2,1);
        \draw[gray, thick] (2,1) -- (2,2);
    \end{tikzpicture}
    \caption{C}
\end{subfigure}

\centering
\begin{subfigure}{0.3\textwidth}
% config D
    \centering
    \begin{tikzpicture}[scale=0.5]
        \draw[gray, thick] (0,0) -- (0,3);
        \draw[gray, thick] (0,3) -- (3,0);
        \draw[gray, thick] (3,0) -- (0,0);
        \filldraw[red] (0,0) circle (3pt);
        \filldraw[red] (0,3) circle (3pt);
        \filldraw[red] (3,0) circle (3pt);
        
        \filldraw[red] (-1,2) circle (3pt);
        \filldraw[red] (2,-1) circle (3pt);
        \filldraw[red] (2,2) circle (3pt);
        \draw[gray, thick] (-1,2) -- (2,2);
        \draw[gray, thick] (2,2) -- (2,-1);
        \draw[gray, thick] (2,-1) -- (-1,2);
        \filldraw[blue] (0,1) circle (3pt);
        \filldraw[blue] (0,2) circle (3pt);
        \filldraw[blue] (1,2) circle (3pt);
        \filldraw[blue] (2,1) circle (3pt);
        \filldraw[blue] (2,0) circle (3pt);
        \filldraw[blue] (1,0) circle (3pt);
    \end{tikzpicture}
    \caption{D}
\end{subfigure}
\begin{subfigure}{0.3\textwidth}
% config E
    \centering
    \begin{tikzpicture}[scale=0.5]
        \filldraw[red] (0,0) circle (3pt);
    \filldraw[red] (0,-3) circle (3pt);
    \filldraw[red] (3,-3) circle (3pt);
    \draw[gray, thick] (0,0) -- (0,-3);
    \draw[gray, thick] (0,0) -- (3,-3);
    \draw[gray, thick] (3,-3) -- (0,-3);
    \filldraw[red] (1,-2) circle (3pt);
    \filldraw[red] (2,0) circle (3pt);
    \filldraw[red] (3,-1) circle (3pt);
    \filldraw[blue] (1,-1) circle (3pt);
    \filldraw[blue] (2,-2) circle (3pt);
    \draw[gray, thick] (1,-2) -- (1,-1);
    \draw[gray, thick] (1,-2) -- (2,-2);
    \draw[gray, thick] (1,-1) -- (2,0);
    \draw[gray, thick] (2,-2) -- (3,-1);
    \draw[gray, thick] (2,0) -- (3,-1);
    \end{tikzpicture}
    \caption{E}
\end{subfigure}
\begin{subfigure}{0.3\textwidth}
% config F
    \centering
    \begin{tikzpicture}[scale=0.5]
        \draw[gray, thick] (0,0) -- (0,3);
        \draw[gray, thick] (0,3) -- (3,0);
        \draw[gray, thick] (3,0) -- (0,0);
        \filldraw[red] (0,0) circle (3pt);
        \filldraw[red] (0,3) circle (3pt);
        \filldraw[red] (3,0) circle (3pt);
        
        \filldraw[red] (1,-1) circle (3pt);
        \filldraw[blue] (1,0) circle (3pt);
        \filldraw[blue] (2,0) circle (3pt);
        \filldraw[blue] (1,2) circle (3pt);
        \filldraw[blue] (2,1) circle (3pt);
        \filldraw[red] (2,3) circle (3pt);
        \filldraw[red] (3,2) circle (3pt);
        \draw[gray, thick] (1,-1) -- (1,2);
        \draw[gray, thick] (1,-1) -- (2,0);
        \draw[gray, thick] (2,0) -- (2,1);
        \draw[gray, thick] (2,1) -- (3,2);
        \draw[gray, thick] (3,2) -- (2,3);
        \draw[gray, thick] (2,3) -- (1,2);
    \end{tikzpicture}
    \caption{F}
\end{subfigure}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}
\centering
\begin{subfigure}{0.3\textwidth}
% config B*
    \centering
    \begin{tikzpicture}[scale=0.5]
        \draw[gray, thick] (0,0) -- (0,3);
        \draw[gray, thick] (0,3) -- (3,0);
        \draw[gray, thick] (3,0) -- (0,0);
        \filldraw[red] (0,0) circle (3pt);
        \filldraw[red] (0,3) circle (3pt);
        \filldraw[red] (3,0) circle (3pt);
        
        \filldraw[red] (-1.5,1) circle (3pt);
        \filldraw[red] (1,-1.5) circle (3pt);
        \filldraw[red] (1,1) circle (3pt);
        \draw[gray, thick] (-1.5,1) -- (1,1);
        \draw[gray, thick] (1,1) -- (1,-1.5);
        \draw[gray, thick] (1,-1.5) -- (-1.5,1);
        \filldraw[blue] (1,0) circle (3pt);
        \filldraw[blue] (0,1) circle (3pt);
    \end{tikzpicture}
    \caption{B*}
\end{subfigure}
\begin{subfigure}{0.3\textwidth}
% config G
    \centering
    \begin{tikzpicture}[scale=0.5]
        \draw[gray, thick] (0,0) -- (0,3);
        \draw[gray, thick] (0,3) -- (3,0);
        \draw[gray, thick] (3,0) -- (0,0);
        \filldraw[red] (0,0) circle (3pt);
        \filldraw[red] (0,3) circle (3pt);
        \filldraw[red] (3,0) circle (3pt);
        
        \filldraw[red] (0.5,1.5) circle (3pt);
        \filldraw[red] (1.5,0.5) circle (3pt);
        \filldraw[red] (-1,-1) circle (3pt);
        \filldraw[blue] (0,1) circle (3pt);
        \filldraw[blue] (1,0) circle (3pt);
        \draw[gray, thick] (0.5,1.5) -- (1.5,0.5);
        \draw[gray, thick] (1.5,0.5) -- (1,0);
        \draw[gray, thick] (1,0) -- (-1,-1);
        \draw[gray, thick] (-1,-1) -- (0,1);
        \draw[gray, thick] (0,1) -- (0.5,1.5);
    \end{tikzpicture}
    \caption{G}
\end{subfigure}
\begin{subfigure}{0.3\textwidth}
% config H
    \centering
    \begin{tikzpicture}[scale=0.5]
        \draw[gray, thick] (0,0) -- (0,3);
        \draw[gray, thick] (0,3) -- (3,0);
        \draw[gray, thick] (3,0) -- (0,0);
        \filldraw[red] (0,0) circle (3pt);
        \filldraw[red] (0,3) circle (3pt);
        \filldraw[red] (3,0) circle (3pt);
        
        \filldraw[red] (0.5,2) circle (3pt);
        \filldraw[red] (2,2) circle (3pt);
        \filldraw[red] (-1,-1) circle (3pt);
        \filldraw[blue] (1,2) circle (3pt);
        \filldraw[blue] (2,1) circle (3pt);
        \filldraw[blue] (1,0) circle (3pt);
        \filldraw[blue] (0,1) circle (3pt);
        \draw[gray, thick] (-1,-1) -- (0,1);
        \draw[gray, thick] (0,1) -- (0.5,2);
        \draw[gray, thick] (0.5,2) -- (1,2);
        \draw[gray, thick] (1,2) -- (2,2);
        \draw[gray, thick] (2,2) -- (2,1);
        \draw[gray, thick] (2,1) -- (1,0);
        \draw[gray, thick] (1,0) -- (-1,-1);
    \end{tikzpicture}
    \caption{H}
\end{subfigure}

\centering
\begin{subfigure}{0.3\textwidth}
% config C*
    \centering
    \begin{tikzpicture}[scale=0.5]
        \draw[gray, thick] (0,0) -- (0,3);
        \draw[gray, thick] (0,3) -- (3,0);
        \draw[gray, thick] (3,0) -- (0,0);
        \filldraw[red] (0,0) circle (3pt);
        \filldraw[red] (0,3) circle (3pt);
        \filldraw[red] (3,0) circle (3pt);
        
        \filldraw[blue] (1,0) circle (3pt);
        \filldraw[blue] (0,1) circle (3pt);
        \filldraw[blue] (2,0) circle (3pt);
        \filldraw[blue] (0,2) circle (3pt);
        \filldraw[red] (0.5,0.5) circle (3pt);
        \filldraw[red] (3,-1) circle (3pt);
        \filldraw[red] (-1,3) circle (3pt);
        \draw[gray,thick] (-1,3) -- (0,1) -- (1,0) -- (3,-1) -- (2,0) -- (0,2) -- cycle;
    \end{tikzpicture}
    \caption{C*}
\end{subfigure}
\caption{The graphs arising from all possible triangle-triangle intersections, degenerate cases removed.}
\label{fig:graphs}
\end{figure}
\end{frame}

\begin{frame}
\note{
 Now that we know what the graphs look like, how do the errors affect them?
 The following graph shows how the errors connect the intersection graphs.
 Blue lines represent errors where two intersections are created or destroyed (created moves downward, destroyed upward).
 Red lines represent errors where an intersection is moved and a vertex moves inside (down) or outside (up) the other triangle.
}
\frametitle{Connectivity}
% how do the errors connect the graphs?
\begin{figure}
\begin{tikzpicture}[
    roundnode/.style={circle, fill=white, draw=black, very thick}]
        \node[roundnode] (maintopic) {A};
        \node[roundnode] (B) [below=of maintopic] {B};
        \node[roundnode] (C) [below=of B] {C};
        \node[roundnode] (D) [below=of C] {D};
        \node[roundnode] (E) [left=of maintopic] {E};
        \node[roundnode] (F) [below=of E] {F};
        \node[roundnode] (B*) [right=of B] {B*};
        \node[roundnode] (G) [right=of C] {G};
        \node[roundnode] (H) [below=of G] {H};
        \node[roundnode] (C*) [left=of C] {C*};
        
        \draw[blue] (maintopic) -- (B);
        \draw[blue] (B) -- (C);
        \draw[blue] (C) -- (D);

        \draw[blue] (G) -- (H);
        \draw[blue] (E) -- (F);
        \draw[blue] (B*) -- (C);

        \draw[red] (E) -- (B);
        \draw[red] (F) -- (C);
        \draw[red] (E) -- (B*);
        \draw[red] (B) -- (G);
        \draw[red] (B*) -- (G);
        \draw[red] (C) -- (H);
        
        \draw[red] (C*) -- (F);
        \draw[red] (C*) -- (H);
        \draw[blue] (C*) -- (B);
    \end{tikzpicture}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{The proof}
\note{
 And now to summarize the proof.
 Only 10 graphs are possible, and only two types of errors (red and blue).
 As we've seen, these errors connect the 10 graphs in a hierarchy.
 They only take one of the 10 graphs and give another one of the 10 graphs.
 There's no deletion of an intersection and no new area springs up.

% Each of the calculations performed is backwards stable.
 That means the position of the vertices and the area of the triangles has an error on the order of machine epsilon.
 The intersection between the triangles, if it exists, will then be calculated with an error at most on the order of machine epsilon.
}
\begin{itemize}
\item An error can only transform a triangle-triangle intersection into another triangle-triangle intersection.
There is no deletion or creation of large, unexpected areas.
\item Since all steps of the algorithm can only introduce errors on the order of machine epsilon, the resulting polygon of intersection will have an error at most of the same order.
\end{itemize}
\end{frame}

%\begin{frame}[allowframebreaks]
%\frametitle{References}
%\bibliographystyle{apalike}
%\bibliography{references.bib}
%%\bibliography{C:/Users/conmc/Documents/LaTeX/references}
%\end{frame}

\begin{frame}
\frametitle{Degenerate cases}
\note{
 There are three types of degeneracies, listed here.

 If a vertex of X is coincident with an edge of Y then the polygon of intersection is equivalent to the same configuration if the vertex is perturbed away from the edge.

 For a vertex of Y on an edge of X, the polygon is the same as if there were an intersection slightly above (or to the right of, situation depending) the vertex.

 Finally, if two vertices are on top of each other, then the polygon is the same as if both of these changes are made: the vertex of X is perturbed towards the exterior of Y and an intersection is placed slightly above the vertex of Y.
}
\begin{columns}
\begin{column}{0.4\textwidth}
Vertex of X coincident with edge of Y

~

Vertex of Y coincident with edge of X

~

Vertices coincident
\end{column}

\begin{column}{0.6\textwidth}
\begin{figure}
    \centering
    \begin{subfigure}{\textwidth}
        \centering
        \begin{tikzpicture}[scale=0.75]
            \fill[lightgray] (0,1) -- (1,0) -- (2,1) -- cycle;
            
            \draw[gray,thick] (0,0) -- (2,0);
            \filldraw[red] (0,0) circle (3pt);
            \filldraw[red] (2,0) circle (3pt);
            
            \draw[gray,thick] (0,1) -- (1,0) -- (2,1);
            \filldraw[red] (1,0) circle (3pt);
            
            \draw[->] (3,0) -- (4,0);
            
            \fill[lightgray] (5,1) -- (6,0.5) -- (7,1) -- cycle;
            
            \draw[gray,thick] (5,0) -- (7,0);
            \filldraw[red] (5,0) circle (3pt);
            \filldraw[red] (7,0) circle (3pt);
            
            \draw[gray,thick] (5,1) -- (6,0.5) -- (7,1);
            \filldraw[red] (6,0.5) circle (3pt);
        \end{tikzpicture}
    \end{subfigure}
    \begin{subfigure}{\textwidth}
        \centering
        \begin{tikzpicture}[scale=0.75]
            \fill[lightgray] (0,2) -- (0,0) -- (2,2) -- cycle;
            
            \draw[gray,thick] (0,2) -- (0,0) -- (2,0);
            \draw[gray,thick] (-1,-1) -- (2,2);
            \filldraw[red] (0,0) circle (3pt);

            \draw[->] (3,0) -- (4,0);
            
            \fill[lightgray] (5,2) -- (5,1) -- (7,2) -- cycle;
            
            \draw[gray,thick] (5,2) -- (5,0) -- (7,0);
            \draw[gray,thick] (4,-1) -- (5,1) -- (7,2);
            \filldraw[red] (5,0) circle (3pt);
            \filldraw[blue] (5,1) circle (3pt);
        \end{tikzpicture}
    \end{subfigure}
    \begin{subfigure}{\textwidth}
        \centering
        \begin{tikzpicture}[scale=0.75]
            \fill[lightgray] (0,2) -- (0,0) -- (2,2) -- cycle;
            
            \draw[gray,thick] (0,2) -- (0,0) -- (2,0);
            \draw[gray,thick] (-1,1) -- (0,0) -- (2,2);
            \filldraw[red] (0,0) circle (3pt);

            \draw[->] (3,0) -- (4,0);
            
            \fill[lightgray] (5,2) -- (5,1) -- (7,2) -- cycle;
            
            \draw[gray,thick] (5,2) -- (5,0) -- (7,0);
            \draw[gray,thick] (4,1) -- (4.5,0.5) -- (5,1) -- (7,2);
            \filldraw[red] (5,0) circle (3pt);
            \filldraw[red] (4.5,0.5) circle (3pt);
            \filldraw[blue] (5,1) circle (3pt);
        \end{tikzpicture}
    \end{subfigure}
\end{figure}
\end{column}
\end{columns}
\end{frame}

\end{document}\grid
