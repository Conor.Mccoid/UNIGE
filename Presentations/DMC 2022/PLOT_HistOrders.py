import pandas as pd
import matplotlib.pyplot as plt

def main():
    df_orders=pd.read_csv('orders.csv',delimiter='|')

    num_orders=df_orders['userID'].value_counts()
    num_orders.plot.hist(bins=278) # indicates vast majority of users made between 5 and 50 orders
    plt.show(block=True)

    user_percentile=[sum(num_orders<i)/sum(num_orders>0) for i in range(1,279)]
    plt.plot(range(1,279),user_percentile)
    plt.show(block=True)

if __name__=='__main__':
    main()