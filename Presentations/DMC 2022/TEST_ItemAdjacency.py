# -*- coding: utf-8 -*-
"""
Created on Mon Apr 18 17:18:29 2022

@author: conmc
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import networkx as nx

def ReadData():
    df_items=pd.read_csv('items.csv',delimiter='|')
    df_orders=pd.read_csv('orders.csv',delimiter='|')
    df_categories=pd.read_csv('category_hierarchy.csv',delimiter='|')
    return(df_items,df_orders,df_categories)

def ItemNetwork(items):
    groups=items.groupby('feature_1').groups
    ind_rows=[]
    ind_cols=[]
    for k in range(-1,11):
        temp=list(groups[k])
        n_temp=np.size(temp)
        for i in temp:
            temp.remove(i)
            for j in range(0,n_temp-1):
                ind_rows.append(i)
                ind_cols.append(temp[j])
            temp.append(i)
    ind_rows=np.array(ind_rows)
    ind_cols=np.array(ind_cols)
    return(ind_rows,ind_cols)

def ItemEdgelist(items,feats):
    item_groups=items.fillna(-1).groupby(feats,dropna=False)
    edgelist=[]
    group_size=[]
    if feats=='feature_1':
        feats_list=range(-1,11)
    elif feats=='feature_2':
        feats_list=[0,1,2,3]
    elif feats=='feature_3':
        feats_list=range(-1,539)
    elif feats=='feature_4':
        feats_list=[-1,0,1,2,3,4]
    elif feats=='feature_5':
        feats_list=range(-1,191)
    else:
        feats_list=item_groups.groups.keys()
    for k in feats_list:
        temp=list(item_groups.get_group(k).iloc[:,0])
        group_size.append(len(temp))
        for i in temp:
            temp.remove(i)
            for j in temp:
                edgelist.append((i,j))
    return(edgelist,feats_list,group_size)

def main():
    items,orders,categories = ReadData()
    # N_items=max(items.iloc[:,0])
    # ind_rows,ind_cols=ItemNetwork(items)
    edgelist,feats_list,group_size=ItemEdgelist(items,['feature_1','feature_2','feature_3','feature_4','feature_5','categories'])
    print('Number of unique groups: ', len(feats_list))
    print('Size of largest unique group: ', max(group_size))
    plt.hist(group_size,bins=max(group_size))
    plt.show(block=True)
    plt.plot(range(0,len(group_size)),group_size.sort())
    plt.show(block=True)
    # G=nx.from_edgelist(edgelist)
    # nx.draw(G)

if __name__=='__main__':
    main()