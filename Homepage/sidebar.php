<?php
echo '
<div class="sidebar" style="background-color: #cf0063;">
  <div class="sidebar-background" style="background-image: url(img/unige_logo_vert.svg);" title="Sidebar">
    <a href="index.html" title="Home">
      <i class="fas fa-home fa-fw"></i>Home
    </a>
    <a href="publications.html" title="Publications">
      <i class="fas fa-copy fa-fw"></i>Publications
    </a>
    <a href="presentations.html" title="Presentations">
      <i class="fas fa-photo-video fa-fw"></i>Presentations
    </a>
    <a href="ongoing.html" title="Ongoing">
      <i class="fas fa-microscope fa-fw"></i>Ongoing
    </a>
    <a href="interactive.html" title="Interactive">
      <i class="fas fa-code fa-fw"></i>Interactive
    </a>
  </div>
</div>
';
?>