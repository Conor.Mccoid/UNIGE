<?php
echo '<div class="bottombox" alt="footer" title="Footer">
  <div class="bottombox-column" title="logo">
    <img src="img/unige_logo.svg">
  </div>
  <div class="bottombox-column">
    <h3>Address</h3>
    <p>Office 605</p>
    <p>Rue du Conseil-General 7-9</p>
    <p>1205 Geneve</p>
    <p>Switzerland</p>
  </div>
  <div class="bottombox-column">
    <h3>Contact</h3>
    <p><i class="fas fa-envelope"></i> conor.mccoid@unige.ch</p> <br>
    <!-- <p><i class="fas fa-phone"></i> +41 22 379 ?? ??</p> -->
  </div>
  <div class="bottombox-column">
    <h3>Academic Profiles</h3>
    <p><i class="fab fa-orcid"></i> <a href="https://orcid.org/0000-0002-2592-5005">ORCID</a></p> <br>
  </div>
</div>';
?>