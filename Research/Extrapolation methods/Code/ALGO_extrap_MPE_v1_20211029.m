function [x_out,r_out]=ALGO_extrap_MPE_v1_20211029(F,X)
% MPE performs minimal polynomial extrapolation using Arnoldi iteration

[d,k] = size(F);
q0 = F(:,1);
H(1,1)=norm(q0);
q0 = q0/H(1,1);
Q = zeros(d,k); Q(:,1) = q0;
x_out=zeros(d,k); x_out(:,1)= X(:,1);
r_out=zeros(1,k); r_out(1)=norm(F(:,1));
for j=2:k
    Q(:,j) = F(:,j);
    for i=1:j-1
        H(i,j) = Q(:,j)'*Q(:,i);
        Q(:,j) = Q(:,j) - H(i,j)*Q(:,i);
    end
    H(j,j)=norm(Q(:,j)); Q(:,j)=Q(:,j)/H(j,j);
    u=zeros(j,1); u(end)=1; u=H\u; u=u/sum(u);
    x_out(:,j)=X(:,1:j)*u;
    r_out(j)=norm(F(:,1:j)*u);
end