function [u1,u2,G] = ALGO_extrap_AltS_v2_20210713(A,b,f,u2,B1,B2,ind1,ind2,n,m,x)
% ALTS performs alternating Schwarz, in a compact implementation
%   [u1,u2] = AltS(?)

N=length(A); I=speye(N);
R1=I(ind1,:); P1=I(:,ind1);     % restriction and prolongation operators
R2=I(ind2,:); P2=I(:,ind2); 
A1=R1*A*P1; b1=R1*b;            % restrict and prolong A and b
A2=R2*A*P2; b2=R2*b;
C1=R1*A*B1*P2; C2=R2*A*B2*P1;   % info passed between subdomains
u1=zeros(N,1); u1=u1(ind1);     % initial guess for u1
for i=1:n
  for j=1:m
    u1 = A1 \ (f(u1) - C1*u2 - b1);
  end
  for j=1:m
    u2 = A2 \ (f(u2) - C2*u1 - b2);
  end
    
  if nargin==11
      plot(x(ind1),u1,'r',x(ind2),u2,'b')
      axis([0,1,-1,0])
      pause(0.1)
  end
  if nargout==3
    G=B1*P2*u2;
  end
end