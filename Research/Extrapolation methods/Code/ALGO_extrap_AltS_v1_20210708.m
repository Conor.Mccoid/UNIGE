function [u1,u2] = ALGO_extrap_AltS_v1_20210708(F1,F2,u2,N)
% ALTS Performs alternating Schwarz
%   [u1,u2] = AltS(F1,F2,u2,N) returns u1 and u2, the solution on the first
%   and second subdomains, respectively, using N iterations of the
%   alternating Schwarz method. The functionals F1 and F2 compute the
%   solutions on the respective subdomains by taking the solution on the
%   previous subdomain for its boundary data.
%
%   nb: This implementation is not designed for efficiency, as each call of
%   F1 and F2 will contain extraneous calculations. An efficient
%   implenentation would not subdivide the algorithm into base components,
%   but this makes it easier to try out different problems.

for i=1:N
  u1=F1(u2);
  u2=F2(u1);
end