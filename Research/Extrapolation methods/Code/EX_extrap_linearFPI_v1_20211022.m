% Example - Extrapolation: fixed point iteration on linear function
%   solve the equation x = Ax + b for random A and b

d = 5; n=2*d;
A = rand(d); 
A = A/norm(A); 
b = rand(d,1); x_true = -(A - eye(d)) \ b;
X = b;
for i=1:n
    X(:,i+1) = A*X(:,i) + b;
end
F = X(:,2:end) - X(:,1:n);

[x_v1,r_v1] = ALGO_extrap_MPE_v1_20211022(F,X(:,1:n));
[x_v2,r_v2] = ALGO_extrap_MPE_v2_20211029(F,X(:,1:n));
% [x_v2,r_v2] = ALGO_extrap_MPE_v3_20211119(F,X(:,1:n));
x = x_v1(:,end);
Error_abs= norm(x - x_true)
Error_rel= Error_abs/norm(x_true)
Residual = norm(A*x + b - x)

Error_v1 = vecnorm(x_v1 - x_true); res_MPE_v1 = vecnorm(A*x_v1-x_v1+b);
Error_v2 = vecnorm(x_v2 - x_true);

[x_GMRES,~,~,~,res_GMRES]=gmres(A-eye(d),-b,d,eps,d,eye(d),eye(d),X(:,1));

figure(1)
semilogy(1:n,Error_v1,'b*--',1:length(Error_v2),Error_v2,'k.--')
xlabel('Iteration')
ylabel('Error in solution')
title('Error vs iteration')
legend('MPE w/ Arnoldi','MPE w/ HH,Givens')

figure(2)
semilogy(1:n,res_MPE_v1,'b*--',1:length(r_v2),r_v2,'k.--',1:length(res_GMRES),res_GMRES,'ro')
xlabel('Iteration')
ylabel('Residual')
title('Residual vs iteration')
legend('MPE w/ Arnoldi','MPE w/ HH,Givens','GMRES')