function [x_out,r_out]=ALGO_extrap_MPE_v3_20211119(F,X)
% MPE performs minimal polynomial extrapolation using Arnoldi iteration
% with Householder reflections

[d,k] = size(F); k=min(d+1,k);
R=F; H=zeros(d,k); % Q=I;
x_out=zeros(d,k); r_out = zeros(k,1);
for i=1:k
    if i<=d
        w = SUB_extrap_Householder_v1_20211119(R(:,i),i);
        R = R - 2*(w*w')*R; % Q = Q - 2*Q*(w*w');
    end
    if i==1
        u=1;
    elseif i==2
        v = zeros(d+1,1); v(1)=-R(1,1); G=eye(d);
        H(1,1)=R(1,2)-R(1,1); u=v(1)/H(1,1); u=[1;u]-[u;0];
    elseif i>2
        ind=1:i-1; indg=i-2:i-1;
        H(ind,i-1)=G(ind,ind)*(R(ind,i)-R(ind,i-1)); H(i-1,i-2)=R(i-1,i-1);
        g = SUB_extrap_Givens_v1_20211124(H,i-2);
        H(indg,indg)=g*H(indg,indg); v(indg)=v(i-2)*g(:,1);
        u=H(ind,ind)\v(ind); u = [1;u] - [u;0];
        G(indg,:)=g*G(indg,:);
    end
    x_out(:,i) = X(:,1:i)*u;
    r_out(i) = norm(F(:,1:i)*u);
end
end

% note: this is equivalent to previous formulation of HH reflections, with
% no significant change in results; also, unsure how to apply GMRES for k
% greater than d

function g = SUB_extrap_Givens_v1_20211124(A,i)
% GIVENS generates 2x2 Givens rotation matrix, zeroing out A(i+1,i)

g=A(i:i+1,i); g=g/norm(g);
g=[g(1), g(2);
  -g(2), g(1)];
end