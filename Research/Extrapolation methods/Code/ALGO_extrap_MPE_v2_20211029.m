function [x_out,r_out]=ALGO_extrap_MPE_v2_20211029(F,X)
% MPE performs minimal polynomial extrapolation

[d,k] = size(F);
x_out=zeros(d,k); x_out(:,1)=X(:,1);
r_out=zeros(1,k); r_out(1)=norm(F(:,1));
for j=2:k
  u=[ones(1,j);F(:,1:(j-1))'*F(:,1:j)] \ [1; zeros(j-1,1)];
  x_out(:,j)=X(:,1:j)*u; r_out(j)=norm(F(:,1:j)*u);
end