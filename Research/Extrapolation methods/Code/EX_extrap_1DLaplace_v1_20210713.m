% EX_extrap_1DLaplace_v1_20210713
%   1DLAPLACE solves the 1D Laplace problem on [0,1] using alternating Schwarz

N=64; x=1:N-1; x=x'/N; M=N-1;
L=ones(M,1); L=spdiags([L,-2*L,L],0:2,M,M+2)*N^2;
B=[-L(:,1),sparse(M,M),-L(:,end)]; L=L(:,2:end-1);
u=ones(N+1,1); f=1;
vexact=-x.^2/2 + x/2 + 1;

v=SUB_extrap_SubdomainSolve_v1_20210713(L,B,f,u,'linear');
figure(1)
plot(x,v,'r*-',x,vexact,'k')

% Subdomains: [0,a] and [b,1]
a=0.6; b=0.4;
ind1=x<a; ind2=x>b; l1=sum(ind1); l2=M-sum(ind2);
I=speye(M); P1=I(:,ind1); R1=I(ind1,:); B1=sparse(l1+1,l1+1,1,M,M)*P2;
            P2=I(:,ind2); R2=I(ind2,:); B2=sparse(l2,l2,1,M,M)*P1;
F1=@(u2) SUB_extrap_SubdomainSolve_v1_20210713(R1*L*P1,-R1*L*B1,R1*(f-B*u),u2,'linear');
F2=@(u1) SUB_extrap_SubdomainSolve_v1_20210713(R2*L*P2,-R2*L*B2,R2*(f-B*u),u1,'linear');

gamma=-5:0.01:5; G=zeros(size(gamma));
for k=1:length(gamma)
  u2=gamma(k)*ones(sum(ind2),1);
  [u1,u2] = ALGO_extrap_AltS_v1_20210708(F1,F2,u2,1);
  u2=P2*u2; G(k)=u2(l1+1);
end
plot(gamma,G,'k.--')