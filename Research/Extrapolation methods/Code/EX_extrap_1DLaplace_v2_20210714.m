% EX_extrap_1DLaplace_v2_20210714
%   1DLAPLACE solves the 1D Laplace problem on [0,1] using alternating Schwarz

N=64; x=1:N-1; x=x'/N; M=N-1;
L=ones(M,1); L=spdiags([L,-2*L,L],0:2,M,M+2)*N^2;
B=[L(:,1),sparse(M,M),L(:,end)]; L=L(:,2:end-1);
u=zeros(N+1,1); % BCs

% Subdomains: [0,a] and [b,1]
a=0.6; b=0.4;
ind1=x<a; ind2=x>b; l1=sum(ind1); l2=M-sum(ind2);
B1=sparse(l1+1,l1+1,1,M,M);
B2=sparse(l2,l2,1,M,M);

gamma=-2:0.01:2; G=zeros(size(gamma)); alpha=3.6;
for i=1:length(gamma)
  u2=gamma(i)*ones(M,1); u2=u2(ind2);
  [u1,u2,bc] = ALGO_extrap_AltS_v2_20210713(L,B*u,@(v) sin(alpha*v),...
                                       u2,B1,B2,ind1,ind2,1,100);
  bc=bc(~ind1); G(i)=bc(1);
end

plot(gamma,G,'b-')
hold on
for k=-1:0.25:1
  plot(gamma,k*sqrt(abs(gamma))+gamma,'k--')
end
hold off
axis([-2,2,-2,2])
axis square
xlabel('\gamma')
ylabel('G(\gamma)')