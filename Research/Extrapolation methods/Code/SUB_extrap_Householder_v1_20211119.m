function w = SUB_extrap_Householder_v1_20211119(v,i)
% HOUSEHOLDER calculates component of Householder reflection

w = v(i:end);
w(1) = w(1) + sign(w(1))*norm(w);
w = w/norm(w); w = [zeros(i-1,1); w];

end