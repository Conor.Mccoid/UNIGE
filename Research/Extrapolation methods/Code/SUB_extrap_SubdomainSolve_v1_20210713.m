function v = SUB_extrap_SubdomainSolve_v1_20210713(A,B,f,u,type)
% SUBDOMAINSOLVE solves problem on a given subdomain
%   v=SubdomainSolve(A,B,f,u,type) gives the solution v to the equation
%   Av=Bu-f. If type='linear' then this is solved directly. If
%   type='nonlinear' then f depends on v and the equation is solved
%   iteratively. (add a choice of iterative solvers?)

if strcmp(type,'linear')
  m=1; f=@(~) f;
elseif strcmp(type,'nonlinear')
  m=20;
end
Bu=B*u; v=zeros(size(u));

for i=1:m
  v = A \ (Bu - f(v));
end