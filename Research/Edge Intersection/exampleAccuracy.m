N=1000;
error_geo=zeros(N,1); error_free=error_geo; error_PANG=error_geo; error_SH=error_geo;
t_geo=error_geo; t_free=error_geo; t_PANG=error_geo; t_SH=error_geo;
aa=pi*linspace(0,1,N+2); aa=aa(2:end-1);

eps_array = eps * 2.^(-4);
eps_contours = zeros(N,length(eps_array));

profile on

for i = 1:N
    a=aa(i);
%     d=0.5*sqrt(1-0.5*(1-cos(a)));
    d=0.5;
    V=[0,1,cos(a);0,0,sin(a)]; % triangle to make reference
    U=d*[cos(a/2),cos(a/2 + 2*pi/3),cos(a/2 + 4*pi/3);
         sin(a/2),sin(a/2 + 2*pi/3),sin(a/2 + 4*pi/3)];
    
    pause(0)
    tic
    [P,Q,R,~] = TriIntersect_20210315(U,V);
    t_geo(i)  = toc;
    P_geo     = SortAndRemoveDoubles([P Q R]);
    area_geo  = polyarea(P_geo(1,:)',P_geo(2,:)');
    
    pause(0)
    tic
    [P,Q,R,~] = TriIntersectRF_20210315(U,V);
    t_free(i) = toc;
    P_free    = SortAndRemoveDoubles([P Q R]);
    area_free = polyarea(P_free(1,:)',P_free(2,:)');
    
    pause(0)
    tic
    P         = PANG_Intersect(U,V);
    t_PANG(i) = toc;
    P_PANG    = SortAndRemoveDoubles(P);
    area_PANG = polyarea(P_PANG(1,:)',P_PANG(2,:)');
    
    pause(0)
    tic
    P         = sutherlandHodgman(U',V');
    t_SH(i)   = toc;
    P_SH      = SortAndRemoveDoubles(P);
    area_SH   = polyarea(P_SH(:,1),P_SH(:,2));
    
    area_exact= d^2 * sin(a/2) * sin(pi/6) / sin(pi - pi/6 - a/2);
    if cos(a/2)<d
        area_exact= area_exact - (d-cos(a/2))^2*tan(pi/6);
    end

    error_geo(i) = abs(area_geo-area_exact)/abs(area_exact);
    error_free(i)= abs(area_free-area_exact)/abs(area_exact);
    error_PANG(i)= abs(area_PANG-area_exact)/abs(area_exact);
    error_SH(i)  = abs(area_SH-area_exact)/abs(area_exact);
    
    eps_contours(i,:) = eps_array/abs(area_exact);

%     triU = [U, U(:,1)];
%     triV = [V, V(:,1)];
%     plot(triU(1,:),triU(2,:),'b-',triV(1,:),triV(2,:),'r-')
%     patch(P_free(1,:),P_free(2,:),'m')
%     axis([-1,1,-1,1])
%     axis square
end

profile report

%%
figure(1)
subplot(1,2,1)
semilogy(aa/pi,error_geo,'r*',aa/pi,error_free,'bo',aa/pi,error_PANG,'ms',aa/pi,error_SH,'g^',aa/pi,eps_contours,'k-')
xlabel('\alpha/\pi')
ylabel('Rel. error')
legend('TriIntersect','TriIntersectRF','PANG','Sutherland-Hodgman','2^{-4} \epsilon_{m}/ Exact area')
axis square
% ylabel('Abs. error')
set(gca,'FontSize',20,'linewidth',2)
% 
% figure(2)
% plot(aa/pi,error_geo>error_free,'k^')
% xlabel('\alpha/\pi')
% ylabel('Geometry<RefFree')

figure(1)
subplot(1,2,2)
semilogy(aa/pi,t_geo,'r*',aa/pi,t_free,'bo',aa/pi,t_PANG,'ms',aa/pi,t_SH,'g^')
xlabel('\alpha/\pi')
ylabel('Computation time')
legend('TriIntersect','TriIntersectRF','PANG','Sutherland-Hodgman')
axis square
set(gca,'FontSize',20,'linewidth',2)

a=aa(round(N/3));
V=[0,1,cos(a);0,0,sin(a)]; % triangle to make reference
U=d*[cos(a/2),cos(a/2 + 2*pi/3),cos(a/2 + 4*pi/3);
         sin(a/2),sin(a/2 + 2*pi/3),sin(a/2 + 4*pi/3)];
figure(2)
subplot(1,2,1)
PlotMesh(V,[1,2,3,2,2,2],'r');
PlotMesh(U,[1,2,3,2,2,2],'b');
PANG2_20210118(V,[1,2,3,2,2,2],U,[1,2,3,2,2,2]);
hold on
ang([0,0],0.2,[0,pi/3],'k-');
text(0.075,0.1,'\alpha','Fontsize',20);
hold off

a=aa(round(2*N/3));
V=[0,1,cos(a);0,0,sin(a)]; % triangle to make reference
U=d*[cos(a/2),cos(a/2 + 2*pi/3),cos(a/2 + 4*pi/3);
         sin(a/2),sin(a/2 + 2*pi/3),sin(a/2 + 4*pi/3)];
figure(2)
subplot(1,2,2)
PlotMesh(V,[1,2,3,2,2,2],'r');
PlotMesh(U,[1,2,3,2,2,2],'b');
PANG2_20210118(V,[1,2,3,2,2,2],U,[1,2,3,2,2,2]);
hold on
ang([0,0],0.2,[0,2*pi/3],'k-');
text(0.075,0.1,'\alpha','Fontsize',20);
hold off