function [P,Q,R,n]=TriIntersectRF(U,V)
% TRIINTERSECTRF intersection of two triangles
%   [P,Q,R,n] = TriIntersectRF(U,V) produces the vertices of U inside V (P), the
%   intersections between the edges of U and V (Q), the vertices of V
%   inside U (R) and the neighbours of U that also intersect V (n).
%   The result is the same as TriIntersect(U,V) up to error on the order of
%   machine epsilon. The calculations are performed without change of
%   coordinates to a reference triangle.

W=V(:,[2,3,1])-V; % vectors between vertices of V
n=zeros(1,3); indR=zeros(3); indP=ones(3);
Q=zeros(2,9); indQ=zeros(1,9);
for i=1:3
  j=mod(i,3)+1; w=W(:,i); wt=[-w(2);w(1)]; % find w and vector perp. to w
  if wt'*W(:,j)<0 % if V is not on + side of w
    wt=-wt; % change sign of wt
  end
  qp=[w,wt]\(U-V(:,i)); indP(i,:)=sign(qp(2,:))>=0;
  for k=1:3
    l=mod(k,3)+1; m=k+3*(i-1);
    if indP(i,k)~=indP(i,l) % vertices on opposite sides of edge?
      q0=(qp(1,k)*qp(2,l)-qp(1,l)*qp(2,k))/(qp(2,l)-qp(2,k));
      if q0<0
        indR(i,[i,j])=indR(i,[i,j])+[0.25,0.75];
      elseif q0>1
        indR(i,[i,j])=indR(i,[i,j])+[0.75,0.25];
      else
        indR(i,[i,j])=indR(i,[i,j])+[0.75,0.75];
        Q(:,m)=V(:,i)+q0*w; indQ(m)=1; n(k)=1; % nhbrs of U intersect V
      end
    end
  end
end
[indP,indR]=VertexGuard(indP([1,3,2],:),indR([1,3,2],:));
P=U(:,indP==1); Q=Q(:,indQ==1); R=V(:,indR==1);
end