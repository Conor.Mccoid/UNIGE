function [P,Q,R,n]=TriIntersect(U,V)
% TRIINTERSECT intersection of two triangles
%   [P,Q,R,n]=TriIntersect(U,V) gives the vertices of U inside V (P),
%   intersections between the edges of U and those of V (Q), the vertices
%   of V inside U (R), and the neighbours of U that also intersect V (n).
v1=V(:,2)-V(:,1);               % vector between V1 and V2
v2=V(:,3)-V(:,1);               % vector between V1 and V3
X=[v1,v2]\(U-V(:,1));           % Step 1: change of coordinates
Q=zeros(2,9); indQ=zeros(1,9); n=zeros(1,3); indP=ones(3); indR=indP;
for i=1:3                       % Step 2: select ref. line
    ind=(1:3) + 3*(i-1);        % indices of Q for this ref. line
    [Q(:,ind),indQ(ind),indR(i,:),indP(i,:)]=EdgeIntersect(X,i);
    n=max(n,indQ(ind));         % update nhbr index
end
[indP,indR]=VertexGuard(indP,indR); % Step 3: vertices of X in Y
P=U(:,indP==1);                 % vertices of U in V
Q=V(:,1) + [v1,v2]*Q(:,indQ==1);% Step 4: reverse change of coordinates
R=V(:,indR==1);                 % vertices of V in U
end

function [Q,ind,indR,sp]=EdgeIntersect(X,edge)
% EDGEINTERSECT intersection of edges of X with reference line of Y
%   [Q,ind,indR,sp]=EdgeIntersect(X,edge) calculates the intersections
%   between all edges of X and the reference line of Y indicated by edge
%   and stores the values in Q. It also calculates Boolean indicators for
%   neighbours of X intersecting Y (ind), vertices of Y inside X (indR),
%   and vertices of X inside Y (sp).
if edge==1                                % v1
    p=X(2,:); q=X(1,:);                     % parametrization of edge
    a=[1;0]; b=[0;0];                       % inversion of parametrization
    R1=1; R2=2;                             % vertices of V on edge
elseif edge==2                            % v2
    p=X(1,:); q=X(2,:); a=[0;1]; b=[0;0]; R1=1; R2=3;
elseif edge==3                            % line connecting v1 and v2
    p=1-X(1,:)-X(2,:); q=0.5*(1-X(1,:)+X(2,:)); a=[-1;1]; b=[1;0]; R1=2; R2=3;
end
Q=zeros(2,3); ind=zeros(1,3); indR=ind; sp=sign(p)>=0;
for i=1:3
    j=mod(i,3)+1;
    if sp(i)~=sp(j)                         % vertices on opposite sides of edge?
        q0=(q(i)*p(j)-q(j)*p(i))/(p(j)-p(i)); % calculate intersection
        if q0<0
            indR([R1,R2])=indR([R1,R2])+[0.25,0.75];
        elseif q0>1
            indR([R1,R2])=indR([R1,R2])+[0.75,0.25];
        else
            indR([R1,R2])=indR([R1,R2])+[0.75,0.75];
            Q(:,i)=q0*a+b; ind(i)=1;
        end
    end
end
end