function [indP,indR]=VertexGuard(matP,matR)
% VERTEXGUARD protection against certain compound intersection errors
%   [indP,indR]=VertexGuard(matP,matR) produces the indices of the vertices
%   of U and V, respectively, that make up the polygon of intersection. A
%   value of 1 indicates that the vertex is part of the polygon. Any other
%   value indicates it is not.
indP=prod(matP);                        % product of sign(p)
indR=double(max(matR==1));              % 0,1 in [q01,q02]?
T=[1,-1,0;1,0,-1;0,1,-1];
for i=1:3
  if indR(i)~=1
    indR(i)=round(sum(matR(:,i)));      % 0-1 or 1-1 configuration
    if indR(i)==2                       % 1-3, 3-1, 0-3 or 3-0
      indR(i)=max(T(i,:)*(matP-matR(:,i))==0);
    elseif indR(i)==3                   % 3-3
      indR(i)=max(matR(:,i)'*matP==0);
    end
  end
end
end