function [P,Q,R,n]=TriIntersect_20210315(U,V)
% TRIINTERSECT intersection of two triangles
%   [P,Q,R,n]=TriIntersect(U,V) gives the vertices of U inside V (P),
%   intersections between the edges of U and those of V (Q), the vertices
%   of V inside U (R), and the neighbours of U that also intersect V (n).
v1=V(:,2)-V(:,1);               % vector between V1 and V2
v2=V(:,3)-V(:,1);               % vector between V1 and V3
X=[v1,v2]\(U-V(:,1));           % Step 1: change of coordinates
p=[X([2,1],:);1-X(1,:)-X(2,:)]; % values of the parameters p
sp=sign(p)>=0;                  % sign of the p values
D=cross([X(1,:);1-X(1,:);X(1,:)],[X(2,:);X(2,:);1-X(2,:)],2); % num.s of q0 and 1-q0
Q=zeros(2,9); indQ=zeros(1,9); n=zeros(1,3); indR=ones(3);
for i=1:3                       % Step 2: select ref. line
    ind=(1:3) + 3*(i-1);        % indices of Q for this ref. line
    [Q(:,ind),indQ(ind),indR(i,:)]=EdgeIntersect(p,sp,i,D(:,[3,1,2]));
    n=max(n,indQ(ind));         % update nhbr index
end
P=U(:,prod(sp)==1);             % vertices of U in V
Q=V(:,1) + [v1,v2]*Q(:,indQ==1);% Step 4: reverse change of coordinates
R=V(:,max(indR==1));            % vertices of V in U
end

function [Q,ind,indR]=EdgeIntersect(p,sp,edge,D)
% EDGEINTERSECT intersection of edges of X with reference line of Y
%   [Q,ind,indR]=EdgeIntersect(p,sp,edge,D) calculates the intersections
%   between all edges of X and the reference line of Y indicated by edge
%   and stores the values in Q. It also calculates Boolean indicators for
%   neighbours of X intersecting Y (ind) and vertices of Y inside X (indR).
if edge==1                                % v1
  a=[1;0]; b=[0;0];                         % inversion of parametrization
  R1=1; R2=2;                               % vertices of V on edge
  D1=D(R1,:); D2=D(R2,:);                   % relevant numerators
  E0=2; E1=3;                               % adjacent ref lines
elseif edge==2                            % v2
  a=[0;1]; b=[0;0]; R1=1; R2=3; D1=-D(R1,:); D2=-D(R2,:); E0=1; E1=3;
elseif edge==3                              % line connecting v1 and v2
  a=[-1;1]; b=[1;0]; R1=2; R2=3; D1=-D(R1,:); D2=D(R2,:); E0=1; E1=2;
end
Q=zeros(2,3); ind=zeros(1,3); indR=ind; D2=sign(D2)>=0;
for i=1:3
  j=mod(i,3)+1;
  if sp(edge,i)~=sp(edge,j)                 % vertices on opposite sides of edge?
    q0=D1(i)/(p(edge,j)-p(edge,i));         % calculate intersection, eqn (2)
    if (q0<0 && sp(E0,i)~=sp(E0,j)) || (~sp(E0,i) && ~sp(E0,j)) % see Sec. 4.1
      indR([R1,R2])=indR([R1,R2])+[0.25,0.75];
    elseif (D2(i)~=sp(edge,j) && sp(E1,i)~=sp(E1,j)) || (~sp(E1,i) && ~sp(E1,j))
      indR([R1,R2])=indR([R1,R2])+[0.75,0.25];
    else
      indR([R1,R2])=indR([R1,R2])+[0.75,0.75];
      Q(:,i)=q0*a+b; ind(i)=1;
    end
  end
end
end