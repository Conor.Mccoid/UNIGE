%% exampleQ
% It's time to look carefully at the underlying calculation of the
% intersection point q0. Let's start with a Monte Carlo analysis of where
% q0 is depending on the transformation.

X=zeros(2,3);
X(:,1)=[-eps;-eps];
X(:,2)=[1;eps];
X(:,3)=[eps;1];
% qi=-eps; qj=1;
% pi=-eps; pj=eps;
% q0=(qi*pj - qj*pi)/(pj-pi);
iter=1e4;
q=zeros(1,iter); p=q;
clf
for i=1:iter
    A =2*rand(2,2)-1;
    v0=2*rand(2,1)-1;
    U0=A*X+v0;
    U =A\(U0-v0);
    q(i)=(U(1,1)*U(2,2)-U(1,2)*U(2,1))/(U(2,2)-U(2,1));
%     q(i)=cond(A);
%     p(i)=det(U)/(U(1,1)-U(1,2));
    p(i)=(U(2,1)*U(1,3)-U(2,3)*U(1,1))/(U(1,3)-U(1,1));
    a=[norm(A(:,1)),norm(A(:,2)),norm(A(:,2)-A(:,1))];
%     p(i)=prod(a./(sum(a)-2*a));     % from a stackexchange code golf challenge
    if q(i)<0 && p(i)<0 && sign(U(2,2))~=sign(U(2,1)) && sign(U(1,3))~=sign(U(1,1))
        PlotMesh([v0,A+v0],[1,2,3,2,2,2],'r')
        title(num2str(U))
        pause
    end
end
scatter(q,p)
% scatter(log(p),log(q))
% axis equal

%%
% The previous analysis has indicated some interesting points. First, there
% is a strong positive correlation between condition number and aspect
% ratio in the triangles. There is an envelope connecting the two, so that
% knowing one will give indications of the other.

% Second, the scattering of q and p intersections for the same line is
% t-shaped, meaning only one of these can have significant error.

% Third, the vast majority of faults in the intersections are comorbid with
% sign(pi)==sign(pj). Therefore, most can be discarded.

% It is clear that we must focus on the reference frame instead of the
% transformation. We then perform the same kind of analysis whilst
% perturbing one of the points in the reference frame.

% intersect=@(X) (X(1,1)*X(2,2)-X(1,2)*X(2,1))/(X(2,2)-X(2,1));

X=zeros(2,2);
delta=1e-5;
X(:,1)=[1+10*delta;-delta];
X(:,2)=[0.75;delta];
q0=TEST_Intersect(X);
% X(:,3)=[eps;1];
iter=1e4;
q=zeros(1,iter);
for i=1:iter
    t=2*delta*rand(2,1) - delta;
    q(i)=TEST_Intersect(X+[zeros(2,1),t]);
%     if q(i)>1
%         plot(X(1,:),X(2,:),'r--',q(i),0,'kx')
%         pause
%     end
end
histogram(q,'BinWidth',eps)