for a=1:0.01:10
  alpha=0.01:0.01:pi;
  kA=zeros(size(alpha)); ex=kA; min=kA;
  for i=1:length(alpha)
    kA(i)=cond([a,cos(alpha(i));0,sin(alpha(i))]);
    x=alpha(i)/2;
    ex(i)=max(a,1/a)*max(cot(x),tan(x));
    min(i)=abs(max(a,1/a)./sin(alpha(i)));
  end

  semilogy(alpha,kA,'r.--',alpha,ex,'k-',alpha,min,'b--')
  xlabel('\alpha')
  ylabel('cond(A)')
  legend('k(A)','cos(\alpha/2)/sin(\alpha/2)','1/sin(\alpha)')
  pause(0.1)
end

mean(kA./ex)

% Conjecture: k(A) is always between a/sin(alpha/2) and a/2sin(alpha/2)