\documentclass{article}

\usepackage{xcolor}
\usepackage{changepage}

\newenvironment{comment}{
\vspace{1em}
\color{blue}
\begin{adjustwidth}{3em}{3em}
}
{\end{adjustwidth}
\vspace{1em}}

\begin{document}
Referee: 1

Recommendation: Needs Major Revision

Comments:
In their research paper (manuscript ID: TOMS-2020-0056) the authors are concerned with a novel approach for triangle-triangle intersection. The focus lies on the robustness of their algorithm. The presented results are new and they are of theoretical and practical interest.

Though the results are worth to be published, in its current form, I advise against the acceptance for submission. The paper requires major revision.
This advice is reasoned foremost in some issues regarding the proof of robustness. The main issue is the following:

• By combining the results in Section 6 the fundamental statement seems to be that the output of the algorithm will always correspond to one of the triangle-triangle intersections given in Fig. 9. Though I can follow the argument in Section 6 to some extend, I miss a well structured proof. How do the results in the previous section ensure that only the type-1 and type-2 rewrites in Fig. 11 are possible? In Section 6.1 it is described how the degenerate cases are handled by the algorithm leading again to one of the configurations in Fig. 9.
In particular, I want to consider configuration G in Fig. 9, where A, B shall be the vertices of X in Y and D the single vertex of Y in X. Let us assume that C and D are very close to each other and that we run into the case accounted in Corollary 3, where D is considered (by the algorithm) to lie outside of X. I fail to see that this implies that one of the edges DE or DF have two intersections. Since $q_0^i$ are computed independently for every reference line, the values might lie outside [0 1] for both lines. Then we would have neither intersections nor an interior point in the neighborhood of D.
If this case cannot be caused by floating-point rounding errors in the computation of $q_0^i$, then this requires a formal proof.

\begin{comment}
The authors would like to thank the referee for pointing out this problematic example.
Indeed, we were too quick to discount most compound intersection errors.
We have reassessed how the intersections are calculated and found a succinct way to eliminate such problems.

In brief, intersection calculations are now paired for a given edge of $X$.
This means if there is an intersection error along an edge of $X$ a second intersection error occurs, preserving consistency.

To the point about a well-structured proof, we have added more discussion in Section 6 that should clarify the proof.
We have rigorously shown that the presented graphs are the only possible outcomes of the algorithm.
\end{comment}

Some other issues are:

• Just before Theorem 1 the authors write that their algorithm enforces consistency and then that this culminates to backward stability of their algorithm. At this point it is not clear how these statements are related. How is consistency meant here? What is meant by robustness of the algorithm? These things should be clarified/defined already in the introduction.

\begin{comment}
We use consistency by its definition, not as a mathematical term.
Two calculations are consistent if they can arise from the same set of data.
If they suggest two separate sets of data, they cannot be considered consistent.

We have clarified our definition of robustness by expanding the statement of Theorem 1 to be more precise.
The remainder of the paper is dedicated to explaining the connection between consistency and robustness.
\end{comment}

• On page 4 it is stated that affine transformations are numerically stable. However, the considered problem is a linear system. This system could be badly conditioned. A high condition number is only possible if an angle of the triangle is extremely small. This does not affect the robustness of the algorithm but should be mentioned and explained.

\begin{comment}
This was an overstatement.
We have clarified that we are concerned with affine transformations for triangles with reasonable aspect ratios and angles.
\end{comment}

• In the last sentence of Section 3.2 it is said that the redundant test is useful for the robustness of the algorithm. How can this be if all decisions are consistent? This needs some explanation or at least a reference to the respective results given in the paper.

\begin{comment}
The pairing of intersections described above now ensures this consistency.
While there is still redundancy the two tests now necessarily agree with one another, as explained in Section 5.2 in the new version of the manuscript.
One can remove the redundant test with an optimized implementation.
\end{comment}

• Lemma 1 and Corollary 1 are evident in the given form. Without the proof it is not clear that they are meant in regard to the decision of the algorithm. This should be made more precise.

\begin{comment}
Lemma 1 and Corollaries 1 and 2 have been combined and now refer more clearly to the problem at hand.
\end{comment}

• In the proof of Lemma 2, the equivalences seem to be derived under the assumption that d is neither equal to a nor to $\tilde{a}$.

\begin{comment}
We have removed Lemma 2 in favour of an exhaustive list of intersection errors.
However, to the point indicated, $d=a$ would be a degenerate case where the intersection is coincident with a vertex of $Y$.
Its equivalence to the case where $d \neq a$ is explained in Section 6.1.
\end{comment}

In addition, I would like to give some ideas on how to improve the readability of the paper and slightly shifting the focus for a better motivation of this research.

1) The authors of PANG claim that their algorithm is numerically robust. They compare their approach with the classical polygon clipping algorithm. I think that it would be very helpful for a quick lead-in into the topic to add a few lines of explanation. What does PANG do differently to the classical approach and why do the authors think that their algorithm is numerically stable? What is causing the issue in Fig. 1. Furthermore, in the introduction a number of algorithms for the intersection of polygons are cited. How do these algorithms compare in speed, consistency, robustness etc.? It is difficult to classify the proposed algorithm without any comparison to alternative approaches. In particular, it is not clear if the proposed algorithm can be generalized for higher dimensions.

\begin{comment}
We have added a paragraph after Theorem 1 to give a brief overview of the algorithm with reference to algorithms that use similar steps.
It is difficult to compare the robustness of these algorithms as each is likely to fail on a highly specific subset of triangle-triangle intersections.
For example, PANG failed when applied to nearly coincident triangles, whereas other algorithms may fail for triangles with disparate aspect ratios.
We have, however, added a comparison of computation times and accuracy as Section 8.1.
\end{comment}

2) If we just want to fix the issue of PANG discussed in the introduction, we could simply add some tolerances within the PointsOfXinY function. Then also vertices of X that lie slightly outside of Y might be detected as lying inside of Y, but these vertices do not change the overall shape and will be removed within the SortAndRemoveDoubles function if redundant. What is the advantage of the proposed approach? How does the count of floating-point operations compare to the original approach? Could we use the values of $q_0$ to find and reduce clustered vertices without using the SortAndRemoveDoubles function?

\begin{comment}
This idea was suggested in the original PANG paper, see Remark B.1 found therein.
The approach suggested in this paper has two advantages.
First, no tolerance and therefore no knowledge of what a reasonable tolerance would be is needed.
Second, we can prove it works.
While adding a tolerance is intuitively the correct thing to do, it has not been proven infallible.
\end{comment}

3) Section 2 and the Subsections 3.4, 3.5 could be combined and summarized in one section. The detailed description of the reference-free parametrization and its FLOP count seems to add little to the overall discussion. The Subsections 3.1-3.3 describe the actual intersection algorithm. It might be beneficial to connect the descriptions with the respective instructions in Program 1 and then summarize the algorithm as done in Section 4.

\begin{comment}
We have combined and pared down these sections.
Certain non-trivial details about the reference-free parametrization, such as the pairing of its intersection calculations (which are new additions to the revisions), warrant their inclusion.

We have added additional comments to Program 1 to connect the description of the algorithm with the code.
\end{comment}

Finally, at some places the language is used very figurative. Some text passages should be rephrased to make the corresponding statements more precise.

p.3: "To PointsOfXinY ... . To EdgeIntersections ..." - Functions are not aware of decisions.
\begin{comment}
We have changed the sentences to ``According to...'' to make clear that the functions are not reacting to decisions but are making determinations about the geometry.
\end{comment}
p.3: "The goal of this paper is to write ..." - Paper has no goal or writes something.
\begin{comment}
This sentence opener is ubiquitous in the literature and refers to the goals the authors' have in mind when writing the paper.
To avoid confusion the word `write' has been replaced with `formulate'.
\end{comment}
p.4: "The idea of consistency is the key for the new algorithms."
\begin{comment} %nb: any better way to say this?
This is a misquote.
The word `key' is used as an adjective and therefore is not preceded by the article `the'.
\end{comment}
p.5: "..., q is orthogonal to p, ..." - The transformation is orthogonal, but the parameters?
\begin{comment}
We have replaced this portion with ``...q increases orthogonally to p,...''.
\end{comment}
p.5: "... there are an infinite number of ..." - 'is'
% probably should reword this one
\begin{comment}
Good catch.
\end{comment}

p.5: "... the polygon of intersection ..."
% nb: review this?
\begin{comment}
Given that the polygon is the product of two intersecting triangles it can be described as being of intersection.
Alternatives we have tested to this phrasing have proven vague, unwieldy and inconsistent.
\end{comment}

p.6: "Proposition 1: ... under this algorithm." - The corresponding algorithm have not been given yet.
\begin{comment}
We've generalized the statement of the proposition to make clear it is the condition of the signs of $p_i$ and $p_j$ that is important and not the as-yet-unwritten algorithm.
\end{comment}
p.14: "... it is harder for a Y-in-X error to remove points ..." - Can something be hard/difficult for an error?
\begin{comment}
The sentence became defunct during revisions.
\end{comment}

Additional Questions:
Please help ACM create a more efficient time-to-publication process: Using your best judgment, what amount of copy editing do you think this paper needs?: Moderate

Most ACM journal papers are researcher-oriented. Is this paper of potential interest to developers and engineers?: Yes

\pagebreak

Referee: 2

Recommendation: Needs Minor Revision

Comments:
This paper presents a new algorithm for computing the intersection
polygon between two triangles. While polygon intersection is a well-known
problem, the inherent errors due to floating-point arithmetic might cause
algorithms to return ill-formed polygons. The authors present two
striking examples (Figures 1 and 16) where existing algorithms conclude
that two almost identical (non-degenerate) triangles have an empty
intersection. The idea of the authors is to write their algorithm in such
a way that the inferred geometry is always consistent. For example, if
a numerical computation indicates that a point is on a given side of
a line (whether that is correct or not), then no subsequent computations
should make it possible to deduce the opposite.

As far as I can tell, the presented algorithm (there are two variants, in
fact) is clever, useful, and worth publishing in TOMS. My main concern,
however, is that the paper is hard to follow. In fact, despite having
read the paper again and again, I am still not confident that the
algorithm is correct. Also, I dispute the validity of the main theorem,
which is that the algorithm is backward stable. But that is a minor
issue, because the algorithm would still be very useful even without that
claim. That is why I am only asking for a minor revision rather than
a major one.

So, back to the main concern with the paper. One usually says that
a figure is worth a thousand words, and given the geometry-oriented
nature of the paper, one would expect a lot of figures. And indeed, there
are quite a few. But they are actually making the article more confusing.
More often than not, I had to draw my own figures to understand the
proofs, because I could not make sense of the ones from the paper.

\begin{comment}
Thank you for these observations.
We have re-examined every figure and, with few exceptions, overhauled each to represent concrete examples.
All drawings made in Lemma 4 are now present in the paper, either in Figure 12 or Figure 20.

Many of the figures were drawn to be abstract and apply to the broadest possible set of situations.
In fact, the proof of Lemma 3, now Lemma 4, was designed under the expectation that the reader would draw the described figures.
We note that this has not been well received.
\end{comment}

Let us consider Figure 3 as an example. It is supposed to show the case
where a vertex of triangle Y is in triangle X. But on the figure, there
are no triangles (only four lines). The letters X and Y do not appear
either, despite the situation not being symmetric. Also, the article
mentions $q_0$ and $q_1$ (the position of the intersection points), yet they
do not appear on the figure. And so on, and so forth.

\begin{comment}
We have added the rest of the triangles and made the appropriate labels.
The figure in question is now numbered 4.
\end{comment}

Let me give another example, Figure 7. The caption of the figure states
that "the upper right-hand quadrant is the triangle Y". But why not just
draw this triangle and write Y next to it? Or what about Figure 5 which
is about AB, but only B appears, and it is up to the user to add A to the
figure? Or what about Figure 10 where the triangles have two right angles
each?! Sure, sometimes making a drawing more abstract helps to understand
the main idea. But here, such a level of abstraction just hinders the
reader. The authors should carefully review each of their figures to see
what they can do to make them clearer and more illustrative of the core
of the article.

\begin{comment}
We have replaced Figure 7 with Figure 9, listing all intersection errors with specific examples.
We have removed Figure 5 due to the related lemma and corollaries being condensed.
Figure 10, now Figure 13, no longer shows triangles with two right angles each.
We have restated the proof attached to this figure to rely less heavily on a graphic description.
\end{comment}

For Figure 9, the authors should indicate the various configurations in
the captions. If I did not get them wrong, that should be 0-3, 2-2, 4-1,
6-0, 2-1, 4-0, 2-2, 2-3, 4-2, 4-1. But I should not have had to compute
them by myself. Also, it would be great if the illegal configurations
could be also drawn (possibly with curved edges, as in Figure 13).
Moreover, the authors should decide whether they want to draw triangles
or graphs. The figures contain a mix of them. For instance, in 9D, points
are properly aligned; but in 9F, there is a triangle with six edges!
Again, that is a situation where too much abstraction does not help with
understanding. Indeed, Figure 9 is used to prove that the algorithm
cannot construct illegal geometries, so it should be visually immediate
that all the geometries are legal (yet a triangle with six edges is
certainly not).

\begin{comment}
Thank you for the excellent suggestion.
We have elected to use a table to connect the graphs and pairings.
This puts all the information in one compact place and shows how the four restrictions help to eliminate impossible pairings.
We have added the illegal graph configurations as Appendix B.
Where possible we have made the graphs look like triangles.
Obviously this is impossible for the illegal configurations.
\end{comment}

For Figure 12, since the nodes are laid out on a grid depending on their
polygon size and their score, these data should appear explicitly.
Moreover, it does not help the reader that some nodes are "misplaced".
Also, as for Figure 9, it would be great if invalid configurations
appeared on the graph, just to make they have all been accounted for. In
particular, as I understand it, it is important for the whole proof that
this graph shows that all the (un)reachable invalid configurations break
the "sheltered polygon" property.

\begin{comment}
We've added labels to the columns and rows of this figure.
Based on this comment we have added further discussion to Section 6.
This figure is intended to show we have accounted for the errors, not the configurations.
Lemma 4 and Corollary 1 are intended to account for all configurations.
The respective statements should now make this clear.
\end{comment}

I have a hard time with Figure 6b. I understand that this is related to
Figures 11b and 14. But I cannot quite grasp what this is about. This
time, it is not just an issue with the figures but it seems something is
missing in the description to get a good intuition of the situation. It
looks like an intersection node becomes a vertex node. But this is
a degenerate case that should have been excluded by definition of the
sign function in Section 3.1. So, I am lost. I would have expected Figure
6b to show a vertical V (by opposition of the horizontal V of 6a and 6c)
moving from left to right and thus moving the intersection node from one
triangle edge to the other.

\begin{comment}
We have changed 6b, now 7b, to more clearly depict a triangle `swinging' through an edge of $Y$.
We've added arrows to Figure 11, now 15, to show how the nodes are moving.
In particular, in 15b we see that the two nodes are swapping positions.
\end{comment}

In Section 3.5, the sentence about Ax = B should be rewritten to make it
clearer that x is not a single vector of length 2, but actually
3 vectors. In the same section, the authors are a bit misleading when
they give the same cost to addition, multiplication, and division. Sure,
they all are floating-point operations, but in practice, divisions are
much more expensive.

\begin{comment}
We've replaced x with $\hat{X}$ and made clear it is a matrix of the same size as $B$.
The FLOP count has been reduced in length and no longer refers to divisions.

% nb: do we still include this? or is it mean?
After reviewing the literature, it is clear that the relative cost of divisions in terms of FLOPs has changed over the years.
Modern references take additions, multiplications and divisions as equally expensive at 1 FLOP.
For example, see ``Low complexity user selection algorithms for multiuser MIMO systems with block diagonalization'' by Shen et al.
\end{comment}

In Section 6, the definition of the "triangle condition" is a bit
confusing. It might be better to replace "edge" by "graph edge" (or
something to that effect), because up to now, "edge" has meant "triangle
edge". Also, talking about some planar embedding does not help,
especially as the authors have to assume that this planar embedding has
"straight edges". If the authors had kept in the geometrical world, they
would not have to make this kind of assumptions.

\begin{comment}
We've made the suggested replacement of edge by graph edge where applicable.
Reference to planar embedding is removed by making the straight line assumption earlier in the text.
We have rewritten the definition of the triangle condition.
\end{comment}

The proof of Lemma 3 contains a bit too much of handwaving, to the point
where the authors have to put things between quotes, e.g., "two vertices
of X are 'below' the reference line". It would be great if the proof
could be straightened a bit.

\begin{comment}
Thank you, the offending section of the proof has been cleaned up.
We think that it is sufficiently rigorous after these revisions.
\end{comment}

As I understand it, Lemma 4 is the crux of the matter. Unfortunately,
I do not understand its very short proof. Perhaps I am missing something
obvious.

\begin{comment}
We have entirely rewritten the proof of this lemma, now numbered 3.
We believe it is much clearer.
We have also restructured Section 6 to bring focus to Lemma 4 and Corollary 1.
\end{comment}

Finally, let me conclude with the matter of backward stability I hinted
earlier. Section 6.2 states that "the two changes of coordinates in this
algorithm are backward stable as they are both affine transformations."
Sure, but these affine transformations are not an input of the algorithm;
they are computed by the algorithm itself, and so are the inverse
transformations. For example, Cramer's rule, which is advocated by the
authors in Section 2, is numerically unstable. So, one should be a lot
more careful with the proof that the changes of coordinates (one way and
the other) are actually backward stable. As a matter of fact, the authors
themselves propose a reference-free variant of the algorithm to reduce
the "shearing" due to the affine transformations.

\begin{comment}
We've replaced the claim of backward stability with a more precise claim of continuity with respect to input.
One can retrieve backward stability by bounding the necessary quantities, which is explained immediately following the proof of Theorem 2.

Incidentally, we've removed the reference to Cramer's rule.
We never intended to advocate for its use.
The original PANG made use of the exact formulae for the elements of $A$ and we wanted to show where they came from.
Cramer's rule was the most direct.
\end{comment}


Additional Questions:
Please help ACM create a more efficient time-to-publication process: Using your best judgment, what amount of copy editing do you think this paper needs?: None

Most ACM journal papers are researcher-oriented. Is this paper of potential interest to developers and engineers?: Yes

\pagebreak

Referee: 3

Recommendation: Needs Major Revision

Comments:
Dear authors,
I reviewed your manuscript for ACM TOMS:
A provably robust algorithm for triangle-triangle intersections in floating-point arithmetic.
The manuscript is well organized, and to provide the MATLAB source is nice for readability
and numerical tests for readers. I have several questions for this research.

1. People in computational geometry worked to overcome robustness problems from
rounding error problems. For example, Shewchuk proposed a robust method for
geometric predicates combined with floating-point filter:
\begin{quote}
http://www.cs.cmu.edu/~quake/robust.html
\end{quote}
Also, we know a robust library for computational geometry, CGAL project:
\begin{quote}
https://www.cgal.org/
\end{quote}
There are many researches toward robustness problems for floating-point arithmetic in
computational geometry. A point in a triangle problem is also solved exactly because 2D
orientation problem is solved exactly using only floating-point arithmetic. Is there
connection of such researches to your problem?

\begin{comment}
Thank you for these resources.
It is clear that we share design philosophies with both Shewchuk and the CGAL project.
Of the two, we seem to be closer to Shewchuk.
As he states in ``Adaptive precision floating-point arithmetic...'', ``[a] correct algorithm... will produce a meaningful result if its test results are wrong but consistent with each other''.
This reflects our stated goal.

The documentation on the CGAL project states that their high-level algorithms will work as long as the ``basic questions'' are answered correctly.
With the right choice of ``basics'' and an exact way to answer them this seems robust.
The algorithm we present here likewise breaks the problem down into components, but we make no assumptions on if the problems are solved correctly.

With regard to exact 2D orientation algorithms like that of Shewchuk, it could absolutely provide an exact answer to whether a vertex of $X$ lies in $Y$.
However, this would not necessarily allow us to use the information of this test to determine the outcome of other tests.
In our algorithm, there is parsimony between this test and the calculation of intersections between edges of the triangles.
Shewchuk quotes Fortune's explanation that a parsimonious algorithm is naturally robust in the same article referenced above.
Our algorithm is now completely parsimonious but we include a proof of robustness for clarity.
\end{comment}

2. You wrote “Affine transformations are numerically stable”. Could you cite a literature
(rounding error analysis for affine transformation) for it? because your proof in Section
6.2 based on this fact (I think this transformation is not ill-conditioned) .

\begin{comment}
We've clarified which affine transformations would be numerically stable.
We've updated the proof of the theorem to be true for all affine transformations and restricted our claim of backward stability for only some triangles.
\end{comment}

3. You showed the robustness of the proposed algorithm in Section 7 by examples. Could
you compare the computing times?

\begin{comment}
We've added a new example that compares computation time and accuracy to both versions of this algorithm, the original PANG and the Sutherland-Hodgman (SH) algorithm.
Ideally we'd next compare the robustness of SH.
However, we expect a good implementation of SH to be robust meaning any example we give it would show no difference between outcomes.
\end{comment}

The followings are minor remarks.

1. You wrote floating point arithmetic. It is replaced by floating-point arithmetic.

\begin{comment}
We've fixed this.
\end{comment}

2. You counted floating-point operations by flops for the proposed method. It is useful in
checking the computational costs. However, Fused Multiply-Add and 256 bits (AVX2.0)
or 512 bits register (AVX512) are supported in recent Intel’s CPUs. My question is that
the flop-count is directly related to computational performance? In addition, it is better
to state that flops indicates floating-point operations because people in high performance
computing think floating-point operations per sec.

\begin{comment}
The FLOP count compares only the expected computational time between the change of coordinates and reference-free parametrization.
With proper implementation and efficient system solves, the difference may be negligible.
Indeed, the implementation of the reference-free parametrization is faster in the new example we added in section 8.1 compared with this version of the change of coordinates.

We have defined the term FLOP in the relevant section.
\end{comment}

3. Matlab may be replaced by MATLAB.

\begin{comment}
Also fixed.
\end{comment}

4. Could you provide a figure for Lemma 2?

\begin{comment}
We've rewritten this section to be more concise and relevant.
We did add a figure that might help though, see Figure 8.
\end{comment}

5. You use floating-point numbers and floating-point arithmetic. It is better to add an
assumption: neither overflow nor underflow occurs in the evaluation.

\begin{comment}
We've added this assumption immediately before the statement of Theorem 1.
\end{comment}

Additional Questions:
Please help ACM create a more efficient time-to-publication process: Using your best judgment, what amount of copy editing do you think this paper needs?: Light

Most ACM journal papers are researcher-oriented. Is this paper of potential interest to developers and engineers?: Yes

\end{document}