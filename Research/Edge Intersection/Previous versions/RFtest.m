% U = [-1,2,-0.5;2,2,-1];
U = [0.5,-1,5;0.5,5,-1];
V = [0,3,0;0,0,3];
triU = [U,U(:,1)];
triV = [V,V(:,1)];
plot(triU(1,:),triU(2,:),'-',triV(1,:),triV(2,:),'-')

[P,Q,R,n] = RefFree(U,V);
P = SortAndRemoveDoubles([P,Q,R]);
patch(P(1,:),P(2,:),'m')