function [P,Q,R,n]=RefFree(U,V)
% REFFREE intersection of two triangles
%    [P,Q,R,n] = RefFree(U,V) produces the vertices of U inside V (P), the
%    intersections between the edges of U and V (Q), the vertices of V
%    inside U (R) and the neighbours of U that also intersect V (n).
%    The result is the same as Geometry(U,V) up to error on the order of
%    machine epsilon. The calculations are performed without change of
%    coordinates to a reference triangle.

W=V(:,[2,3,1])-V;                           % vectors between vertices of V
n=zeros(1,3); indR=n; indP=ones(1,3);
Q=zeros(2,9); indQ=zeros(1,9);
for i=1:3
  j=mod(i,3)+1; w=W(:,i); wt=[-w(2);w(1)];  % find w and vector perp. to w
  if wt'*W(:,j)<0                           % if V is not on + side of w
    wt=-wt;                                 %  change sign of wt
  end
  qp=[w,wt]\(U-V(:,i));
  sp=sign(qp(2,:))>=0;
  qold=NaN;
  for k=1:3
    l=mod(k,3)+1; m=k+3*(i-1);
    if sp(k)~=sp(l)                         % vertices on opposite sides of edge?
      q0=(qp(1,k)*qp(2,l)-qp(1,l)*qp(2,k))/(qp(2,l)-qp(2,k));
      if q0>=0 && q0<=1                     % intersection lies on edge of Y?
        Q(:,m)=V(:,i)+q0*w; indQ(m)=1;
        n(k)=1;                             % nhbrs of U intersect V
      end
      if isnan(qold)                        % 1st intersection not calculated?
        qold=q0;
      else
        indR(i)=indR(i)|sign(qold)~=sign(q0);       % 0 in [q0,qold]?
        indR(j)=indR(j)|sign(qold-1)~=sign(q0-1);   % 1 in [q0,qold]?
      end
    end
  end
  indP=indP.*sp;
end
P=U(:,indP==1);
Q=Q(:,indQ==1);
R=V(:,indR==1);
end