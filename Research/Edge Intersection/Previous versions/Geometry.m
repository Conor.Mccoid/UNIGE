function [P,Q,R,n] = Geometry(U,V)
% GEOMETRY intersection of two triangles
%   [P,Q,R,n] = Geometry(U,V) gives the vertices of U inside V (P),
%   intersections between the edges of U and those of V (Q), the vertices 
%   of V inside U (R), and the neighbours of U that also intersect V (n).

v1=V(:,2)-V(:,1);  % vector between V1 and V2
v2=V(:,3)-V(:,1);  % vector between V1 and V3
% d00=v1'*v1;        % norm of v1
% d01=v1'*v2;
% d11=v2'*v2;        % norm of v2
% id =d00*d11 - d01*d01; % this value is used to normalize quantities
% 
% %---Change of Coordinates (sec.2)---%
% X = zeros(2,3);
% for i=1:3 
%     r=U(:,i)-V(:,1); % vector between ith vertex of triangle U and V1
%     d02=v1'*r;       % projection of r in v1 direction
%     d12=v2'*r;       % projection of r in v2 direction
%     X(1,i)=(d11*d02-d01*d12)/id;  % amount of r in v1 direction
%     X(2,i)=(d00*d12-d01*d02)/id;  % amount of r in v2 direction
%     % end result: r = X(1,i)*v1 + X(2,i)*v2
% end
X = [v1, v2] \ (U - V(:,1));

%---Intersections (sec.3)---%
Q    = zeros(2,9);
indQ = zeros(1,9);
n    = [0,0,0];
indR = n; % 1-(0,0); 2-(1,0); 3-(0,1)
indP = ones(1,3);

% v1
p = X(2,:); q=X(1,:); sp=sign(p)>=0; qold=NaN;
for i=1:3
    j=mod(i,3)+1;
    if sp(i)~=sp(j)
        q0 = (q(i)*p(j) - q(j)*p(i))/(p(j)-p(i));
        if q0>=0 && q0<=1
            Q(1,i)=q0; indQ(i)=1; n(i)=1;
        end
        if isnan(qold)
            qold=q0;
        else
            indR(1)=indR(1) | sign(qold)~=sign(q0);
            indR(2)=indR(2) | sign(qold-1)~=sign(q0-1);
        end
    end
end
indP = indP.*sp;

% v2
p = X(1,:); q=X(2,:); sp=sign(p)>=0; qold=NaN;
for i=1:3
    j=mod(i,3)+1;
    if sp(i)~=sp(j)
        q0 = (q(i)*p(j) - q(j)*p(i))/(p(j)-p(i));
        if q0>=0 && q0<=1
            Q(2,3+i)=q0; indQ(3+i)=1; n(i)=1;
        end
        if isnan(qold)
            qold=q0;
        else
            indR(1)=indR(1) | sign(qold)~=sign(q0);
            indR(3)=indR(3) | sign(qold-1)~=sign(q0-1);
        end
    end
end
indP = indP.*sp;

% line connecting v1 and v2
p = 1-X(1,:)-X(2,:); q = 0.5*(1-X(1,:)+X(2,:)); sp=sign(p)>=0; qold=NaN;
for i=1:3
    j=mod(i,3)+1;
    if sp(i)~=sp(j)
        q0 = (q(i)*p(j) - q(j)*p(i))/(p(j)-p(i));
        if q0>=0 && q0<=1
            Q(:,6+i)=[1-q0;q0]; indQ(6+i)=1; n(i)=1;
        end
        if isnan(qold)
            qold=q0;
        else
            indR(2)=indR(2) | sign(qold)~=sign(q0);
            indR(3)=indR(3) | sign(qold-1)~=sign(q0-1);
        end
    end
end
indP = indP.*sp;

% for edge=1:3
%     ind      = (1:3) + 3*(edge-1);
% 
%     if edge==1 % v1
%         p = X(2,:); q = X(1,:);               % parametrization of edge
%         a = [1;0]; b=[0;0];            % inversion of parametrization
%         R1 = 1; R2 = 2;                       % vertices of V on edge
%     elseif edge==2 % v2
%         p = X(1,:); q = X(2,:);
%         a = [0;1]; b=[0;0];
%         R1 = 1; R2 = 3;
%     elseif edge==3 % line connecting v1 and v2
%         p = 1-X(1,:)-X(2,:); q = 0.5*(1-X(1,:)+X(2,:));
%         a = [-1;1];b=[1;0];
%         R1 = 2; R2 = 3;
%     end
% 
%     sp= sign(p)>=0; qold = NaN;
%     for i = 1:3
%         j = mod(i,3) + 1;
%         if sp(i) ~= sp(j) % test if vertices of X lie on opposite sides of edge
%             q0=(q(i)*p(j) - q(j)*p(i))/(p(j)-p(i)); % calculate intersection
%             if q0>=0 && q0<=1   % test if intersection lies on edge of Y
%                 Q(:,ind(i)) = a*q0 + b;
%                 indQ(ind(i))= 1;
%                 n(i)        = 1;
%             end
%             if isnan(qold)  % test if 1st intersect. already calculated
%                 qold=q0;
%             else
%                 indR(R1) = indR(R1) | sign(qold)~=sign(q0);
%                 indR(R2) = indR(R2) | sign(qold-1)~=sign(q0-1);
%             end
%         end
%     end
% %     [Qtemp,ntemp,indRtemp,sp] = EdgeIntersect(X,edge);
% %     Q(:,ind) = Qtemp;
% %     indQ(ind)= ntemp;
% %     n        = max(n,ntemp);
% %     indR     = max(indR,indRtemp);
%     indP     = indP.*sp;
% end

P = U(:,indP==1);
Q = V(:,1) + [v1,v2]*Q(:,indQ==1);
R = V(:,indR==1);
end

function [Q,ind,indR,sp]=EdgeIntersect(X,edge)
% EDGEINTERSECT intersection of edges of X with reference line of Y
%   [Q,ind,indR,sp] = EdgeIntersect(X,edge) calculates the intersections
%   between all edges of X and the reference line of Y indicated by edge
%   and stores the values in Q.
%   Boolean indicators for neighbours of U intersecting V, vertices of V
%   inside U, and vertices of U inside V are stored in ind, indR and sp,
%   respectively.

% Particulars for each edge of reference triangle
if edge==1 % v1
    p = X(2,:); q = X(1,:);               % parametrization of edge
    a = [1;0]; b=[0;0];            % inversion of parametrization
    R1 = 1; R2 = 2;                       % vertices of V on edge
elseif edge==2 % v2
    p = X(1,:); q = X(2,:);
    a = [0;1]; b=[0;0];
    R1 = 1; R2 = 3;
elseif edge==3 % line connecting v1 and v2
    p = 1-X(1,:)-X(2,:); q = 0.5*(1-X(1,:)+X(2,:));
    a = [-1;1];b=[1;0];
    R1 = 2; R2 = 3;
end

Q = zeros(2,3); ind = zeros(1,3); indR = ind;
sp= sign(p)>=0; qold = NaN;
for i = 1:3
    j = mod(i,3) + 1;
    if sp(i) ~= sp(j) % test if vertices of X lie on opposite sides of edge
        q0=(q(i)*p(j) - q(j)*p(i))/(p(j)-p(i)); % calculate intersection
        if q0>=0 && q0<=1   % test if intersection lies on edge of Y
            Q(:,i) = a*q0 + b;
            ind(i) = 1;
        end
        if isnan(qold)  % test if 1st intersect. already calculated
            qold=q0;
        else
            indR(R1) = indR(R1) | sign(qold)~=sign(q0);
            indR(R2) = indR(R2) | sign(qold-1)~=sign(q0-1);
        end
    end
end
end