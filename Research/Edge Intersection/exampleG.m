% exampleG
% attempt to create a (f)(f) error on graph G to prove its catastrophic
% effects on the shape of the polygon of intersection
% the calculation of an intersection appears much more numerically stable
% than anticipated

Y=[0,1,0;0,0,1]; T=[1,2,3,2,2,2];
a=-10;
X=[0.5*eps,1-a-eps,-0.5*eps;a,a-eps,1+2*eps];
M=1; b=1e0;
while norm(M,inf)~=0
    t=2*b*rand(2,3) - b;
    U=t(:,2:3)*X + t(:,1);
    V=t(:,2:3)*Y + t(:,1);

    fig=figure(1);
    clf
    set(fig,'DoubleBuffer','on');
    PlotMesh(U,T,'b');
    PlotMesh(V,T,'r');
    M=PANG2_20210118(V,T,U,T);
end
text(V(1,1),V(2,1),'V0')
save('G.mat','U','V','T')

%%
% There is now a combination such that the 2nd reference line (y=0)
% produces an error of the kind we're looking for. We now perturb the
% remaining vertex of V (2nd) and U to get the other error.

load('G.mat')
M=1; b=0.5;
while norm(M,inf)~=0
    t=[2*b*rand(2,1),zeros(2,1)];
    A=V(:,2:3)+t - V(:,1);
    U(:,2)=A*X(:,2) + V(:,1);
    
    fig=figure(1);
    clf
    set(fig,'DoubleBuffer','on');
    PlotMesh(U,T,'b');
    PlotMesh(V+[zeros(2,1),t],T,'r');
    M=PANG2_20210118(V+[zeros(2,1),t],T,U,T);
    axis([0.3,0.6,0.3,0.5])
    pause(0)
end

%%
% Having found a combination where one of the intersections produces an
% error we perturb the third vertex of U to get the other error to occur.

load('G.mat')
[~,Q,~,~] = Geometry(U,V); U(:,2) = Q;
b=10*eps; w=V(:,2)-V(:,1);
a=1e-2*norm(w)/norm(b); w=w/norm(w); %t=[w,zeros(2,2)];
M=1;
while norm(M,inf)~=0
%     t=t+b*t; a=a*(1-b);
    t=2*b*rand(2,1)-b;
    t=[zeros(2,1),t,zeros(2,1)];
    
    fig=figure(1);
    clf
    set(fig,'DoubleBuffer','on');
    PlotMesh(U+t,T,'b');
    PlotMesh(V,T,'r');
    M=PANG2_20210118(V,T,U+t,T);
%     axis([V(1,3)-a,V(1,3)+a,V(2,3)-a,V(2,3)+a])
    axis([min(U(1,:)),max(U(1,:)),min(U(2,:)),max(U(2,:))])
    pause(0)
end

%%
% Third attempt. We've found a failure in the 2nd reference line and so now
% we change X to be more likely of failure in the 3rd reference line and
% take the resulting U.

load('G.mat')
X=[0.5*eps,1-a-eps,-0.5*eps;a,a-2*eps,1+1.5*eps];
A=V(:,2:3)-V(:,1);
U=A*X + V(:,1);

fig=figure(1);
clf
set(fig,'DoubleBuffer','on');
PlotMesh(U,T,'b');
PlotMesh(V,T,'r');
M=PANG2_20210118(V,T,U,T);
%     axis([V(1,3)-a,V(1,3)+a,V(2,3)-a,V(2,3)+a])
axis([min(U(1,:)),max(U(1,:)),min(U(2,:)),max(U(2,:))])
pause(0)

%%
% The stability of two intersection errors over the 2nd and 3rd reference
% lines is unexpected. It is clear that we need to use some kind of
% symmetry to get a double header.

% I'm switching to the 1st (or zeroth) vertex of V, where Y has its origin.
% This way, if we get an (f) error along one of the lines, the other must
% surely follow by symmetry. The trouble is that this requires the value of
% the intersection to have an error in its sign. This is multiplicative and
% so doesn't rely on the relative arguments we've been making for the other
% vertices.

Y=[0,1,0;0,0,1]; T=[1,2,3,2,2,2];
a=10;
X=[-0.5*eps,a,0.5*eps;-0.5*eps,0.5*eps,a];
M=1; b=1e0;
while norm(M,inf)~=0
    t=2*b*rand(2,3) - b;
    U=t(:,2:3)*X + t(:,1);
    V=t(:,2:3)*Y + t(:,1);

    clf
%     M=PANG2_20210118(V,T,U,T);
    M=InterfaceMatrixRF(V,T,U,T);
end

    fig=figure(1);
    clf
    set(fig,'DoubleBuffer','on');
    PlotMesh(U,T,'b');
    PlotMesh(V,T,'r');
%     M=PANG2_20210118(V,T,U,T);
    M=InterfaceMatrixRF(V,T,U,T);
text(V(1,1),V(2,1),'V0')
save('G2.mat','U','V','T')