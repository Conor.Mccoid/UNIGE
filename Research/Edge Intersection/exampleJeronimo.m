load triangulos.mat
fig=figure(1);
clf
set(fig,'DoubleBuffer','on');
PlotMesh(N_P,T_P,'b');
PlotMesh(N_S,T_S,'r');
perturb = 0 * (rand(2,3)-0.5);
M=InterfaceMatrix(N_P,T_P,N_S,T_S);
% M=InterfaceMatrixNew(N_P,T_P,N_S + perturb,T_S);
% M=QuadComplex(N_P,T_P,N_S+perturb,T_S);
% M=InterfaceMatrixRF(N_P,T_P,N_S+perturb,T_S);
% M=PANG2_20210118(N_P,T_P,N_S,T_S);

%%
SH=sutherlandHodgman(N_S',N_P');
[P,Q,R,~]=TriIntersect(N_P,N_S); PANG2=[P,Q,R];
[P,Q,R,~]=TriIntersectRF(N_P,N_S); RF=[P,Q,R];
PANG=PANG_Intersect(N_P,N_S);