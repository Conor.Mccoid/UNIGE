[N,T,Np]=CircleMesh(20,10*eps);

%%
figure(1)
clf
PlotMesh(N,T,'b')
PlotMesh(Np,T,'r')
% M = InterfaceMatrix(N,T,Np,T);
% M = InterfaceMatrixAF(N,T,Np,T);
% M = PANG2_20210118(N,T,Np,T);
M = PANG2RF_20210315(N,T,Np,T);
% M = QuadComplex(N,T,Np,T);
pause(1)
% savefig('CirclePANG.fig')

%%
clf
PlotMesh(N,T,'b')
PlotMesh(Np,T,'r')
M = InterfaceMatrixNew(N,T,Np,T);
pause(1)
% savefig('CircleNew.fig')

%%
clf
PlotMesh(N,T,'b')
PlotMesh(Np,T,'r')
[M,cand] = InterfaceMatrixRF(N,T,Np,T);
% savefig('CircleNew.fig')