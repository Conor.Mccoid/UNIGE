function q0=TEST_Intersect(X)
sp=sign(X(2,:))>=0;
if sp(1)~=sp(2)
    q0=(X(1,1)*X(2,2)-X(1,2)*X(2,1))/(X(2,2)-X(2,1));
else
    q0=NaN;
end