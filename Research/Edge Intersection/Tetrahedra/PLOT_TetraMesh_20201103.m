function PLOT_TetraMesh_20201103(N1,T1,N2,T2)
% TETRAMESH plots the edges of up to two tetrahedral meshes

[n,~]=size(T1);
axis('equal')
view(-37.5,30)
for i=1:n
    X=N1(:,T1(i,1:4));
    line(X(1,:),X(2,:),X(3,:),'Color','b')
    line(X(1,[1,4]),X(2,[1,4]),X(3,[1,4]),'Color','b')
    line(X(1,[2,4]),X(2,[2,4]),X(3,[2,4]),'Color','b')
    line(X(1,[1,3]),X(2,[1,3]),X(3,[1,3]),'Color','b')
end

if nargin>2
    [n2,~]=size(T2);
    for i=1:n2
        X=N2(:,T2(i,1:4));
        line(X(1,:),X(2,:),X(3,:),'Color','r')
        line(X(1,[1,4]),X(2,[1,4]),X(3,[1,4]),'Color','r')
        line(X(1,[2,4]),X(2,[2,4]),X(3,[2,4]),'Color','r')
        line(X(1,[1,3]),X(2,[1,3]),X(3,[1,3]),'Color','r')
    end
end