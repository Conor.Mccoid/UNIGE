%===EX_3DHecht_v1_20201103===%
th=pi/10; disp=0.1*ones(3,1);
[N,T,N2]=MESH_d20_20201103(th,th,th,disp);
U=N2(:,T(1,1:4)); V=N(:,T(1,1:4));

figure(1)
clf
subplot(1,3,1)
PLOT_TetraMesh_20201103(N,T,N2,T);
ALGO_ProjectionMatrix3D_v1_20201104(N,T,N2,T);

figure(1)
subplot(1,3,2)
PLOT_TetraMesh_20201103(N,T(1,:),N2,T(1,:));
[Z1,nhbrs1]=ALGO_TetraIntersect_v1_20201103(V,U);

figure(1)
subplot(1,3,3)
PLOT_TetraMesh_20201103(N,T(1,:),N2,T(1,:));
% ALGO_ProjectionMatrix3D_v1_20201104(N,T,N2,T);
% [Z2,nhbrs2]=ALGO_simplices_IntersectSimplices_v1_20211216(V,U);
[Z2,nhbrs2]=ALGO_simplices_IntersectSimplices_v3_20220308(V,U);