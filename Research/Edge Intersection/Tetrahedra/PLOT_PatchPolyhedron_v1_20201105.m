function PLOT_PatchPolyhedron_v1_20201105(P)
% PATCHPOLYHEDRON plots the surfaces of an arbitrary polyhedron

tol=1e-15;
P=SUB_RemoveDoubles_v1_20220128(P,tol);
m=size(P,2);
if m>3
    K=convhull(P(1,:),P(2,:),P(3,:),'simplify',true);   % take convex hull (triangulation of surface)
    [K,l]=SUB_Neighbours_v1_20201105(K);                % list nhbrs of each triangle
    flagK=zeros(1,l);                                   % init. flags of which triangles looked at
    while ~isempty(K(flagK==0,:))
        flag=zeros(1,m);                                % init. flags of which nodes looked at
        ind=find(flagK==0,1);                           % pick first triangle not already used
        L=K(ind,:); flagK(ind)=1;                       % flag triangle
        O=L(1:3); flag(O)=1;                            % flag nodes of this triangle
        V=P(1:3,L(1:3)); v0=V(:,1); n=cross(V(:,2)-v0,V(:,3)-v0); % transform to plane containing this triangle
        nhbrs=L(4:6);                                   % make list of nhbrs of this triangle
        while ~isempty(nhbrs)
            cand=K(nhbrs(1),1:3); cand=cand(flag(cand)==0); % take first node of nhbrs not yet considered
            if abs(n'*(P(1:3,cand)-v0))<tol               % if it is reasonably within the plane
                nhbrs=[nhbrs,K(nhbrs(1),4:6)];          % add its nhbrs to list of nhbrs
                O=[O,cand];                             % add this node to polygon in the plane
                flagK(nhbrs(1))=1;                      % flag the nhbr (it is now part of a larger object)
            end
            flag(cand)=1; nhbrs=nhbrs(2:end);           % flag the nhbr so as to not consider it in this plane again
        end
        O=P(1:3,O); O=SUB_SortClockwise_v1_20201105(O);   % take points and order them in the plane
        patch(O(1,:),O(2,:),O(3,:),'m')                 % patch the polygon
    end
end
end

function O = SUB_SortClockwise_v1_20201105(O)
% SORTCLOCKWISE sorts a set of coplanar 3D points
%   Currently a misnomer, as it does not necessarily sort in a clockwise
%   direction, which is largely meaningless in an arbitrary plane

n=size(O,2)-1; th=zeros(1,n); v0=O(:,1);                % init. coordinates and angles (first point is origin)
v=O(:,2)-v0; a=norm(v);                                 % second point starts at 0 radians
u=O(:,3)-v0; b=norm(u);                                 % third point defines positive angles
al=dot(u,v); th(2)=acos(al/(a*b)); w=u-al*v/a^2;        % calculate angle and positive vector
for i=3:n
    u=O(:,i+1)-v0;                                      % vector between origin and current node
    th(i)=sign(dot(u,w))*acos(dot(u,v)/(a*norm(u)));    % angle (+/-) between v (0 radians) and u
end
[~,ind]=sort(th); ind=ind+1;                            % sort angles from smallest to largest, incl. positivity
O=O(:,[1,ind]);                                         % use this sorting to sort nodes
end