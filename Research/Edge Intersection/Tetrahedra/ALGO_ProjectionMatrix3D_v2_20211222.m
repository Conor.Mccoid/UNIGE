function M = ALGO_ProjectionMatrix3D_v2_20211222(Na,Ta,Nb,Tb)
% PROJECTIONMATRIX3D calculates the projection matrix from tesselation 1 to
% tesselation 2
%   M=ProjectionMatrix3D(N1,T1,N2,T2) constructs the projection matrix M
%   from the nodes N1 to the nodes N2, with the tesselations T1 and T2,
%   respectively.

%===Advancing front algorithm (WIP)===%
na=size(Ta,1); nb=size(Tb,1);
cand=[1;1];
listb=1;
flagb=zeros(nb+1,1); flagb([1,end])=1;
while ~isempty(listb)
    Tetb=listb(1); listb=listb(2:end);
    lista=cand(1,cand(2,:)==Tetb);
    lista=unique(lista);
    flaga=zeros(na+1,1);
    flaga(end)=1; flaga(lista)=1;
    while ~isempty(lista)
        Teta=lista(1); lista=lista(2:end);
        [P,n]=ALGO_simplices_IntersectSimplices_v1_20211216(Nb(:,Tb(Tetb,1:4)),Na(:,Ta(Teta,1:4)));
        if ~isempty(P)
            nhbrsa=Ta(Teta,5:8);
            lista=[lista, nhbrsa(flaga(nhbrsa)==0)];
            flaga(nhbrsa)=1;
            nhbrsb=Tb(Tetb,5:8);
            nhbrsb=nhbrsb(n==1);  % nhbrs of Tetb which intersect Teta
            listb=[listb, nhbrsb(flagb(nhbrsb)==0)];
            flagb(nhbrsb)=1;
            nhbrsb=[Teta*ones(size(nhbrsb));nhbrsb];
            cand=[cand, nhbrsb];
        end
    end
end
end