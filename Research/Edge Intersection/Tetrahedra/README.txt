NAMING CONVENTIONS

%===Code===%

TYPE_Name_v1_YYYYMMDD.m

Types: 	MESH (mesh generation)
	ALGO (algorithm)
	EX (example or experiment)
	PLOT (figure generator)

Version numbering need not be applied to code for which no additional versions are expected

%===Publications===%

firstauthorYYYYfirstword.tex