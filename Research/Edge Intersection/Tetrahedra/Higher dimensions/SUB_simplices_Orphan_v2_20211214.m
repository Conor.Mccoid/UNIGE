function [H_new,J_new,T_new,m_new] = SUB_simplices_Orphan_v2_20211214(H,J,T,y)
% ORPHAN determines adjacent intersections and generates index set
%   [H_new,J_new,T_new] = Orphan(H,J,T,A,y,X) finds all intersections
%   H_new between the n-simplex X and the hyperplanes indexed by T and y.
%
%   Inputs:
%       H - intersections between X and Y, indexed by J and T;
%       J - vertices of X that form the m-face on which the intersections
%           of H lie;
%       T - hyperplanes of Y where the intersections H lie;
%       y - next hyperplane to intersect the convex hull of H with;
%
%   Outputs:
%       H_new - intersections between X and Y indexed by J_new and T_new;
%       J_new - vertices of X for the intersections of H_new;
%       T_new - T in union with y.

[n,m]=size(H);
m_max=floor((m+1)/2)*ceil((m+1)/2);
m_new=0;
T_new=T; T_new(y)=1; J_new=zeros(n,m_max); H_new=zeros(n,m_max);
for i=1:m-1
    for j=i+1:m
        if sign(H(y,i))~=sign(H(y,j))
            if nnz(xor(J(:,i),J(:,j)))==2
            % calculate intersection
                m_new=m_new+1;
                J_new(:,m_new)=J(:,i) | J(:,j);
                H_new(:,m_new)=SUB_simplices_IntersectHyperplane_v1_20211217(J_new(:,m_new),T_new);
            end
        end
    end
end
J_new=J_new(:,1:m_new); H_new=H_new(:,1:m_new);