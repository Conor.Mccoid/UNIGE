% Example: comparison of brute force vs generational adjacency
%   I conjectured that the brute force method would require too many
%   comparisons of true/false to be efficient. This tests whether that
%   holds true.

H=[1,-1,1,-1,1,1;zeros(4,6)];
A= [0,1,1,0,1,0;
    0,0,0,1,0,1;
    0,0,0,1,1,0;
    0,0,0,0,0,1;
    0,0,0,0,0,1;
    0,0,0,0,0,0]; A=A==1;
J= [1,0,1,0,1,0;
    1,1,0,0,0,0;
    0,1,0,1,0,1;
    0,0,1,1,0,0;
    0,0,0,0,1,1];

tic
[~,J_v2,~] = SUB_simplices_Orphan_v2_20211214(H,J,zeros(5),1,zeros(5));
t_v2=toc

tic
[~,J_v1,~,C] = SUB_simplices_Orphan_v1_20211126(H,J,zeros(5),A,1,zeros(5));
t_v1=toc

t_v1<t_v2
J_v1==J_v2