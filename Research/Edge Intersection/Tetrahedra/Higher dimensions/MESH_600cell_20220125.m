function [N,T,N2]=MESH_600cell_20220125(theta,disp)
% 600CELL constructs a mesh of the hexacosichoron, the 4D equivalent of the
% icosahedron

N=zeros(121,4);
for i=1:8       % 16-cell vertices
  N(1+i,ceil(i/2))=(-1)^i;
end
for i=1:16      % tesseract vertices
  N(9+i,:)=[0.5,0.5,0.5,0.5].*((-1).^[i,ceil(i/2),ceil(i/4),ceil(i/8)]);
end
p=(1+sqrt(5))/2;
temp=[p/2,0.5,1/(2*p),0];
perm=[1,2,3,4;
      3,1,2,4;
      2,3,1,4;
      2,1,4,3;
      1,4,2,3;
      4,2,1,3;
      1,3,4,2;
      4,1,3,2;
      3,4,1,2;
      3,2,4,1;
      2,4,3,1;
      4,3,2,1];
for i=1:96      % snub 24-cell vertices
  if mod(i,12)==0
    N(25+i,:)=temp(perm(end,:));
    temp(1)=-temp(1);
    if mod(i,24)==0
      temp(2)=-temp(2);
      if mod(i,48)==0
        temp(3)=-temp(3);
      end
    end
  else
    N(25+i,:)=temp(perm(mod(i,12),:));
  end
end
N=N';

%---Graph of vertices---%
A=false(121); A(1,2:end)=true;
ind=1:120; ind=ind+1;
for i=2:120
  ind=ind(2:end);
  dist=vecnorm(N(:,ind)-N(:,i),2,1);
  A(i,i+1:end)=abs(dist-1/p)<1e-3;
end

%---4-simplices---%
T=zeros(600,5);
T(:,1)=1; % v0 lies at the origin, meaning no translation is needed when transforming to the reference simplex
count=0; ind=1:121;
for i=2:120
  v1=i; % take arbitrary vertex as v1 of 4-simplex
  list_v1=ind(A(v1,:)); % find all neighbours of v1 (that are not v0)
  while ~isempty(list_v1)
    v2=list_v1(1); % take arbitrary nhbr of v1 as v2
    list_v2=list_v1(A(v2,list_v1)); % find all nhbrs of v2 that are also nhbrs of v1
    while ~isempty(list_v2)
      v3=list_v2(1); % take arbitrary nhbr of v2 as v3
      list_v3=list_v2(A(v3,list_v2)); % find all nhbrs of v3 that are also nhbrs of v2
      while ~isempty(list_v3)
        v4=list_v3(1); % take arbitrary nhbr of v3 as v4
        count=count+1; % count off one more simplex
        T(count,2:5)=[v1,v2,v3,v4]; % construct simplex
        list_v3=list_v3(2:end); % remove nhbr of v3 already considered
      end
      list_v2=list_v2(2:end); % remove nhbr of v2 already considered
    end
    list_v1=list_v1(2:end); % remove nhbr of v1 already considered
  end
end

%---Neighbours of simplices---%
T=SUB_Neighbours_v1_20201105(T);

%---Perturbed nodes---%
if nargin>0
  ind=nchoosek(1:4,2); R=eye(4);
  for i=1:length(ind)
    R=R*SUB_Rotate4D_v1_20220125(theta,ind(i,1),ind(i,2));
  end
  N2=R*N + disp;
end
             
end

function R=SUB_Rotate4D_v1_20220125(theta,i,j)
% ROTATE4D constructs a rotation matrix of angle theta around axes i and j
% in 4D

R=eye(4);
R(i,i)=cos(theta); R(i,j)=-sin(theta); R(j,i)=sin(theta); R(j,j)=cos(theta);
end