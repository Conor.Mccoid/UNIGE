function [W,S,list_JK,list_Teta,flag_q] = SUB_simplices_Intersections_v1_20220308(X,S,list_J,list_T,flag_JT,A,v0,V)
% INTERSECTIONS calculates intersections between m-faces and m hyperplanes

% calculates intersections and their signs of a given generation
% list_T is a matrix with each column a collection of hyperplanes
% list_J is the same but for vertex sets
% S is a 3D tensor containing the signs of the previous generation
% flag_JT is a matrix indicating which combinations of J and T give
%   intersections from the previous generation
% A is the comb adj mat for dimension n+1 with choice of m indices,
%   preferaby upper triangular
% v0 and V are the reverse transformation from reference coordinates

%===Might need special case for m=0...===%

n=length(X);    % n: dimension (+1) of the space, number of vertices
[m,n_T]=size(list_T);% m: dimension (-1) of m-face of X
                % n_T: number of collections from the previous generation
ind_T=1:n_T;    % list of hyperplane collections to go through

[~,n_J]=size(list_J);% n_J: number of intersections from previous gen
ind_J=1:n_J;  % list of intersections to go through
ind_A=SUB_simplices_ConvertCombinadics_v1_20220309(list_J);
                    % combinadics rep of the index sets

flag_q=zeros(nchoosek(n,m+2),nchoosek(n,m+1));
                % flag of which intersections have already been calculated
flag_s=NaN(n_T,n,nchoosek(n,m+2));
                % flag of known signs

Q=cell(nchoosek(n,m+2),nchoosek(n,m+1)); % storage for intersections
list_JK=zeros(m+2,nchoosek(n,m+2)); % list for index sets J+K
list_Teta=zeros(m+1,nchoosek(n,m+1)); % list for collections of hyperplanes

%---Step 3(i)---%

for t=ind_T % all Gamma

    T=list_T(:,t); % specific collection of hyperplanes
%     N_T=SUB_simplices_ConvertCombinadics_v1_20220309(T); % combinadics of T
        % could avoid this and several other uses of this conversion by
        % adding ind_JK and ind_Teta to the output

    ind_JT=ind_J(flag_JT(:,t)==1); % list of vertex sets for which there is an intersection for this choice of T
    ind_AJ=ind_A(ind_JT); % combinadics rep specific to this subset of Js
    
    for eta=SUB_simplices_SetRemains(T,n) % viable eta
        Teta=sort([T;eta]); % collection T with added eta
        N_Teta=SUB_simplices_ConvertCombinadics_v1_20220309(Teta);

        for j=ind_JT % all J
            J=list_J(:,j); % specific index set

            for k=ind_JT(A(ind_A(j),ind_AJ)==1) % adjacency
                K=list_J(:,k); % an index set adj to J
%                 JK=unique([J;K]); % union of J and K
                JK=SUB_simplices_SetSum(J,K); % replaces unique(), which took too much comp time
                N_JK=SUB_simplices_ConvertCombinadics_v1_20220309(JK); % combinadics of J union K

                if S(j,t,eta)~=S(k,t,eta) % sign check
                    if flag_q(N_JK,N_Teta)~=1
                        %---Intersection calculation---%
                        u=[ones(1,m+2);X(Teta,JK)] \ eye(m+2,1);
                        Q{N_JK,N_Teta}=X(:,JK)*u;
                        %---%

                        flag_q(N_JK,N_Teta)=1;
                        list_JK(:,N_JK)=JK;
                        list_Teta(:,N_Teta)=Teta;
                        % also store: JK, N_JK(?), Teta, N_Teta(?)
                    end
                else
                    flag_s(t,eta,N_JK)=S(j,t,eta); % store direction eta and sign s in flag of known signs
                end
                
            end

        end

    end

end

ind_JK=1:nchoosek(n,m+2); ind_JK=ind_JK(sum(flag_q,2)>0); % list of indices of JK calculated -- could also store N_JK directly, but issues with duplicates
S_denom=NaN(nchoosek(n,m+2),nchoosek(n,m+1)); % denominators in Cramer's rule
S_num=NaN(nchoosek(n,m+2),nchoosek(n,m+2)); % numerators
S=zeros(nchoosek(n,m+2),nchoosek(n,m+1),n);   % signs of q

%---Step 3(ii)---%

for N_JK=ind_JK
    JK=list_JK(:,N_JK);

    ind_flagged=ind_T(sum(~isnan(flag_s(:,:,N_JK)),2)>0);
        % indices of those collections for which JK has been flagged
    for t=ind_flagged
        T=list_T(:,t);

        ind_eta=1:n; ind_eta=ind_eta(~isnan(flag_s(t,:,N_JK))); % those directions flagged
        for eta=ind_eta
            
            for gamma=SUB_simplices_SetRemains([T;eta],n)
%             for gamma=setdiff(1:n,sort([T;eta]))
                Tgamma=sort([T;gamma]);
                N_Tgamma=SUB_simplices_ConvertCombinadics_v1_20220309(Tgamma);
                if flag_q(N_JK,N_Tgamma)==1 % test if this intersection exists
                  
                    N_Teg=SUB_simplices_ConvertCombinadics_v1_20220309([T;eta;gamma]);

                    S(N_JK,N_Tgamma,eta)=flag_s(t,eta,N_JK);
                    if isnan(S_denom(N_JK,N_Tgamma))
                        S_denom(N_JK,N_Tgamma)=sign(det([ones(m+2,1), X(Tgamma,JK)']));
                        % it would be better to replace this calculation
                        % with the recurrence formulas from the sign
                        % connectivity section of the thesis
                    end
                    S_num(N_JK,N_Teg)=(-1)^(sum(Tgamma<eta)) * S(N_JK,N_Tgamma,eta)*S_denom(N_JK,N_Tgamma); %pm?
                    
                end
                
            end

        end

    end

    ind_Teta=1:nchoosek(n,m+1); ind_Teta=ind_Teta(flag_q(N_JK,:)>0);
    for t=ind_Teta
        Teta=list_Teta(:,t);
        N_Teta=SUB_simplices_ConvertCombinadics_v1_20220309(Teta);

            % list of remaining directions to check
        for gamma=SUB_simplices_SetRemains(Teta,n)
            N_Teg=SUB_simplices_ConvertCombinadics_v1_20220309([Teta;gamma]);

            if isnan(S_denom(N_JK,t))
                S_denom(N_JK,t)=sign(det([ones(m+2,1), X(Teta,JK)']));
                % see earlier note
            end

            if isnan(S_num(N_JK,N_Teg))
                q=Q{N_JK,N_Teta};
                S(N_JK,N_Teta,gamma)=sign(q(gamma));
                S_num(N_JK,N_Teg)=(-1)^(sum(Teta<gamma)) * S(N_JK,N_Teta,gamma)*S_denom(N_JK,N_Teta); %pm?
            else
                S(N_JK,N_Teta,gamma)=(-1)^(sum(Teta<gamma)) * S_num(N_JK,N_Teg)*S_denom(N_JK,N_Teta); %pm?
            end

        end

    end

end

ind_Teta=1:nchoosek(n,m+1); ind_Teta=ind_Teta(sum(flag_q,1)>0); % list of Teta for intersection calculated

%---Step 3(iii)---%
% needs fixing
S_temp=prod(S>=0,3).*flag_q;
W=zeros(n-1,nnz(S_temp));
counter=0;
for JK=ind_JK
    for Teta=ind_Teta
        if S_temp(JK,Teta)==1
            q=Q{JK,Teta}; q=q(2:end);
            counter=counter+1;
            W(:,counter)=v0 + V*q; % transform back into original coordinates
        end
    end
end

% reduce outputs in size, taking only those portions which correspond to
% calculated intersections
W=W(:,1:counter);
S=S(ind_JK,ind_Teta,:);
list_JK=list_JK(:,ind_JK);
list_Teta=list_Teta(:,ind_Teta);
flag_q=flag_q(ind_JK,ind_Teta);
end

function JK=SUB_simplices_SetSum(J,K)
% SETSUM computes the sum of two sets who differ by one element
% JK=SetSum(J,K) finds the sum of two sets, J and K, whose elements are
%   arranged in ascending order and who are adjacent. Therefore, there is
%   one element in J that is not in K and one element in K that is not in
%   J.

JK=zeros(length(J)+1,1);
i=0;
while ~isempty(J)
    i=i+1;
    if J(1)==K(1)
        JK(i)=J(1);
        J=J(2:end); K=K(2:end);
    elseif J(1)<K(1)
        JK(i)=J(1);
        JK(i+1:end)=K;
        J=[]; K=[];
    elseif J(1)>K(1)
        JK(i)=K(1);
        JK(i+1:end)=J;
        J=[]; K=[];
    end
end
end

function ind=SUB_simplices_SetRemains(T,n)
% SETREMAINS finds those indices not in a given set
% ind=SetRemains(T,n) finds those indices between 1 and n that are not
%   found within the set T.

% designed to replace setdiff, which was slowing things down

ind=1:n; ind(T)=0; ind=ind(ind>0); % option 1
end