function M = ALGO_PMnD_v1_20220128(Na,Ta,Nb,Tb)
% PMND calculates the projection matrix from tesselation 1 to tesselation
% 2 in arbitrary dimension; it is a generalization of ProjectionMatrix3D

%===Advancing front algorithm===%
n=size(Na,1); n=n+1;          % dimension of the space, then number of vertices in each simplex
na=size(Ta,1); nb=size(Tb,1); % number of simplices in each tesselation
cand=[1;1];                   % algo. is non-self-starting; first simplices in each list must intersect
listb=1;                      % initialize list of simplices in tesselation 2 to try
flagb=zeros(nb+1,1); flagb([1,end])=1; % note that the first simplex of tesselation 2 has been tried
while ~isempty(listb)
  Simpb=listb(1); listb=listb(2:end); % choose first untried simplex of tess. 2
  lista=cand(1,cand(2,:)==Simpb);     % choose first untried simplex of tess. 1
                                      %   known to intersect simp. b
  lista=unique(lista);                % don't double dip
  flaga=zeros(na+1,1);
  flaga(end)=1; flaga(lista)=1;       % note which simplices in tess. 1 have been tried
                                      %   with simp. b
  while ~isempty(lista)
    Simpa=lista(1); lista=lista(2:end); % choose first untried simplex of list a
%     [Z,nhbrs]=ALGO_simplices_IntersectSimplices_v2_20220128(Nb(:,Tb(Simpb,1:n)),Na(:,Ta(Simpa,1:n)));
    [Z,nhbrs]=ALGO_simplices_IntersectSimplices_v3_20220308(Nb(:,Tb(Simpb,1:n)),Na(:,Ta(Simpa,1:n)));
    if ~isempty(Z)
      nhbrsa=Ta(Simpa,(n+1):end); % all nhbrs of simp. a
      lista=[lista, nhbrsa(flaga(nhbrsa)==0)]; % add those nhbrs of simp. a not yet
                                  %   considered to list of those that can
                                  %   intersect simp. b
      flaga(nhbrsa)=1;
      nhbrsb=Tb(Simpb,(n+1):end); % all nhbrs of simp. b
      nhbrsb=nhbrsb(nhbrs==1);    % nhbrs of simp. b which also intersect simp. a
      listb=[listb, nhbrsb(flagb(nhbrsb)==0)];
      flagb(nhbrsb)=1;
      nhbrsb=[Simpa*ones(size(nhbrsb));nhbrsb];
      cand=[cand, nhbrsb];        % add the pairs of simp. a and nhbrs of simp. b
                                  %   that intersect to list of candidates
    end
  end
end
end