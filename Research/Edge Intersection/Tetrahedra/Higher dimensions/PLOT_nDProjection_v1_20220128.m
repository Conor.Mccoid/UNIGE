function PLOT_nDProjection_v1_20220128(V,U,Z)
% NDPROJECTION plots a 3D projection of nD simplices U and V with
% intersection Z

[n,d]=size(V); % n is the dimension of the space, d the number of vertices
map=nchoosek(1:n,3);
ind=nchoosek(1:d,2);
m=ceil(sqrt(length(map)));
figure(1)
clf
for j=1:length(map)
  Vm=V(map(j,:),:); Um=U(map(j,:),:); Zm=Z(map(j,:),:);
  subplot(m,m,j)
  axis('equal')
  view(-37.5,30)
  for i=1:length(ind)
    line(Vm(1,ind(i,:)),Vm(2,ind(i,:)),Vm(3,ind(i,:)),'Color','b')
    line(Um(1,ind(i,:)),Um(2,ind(i,:)),Um(3,ind(i,:)),'Color','r')
  end
  PLOT_PatchPolyhedron_v1_20201105(Zm)
end