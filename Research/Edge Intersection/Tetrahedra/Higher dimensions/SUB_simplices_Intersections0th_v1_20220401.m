function [W,S,list_JK,list_eta,flag_q,nhbrs]=SUB_simplices_Intersections0th_v1_20220401(X,S,v0,V)
% INTERSECTIONS0TH runs the first step of Intersections

n=length(X);    % n: dimension (+1) of the space, number of vertices
                % m=0 n_T=0
list_JK=zeros(2,nchoosek(n,2));
list_eta=zeros(1,n);

flag_q=zeros(nchoosek(n,2),n);
flag_s=NaN(n,nchoosek(n,2));
Q=cell(nchoosek(n,2),n);

for eta=1:n
    for j=1:n
        for k=(j+1):n
            JK=[j;k];
            N_JK=SUB_simplices_ConvertCombinadics0th(j,k);
            
            if S(eta,j)~=S(eta,k)
                if flag_q(N_JK,eta)~=1
                    u=[ones(1,2); X(eta,JK)] \ eye(2,1);
                    Q{N_JK,eta}=X(:,JK)*u;
                    
                    flag_q(N_JK,eta)=1;
                    list_JK(:,N_JK)=JK;
                    list_eta(:,eta)=eta;
                end
            else
                flag_s(eta,N_JK)=S(eta,j);
            end
            
        end
    end
end

ind_JK=1:nchoosek(n,2); ind_JK=ind_JK(sum(flag_q,2)>0);
S_denom=NaN(nchoosek(n,2),nchoosek(n,1));
S_num=NaN(nchoosek(n,2),nchoosek(n,2));
S=zeros(nchoosek(n,2),nchoosek(n,1),n);

for N_JK=ind_JK
    JK=list_JK(:,N_JK);
    
    ind_flagged=1:n; ind_flagged=ind_flagged(~isnan(flag_s(:,N_JK)));
    for eta=ind_flagged
        ind_gamma=[1:(eta-1),(eta+1):n];
        for gamma=ind_gamma
            if flag_q(N_JK,gamma)==1
                N_etagamma=SUB_simplices_ConvertCombinadics0th(eta,gamma);
                S(N_JK,gamma,eta)=flag_s(eta,N_JK);
                if isnan(S_denom(N_JK,gamma))
                    S_denom(N_JK,gamma)=sign(det([ones(2,1), X(gamma,JK)']));
                end
                S_num(N_JK,N_etagamma)=(-1)^(eta>gamma) * S(N_JK,gamma,eta)*S_denom(N_JK,gamma);
                % the pm on the sign here is related to how many swaps are
                % needed to put eta in front starting from an ordered list
                % of [gamma,eta], and in the general case [T,eta]
            end
        end
    end
    
    ind_eta=1:n; ind_eta=ind_eta(flag_q(N_JK,:)>0);
    for t=ind_eta
    	eta=list_eta(:,t);
        
        ind_gamma=[1:(eta-1),(eta+1):n];
        for gamma=ind_gamma
            N_etagamma=SUB_simplices_ConvertCombinadics0th(eta,gamma);
            if isnan(S_denom(N_JK,t))
                S_denom(N_JK,t)=sign(det([ones(2,1), X(eta,JK)']));
            end
            
            if isnan(S_num(N_JK,N_etagamma))
                q=Q{N_JK,eta};
                S(N_JK,eta,gamma)=sign(q(gamma));
                
                S_num(N_JK,N_etagamma)=(-1)^(eta<gamma) * S(N_JK,eta,gamma)*S_denom(N_JK,eta);
            else
                S(N_JK,eta,gamma)=(-1)^(eta<gamma) * S_num(N_JK,N_etagamma)*S_denom(N_JK,eta);
            end
        end
        
    end
    
end

ind_eta=1:n; ind_eta=ind_eta(sum(flag_q,1)>0);

S_temp=prod(S>=0,3).*flag_q;
W=zeros(n-1,nnz(S_temp));
counter=0;
nhbrs=zeros(1,n);
for JK=ind_JK
    for eta=ind_eta
        if S_temp(JK,eta)==1
            q=Q{JK,eta}; q=q(2:end);
            counter=counter+1;
            W(:,counter)=v0+V*q;
            nhbrs(n+1-eta)=1;
        end
    end
end
W=W(:,1:counter);

S=S(ind_JK,ind_eta,:);
list_JK=list_JK(:,ind_JK);
list_eta=list_eta(ind_eta);
flag_q=flag_q(ind_JK,ind_eta);
end

function N=SUB_simplices_ConvertCombinadics0th(j,k)
% CONVERTCOMBINADICS0TH computes the combinadic representation of a set
% containing only two indices
%   N=ConvertCombinadics0th(j,k) finds the representation of the set [j,k]
%   in the combinatorial number system.

% designed to replace ConvertCombinadics for sets of two numbers

if k<j
    l=j; j=k; k=l;
end
if k==2
    N=1;
else
    N=nchoosek(k-1,2) + j;
end
end