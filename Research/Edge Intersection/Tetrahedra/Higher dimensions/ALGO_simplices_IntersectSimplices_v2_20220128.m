function [Z,nhbrs]=ALGO_simplices_IntersectSimplices_v2_20220128(V,U)
% INTERSECTSIMPLICES calculates the intersection between two simplices of
% arbitrary dimension

% Important note: this version, and the related subfunctions, do not
% include a formal treatment of the signs of each component. A version that
% is in keeping with the principles laid out in the paper would feature an
% in-depth network of connections between signs, as well as tests for the
% same sign of components of previous intersections in the event that the
% m-face of X does not have a maximum number of intersections at a given
% step.

% Also missing is the compatibility with an advancing front algorithm.
% Without it, this cannot be considered part of the PANG family. We need to
% add a variable n that indicates which neighbours of X (or Y?) also
% intersect Y (or X?).
% On this matter, the order so far prescribed for the 2D and 3D simplices
% is to have the e_0 plane last and the others in order.

% Update 28.01.2022: the order of neighbours, based on hyperplanes, now
% follows a unified format defined by nchoosek(1:n+1,n) where n is the
% dimension of the space. This order is P_n, P_{n-1}, ..., P_0. This
% corresponds to T(n+1)=1, T(n)=1, ..., T(1)=1, using the index T which has
% consistently identified the hyperplanes in this and all subfunctions.

% 3rd version (forthcoming) will make use of three upgrades:
%   - combinadics and adjacency matrix, to minimize storage and remove need
%   to compare sets J and K
%   - connectivity of signs, to allow use of alternative methods for
%   calculating the intersection
%   - redundant sign check subroutine, to ensure persimony of the algorithm

n=length(U); % there are n vertices of U (and V) which means the dimension
             %  of the space is n-1
             
%--Change of Coordinates--%
v0=V(:,1); A=V(:,2:end)-v0;
global X
X=A \ (U-v0);
X=[1-ones(1,n-1)*X; X]; % tack on the x*e_0 'coordinates'
                        % the row order in X must match the column order
                        % for binV, ie. for the vertices of V

%--Vertices of X in Y--%
nhbrs=zeros(1,n);     % which neighbours of V also intersect U
S=X>=0;
if prod(sum(S,2))==0   % if there is a given coordinate where all X values are negative
%   disp('Simplices do not intersect') % then there is a hyperplane that entirely divides X and Y
  Z=[];
  return
end
S=prod(S); m=sum(S==1); Z=U(:,S==1);

%--Intersections--%
global D
D=zeros(2^n,2^(n+1)); % storage for determinants
Z=[Z,zeros(n-1,2^n * n)]; % is this the actual number of possible intersections? surely we can prove a tighter bound
H=cell(1); J=H; T=H;
H{1}=X; J{1}=eye(n); T{1}=zeros(n,1);
for i=1:n-2
  [H,J,T]=SUB_simplices_Unfold_v1_20211214(H,J,T);
  if isempty(H)         % if no intersections are calculated for a given step then there are no more intersections to be found
    Z=Z(:,1:m);
    return
  end
  for j=1:length(T)     % given this unpacking it is probably more efficient to do this step within the subfunctions
    H1=H{j}; S=prod(H1>=0); mj=sum(S==1);
    Z(:,m+(1:mj))=A*H1(2:end,S==1)+v0; m=m+mj;
    if i==1 && sum(S)~=0             % check which hyperplanes of V intersect with edges of U
      ind=1:n; ind=n+1-ind(T{j}==1); nhbrs(ind)=1;
    end
  end
end
Z=Z(:,1:m);

%--Vertices of Y in X--%
binV=zeros(n,1); indV=binV; % another probably terrible way of doing things... see notes on v1 of Unfold and v2 of Orphan for thoughts
while ~isempty(H)
  H1=H{1}; T1=T{1};
  indT=1:n; indT=indT(T1==0); % at most two values in indT
  for i=indT
    if indV(i)==0
      j=indT(indT~=i);
      if sign(max(H1(j,:)))~=sign(min(H1(j,:)))
        binV(i)=1;
      end
      indV(i)=1;
    end
  end
  m=length(T); indT=zeros(m,1);
  for i=1:m
    if nnz(indV | T{i})==n
      indT(i)=1;
    end
  end
  H=H(indT==0); T=T(indT==0);
end
Z=[Z,V(:,binV==1)];
if sum(binV)>=n-1
  nhbrs=ones(1,n);
end

% %--Remove duplicate entries in Z--%
% Z=SUB_RemoveDoubles_v1_20220128(Z);

%--Plot results--%
if n==4
  PLOT_PatchPolyhedron_v1_20201105(Z)
  pause(0)
% elseif n==5
%   ind=nchoosek(1:n-1,3);
%   for i=1:length(ind)
%     subplot(2,2,i)
%     PLOT_PatchPolyhedron_v1_20201105(Z(ind(i,:),:))
%     pause(0)
%   end
elseif n>4
%   PLOT_nDProjection_v1_20220128(V,U,Z)
  PLOT_4DProjection_v1_20220128(V,U,Z)
  pause(0)
end