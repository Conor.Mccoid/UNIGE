function PLOT_4DProjection_v1_20220128(V,U,Z)
% NDPROJECTION plots the 3D projection of 4D simplices U and V with
% intersection Z with the hyperplane x_4=0

offset_min=min(V(end,:)); offset_max=max(V(end,:));
% offset_min=-1; offset_max=1;
for offset=offset_min:0.01:offset_max
  p=[0;0;0;offset];
  Vm=SUB_ProjectObject(V+p);
  Um=SUB_ProjectObject(U+p);
  Zm=SUB_ProjectObject(Z+p);
  figure(1)
  clf
  axis([-1,1,-1,1,-1,1])
  view(-37.5,30)
  d=size(Vm,2);
  if d>1
    indV=nchoosek(1:d,2);
    for i=1:size(indV,1)
      line(Vm(1,indV(i,:)),Vm(2,indV(i,:)),Vm(3,indV(i,:)),'Color','b')
    end
  end
  d=size(Um,2);
  if d>1
    indU=nchoosek(1:d,2);
    for i=1:size(indU,1)
      line(Um(1,indU(i,:)),Um(2,indU(i,:)),Um(3,indU(i,:)),'Color','r')
    end
  end
  PLOT_PatchPolyhedron_v1_20201105(Zm)
  pause(0.01)
end
end

function B=SUB_ProjectObject(A)

[m,n]=size(A); % m is dimension of space, n is number of vertices in object A
B=zeros(m-1,0);
for i=1:n
  si=sign(A(end,i))>=0;
    for j=i+1:n
      sj=sign(A(end,j))>=0;
      if sj~=si
        % calculate intersection between this edge of A and hyperplane
        h=zeros(m-1,1);
        for k=1:m-1
          h(k)=det([A([k,end],i), A([k,end],j)])/(A(end,i)-A(end,j)); % brute-force, there are better options for this calculation
        end
        B=[B,h];
      end
    end
end
end