function N = SUB_simplices_ConvertCombinadics_v1_20220309(A)
% CONVERTCOMBINADICS converts a set of integers into its combinadic
% representation
%   N=ConvertCombinadics(A) converts the matrix A into a list of combinadic
%   representations of its combinations. Each column of A is taken to be a
%   m-combination, where m is the number of rows of A. The elements of A
%   may be from 1 to n, where n is the number of indices being chosen
%   from. N may be from 1 to n choose m, where m is the size of A.

[m,n]=size(A);
N=zeros(1,n);
A=sort(A,'ascend')-1;
for i=1:n
    for j=1:m
        if A(j,i)>=j
            N(i)=N(i) + nchoosek(A(j,i),j);
        end
    end
end
N=N+1;