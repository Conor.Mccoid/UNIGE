function [H_new,J_new,T_new,m_new] = SUB_simplices_Unfold_v1_20211214(H_all,J_all,T_all)
% UNFOLD marches through combinations of hyperplane intersections
%   [H_new,J_new,T_new]=Unfold(H_all,J_all,T_all) computes the new set of
%   intersections H_new with indices of X J_new and indices of Y T_new.

%   H_all - set of all intersections calculated from the previous step,
%           arranged in a cell array where each cell has a specific set of
%           indices of Y associated with it;
%   J_all - indices relating to vertices of X for each intersection in
%           H_all;
%   T_all - indices relating to hyperplanes of Y for the given set of
%           intersections in H_all;

% This first version will feature a brute-force method that (hopefully)
% does not go over the same combination of hyperplanes twice.

% Things to improve in version 2:
%   - if a combination of hyperplanes, esp. a single hyperplane, had no
%       intersections then it should be excluded from future combinations;
%   - is there not a faster/less involved way to remove dupliate
%       combinations?

T=T_all{1}; n=length(T); d=nnz(T)+1; m_max=nchoosek(n,d);
H_new=cell(m_max,1); J_new=H_new; T_new=H_new; m_new=0;
% D=zeros(m_max,nchoosek(n+1,d)); % possibly a faster way to get second input from first
m=length(T_all); indTy=zeros(n,m);
for i=1:m
    H=H_all{i}; J=J_all{i}; T=T_all{i};
    indTy(T==0,i)=indTy(T==0,i)+1;
    indy=1:n; indy=indy(indTy(:,i)==1);
    for y=indy
        [H_temp,J_temp,T_temp]=SUB_simplices_Orphan_v2_20211214(H,J,T,y);
        if ~isempty(H_temp)
            m_new=m_new+1;      % how many new hyperplane combos have we computed?
            H_new{m_new}=H_temp; J_new{m_new}=J_temp; T_new{m_new}=T_temp;
        end
    end
    for j=i+1:m
        temp=T_all{j}-T;
        if nnz(temp)==2
            indTy(temp~=0,j)=-1; % remove hyperplane combinations already completed in this iteration
        end
    end
end
H_new=H_new(1:m_new); J_new=J_new(1:m_new); T_new=T_new(1:m_new);