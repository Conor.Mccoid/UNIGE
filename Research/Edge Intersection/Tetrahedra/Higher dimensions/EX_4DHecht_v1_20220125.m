%===EX_4DHecht_v1_20220125===%
th=pi/10; disp=0.1;
[N,T,N2]=MESH_600cell_20220125(th,disp);

% T=T(1,:); T(6:end)=2;

% figure(1)
% clf
% PLOT_4DMesh_20220125(N,T,N2,T)

figure(1)
clf
ALGO_PMnD_v1_20220128(N,T,N2,[T(1,1:5),2*ones(1,5)])