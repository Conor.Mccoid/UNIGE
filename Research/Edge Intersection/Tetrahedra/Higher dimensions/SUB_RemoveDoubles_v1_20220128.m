function Z=SUB_RemoveDoubles_v1_20220128(Z,tol)
% REMOVEDOUBLES deletes duplicate entries from polytope Z
%   Z=RemoveDoubles(Z) computes the Euclidean distance between vertices of
%   Z and deletes a given vertex if this distance is less than tol.

if nargin<2
  tol=0;
end
[n,m]=size(Z); % n is the dimension of the space, m the number of vertices in Z
i=1;           % initialize search
while i<m      % pick a vertex of Z
  j=i+1;       % pick another vertex of Z
  while j<=m   % for every other vertex of Z
    if norm(Z(:,i)-Z(:,j))<=tol % check Euclidean distance between the two
      ind=1:m; ind=ind~=j; Z=Z(:,ind); % remove the second vertex
      m=m-1;   % reduce the number of vertices
    else
      j=j+1;
    end
  end
  i=i+1;       % check the next 
end