function [H_new,J_new,T_new,C] = SUB_simplices_Orphan_v1_20211126(H,J,T,A,y,X)
% ORPHAN generates adjacency matrix for intersections
%   [H_new,J_new,T_new,C] = Orphan(H,J,T,A,y,X) finds all intersections
%   H_new between the n-simplex X and the hyperplanes indexed by T and y.
%
%   Inputs:
%       H - intersections between X and Y, indexed by J and T;
%       J - vertices of X that form the m-face on which the intersections
%           of H lie;
%       T - hyperplanes of Y where the intersections H lie;
%       A - defines which intersections of H are neighbours, ie. the
%           adjacency matrix for the convex polyhedron embedded in T;
%       y - next hyperplane to intersect the convex hull of H with;
%       X - position of the vertices of X.
%
%   Outputs:
%       H_new - intersections between X and Y indexed by J_new and T_new;
%       J_new - vertices of X for the intersections of H_new;
%       T_new - T in union with y;
%       C     - adjacency matrix for H_new.

[n,m]=size(H);
m_max=floor((m+1)/2)*ceil((m+1)/2);
m_new=0; B=false(m,m_max);
T_new=T; T_new(y)=1; J_new=zeros(n,m_max); H_new=zeros(n,m_max);
dim=sum(J(:,1)); % number of non-zero indices, ie. current dimension of the space
ind=1:m;
for i=1:m-1
    indj=ind(A(i,:));
    for j=indj
        if sign(H(y,i))~=sign(H(y,j))
            % calculate intersection
            m_new=m_new+1; B([i,j],m_new)=true;
            J_new(:,m_new)=J(:,i) | J(:,j);
%             H_new(:,m_new)=SUB_simplices_Intersection_v1_?(X,J_new(:,m_new),T_new);
        end
    end
end
J_new=J_new(:,1:m_new); H_new=H_new(:,1:m_new);
C=false(m_new,m_new);
for i=1:m_new
    temp=B(B(:,i),i+1:m_new);
    C(i,(i+1):end)=temp(1,:) | temp(2,:);
    if dim>1
        nhbrs=A(B(:,i),:);
        indj=ind(nhbrs(1,:));
        for j=indj
            if sum(A(j,:) & nhbrs(2,:))>0
                C(i,(i+1):end)=C(i,(i+1):end) | B(j,i+1:m_new);
            end
        end
    end
end