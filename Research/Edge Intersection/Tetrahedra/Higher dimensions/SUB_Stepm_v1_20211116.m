function output = SUB_Stepm_v1_20211116(D,J,H)

d=D(:,1); D=D(:,2:end);
j=J(:,1); J=J(:,2:end);
h=H(:,1); H=H(:,2:end);
while ~isempty(D)
    indT=prod(d==D); D=D(:,indT==0);
    JT=J(:,indT==1); J=J(:,indT==0);
    HT=J(:,indT==1); H=H(:,indT==0);
    while ~isempty(JT)
        indj=sum(j~=JT);
        Jj=JT(:,indj==2); JT=JT(:,indj<2);
        Hj=HT(:,indj==2); HT=HT(:,indj<2);
    end
end