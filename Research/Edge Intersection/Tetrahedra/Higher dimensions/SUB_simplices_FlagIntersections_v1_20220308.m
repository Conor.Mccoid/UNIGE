function results = SUB_simplices_FlagIntersections_v1_20220308(S,J,T,list_eta,A)
% INTERSECTIONS flags intersections to calculate

n=length(T); % number of intersections of the previous generation
ind_T=1:n;
for t=ind_T % all Gamma
    m=length(J{t}); ind_J=1:m;
    A_J=A(N(J{t}),N(J{t}));
    for eta=list_eta(:,t) % viable eta
        for j=ind_J % all J
            for k=A_J(j,:) % adjacency
                if S(j,eta)~=S(k,eta) % sign check
                    if flag_q(N(j,k),N(t,eta))~=1
                        % calculate intersection
                        flag_q(N(j,k),N(t,eta))=1;
                    end
                else
                    % flag j+k, t, eta, s
                end
            end
        end
    end
end