function h=SUB_simplices_IntersectHyperplane_v1_20211217(J,T)
% INTERSECTHYPERPLANE computes the intersection of an m-face of X and a
% collection of hyperplanes of Y

% Uses Cramer's rule with a look-up table to find determinants indexed by J
% and T

global X D % recall X and D from IntersectSimplices

%--Initialize outputs--%
n=length(T); m=nnz(T)+1;
h=zeros(n,1);

%--Determine which coordinates of the intersection are needed--%
indT=1:n; indT=indT(T==0);

%--Denominator is the same for all--%
j=bit2int(J,n);
k1=bit2int([1;T],n+1);
if D(j,k1)==0
    D(j,k1)=det([ones(m,1), X(T==1,J==1)']); % could also be calculated with other entries of D now that they're indexed with binary
end

for i=indT
    %--Convert T with i to coordinates of D--%
    count=sum(T(1:i));
    T_new=T; T_new(i)=1;
    k=bit2int([0;T_new],n+1);
    if D(j,k)==0
        D(j,k)=det(X(T_new==1,J==1));
    end
    h(i)=(-1)^count * D(j,k)/D(j,k1); % need to multiply by a sign to indicate column swaps between num and denom
end
end

function k=bit2int(v,n)
% BIT2INT converts binary string to integer

k=0;
for i=1:n
    if v(i)==1
        k=k + 2^(n-i);
    end
end
end