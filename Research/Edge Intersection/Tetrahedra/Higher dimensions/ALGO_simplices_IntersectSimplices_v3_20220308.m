function [Z,nhbrs]=ALGO_simplices_IntersectSimplices_v3_20220308(V,U)
% INTERSECTSIMPLICES calculates the intersection between two simplices of
% arbitrary dimension

% Update 28.01.2022: the order of neighbours, based on hyperplanes, now
% follows a unified format defined by nchoosek(1:n+1,n) where n is the
% dimension of the space. This order is P_n, P_{n-1}, ..., P_0. This
% corresponds to T(n+1)=1, T(n)=1, ..., T(1)=1, using the index T which has
% consistently identified the hyperplanes in this and all subfunctions.

% Update 02.04.2022: 3rd version now makes use of combinadics adjacency
% matrix, redundant sign checks and some of the connectivity of signs. The
% calculation of the sign of the denominators is still direct and does not
% use the recurrence relations. This is likely the last thing to be
% improved upon.

% Notes 03.09.2022: precomputing Pascal's triangle would likely save a
% significant chunk of computation time, since there are several calls to
% nchoosek() throughout the code.

n=length(U); % there are n vertices of U (and V) which means the dimension
             %  of the space is n-1
             
%--Change of Coordinates--%
v0=V(:,1); B=V(:,2:end)-v0;
X=B \ (U-v0);
X=[1-ones(1,n-1)*X; X]; % tack on the x*e_0 'coordinates'
                        % the row order in X must match the column order
                        % for binV, ie. for the vertices of V

%--Vertices of X in Y--%
nhbrs=zeros(1,n);     % which neighbours of V also intersect U
S=X>=0;
if prod(sum(S,2))==0   % if there is a given coordinate where all X values are negative
%   disp('Simplices do not intersect') % then there is a hyperplane that entirely divides X and Y
  Z=[];
  return
end
S_temp=prod(S); m=sum(S_temp==1); Z=U(:,S_temp==1);

%--Combinadic adjacency matrix--%
A_all=SUB_simplices_Combinadics_v1_20220304(n);

%--Intersections--%
S=double(S); S(S==0)=-1; % need to change the 0s to -1s so 0^2=1
Z=[Z,zeros(n-1,2^n * n)]; % is this the actual number of possible intersections? surely we can prove a tighter bound
[W,S,List_J,List_T,flag_JT,nhbrs]=SUB_simplices_Intersections0th_v1_20220401(X,S,v0,B); % initial setup to find nhbrs
mi=size(W,2); Z(:,m+(1:mi))=W; m=m+mi;
for i=2:n-2
    [W,S,List_J,List_T,flag_JT]=SUB_simplices_Intersections_v1_20220308(X,S,List_J,List_T,flag_JT,A_all{i},v0,B);
    mi=size(W,2); Z(:,m+(1:mi))=W; m=m+mi;
end
Z=Z(:,1:m);

%--Vertices of Y in X--%
ind_V=zeros(n,1); flag_V=ind_V;
while ~isempty(List_T)
    T=List_T(:,1); List_T=List_T(:,2:end); % consider first collection then remove
    s=permute(S(:,1,:),[1,3,2]); S=S(:,2:end,:); % same for the set of signs
    T=setdiff(1:n,T); gamma=T(1); eta=T(2); % find the two directions remaining outside T
    s=s(sum(s~=0,2)>0,:); % pick out the two intersections (J and K) for this collection (T)
    if flag_V(eta)~=1
        flag_V(eta)=1;
        if s(1,gamma)~=s(2,gamma)
            ind_V(eta)=1;
        end
    end
    if flag_V(gamma)~=1
        flag_V(gamma)=1;
        if s(1,eta)~=s(2,eta)
            ind_V(gamma)=1;
        end
    end
end
Z=[Z,V(:,ind_V==1)];
if sum(ind_V)>=n-1
  nhbrs=ones(1,n);
end

% %--Remove duplicate entries in Z--%
% Z=SUB_RemoveDoubles_v1_20220128(Z);

% %--Plot results--%
% if n==4
%   PLOT_PatchPolyhedron_v1_20201105(Z)
%   pause(0)
% % elseif n==5
% %   ind=nchoosek(1:n-1,3);
% %   for i=1:length(ind)
% %     subplot(2,2,i)
% %     PLOT_PatchPolyhedron_v1_20201105(Z(ind(i,:),:))
% %     pause(0)
% %   end
% elseif n>4
% %   PLOT_nDProjection_v1_20220128(V,U,Z)
%   PLOT_4DProjection_v1_20220128(V,U,Z)
%   pause(0)
% end