function PLOT_4DMesh_20220125(N1,T1,N2,T2)
% 4DMESH plots the edges of up to two meshes in 4D

[n,d]=size(T1); d=d/2;
T1=T1(:,1:d);
if nargin>2
  T2=T2(:,1:d);
end
map=[1,2,3;
     1,2,4;
     1,3,4;
     2,3,4];
for i=1:4
  subplot(2,2,i)
  axis('equal')
  view(-37.5,30)
  for k=1:n
    X=N1(map(i,:),T1(k,:));
    ind=nchoosek(1:d,2);
    for j=1:length(ind)
      line(X(1,ind(j,:)),X(2,ind(j,:)),X(3,ind(j,:)),'Color','b')
    end
    if nargin>2
      X=N2(map(i,:),T2(k,:));
      ind=nchoosek(1:d,2);
      for j=1:length(ind)
        line(X(1,ind(j,:)),X(2,ind(j,:)),X(3,ind(j,:)),'Color','r')
      end
    end
  end
end
end