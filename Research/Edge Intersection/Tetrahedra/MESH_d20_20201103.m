function [N,T,N2] = MESH_d20_20201103(yaw,pitch,roll,disp)
% D20 constructs a tetrahedral mesh of an icosahedron
%   [N,T] = d20 builds a mesh N of the vertices of an icosahedron with
%   unit length with the origin at its centre and the collection of
%   tetrahedra T that form the icosahedron. Each column of N contains the
%   coordinates in R3 of a vertex and each row of T contains the indices of
%   the four vertices in N that make up a tetrahedron and the four
%   neighbours of the tetrahedron.

p=(1+sqrt(5))/2;
N=[0,0,0;
    0, 1, p;
    0, 1,-p;
    0,-1, p;
    0,-1,-p;
    1, p, 0;
    1,-p, 0;
   -1, p, 0;
   -1,-p, 0;
    p, 0, 1;
   -p, 0, 1;
    p, 0,-1;
   -p, 0,-1];
N=N';
T=[ 1, 2, 4,11;
    1, 2, 4,10;
    1, 2, 6,10; 
    1, 2, 6, 8; 
    1, 2, 8,11; 
    1, 4,11, 9; 
    1, 4, 7, 9; 
    1, 4, 7,10; 
    1,10, 6,12; 
    1,10, 7,12; 
    1, 6,12, 3; 
    1, 6, 8, 3; 
    1,11, 8,13; 
    1,11,13, 9; 
    1,13, 9, 5;
    1, 5, 7, 9; 
    1, 5, 7,12;
    1, 3, 5,12;
    1, 3, 8,13;
    1, 3, 5,13];
% T_nhbrs=[2, 5, 6,21;
%          1, 3, 8,21;
%          2, 4, 9,21; 
%          3, 5,12,21;
%          4, 1,13,21;
%          1, 7,14,21;
%          6, 8,16,21;
%          7, 2,10,21;
%          3,10,11,21;
%          9, 8,17,21;
%          9,12,18,21;
%          4,11,19,21;
%          5,14,19,21;
%          6,13,15,21;
%         14,16,20,21;
%          7,15,17,21;
%         10,16,18,21;
%         11,17,20,21;
%         12,13,20,21;
%         15,18,19,21];
% T=[T,T_nhbrs];
T=SUB_Neighbours_v1_20201105(T);

if nargin>0
    R=[cos(yaw),-sin(yaw),0; sin(yaw),cos(yaw),0;0,0,1] * ...
        [cos(pitch),0,sin(pitch); 0,1,0; -sin(pitch),0,cos(pitch)] * ...
        [1,0,0; 0,cos(roll),-sin(roll); 0,sin(roll),cos(roll)];
    N2=R*N;
    N2=N2+disp;
end
end