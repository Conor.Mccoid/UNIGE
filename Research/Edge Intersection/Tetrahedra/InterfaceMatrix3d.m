function InterfaceMatrix3d(Na,Ta,Nb,Tb)
% INTERFACEMATRIX3D projection matrix for nonmatching thetrahedra grids
%   M=InterfaceMatrix3d(Na,Ta,Nb,Tb); takes two tetrahedra meshes Ta
%   and Tb with associated nodal coordinates in Na and Nb and
%   computes the projection matrix M. As a precondition, the first
%   thetrahedra in Ta and Tb need to intersect.
bl=1;                        % bl: list of tetrahedra of Tb to treat
bil=1;                       % bil: list of tetrahedra Ta to start with
bd=zeros(size(Tb,1)+1,1);      % bd: flag for tetrahedra in Tb treated
bd(end)=1;                     % guard, to treat boundaries
bd(1)=1;                       % mark first tetrahedra in b list.
while ~isempty(bl)
    bc=bl(1); bl=bl(2:end);      % bc: current tetrahedra of Tb
    al=bil(1); bil=bil(2:end);   % tetrahedra of Ta to start with
    ad=zeros(size(Ta,1)+1,1);    % same as for bd
    ad(end)=1;
    ad(al)=1;
    n=[0 0 0 0];                 % tetrahedra intersecting with neighbors
    while ~isempty(al)
        ac=al(1); al=al(2:end);    % take next candidate
        [P,nc,H,v]=Intersection3d(Nb(:,Tb(bc,1:4)),Na(:,Ta(ac,1:4)));
        if ~isempty(P)             % intersection found
            t=Ta(ac,4+find(ad(Ta(ac,5:8))==0));
            al=[al t];               % add neighbors
            ad(t)=1;
            n(find(nc>0))=ac;        % ac is starting candidate for neighbor
        end
    end
    tmp=find(bd(Tb(bc,5:8))==0); % find non-treated neighbors
    idx=find(n(tmp)>0);          % only if intersecting with Ta
    t=Tb(bc,4+tmp(idx));
    bl=[bl t];                   % and add them
    bil=[bil n(tmp(idx))];       % with starting candidates Ta
    bd(t)=1;
end
end

function [P,nc,H,v]=Intersection3d(X,Y)
% INTERSECTION3D computes intersection polyhedron of two tetrahedra
%   [P,nc,H,v]=Intersection3d(X,Y); computes for the two given tetrahedra
%   X and Y (point coordinates are stored column-wise) the points P
%   where they intersect, in nc the bolean information of which
%   neighbors of X are also intersecting with Y, in H stored row-wise
%   the polygons of the faces of the intersection polyhedron, stored
%   counter-clock-wise looking from the outside, and in v the volume
%   of the intersection.

P=[];nc=[0 0 0 0];
sxk=zeros(4,1);                        % intersection node index
syk=zeros(4,1);                        % on surfaces of X and Y
l1=[1 2 3 4 1 2];                     %enumeration of lines
l2=[2 3 4 1 3 4];
s1=[1 1 2 3 1 2];                     %faces touching each line
s2=[4 2 3 4 3 4];
ni=[1 3 4; 1 2 4; 1 2 3; 2 3 4];      %faces touching each node
for i=1:6                              % find intersections of edges of
    for j=1:4                            % X with surfaces of Y
        p=TriangleLineIntersection([X(:,l1(i)) X(:,l2(i))],...
            [Y(:,j) Y(:,mod(j,4)+1) Y(:,mod(j+1,4)+1)]);
        if ~isempty(p)
            [k,P]=InsertPoint(p,P);          % insert point if new
            nc(s1(i))=1;
            nc(s2(i))=1;
            syk(j)=syk(j)+1;
            SY(j,syk(j))=k;  % remember to which surfaces
            sxk(s1(i))=sxk(s1(i))+1;
            SX(s1(i),sxk(s1(i)))=k;  % it belongs
            sxk(s2(i))=sxk(s2(i))+1;
            SX(s2(i),sxk(s2(i)))=k;
        end
    end
end
for i=1:6                              % find intersections of edges of
    for j=1:4                            % Y with surfaces of X
        p=TriangleLineIntersection([Y(:,l1(i)) Y(:,l2(i))],...
            [X(:,j) X(:,mod(j,4)+1) X(:,mod(j+1,4)+1)]);
        if ~isempty(p)
            [k,P]=InsertPoint(p,P);
            nc(j)=1;
            sxk(j)=sxk(j)+1;
            SX(j,sxk(j))=k;
            syk(s1(i))=syk(s1(i))+1;
            SY(s1(i),syk(s1(i)))=k;
            syk(s2(i))=syk(s2(i))+1;
            SY(s2(i),syk(s2(i)))=k;
        end
    end
end
fn=[0 0 0 0];
for i=1:4                              % find interior points of X in Y
    if PointInTetrahedron(X(:,i),Y)
        [k,P]=InsertPoint(X(:,i),P);
        sxk(ni(i,1))=sxk(ni(i,1))+1;
        SX(ni(i,1),sxk(ni(i,1)))=k;
        sxk(ni(i,2))=sxk(ni(i,2))+1;
        SX(ni(i,2),sxk(ni(i,2)))=k;
        sxk(ni(i,3))=sxk(ni(i,3))+1;
        SX(ni(i,3),sxk(ni(i,3)))=k;
        p=X(:,i); 
        fn(i)=1;
    end
    if PointInTetrahedron(Y(:,i),X)       % and vice versa
        [k,P]=InsertPoint(Y(:,i),P);
        syk(ni(i,1))=syk(ni(i,1))+1;
        SY(ni(i,1),syk(ni(i,1)))=k;
        syk(ni(i,2))=syk(ni(i,2))+1;
        SY(ni(i,2),syk(ni(i,2)))=k;
        syk(ni(i,3))=syk(ni(i,3))+1;
        SY(ni(i,3),syk(ni(i,3)))=k;
        p=Y(:,i);
    end
end
if sum(fn)>1                           % more than one point inside
    nc=[1 1 1 1];                        % means all neighbors intersect
end
H=[];                                  % contains surfaces of the
n=0;                                   % intersection polyhedron
v=0;
if size(P,2)>3                         % construct intersection polyhedra,
    cm=sum(P')'/size(P,2);               % center of mass
    for i=1:4                            % scan surfaces of X and Y
        if sxk(i)>0
            no=RemoveDuplicates(SX(i,1:sxk(i)));
            if length(no)>2 && NewFace(H,no)  % don’t compute degenerate polygons
                p=P(:,no);                     % and check if face is new
                [p,id]=OrderPoints(p,cm);      % order points counter-clock-wise
                n=n+1;
                H(n,1:length(id))=no(id);      % add surface
                v=v+PyramidVolume(p,cm);       % add volume
            end
        end
        if syk(i)>0
            no=RemoveDuplicates(SY(i,1:syk(i)));
            if length(no)>2 && NewFace(H,no)
                p=P(:,no);
                [p,id]=OrderPoints(p,cm);
                n=n+1;
                H(n,1:length(id))=no(id);
                v=v+PyramidVolume(p,cm);
            end
        end
    end
    PLOT_PatchPolyhedron_v1_20201105(P)
    pause(0)
end
end

function p=TriangleLineIntersection(X,Y)
% TRIANGLELINEINTERSECTION intersection of a line and a triangle
%   p=TriangeLineIntersection(X,Y); computes for a given line X in 3d,
%   and a triangle Y in 3d, the point p of intersection in the
%   triangle, and otherwise returns p=[];
p=[];
b=Y(:,1)-X(:,1);
A=[X(:,2)-X(:,1) Y(:,1)-Y(:,2) Y(:,1)-Y(:,3)];
if rank(A)==3                         % edge and triangle not parallel
    r=A\b;
    if r(1)>=0 && r(1)<=1 && r(2)>=0 && r(3)>=0 && r(2)+r(3)<=1
        p=X(:,1)+r(1)*(X(:,2)-X(:,1));
    end
end
end

function p=PointInTetrahedron(X,Y)
% POINTINTETRAHEDRON check if a point is inside a tetrahedron
%   p=PointInTetrahedron(X,Y); checks if the point X is contained in
%   the tetrahedron Y.
D0=det([Y; ones(1,4)]);
D1=det([X Y(:,2:4); ones(1,4)]);
D2=det([Y(:,1) X Y(:,3:4); ones(1,4)]);
D3=det([Y(:,1:2) X Y(:,4); ones(1,4)]);
D4=det([Y(:,1:3) X; ones(1,4)]);
p=sign(D0)==sign(D1) & sign(D0)==sign(D2) & sign(D0)==sign(D3) & ...
    sign(D0)==sign(D4);
end

function [k,P]=InsertPoint(p,P)
% INSERTPOINT inserts a new intersection point
%   [k,P]=InsertPoint(p,P); inserts an intersection point p into
%   the list of intersection points P. If the point p is already
%   contained in P, the point is not inserted, and k is the index
%   of the point found in the list. Otherwise, the new point is
%   inserted, and k is its index in the list.
ep=10*eps;                         % tolerance for identical nodes
k=1;
while k<=size(P,2) && norm(p-P(:,k))>ep
    k=k+1;
end
if k>size(P,2)                     % new node is not contained in P
    P(:,k)=p;
end
end

function p=RemoveDuplicates(p)
% REMOVEDUPLICATES removes multiple entries
%   p=RemoveDuplicates(p) removes duplicate entries from the vector
%p.
p=sort(p);
i=1;
j=2;                          % remove duplicates
while j<=length(p)
    if p(i)<p(j)
        i=i+1;
        p(i)=p(j);
        j=j+1;
    else
        j=j+1;
    end
end
p=p(1:i);
end

function b=NewFace(H,id)
% NEWFACE checks if index set is contained already in H
%   b=NewFace(H,id) checks if a permutation of the vector id is
%   contained in a row of the matrix H.
n=size(H,1);
m=length(id);
A=zeros(n,m);
A(1:n,1:size(H,2))=H;
id=sort(id);
i=0;
b=1==1;                    % true
while b && i<n
    i=i+1;
    b=norm(sort(A(i,1:m))-id)>0;
end
end

function [p,id]=OrderPoints(p,cm)
% ORDERPOINTS order points counterclockwise
%   [p,id]=OrderPoints(p,cm); orders the points in a plane in 3d
%   stored columnwise in the matrix p counterclockwise looking from
%   the side opposite to the point cm outside the plane. There must
%   be more than two points. p contains the reorderd points, and id
%   contains the index reordering.
d1=p(:,2)-p(:,1);               % two reference vectors
d2=p(:,3)-p(:,1);
if (cm-p(:,1))'*cross(d1,d2)<0  % good orientation ?
    dr=d1/norm(d1);               % ’x-axis’ unit vector
    dn=d2-d2'*dr*dr;
    dn=dn/norm(dn);               % ’y-axis’ unit vector
else
    dr=d2/norm(d2);               % ’x-axis’ unit vector
    dn=d1-d1'*dr*dr;
    dn=dn/norm(dn);               % ’y-axis’ unit vector
end
for j=2:size(p,2)
    d=p(:,j)-p(:,1);
    ao(j-1)=angle(d'*dr+sqrt(-1)*d'*dn);
end
[tmp,id]=sort(ao);
id=[1 id+1];
p=p(:,id);
end

function v=PyramidVolume(P,c)
% PYRAMIDVOLUME computes the volume of a pyramid
%   v=PyramidVolume(P,c) computes for the pyramid in 3d with base
%   polygone given by the nodes P stored columnwise and the top c
%   its volume v.
a=0;                           % area
for i=3:size(P,2)
    a=a+norm(cross(P(:,i)-P(:,1),P(:,i-1)-P(:,1)))/2;
end
no=cross(P(:,3)-P(:,1),P(:,2)-P(:,1));
no=no/norm(no);
h=(c-P(:,1))'*no;
v=abs(a*h/3);
end