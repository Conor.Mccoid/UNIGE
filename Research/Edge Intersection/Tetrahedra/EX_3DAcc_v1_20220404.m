%% Accuracy example
% Simple extension of the 2D accuracy example into 3D.
% Swapping V and U in the PANG algorithm yields interesting results.
% Clearly there is a preferred direction for this algorithm. For the new
% algorithm the only notable difference is in computation time, and there
% only how the time is distributed.

N=1000;
error_algo=zeros(N,1);
error_PANG=error_algo;
time_algo=error_algo;
time_PANG=error_algo;

aa=pi*linspace(0,1,N+2); aa=aa(2:end-1);
T=[1:4, 2*ones(1,4)];

for i=1:N
    a=aa(i);
    d=0.5;
    V=[ 0,1,0,cos(a);
        0,0,1,0;
        0,0,0,sin(a)];
    U=[cos(a/2),cos(a/2 + 2*pi/3),cos(a/2 + 4*pi/3),0;
       0,       0,                0,                1;
       sin(a/2),sin(a/2 + 2*pi/3),sin(a/2 + 4*pi/3),0];
    U=d*U;
    
    pause(0)
    tic
    [P_PANG,~,~,vol_PANG]=Intersection3d(V,U);
    time_PANG(i)=toc;
    
    pause(0)
    tic
    [P_algo,~]=ALGO_simplices_IntersectSimplices_v3_20220308(V,U);
    time_algo(i)=toc;
    [~,vol_algo]=convhull(P_algo');
    
    vol_exact=d^2 * sin(a/2) * sin(pi/6) / sin(pi - pi/6 - a/2) / 6; % factor of h/3 for vol of pyramid
    if cos(a/2)<d
        temp=(cos(a/2)-d)/(cos(a/2)-1);
        vol_exact= vol_exact - (d-cos(a/2))^2*tan(pi/6)*temp/3;
    end
    
    error_PANG(i)=abs(vol_exact - vol_PANG)/vol_exact;
    error_algo(i)=abs(vol_exact - vol_algo)/vol_exact;
    
    if i==round(N/3)
      figure(1)
      clf
      subplot(1,2,1)
      PLOT_TetraMesh_20201103(V,T,U,T)
      PLOT_PatchPolyhedron_v1_20201105(P_algo)
  %     view([0,1,0])
      view([1,1,1])
      pause(0)
    elseif i==round(2*N/3)
      figure(1)
      subplot(1,2,2)
      PLOT_TetraMesh_20201103(V,T,U,T)
      PLOT_PatchPolyhedron_v1_20201105(P_algo)
      view([1,1,1])
      pause(0)
    end
end

figure(2)
clf
subplot(1,2,1)
semilogy(aa/pi,error_algo,'r*',aa/pi,error_PANG,'bo')
xlabel('\alpha/\pi')
ylabel('Rel. error')
legend('PANG2-3D','PANG')
axis square
set(gca,'FontSize',30,'linewidth',2)

figure(2)
subplot(1,2,2)
semilogy(aa/pi,time_algo,'r*',aa/pi,time_PANG,'bo')
xlabel('\alpha/\pi')
ylabel('Computation time')
legend('PANG2-3D','PANG')
axis square
set(gca,'FontSize',30,'linewidth',2)