function [P,n] = ALGO_TetraIntersect_v1_20201103(V,U)
% TETRAINTERSECT calculates the intersection between two tetrahedra
%   [P,n]=TetraIntersect(V,U) calculates the vertices of the polyhedron of
%   intersection (stored in P) between the subject tetrahedron V and the
%   clipping tetrahedron U. Neighbours of V that may also intersect U are
%   listed in n.

%===Change of Coordinates===%
v0=V(:,1); v1=V(:,2)-v0; v2=V(:,3)-v0; v3=V(:,4)-v0;
A=[v1,v2,v3]; X=A\(U-v0);

%===Intersections===%
T=NaN(12,5); indP=ones(1,4); Q1=zeros(3,0); n=zeros(1,4);
for plane=1:4
    index=(1:3) + 3*(plane-1);
    [G,T(index,:),sp] = SUB_PlaneIntersect_v1_20201103(X,plane);
    indP=indP.*sp;
    if ~isempty(G)
        Q1=[Q1, G];
        n(plane)=1;
    end
end
P=U(:,indP==1);                        % X-in-Y
Q3=A*Q1 + v0;                       % X-with-Y

%===Edges of Y===%
R_id=[1,2;1,3;1,4;2,3;4,2;3,4]; indR=false(1,4);
T1=zeros(1,6); T2=T1; indT1=false(1,6); indT2=indT1;
plane_id=[2,3;1,3;1,2;3,4;2,4;1,4];
for edge=1:6
    index=T(:,1)==edge; temp=T(index,2:end);
    T1(edge)=min(temp(:)); T2(edge)=max(temp(:));
    indT1(edge)= T1(edge)>=0 && T1(edge)<=1;
    indT2(edge)= T2(edge)>=0 && T2(edge)<=1;
    indR(R_id(edge,1))=indR(R_id(edge,1)) || (T1(edge)<0 && 0<=T2(edge));
    indR(R_id(edge,2))=indR(R_id(edge,2)) || (T1(edge)<1 && 1<=T2(edge));
    n(plane_id(edge,:))=1;
end
a=[1,0,0;0,1,0;0,0,1;-1,1,0;1,0,-1;0,-1,1]; a=a';
b=[1,0,0;0,0,1;0,1,0]; b=[zeros(3) b'];
T1=a.*T1+b; T2=a.*T2+b;

Q2=A*[T1(:,indT1),T2(:,indT2)]+v0;  % Y-with-X
R=V(:,indR);                        % Y-in-X

if size(R,2)>2                          % if three or more interior points
    n=[1 1 1 1];                        % all neighbours of V intersect with
end                                     % U

P=SortAndRemoveDoubles([P,Q3,Q2,R]);
PLOT_PatchPolyhedron_v1_20201105(P)
pause(0)
end

function [G,T,sp] = SUB_PlaneIntersect_v1_20201103(X,plane)
% PLANEINTERSECT calculates intersection between tetrahedron and plane

if plane==1                         % plane x=0
    p=X(1,:); q=X(2,:); r=X(3,:);       % transform to p,q,r
    edge_id=[3;2;6];                    % indexing of edges of Y
    a=[0,0;1,0;0,1]; b=[0;0;0];         % reverse transform to x,y,z
elseif plane==2                     % plane y=0
    p=X(2,:); q=X(3,:); r=X(1,:);
    edge_id=[1;3;5];
    a=[0,1;0,0;1,0]; b=[0;0;0];
elseif plane==3                     % plane z=0
    p=X(3,:); q=X(1,:); r=X(2,:);
    edge_id=[2;1;4];
    a=[1,0;0,1;0,0]; b=[0;0;0];
elseif plane==4                     % plane x+y+z=1
    p=1-sum(X,1); q=X(1,:); r=X(2,:);
    edge_id=[6;5;4];
    a=[1,0;0,1;-1,-1]; b=[0;0;1];
end

G=zeros(2,6); sp=sign(p)>=0; k=0; ind=zeros(1,6);
for i=1:3
    for j=(i+1):4
        k=k+1;
        if sp(i)~=sp(j)             % opposite sides of plane
            G(1,k)=(q(i)*p(j) - q(j)*p(i))/(p(j)-p(i));
            G(2,k)=(r(i)*p(j) - r(j)*p(i))/(p(j)-p(i));
            ind(k)=1;
        end
    end
end
G=G(:,ind==1);

T=NaN(3,4); ind=ones(size(G(1,:)));
if ~isempty(G)
    for edge=1:3
        [T(edge,:),ss] = SUB_EdgeIntersect_v1_20201103(G,edge);
        if plane==4 && edge_id(edge)==6 % special case edges
            T(edge,:)=1-T(edge,:);
        end
        ind=ind.*ss;
    end
end
T=[edge_id,T];
G=a*G(:,ind==1) + b;
end

function [T,ss] = SUB_EdgeIntersect_v1_20201103(G,edge)
% EDGEINTERSECT calculates intersection between triangle and reference line

if edge==1                          % edge q=0
    s=G(1,:); t=G(2,:);                 % transform to s,t
elseif edge==2                      % edge r=0
    s=G(2,:); t=G(1,:);
elseif edge==3                      % edge q+r=1
    s=1-sum(G,1); t=(1-G(1,:)+G(2,:))/2;
end

m=length(s); ss=sign(s)>=0; T=NaN(1,4); k=0;
for i=1:m-1
    for j=(i+1):m
        if ss(i)~=ss(j)
            k=k+1;
            T(k)=(t(i)*s(j) - t(j)*s(i))/(s(j)-s(i));
        end
    end
end
end