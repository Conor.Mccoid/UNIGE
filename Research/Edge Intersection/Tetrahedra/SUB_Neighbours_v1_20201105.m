function [T,n] = SUB_Neighbours_v1_20201105(T)
% NEIGHBOURS adds the ordered list of neighbours of triangles or tetrahedra

[n,dim]=size(T);
T=[T,(n+1)*ones(n,dim)];
if dim==3                       % triangles
    map=[1,3;1,2;2,3];              % nhbrs (shared edges) listed in this order:
                                    % x=0, y=0, x+y=1
elseif dim==4                   % tetrahedra
    map=[1,3,4;1,2,4;1,2,3;2,3,4];  % nhbrs (shared planes) listed in this order:
                                    % x=0, y=0, z=0, x+y+z=1
else                            % n-simplex
    map=nchoosek(1:dim,dim-1);      % nhbrs (shared hyperplanes) are listed as 
                                    % [1,...,dim-1], [1,...,dim-2,dim], ..., [2,...,dim]
end

for i=1:n                           % do for every object in list
    for d=1:dim                     % do for each nhbr
        temp=zeros(n,dim-1);        % init. sum of logicals
        for k=1:dim-1               % find rows containing this node
            temp(:,k)=sum(T(:,1:dim)==T(i,map(d,k)),2);
        end                         % find rows containing all shared nodes
        ind=find(sum(temp,2)==(dim-1)); ind=ind(ind~=i); % ignore current object
        if ~isempty(ind)            % if there is a nhbr,
            T(i,dim+d)=ind;         % indicate
        end
    end
end