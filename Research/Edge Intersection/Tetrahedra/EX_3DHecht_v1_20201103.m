%===EX_3DHecht_v1_20201103===%
th=pi/10; disp=0.1*ones(3,1);
[N,T,N2]=MESH_d20_20201103(th,th,th,disp);

% profile on

figure(1)
clf
subplot(1,2,1)
PLOT_TetraMesh_20201103(N,T,N2,T);
InterfaceMatrix3d(N,T,N2,T);    % comment out this line if primitives are missing

figure(1)
subplot(1,2,2)
PLOT_TetraMesh_20201103(N,T,N2,T);
% ALGO_ProjectionMatrix3D_v1_20201104(N,T,N2,T);
% ALGO_ProjectionMatrix3D_v2_20211222(N,T,N2,T);
ALGO_PMnD_v1_20220128(N,T,N2,T) % to display intersections in magenta, some
                                % lines need to be uncommented in
                                %    ALGO_simplices_IntersectSimplices

% profile report