\documentclass{article}

\usepackage{preamble}
\usepackage{tikz}
\usetikzlibrary{positioning,calc,shapes.geometric,intersections,decorations.pathreplacing}

\title{Parsimony and robustness of the Sutherland-Hodgman algorithm}
\author{Conor McCoid}
%\email{conor.mccoid@unige.ch}
\author{Martin J. Gander}
%\email{martin.gander@unige.ch}
%\affiliation{
%    \institution{University of Geneva}
%    \streetaddress{2-4 Rue du Li\`evre}
%    \city{1211 Geneva 4}
%    \country{Switzerland}
%}

\begin{document}

\section{Introduction}
\label{sec:Introduction}

The Sutherland-Hodgman algorithm is the repeated application of a plane bisection algorithm.
For each edge of the clipping polygon the infinite line extending from this edge bisects the plane.
We compute the intersections between the edges of the subject polygon and keep only these and those vertices of the subject polygon on the side of the line containing the clipping polygon.

To prove the robustness of the Sutherland-Hodgman algorithm we prove first that this bisection algorithm is parsimonious.
We then prove that repeated applications are parsimonious
We conclude that the algorithm as a whole is robust.

\section{Plane bisection algorithm}
\label{sec:Plane}

Let the clipping polygon $V$ be a convex polygon with $n$ vertices and edges.
We suppose the vertices of $V$ are listed counter-clockwise.
Choose an edge of $V$ that runs from the $i$--th vertex $\mathbf{v}_i$ to the $j$--th vertex $\mathbf{v}_j$, where $j = \mod(i+1,n)$.

To simplify calculations in this algorithm we perform a translation and rotation to all vertices.
The vertices of both polygons are translated by $-\mathbf{v}_i$ so that the $i$--th vertex of $V$ is mapped to the origin.
Next, the plane is rotated to align the vector $\mathbf{w}_i = \mathbf{v}_j - \mathbf{v}_i$ with the axis $y=0$.
This is a Givens rotation and can be implemented in a precise fashion.

With this transformation performed it is straightforward to determine which vertices of the subject polygon $U$ are on the side of the line $y=0$ that contains $V$, as it reduces to a check of the sign bit.
Moreover, it is clear which intersections between edges of $U$ and the line $y=0$ need to be calculated.
Suppose that $U$ has $m$ vertices, sorted counter-clockwise.
Let $(x_k, y_k)$ be the position of the $k$--th vertex of $U$ after translation and rotation.
Then an intersection is calculated for the $k$--th edge of $U$ if $\sign(y_k) \neq \sign(y_l)$ where $l = \mod(k+1,m)$.
This intersection has position $(z_k,0)$.

Those vertices of $U$ with $y_k < 0$ are discarded.
We construct a new clipped polygon $W$ out of the calculated intersections and those vertices of $U$ with $y_k \geq 0$.
The positions of the intersections in original coordinates are $\mathbf{v}_i + z_k \mathbf{w}_i$.
There is only as many intersections as there are edges of $U$ that intersect the line $y=0$.
As such, there is exactly as many intersections as there are entries and exits from the non-negative half of the plane.

% what am i trying to say here? what do i need to say here?

\begin{prop}
The number of intersections calculated is even.
If $U$ is simple then $W$ is the union of simple polygons.
\end{prop}

\begin{proof}
Suppose that $y_k \geq 0$ and $y_{k+1} < 0$.
Then an intersection is calculated for the $k$--th edge of $U$.
Suppose $y_{k+2}, y_{k+3}, \dots, y_{k+l-1} < 0$ and $y_{k+l} \geq 0$ for some $l$, where $y_{p+m} = y_p$ for all $p \in \set{1, \dots, m}$.
Then an intersection is calculated for the $(k+l-1)$--th edge.
Since $y_k \geq 0$, $l \leq m$ and so this intersection is guaranteed to be calculated.

This may be repeated for any edge where $y_k \geq 0$ and $y_{k+1} < 0$.
Thus, for any such edge there is a corresponding edge with $y_{k+l-1} < 0$ and $y_{k+l} \geq 0$.
Since the intersections occur in pairs the number of intersections is even.

To avoid degenerate cases and possible miscalculations, edges of $W$ are placed between neighbouring intersections.
That is, if $z_q$ is the smallest value of $z_k$ for all $k$, and $z_p$ is the second smallest, then there is an edge in the clipped polygon $W$ between $(z_q,0)$ and $(z_p,0)$.
The direction of edges of simple polygons alternates as they cross any given line in the plane (nb: cite?).
Therefore, $(z_q,0)$ represents an exit from the non-negative half of the plane while $(z_p,0)$ is an entry.
This process replaces the edges of $U$ from $(x_q,y_q)$ to $(x_p,y_p)$ with three edges of $W$:
$(x_q,y_q)$ to $(z_q,0)$; $(z_q,0)$ to $(z_p,0)$, and; $(z_p,0)$ to $(x_p,y_p)$.
While this may disconnect $W$ it keeps $W$ closed.

Those edges of $U$ that lie entirely on the non-negative half of the plane are preserved.
Those with at least one vertex on the negative half of the plane are replaced.
The replacement process, described above, does not cause any of the new edges to cross either themselves or any of the original edges.
Therefore, the components of $W$ are simple.
\end{proof}

\section{Repeated bisection}
\label{sec:Repeated}

In Section \ref{sec:Plane} the plane bisection algorithm was shown to clip the subject polygon $U$ by the clipping polygon $V$, resulting in the clipped polygon $W$.
The polygon $W$ was proven to be simple, or the union of simple polygons, and lies entirely on the non-negative side of the line used to bisect the plane.
Let $L_i(U)$ be the application of this algorithm to the polygon $U$ such that $W = L_i(U)$.
The line bisecting the plane is parallel to $\mathbf{w}_i$ and runs through $\mathbf{v}_i$.

The Sutherland-Hodgman algorithm may be represented as repeated application of this algorithm.
That is, starting with the subject polygon $U$ and $i=1$, the Sutherland-Hodgman algorithm proceeds as
\begin{align*}
U_1 & = L_1(U), \\
U_2 & = L_2(U_1), \\
& \vdots \\
U_n & = L_n(U_{n-1}).
\end{align*}
The result, $U_n$, lies on the non-negative side of the $n$ lines that form the border of the clipping polygon $V$.

The clipping polygon $V$ is the intersection of the non-negative halves of the plane bisected by the $n$ lines extending from its edges.
The intersection between $U$ and $V$ is then the intersection between the intersections of $U$ with the non-negative halves of the plane.

Let $P_i$ be the non-negative half of the plane bisected by the $i$--th line.
One can represent $V$ as
\begin{equation*}
V = \bigcap_{i=1}^n P_i.
\end{equation*}
Thus, one can represent the intersection between $U$ and $V$ as
\begin{align*}
U \cap V & = U \bigcap_{i=1}^n P_i \\
& = (U \cap P_1) \bigcap_{i=2}^n P_i \\
& = U_1 \bigcap_{i=2}^n P_i \\
& \vdots \\
& = U_{n-1} \cap P_n = U_n.
\end{align*}

Since each application of $L_i(U_{i-1})$ is parsimonious, repeated applications are likewise parsimonious.
Moreover, since each $L_i(U_{i-1})$ is simple and closed the final result must also be simple and closed.

\section{Conclusion}
\label{sec:Conclusion}

We have shown that a plane bisection algorithm can be implemented with parsimony and consistency.
We then showed that repeated applications of such an algorithm will result in the polygon at the intersection between the subject polygon and the area enclosed by the bisecting lines.
Taken together this means that the Sutherland-Hodgman algorithm is parsimonious.

A parsimonious algorithm is naturally robust (nb: cite Fortune/Hoffman).
A small change in either $U$ or $V$ will therefore cause no more than a small change in their intersection as calculated by this algorithm.

\end{document}