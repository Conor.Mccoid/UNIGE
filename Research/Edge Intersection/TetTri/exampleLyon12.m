% take a tetrahedra mesh and a 3d triangular mesh:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

d=0.1;
[N1,T1]=NewMesh3d(1);
[N1,T1]=RefineMesh3d(N1,T1);
%[N1,T1]=RefineMesh3d(N1,T1);
%for i=1:size(N1,2)
%  for j=1:3
%    if N1(j,i)>0 & N1(j,i)<1
%      N1(j,i)=N1(j,i)+d*(rand(1,1)-0.5);
%    end
%  end
%end

[N2,T2]=NewMesh(1);
[N2,T2]=RefineMesh(N2,T2);
[N2,T2]=RefineMesh(N2,T2);
%[N2,T2]=RefineMesh(N2,T2);
%[N2,T2]=RefineMesh(N2,T2);
%f=@(x,y) (x+y)/2;
f=@(x,y) sin(x).*sin(y);
% f=@(x,y) 0.5
N2=Convert2dMeshTo3d(N2,f);
%N2=Convert2dMeshTo3d(N2,'ff');

%for i=1:size(N2,2)
%  for j=1:3
%    if N2(j,i)>0 & N2(j,i)<1
%      N2(j,i)=N2(j,i)+0.1*(rand(1,1)-0.5);
%      %      N2(j,i)=N2(j,i)+0.1
%    end
%  end
%end

clf
PlotMesh3d(N1,T1,'b');
Plot2dInterface3d(N2,T2,'r');
pause

S=ProjectionMatrix2dWith3d(N1,T1,N2,T2)


disp('Now and example which still poses problems')
pause

d=0.5;
[N1,T1]=NewMesh3d(1);
[N1,T1]=RefineMesh3d(N1,T1);
for i=1:size(N1,2)
  for j=1:3
    if N1(j,i)>0 && N1(j,i)<1
      N1(j,i)=N1(j,i)+d*(rand(1,1)-0.5);
    end
  end
end

[N2,T2]=NewMesh(1);
[N2,T2]=RefineMesh(N2,T2);
[N2,T2]=RefineMesh(N2,T2);
%f=@(x,y) (x+y)/2;
%f=@(x,y) sin(x).*sin(y);
% f=@(x,y) 0.5
%N2=Convert2dMeshTo3d(N2,f);
N2=Convert2dMeshTo3d(N2,'ff');

% for i=1:size(N2,2)
%   for j=1:3
%     if N2(j,i)>0 & N2(j,i)<1
%       N2(j,i)=N2(j,i)+0.1*(rand(1,1)-0.5);
%       %      N2(j,i)=N2(j,i)+0.1
%     end
%   end
% end

clf
PlotMesh3d(N1,T1,'b');
Plot2dInterface3d(N2,T2,'r');

S=ProjectionMatrix2dWith3d(N1,T1,N2,T2)






















