function [P,n,s] = TriTetraIntersect(V,U)
% TRITETRAINTERSECT Calculates the polygon of intersection between a
% triangle and a tetrahedron
%   [P,n,s]=TriTetraIntersect(V,U) returns the list of points P that form
%   the intersection between the triangle V and the tetrahedron U. It also
%   returns a list of neighbours n of V that U might also intersect, and
%   the area s of the intersection.

% transform to reference plane
v0=V(:,1);
v1=V(:,2)-v0;
v2=V(:,3)-v0;
v3=cross(v1,v2); v3=v3/norm(v3);
A=[v1,v2,v3]; X=A\(U-v0);

% find intersections between edges of tetra. and plane
s=sign(X(3,:))>=0; count=0; S=zeros(2,4);
for i=1:3
    for j=(i+1):4
        if s(i)~=s(j)
            count=count+1;
            S(1,count)=(X(1,i)*X(3,j)-X(1,j)*X(3,i))/(X(3,j)-X(3,i));
            S(2,count)=(X(2,i)*X(3,j)-X(2,j)*X(3,i))/(X(3,j)-X(3,i));
        end
    end
end
S=S(:,1:count);

% intersect resulting polygon with triangle
Y=[0,1,0;0,0,1];
if count==0
    P=zeros(2,0); n=zeros(1,3);
else
    [indP,Q,indR,n]=TriIntersectSub(S,count);
    P=[S(:,indP==1),Q,Y(:,indR==1)];
end
P=SortAndRemoveDoubles(P);
if size(P,2)>2
    [indP,s]=convhull(P'); P=P(:,indP);
    s=det(A)*norm(A'\[0;0;1])*s;
    if length(indP)>1
        n=ones(1,3);
    end
    Ptemp = [P;zeros(1,size(P,2))];
    P=v0+A*Ptemp;
    patch(P(1,:),P(2,:),P(3,:),'m')
    pause(0)
else
    s=0;
end
end

function [indP,Q,indR,n] = TriIntersectSub(X,count) % adapted from TriIntersect for 2D triangle-triangle case
% TRIINTERSECTSUB Calculates intersection between arbitrary triangle and
% reference triangle in transformed space

Q=zeros(2,6); indQ=zeros(1,6); n=zeros(1,3); indP=ones(1,count); indR=n;
for i=1:3
    [Qtemp,ntemp,indRtemp,sp]=EdgeIntersect(X,i,count);
    ind=(1:2) + 3*(i-1);
    Q(:,ind)=Qtemp; indQ(ind)=ntemp;
    n(i)=max(ntemp); indR=max(indR,indRtemp);
    indP=indP.*sp;
end
Q=Q(:,indQ==1);
end

function [Q,ind,indR,sp]=EdgeIntersect(X,edge,count)
% EDGEINTERSECT intersection of edges of X with reference line of Y
%   [Q,ind,indR,sp]=EdgeIntersect(X,edge) calculates the intersections
%   between all edges of X and the reference line of Y indicated by edge
%   and stores the values in Q. It also calculates Boolean indicators for
%   neighbours of X intersecting Y (ind), vertices of Y inside X (indR),
%   and vertices of X inside Y (sp).

if edge==1                                      % v1
    p=X(2,:); q=X(1,:);                         % parametrization of edge
    a=[1;0]; b=[0;0];                           % inversion of parametrization
    R1=1; R2=2;                                 % vertices of V on edge
elseif edge==2                                  % v2
    p=X(1,:); q=X(2,:); a=[0;1]; b=[0;0]; R1=1; R2=3;
elseif edge==3                                  % line connecting v1 and v2
    p=1-X(1,:)-X(2,:); q=0.5*(1-X(1,:)+X(2,:)); a=[-1;1]; b=[1;0]; R1=2; R2=3;
end
Q=NaN(1,count); indR=zeros(1,3); sp=sign(p)>=0; k=0;
for i=1:count-1
    for j=(i+1):count
        if sp(i)~=sp(j)                             % vertices on opposite sides of edge?
            k=k+1;
            Q(k)=(q(i)*p(j)-q(j)*p(i))/(p(j)-p(i));   % calculate intersection
        end
    end
end
Q=[min(Q),max(Q)];
if k>0
    indR(R1)=sign(Q(1))~=sign(Q(2));
    indR(R2)=sign(Q(1)-1)~=sign(Q(2)-1);
end
ind=Q>=0 & Q<=1;                       % intersection lies on edge of Y?
Q=Q.*a+b;
end

function P=SortAndRemoveDoubles(P) % ubiquitous primitive
% SORTANDREMOVEDOUBLES sort points and remove duplicates
%   P=SortAndRemoveDoubles(P); orders polygon corners in P counter
%   clock wise and removes duplicates

ep=10*eps;                           % tolerance for identical nodes
m=size(P,2); 
if m>0                              
    c=sum(P,2)/m;                      % order polygon corners counter 
    ao=zeros(1,m);
    for i=1:m                          % clockwise
        d=P(:,i)-c; ao(i)=angle(d(1)+1i*d(2));
    end
    [~,id]=sort(ao); 
    P=P(:,id);
    i=1;j=2;                           % remove duplicates
    while j<=m
        if norm(P(:,i)-P(:,j))>ep
            i=i+1;P(:,i)=P(:,j);j=j+1;
        else
            j=j+1;
        end
    end
    P=P(:,1:i);
end
end