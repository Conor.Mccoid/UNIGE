function S=ProjectionMatrix2dWith3d(TetNodes,Tets,TriNodes,Tris)
% PROJECTIONMATRIX2DWITH3D projection for triangular grid in 3d tetrahedras
%   S=ProjectionMatrix2dWith3d(TetNodes,Tets,TriNodes,Tris) takes a
%   tetrahedral mesh defined by nodes TetNodes and indices Tets and a
%   triangular mesh (TriNodes, Tris) in 3D and computes the sum of the area
%   of intersection between the two. As a precondition, the first
%   tetrahedron and the first triangle must intersect, as the algorithm
%   uses an advancing front technique.

S=0;                            % total area of the intersection
numtets=size(Tets,1);           % number of tetrahedra
numtris=size(Tris,1);           % number of triangles
cand=[1;1];                     % initialize candidate pairs
trilist=1;                      % list of triangles to treat
triflag=zeros(numtris+1,1);     % init. flags for triangles
triflag([1,end])=1;             % flag first and last entry (treated, stopping criteria)
while ~isempty(trilist)         % while there are still triangles in the list
    tricurrent=trilist(1); trilist=trilist(2:end); % take first entry in list of tri.
    tetlist=cand(1,(cand(2,:)==tricurrent));       % list of tetra. candidates intersecting tricurrent
    tetlist=unique(tetlist);                       % avoid duplicates
    tetflag=zeros(numtets+1,1);                    % flag tetra. already intersected with tricurrent
    tetflag([tetlist,end])=1;
    while ~isempty(tetlist)                        % while there are still tetra. in the list
        tetcurrent=tetlist(1); tetlist=tetlist(2:end); % take first entry in list of tetra.
        [P,nhbrs,s]=TriTetraIntersect(TriNodes(:,Tris(tricurrent,1:3)),TetNodes(:,Tets(tetcurrent,1:4)));
        if ~isempty(P)
            S=S+s;
            nhbrstet=Tets(tetcurrent,5:8);          % nhbrs of current tetra.
            tetlist=[tetlist, nhbrstet(tetflag(nhbrstet)==0)]; % unflagged nhbrs added to list of tetra.
            tetflag(nhbrstet)=1;                    % flag new entries in list
            nhbrstri=Tris(tricurrent,4:6);          % nhbrs of current tri.
            nhbrstri=nhbrstri(:,nhbrs==1);          % nhbrs of current tri. that intersect current tetra.
            trilist=[trilist, nhbrstri(triflag(nhbrstri)==0)]; % add untreated nhbrs of current tri. to list
            triflag(nhbrstri)=1;                    % flag new entries in list
            nhbrstri=[ tetcurrent*ones(1,length(nhbrstri)); nhbrstri ]; % pair nhbrs with current tetra.
            cand=[cand, nhbrstri];                  % add candidate pairs
        end
    end
end
end