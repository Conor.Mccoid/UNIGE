\documentclass{article}

\usepackage{preamble}
\usepackage{float}

\floatstyle{boxed}
\newfloat{algorithm}{t}{}
\floatname{algorithm}{Algorithm}

\title{Collocation matrices representing inverse operators - notes}
\author{Conor McCoid}

\begin{document}

\maketitle

\newcommand{\W}[2]{W \left ( #1 ; #2 \right )}
\newcommand{\poly}[1]{\frac{x^{#1}}{(#1)!}}
\newcommand{\Poly}[1]{\frac{x^#1}{#1!}}

\section{Constant coefficients: old proofs and techniques}

The IOM constructed in the previous section relies on knowledge of the homogeneous solutions of the linear operator and either their Wronskians or their derivatives.
This precludes using the IOM as a black box preconditioner, as the upfront cost and foreknowledge required are prohibitive.
Instead, one may focus on using the IOM for a simple class of linear operators: ones whose coefficients are constant:
\begin{equation*}
\mathcal{L}u(x) = u^{(m)}(x) + \sum_{j=1}^{m} a_j u^{(m-j)}(x) .
\end{equation*}

This operator has an associated polynomial $p(x) = x^m + \sum_{j=1}^{m} a_j x^{m-j}$.
The fundamental solution set of this operator is a set of exponential functions multiplied by polynomials:
\begin{equation*}
\Set{x^{i} e^{\lambda_k x}}{p(\lambda_k) = 0 \ \text{with multiplicity } n>i \in \mathbb{N} \cup 0} .
\end{equation*}
The Wronskian of such a set may be written explicitly.
Combined with lemma \ref{lem:sum} one may write out solutions to equations (\ref{gamma}) and (\ref{eq:betas}) without extraneous calculations.

We present the Wronskians for these sets by first examining three special cases and then presenting the Wronskians in their most general form for constant coefficient linear operators.
Before proceeding we make a few notes on notation.
$\mathcal{L}$ and its associated polynomial $p(x)$ we have already defined.
There are $M$ unique roots of the polynomial $p(x)$, $\set{\lambda_k}_{k=1}^M$.
The root $\lambda_k$ of the polynomial $p(x)$ has multiplicity $m_k$, and $\sum_{k=1}^M m_k = m$.
The sets of functions associated with the root $\lambda_k$, their union over the degrees of polynomials and the union of the unions over $k$ are denoted by $E_{k,j}$, $E_k$ and $E$, respectively:
\begin{equation*}
E_{k,j} = \set{\Poly{j} e^{\lambda_k x}}, \ E_k = \bigcup_{j=0}^{m_k - 1} E_{k,j}, \ E = \bigcup_{k=1}^M E_k .
\end{equation*}

\subsection{$m$ unique roots}

Consider the case where $M=m$ and there are $m$ unique roots of the polynomial $p(x)$.
In this case, $E_k$ is the set containing only the exponential function $e^{\lambda_k x}$.
The Wronskians involve only these exponential functions.
By Abel's identity it is clear that $\W{E}{x} = \W{E}{0} e^{-a_1 x}$.

The value of $\W{E}{0}$ must be expressed as the determinant of a matrix.
The same is true for $\W{E \setminus{E_i}}{0}$.
These matrices will be denoted by:
\begin{align*}
\Lambda & = \begin{bmatrix} 1 & \dots & 1 \\ \lambda_1 & \dots & \lambda_m \\ \vdots & \ddots & \vdots \\ \lambda_1^{m-1} & \dots & \lambda_m^{m-1} \end{bmatrix}, \\
\Lambda_i & = \begin{bmatrix} 1 & \dots & 1 & 1 & \dots & 1 \\ \lambda_1 & \dots & \lambda_{i-1} & \lambda_{i+1} & \dots & \lambda_m \\ \vdots & & \vdots & \vdots & & \vdots \\ \lambda_1^{m-2} & \dots & \lambda_{i-1}^{m-2} & \lambda_{i+1}^{m-2} & \dots & \lambda_m^{m-2} \end{bmatrix} .
\end{align*}

For the Wronskians of $E \setminus E_i$ we present the following lemma:

\begin{lem}[Wronskians of exponential functions]
Let $\set{\lambda_k}_{k=1}^m \in \mathbb{R}$ and $\Lambda$ the matrix defined earlier for such a set, then
\begin{equation*}
\W{\set{e^{\lambda_k x}}_{k=1}^m}{x} = \abs{\Lambda}
e^{x \sum_{k=1}^m \lambda_k} .
\end{equation*}
\label{lem:exp}
\end{lem}

\begin{proof}
The case of any two $\lambda_k$ being equal is trivially true as both sides are necessarily zero.
For the nontrivial case, $\lambda_k \neq \lambda_i$ for all $k \neq i$.

Let $p(x)$ be the polynomial with roots $\lambda_k$ and with coefficients $a_j$ such that:
\begin{equation*}
p(x) = \Pi_{k=1}^m \left (x - \lambda_k \right) = x^m + \sum_{j=1}^m a_j x^{m-j}.
\end{equation*}
Note that $a_1 = -\sum_{k=1}^m \lambda_k$.

%this paragraph might be unnecessary
Let $\mathcal{L}$ be the linear operator with coefficients equal to $a_j$:
$$
\mathcal{L} u(x) = u^{(m)}(x) + \sum_{j=1}^m a_j u^{(m-j)}(x).
$$
Then $\mathcal{L} e^{\lambda_k x} = e^{\lambda_k x} p(\lambda_k) = 0$.
Therefore, $\set{e^{\lambda_k x}}$ is the fundamental solution set of $\mathcal{L}$.

By Abel's identity $\W{\set{e^{\lambda_k x}}_{k=1}^m}{x} = \W{\set{e^{\lambda_k x}}_{k=1}^m}{0} e^{-\int_0^x a_1}.$
The remainder follows from the definition of $\W{\set{e^{\lambda_k x}}_{k=1}^m}{0}$.
%(nb: this lemma and proof likely already exists in literature)
\end{proof}

One may use lemma \ref{lem:exp} to calculate $\W{E \setminus E_i}{x}$ as the set $E \setminus E_i$ is still a set of purely exponential functions.
As such, the only changes that need to be made to the Wronskian are the sum of the values $\lambda_k$ and the matrix $\Lambda$.
In particular, the sum is as it was before minus the contribution of $\lambda_i$ and the matrix $\Lambda$ is replaced by $\Lambda_i$.

We may now write down the functions $\omega_n(x)$ from equation (\ref{eq:Wronskian coeffs}), applying lemma \ref{lem:exp} to both the set $E$ and $E \setminus E_n$:
\begin{equation*}
\begin{split}
\omega_n(x) & = (-1)^{m+n} \frac{\W{E \setminus E_n}{x}}{\W{E}{x}} \\
			& = (-1)^{m+n} \frac{\abs{\Lambda_n}}{\abs{\Lambda}} \frac{e^{x \sum_{k \neq n} \lambda_k}}{e^{x \sum_{k=1}^m \lambda_k}} \\
			& = (-1)^{m+n} \frac{\abs{\Lambda_n}}{\abs{\Lambda}} e^{-\lambda_n x} .
\end{split}
\end{equation*}

\subsection{$\mathcal{L} u = u^{(m)}$}

\newcommand{\Wpoly}[2]{\W{\set{\Poly{k}}_{#1}^{#2}}{x}}

We now consider the special case where the linear operator is $m$--th order differentiation.
The PSIM may already be used as the inverse for the matrix $A$ for such a problem, but an alternative formulation of these matrices is presented here for completeness.

The special case is equivalent to $M=1$ and $\lambda_1 = 0$.
As such, the set $E = \set{\Poly{j}}_{j=0}^{m-1}$.
The following lemma presents the Wronskians for such polynomials:

\begin{lem}[Wronskians of polynomials]
\begin{align*}
(i) && \Wpoly{k=0}{m} & = 1 \\
(ii) && \Wpoly{k=0,k \neq j}{m} & = \Wpoly{k=1}{m-j} \\
(iii) && \Wpoly{k=1}{m} & = \Poly{m}
\end{align*}
\label{lem:poly}
\end{lem}

\begin{proof}
\begin{description}
\item[$(i)$] This is simply the determinant of an upper triangular matrix with ones along the diagonal:
\begin{equation*}
\Wpoly{k=0}{m} = \begin{vmatrix} 1 & x & \Poly{2} & \dots & \Poly{m} \\
0 & 1 & x & \dots & \poly{m-1} \\
\vdots & \ddots & \ddots & \ddots & \vdots \\
0 & 0 & \dots & 0 & 1 \end{vmatrix} = 1 .
\end{equation*}
\item[$(ii)$] \begin{align*}
\Wpoly{k=0,k \neq j}{m} & =
\begin{vmatrix}
1      & x & \dots  & \poly{j-1} & \poly{j+1} & \dots & \Poly{m}    \\
0      & 1 & \dots  & \poly{j-2} & \Poly{j}   & \dots & \poly{m-1}  \\
\vdots &   & \ddots & \ddots     & \ddots     & \ddots& \vdots      \\
0      &   & \dots  & 1          & \Poly{2}   & \dots & \poly{m-j+1}\\
       &   &        & 0          & x          &       & \poly{m-j}  \\
       &   &        & 0          & 1          &       & \poly{m-j-1}\\
       &   &        & \vdots     & \vdots     & \ddots& \vdots      \\
       &   &        & 0          & 0          & \dots & 1
\end{vmatrix} \\
& = \Wpoly{k=0}{j-1} \Wpoly{k=1}{m-j} \\
& = \Wpoly{k=1}{m-j} .
\end{align*}
\item[$(iii)$] The statement is trivially true for $m=1$.
Apply strong induction by supposing it is true for $n<m$.
Calculate the Wronskian by expanding through the top row of the matrix:
\begin{align*}
\Wpoly{k=1}{m} & = \begin{vmatrix}
x & \dots & \Poly{m} \\
1 & \ddots & \vdots \\
0 & 1 & x \end{vmatrix}
%\\
%\vdots & \ddots & \\
%0 & \dots & 1
%\end{vmatrix}
\\
& = x \begin{vmatrix} x & \dots & \poly{m-1} \\ 1 & \ddots & \vdots \\ 0 & 1 & x \end{vmatrix} - \dots + (-1)^{m+1} \Poly{m} \begin{vmatrix} 1 & \dots & \poly{m-2} \\  & \ddots & \vdots \\ 0 &  & 1 \end{vmatrix} \\
& = \sum_{n=1}^m (-1)^{n+1} \Poly{n} \Wpoly{k=0,k \neq n-1}{m-1} \\
& = \sum_{n=1}^m (-1)^{n+1} \Poly{n} \Wpoly{k=1}{m-n} \quad \text{by $(ii)$} \\
& = \sum_{n=1}^m (-1)^{n+1} \Poly{n} \poly{m-n} \quad \text{by our induction hypothesis} \\
& = \Poly{m} \sum_{n=1}^m (-1)^{n+1} \binom{m}{n} \\
& = \Poly{m}
\end{align*}
by an identity of the binomial coefficients. (nb: cite)
\end{description}
\end{proof}

As before we may now write out the functions $\omega_n(x)$:
\begin{equation*}
\begin{split}
\omega_n(x) & = (-1)^{m+n} \frac{ \W{E \setminus \set{\Poly{n}}}{x}}{\W{E}{x}} \\
			& = (-1)^{m+n} \frac{\W{\set{\Poly{k}}_{k \neq n}}{x}}{1} \ \text{by $(i)$} \\
			& = (-1)^{m+n} \W{\set{\Poly{k}}_{k=1}^{m-n}}{x} \ \text{by $(ii)$} \\
			& = (-1)^{m+n} \poly{m-n} \ \text{by $(iii)$.}
\end{split}
\end{equation*}

As a corollary, we may establish the inverse of the fundamental matrix for the set of consecutive polynomials by noting that it is Toeplitz and upper triangular.
\begin{cor}[Inverse of the fundamental matrix for polynomials] \label{cor:poly}
\begin{equation*}
\begin{bmatrix} 1 & x & \dots & \Poly{m} \\ 0 & \ddots & \ddots & \ddots \end{bmatrix}^{-1} =
\begin{bmatrix} 1 & -x & \Poly{2} & -\Poly{3} & \dots & (-1)^m \Poly{m} \\ 0 & \ddots & \ddots & \ddots & \ddots & \ddots \end{bmatrix} 
\end{equation*}
\end{cor}

\begin{proof}
First note that the matrix is upper triangular and Toeplitz and so its inverse is upper triangular and Toeplitz (nb:cite).
As such, it suffices to calculate the last column of the inverse.
That is, it suffices to solve:
\begin{equation*}
\begin{bmatrix} 1 & x & \dots & \Poly{m} \\ 0 & \ddots & \ddots & \ddots \end{bmatrix} \omega = \begin{bmatrix} 0 \\ \vdots \\ 0 \\ 1 \end{bmatrix} .
\end{equation*}
Recall from equation (\ref{eq:Wronskian coeffs}) that the elements of $\omega$ are given by fractions of Wronskians, which have been found in lemma \ref{lem:poly} and presented immediately above the hypothesis.
Thus, the vector $\omega$ is equal to:
\begin{equation*}
\omega = \begin{bmatrix} (-1)^m \poly{m} \\ \vdots \\ -x \\ 1 \end{bmatrix} .
\end{equation*}
This vector then defines the Toeplitz matrix in the hypothesis.

As an alternative proof, note that the left-hand matrix is equal to $\exp(xJ)$ where
\begin{equation}
J = \begin{bmatrix}
0 & 1 & & \\ & \ddots & \ddots & \\ & & \ddots & 1 \\ & & & 0
\end{bmatrix} .
\end{equation}
The matrix $J$ is nilpotent and the exponential function has a Taylor series expansion such that $\exp(xJ) = I + xJ + \frac{(xJ)^2}{2} + \dots + \frac{(xJ)^m}{m!}$.
Naturally, the inverse of $\exp(xJ)$ is $\exp(-xJ)$, which is exactly the right-hand matrix.
\end{proof}

\subsection{One root with multiplicity $m$}

The case for one root with multiplicity $m$, represented by $M=1$, is a generalization of the previous case.
The set $E = E_1 = \set{\Poly{k} e^{\lambda_1 x}}_{k=1}^m$ which is the set $E$ from the previous case multiplied by $e^{\lambda_1 x}$.
The following lemma then makes the generalization simple.

\begin{lem}
\begin{equation*}
W(\{ f_k g \}_{k=1}^m ; x) = g^m W(\{ f_k \}_{k=1}^m ; x )
\end{equation*}
\label{lem:group}
\end{lem}

\begin{proof}
It is trivially true for $m=1$.
Apply strong induction by supposing it is true for $n<m$, for any set of functions.
Calculate the Wronskian by expanding through the bottom row of the matrix:
\begin{equation*}
\begin{split}
\W{\set{f_k g}_{k=1}^m}{x}
& = \begin{vmatrix}
f_1 g & \dots & f_m g \\
\vdots & & \vdots \\
\sum_{j=0}^{m-1} \binom{m-1}{j} f_1^{(m-1-j)} g^{(j)}
& \dots & \sum_{j=0}^{m-1} \binom{m-1}{j} f_m^{(m-1-j)} g^{(j)}
\end{vmatrix} \\
& = \sum_{i=1}^m (-1)^{i+m} W(\{f_k g\}_{k \neq i} ; x) \sum_{j=0}^{m-1} \binom{m-1}{j} f_i^{(m-1-j)} g^{(j)} \\
& = \sum_{i=1}^m (-1)^{i+m} g^{m-1} W(\{ f_k \}_{k \neq i} ; x) \sum_{j=0}^{m-1} \binom{m-1}{j} f_i^{(m-1-j)} g^{(j)}
\end{split}
\end{equation*}
by our induction hypothesis.
We next need to interchange the summations:
\begin{equation*}
\begin{split}
\W{\set{f_k g}_{k=1}^m}{x}
& = g^{m-1} \sum_{j=0}^{m-1} \binom{m-1}{j} g^{(j)}
\sum_{i=1}^m (-1)^{i+m} W(\{f_k\}_{k\neq i} ; x) f_i^{(m-1-j)} \\
& = g^{m-1} \sum_{j=0}^{m-1} \binom{m-1}{j} g^{(j)}
\begin{vmatrix} f_1 & \dots & f_m \\
\vdots & & \vdots \\
f_1^{(m-2)} & \dots & f_m^{(m-2)} \\
f_1^{(m-1-j)} & \dots & f_m^{(m-1-j)} \end{vmatrix} .
\end{split}
\end{equation*}
Note that for $j \neq 0$ the last row of the matrix is identical to one of the previous rows.
Therefore, its determinant is only nonzero for $j = 0$.
This gives:
\begin{equation*}
\begin{split}
\W{\set{f_k g}_{k=1}^m}{x}
& = g^{m-1} \sum_{j=0}^{m-1} \binom{m-1}{j} g^{(j)} \times \begin{cases} 0 & j\neq 0 \\
W(\{f_k\}_{k=1}^m ; x) & j = 0 \end{cases} \\
& = g^m W(\{f_k\}_{k=1}^m ; x) .
\end{split}
\end{equation*}
\end{proof}

Applying this lemma to the case of $M=1$ we find that:
\begin{align*}
\W{E}{x} & = e^{m \lambda_1 x} \W{\set{\Poly{k}}_{k=0}^{m-1}}{x} = e^{m \lambda_1 x} , \\
\W{E \setminus E_{1,j}}{x} & = e^{(m-1) \lambda_1 x} \poly{m-j} .
\end{align*}
The function $\omega_n(x)$ is then:
\begin{equation*}
\omega_n(x) = (-1)^{m+n} \poly{m-n} e^{-\lambda_1 x} .
\end{equation*}

\subsection{Mixed multiplicities}

% Recall notation: E_{k,j}, with k for root and j for polynomial, m for order of problem, M for number of roots, m_k for multiplicity of kth root
% Counting variables left over: i, n, l, N?

We now examine the most general case of constant coefficients: $1 < M < m$, so that $m_k > 1$ for at least one $k$.
We begin by defining two objects which will be necessary in both the proofs and results to come.
First is the vector $w_k^n$, whose $i$--th entry is defined as:
\begin{equation*}
\left ( w_k^n \right )_i = \begin{cases} \binom{i}{n} \lambda_k^{i - n} & \lambda \neq 0 \\
\delta_{i,n} & \lambda = 0, \end{cases}
\end{equation*}
where $\delta_{i,n}$ is the Kronecker delta and we use the convention that $\binom{i}{n} = 0$ if $n>i$.
For ease of notation, the length of $w_k^n$ will be context sensitive: when considering the Wronskian of the set $E$ $w_k^n \in \mathbb{R}^m$, but when considering $E \setminus E_{k,j}$ $w_k^n \in \mathbb{R}^{m-1}$.

Second is a matrix $\Omega \in \mathbb{R}^{m \times m}$ composed of the vectors $w_k^n$:
\begin{equation*}
\Omega = \begin{bmatrix} w_1^0 & \dots w_1^{m_1 - 1} & w_2^0 & \dots & w_M^{m_M-1} \end{bmatrix} .
\end{equation*}
The submatrix of $\Omega$ resulting from removing row $m$ and the column equal to $w_k^j$ is represented by $\Omega_k^j$.

\begin{thm}[Wronskians for constant coefficient operators] \label{thm:cc}
\begin{align*}
(i) && \W{E}{x} & = e^{a_1 x} \abs{\Omega}, \\
(ii) && \W{E \setminus E_{k,j}}{x} & = e^{(a_1 - \lambda_k) x} \sum_{n=j}^{m_k - 1} \poly{n-j} \abs{\Omega_k^n} .
\end{align*}
\end{thm}

\begin{proof}
First consider $\lambda_k \neq 0$.
Let $\hat{w}_{k,j}$ be the vector whose elements are the sequential derivatives of the function contained in $E_{k,j}$.
Take the $i$--th derivative of the function contained in $E_{k,j}$:
\begin{equation*}
\left ( \Poly{j} e^{\lambda_k x} \right )^{(i)} = \sum_{n=0}^j \binom{i}{n} \lambda_k^{i-n} \poly{j-n} e^{\lambda_k x} .
\end{equation*}
The presence of $\binom{i}{n} \lambda_k^{i-n}$ indicates that $\hat{w}_{k,j}$ is equal to a linear combination of the vectors $w_k^n$:
\begin{equation*}
\hat{w}_{k,j} = e^{\lambda_k x} \sum_{n=0}^j \poly{j-n} w_k^n .
\end{equation*}
This statement may also be shown for $\lambda_k = 0$.

Since we are concerned with the determinant of the matrix composed of the vectors $\hat{w}_{k,j}$ we may orthogonalize these vectors in a Gram-Schmidt process (nb: cite).
For example, $\hat{w}_{k,0} = e^{\lambda_k x} w_k^0$ and $\hat{w}_{k,1} = e^{\lambda_k x} w_k^1 + e^{\lambda_k x} x w_k^2$.
Because $w_k^0$ is a part of both vectors, we may remove it from $\hat{w}_{k,1}$ without changing the determinant.
We may repeat this for all $\hat{w}_{k,j}$ such that $w_k^0$ appears explicitly only in the first column.
We may then repeat this procedure for $w_k^1$, such that $w_k^1$ appears only in the second column.
In this way, we have that:
\begin{equation*}
\begin{split}
\W{E}{x} & = \begin{vmatrix} e^{\lambda_1 x} w_1^0 & \dots & e^{\lambda_1 x} w_1^{m_1-1} & e^{\lambda_2 x} w_2^0 & \dots & e^{\lambda_M x} w_M^{m_M-1} \end{vmatrix} \\
		 & = e^{x \sum_{k=1}^M m_k \lambda_k} \abs{\Omega} \\
		 & = e^{a_1 x} \abs{\Omega}.
\end{split}
\end{equation*}
and statement $(i)$ is proven.

To begin proving statement $(ii)$ consider the set $\hat{E} = E \setminus E_{k,m_k-1}$.
Such a set represents the fundamental solution set of a $(m-1)$--th order linear constant coefficient operator, the corresponding polynomial of which is $p(x) / (x - \lambda_k)$.
The matrix $\Omega$ for this set is exactly $\Omega_k^{m_k-1}$.
Therefore, by statement $(i)$ $\W{\hat{E}}{x} = e^{(a_1 - \lambda_k)x} \abs{\Omega_k^{m_k-1}}$ and statement $(ii)$ is true for $j = m_k-1$ for any $1 \leq k \leq M$.

We proceed by strong induction: we suppose statement $(ii)$ is true for $j^* < j \leq m_k-1$ and prove it is true for $j = j^* \geq 0$. (nb: remember to replace all instances of $j^*$ with $j^*$)
First, we perform the Gram-Schmidt process described previously.
We may also take out the exponential functions from the determinant:
\begin{equation*}
\begin{split}
\W{E \setminus E_{k,j^*}}{x} = & \begin{vmatrix} \dots & \hat{w}_{k,0} & \dots & \hat{w}_{k,j^*-1} & \hat{w}_{k,j^*+1} & \dots & \hat{w}_{k,m_k-1} & \dots \end{vmatrix} \\
 = e^{(a_1 - \lambda_k)x}	  & \begin{vmatrix} \dots & w_k^0 & \dots & w_k^{j^*-1} & w_k^{j^*+1} + x w_k^{j^*} & \dots & \sum\limits_{n=j^*}^{m_k-1} \poly{m_k-1-n} w_k^n & \dots \end{vmatrix} .
\end{split}
\end{equation*}

Consider the column associated with $E_{k,j^*+1}$:
its two component vectors appear in the subsequent columns.
One may rewrite the determinant of this matrix as a summation of two determinants, arising from splitting up these two component vectors:
\begin{equation*}
\begin{split}
\W{E \setminus E_{k,j^*}}{x} & e^{(\lambda_k - a_1) x} \\
= & \begin{vmatrix} \dots & w_k^{j^*-1} & w_k^{j^*+1} + x w_k^{j^*} & \dots & \sum\limits_{n=j^*}^{m_k-1} \poly{m_k-1-n} w_k^n & \dots \end{vmatrix} \\
= & \begin{vmatrix} \dots & w_k^{j^*-1} & x w_k^{j^*} & \dots & \sum\limits_{n=j^*+1}^{m_k-1} \poly{m_k-1-n} w_k^n & \dots \end{vmatrix} \\
& + \begin{vmatrix} \dots & w_k^{j^*-1} & w_k^{j^*+1} & \dots & \sum\limits_{n=j^*, n\neq j^*+1}^{m_k-1} \poly{m_k-1-n} w_k^n & \dots \end{vmatrix} \\
= & x \W{E \setminus E_{k,j^*+1}}{x} e^{(\lambda_k - a_1) x} \\
& + \begin{vmatrix} \dots & w_k^{j^*-1} & w_k^{j^*+1} & w_k^{j^*+2} + \Poly{2} w_k^{j^*} & \dots & \sum\limits_{n=j^*, n\neq j^*+1}^{m_k-1} \poly{m_k-1-n} w_k^n & \dots \end{vmatrix} \\
= & x \W{E \setminus E_{k,j^*+1}}{x} e^{(\lambda_k - a_1) x} \\
& - \begin{vmatrix} \dots & w_k^{j^*-1} & w_k^{j^*+2} + \Poly{2} w_k^{j^*} & w_k^{j^*+1} & \dots & \sum\limits_{n=j^*, n\neq j^*+1}^{m_k-1} \poly{m_k-1-n} w_k^n & \dots \end{vmatrix} .
\end{split}
\end{equation*}
We may then repeat the procedure with the second determinant in the last line, writing it as the sum of two determinants, each choosing one of the component vectors for the given column.

The procedure may be repeated until we have reduced the determinant to a summation of Wronskians that, by the induction hypothesis, we already have results for.
At each step we subdivide along the column containing $w_k^{j^*+l} + \Poly{l} w_k^{j^*}$ into two parts, such that one addend contains $w_k^{j^*+l}$ and the other contains $\Poly{l} w_k^{j^*}$.
The latter addend is now identical to $\Poly{l} \W{E \setminus E_{k,j^*+l}}{x} e^{(\lambda_k - a_1)x}$ as one may bring the polynomial out of the determinant and the vectors $w_k^j$ are arranged in order for $0 \leq j < j^*+l$, with $w_k^{j^*+l}$ missing.
Meanwhile, one may perform the Gram-Schmidt process on the addend containing $w_k^{j^*+l}$, thereby removing this vector from all subsequent columns.
In particular, the column associated with $E_{k,j^*+l+1}$ becomes $w_k^{j^*+l+1} + \poly{l+1} w_k^{j^*}$.
Exchanging this column with the one containing $w_k^{j^*+l}$ and introducing a minus sign in front of the determinant we arrive at the start of the $(l+1)$--th step.

We stop the procedure at $j^*+l=m_k-1$: %nb: check signs
\begin{equation*}
\begin{split}
\W{E \setminus E_{k,j^*}}{x} & e^{(\lambda_k - a_1)x} \\
= & \sum_{l=1}^{m_k-1-j^*} (-1)^{l+1} \Poly{l} \W{E \setminus E_{k,j^*+l}}{x} e^{(\lambda_k - a_1)x} \\
& + \begin{vmatrix} \dots & w_k^{j^*-1} & w_k^{j^*+1} & \dots & w_k^{m_k-1} & \dots \end{vmatrix} \\
= & \sum_{l=1}^{m_k-1-j^*} (-1)^{l+1} \Poly{l} \W{E \setminus E_{k,j^*+l}}{x} e^{(\lambda_k - a_1)x} + \abs{\Omega_k^{j^*}} .
\end{split}
\end{equation*}
We now use our induction hypothesis to arrive at our result:
\begin{equation*}
\begin{split}
\W{E \setminus E_{k,j^*}}{x} & e^{(\lambda_k - a_1)x} \\
= & \abs{\Omega_k^{j^*}} + \sum_{l=1}^{m_k-1-j^*} (-1)^{l+1} \Poly{l} \sum_{n=j^*+l}^{m_k-1} \poly{n-j^*-l} \abs{\Omega_k^n} \\
= & \abs{\Omega_k^{j^*}} + \sum_{l=1}^{m_k-1-j^*} \sum_{n=j^*+l}^{m_k-1} (-1)^{l+1} \poly{n-j^*} \frac{ (n-j^*)! }{(n-j^*-l)! l!} \abs{\Omega_k^n} \\
= & \abs{\Omega_k^{j^*}} + \sum_{l=1}^{m_k-1-j^*} \sum_{n=j^*+l}^{m_k-1} (-1)^{l+1} \poly{n-j^*} \binom{n-j^*}{l} \abs{\Omega_k^n} \\
= & \abs{\Omega_k^{j^*}} + \sum_{n=j^*+1}^{m_k-1} \poly{n-j^*} \abs{\Omega_k^n} \sum_{l=1}^{n-j^*} (-1)^{l+1}  \binom{n-j^*}{l} \\
= & \Poly{0} \abs{\Omega_k^{j^*}} + \sum_{n=j^*+1}^{m_k-1} \poly{n-j^*} \abs{\Omega_k^n} \\
= & \sum_{n=j^*}^{m_k-1} \poly{n-j^*} \abs{\Omega_k^n} .
\end{split}
\end{equation*}
Note that the second-to-last step uses an identity of the binomial coefficients (nb: cite).

We thus have that if statement $(ii)$ is true for $j^* < j \leq m_k-1$ then it is true for $j=j^*$.
Thus, by strong induction, it is true for all $0 \leq j \leq m_k-1$.
\end{proof}

Lemma \ref{lem:exp} is the special case of this theorem with $m_k = 1$ for all $k$ and so the summation in statement $(ii)$ is over a single element.
The special case of $M=1$ has $\abs{\Omega_k^n} = \delta_{n,m_k-1}$ and so we may retrieve the results of lemma \ref{lem:poly} and those discussed in the previous subsection for $\lambda_1 \neq 0$.

The general form of the functions $\omega_i(x)$ may now be written as:
\begin{equation*}
\omega_i(x) = (-1)^{m+i} e^{-\lambda_k x} \sum_{n=j}^{m_k-1} \poly{n-j} \frac{\abs{\Omega_k^n}}{\abs{\Omega}}
\end{equation*}
where $\hat{P}_i(x) = \Poly{j} e^{\lambda_k x}$.

A simpler procedure may be employed to calculate the functions $\omega_i(x)$ by solving matrix systems rather than processing determinants.
To explain this procedure, recall that $\hat{P}(x)$ represents the fundamental matrix for the set $E$.
It may be written as the product of two matrices: the matrix $\Omega$ and a block upper triangular matrix whose blocks are Toeplitz:
\begin{equation*}
\hat{P}(x) = \Omega \begin{bmatrix} e^{\lambda_1 x} & x e^{\lambda_1 x} & \dots \\
0 & e^{\lambda_1 x} & \dots \\
\vdots & \ddots & \ddots \end{bmatrix}.
\end{equation*}

Each upper triangular Toeplitz block corresponds to one of the $\lambda_k$ and can themselves be written as the product of $e^{\lambda_k x}$ and the fundamental matrix for consecutive polynomials up to degree $m_k-1$, denoted hereafter as $F_k(x)$.
Since the blocks are disjoint and $F_k^{-1}(x)$ is known by corollary \ref{cor:poly} the inverse of the block upper triangular matrix is known.
One may write out equation (\ref{eq:Wronskian system}) with this in mind:
\begin{align*}
\Omega \begin{bmatrix} e^{\lambda_1 x} F_1(x) & 0 & \dots & \\
0 & e^{\lambda_2 x} F_2(x) & & \\
\vdots & & \ddots & \\
 & & & e^{\lambda_M x} F_M(x) \end{bmatrix} \omega(x) = \begin{bmatrix} 0 \\ \vdots \\ 0 \\ 1 \end{bmatrix} \\
 \implies  \omega(x) = \begin{bmatrix} e^{-\lambda_1 x} F_1^{-1}(x) & 0 & \dots & \\
0 & e^{-\lambda_2 x} F_2^{-1}(x) & & \\
\vdots & & \ddots & \\
 & & & e^{-\lambda_M x} F_M^{-1}(x) \end{bmatrix} \Omega^{-1} \begin{bmatrix} 0 \\ \vdots \\ 0 \\ 1 \end{bmatrix} \\
 \implies \tilde{I}_k \omega(x) = e^{-\lambda_k x} F_k^{-1}(x) \tilde{I}_k \Omega^{-1} \begin{bmatrix} 0 \\ \vdots \\ 0 \\ 1 \end{bmatrix}
\end{align*}
where $\tilde{I}_k $ indicates the selection of those rows corresponding to $\lambda_k$.
Note that all $F_k^{-1}(x)$ are principal submatrices of $F_{k^*}^{-1}$, where $m_{k^*} \geq m_k$ for all $k=1,...,M$.

\section{Other attempts}

\newcommand{\Lcal}{\mathcal{L}}
\newcommand{\ddx}{\frac{d}{dx}}

Let $\hat{\Lcal}$ be the operator for the set $\set{\Poly{k} e^{\lambda_j x}}_{j \neq q}$ and $\tilde{\Lcal}$ the one for $\set{\Poly{k} e^{\lambda_q x}}_{k \neq n}$, then
\begin{align*}
\Lcal & = \hat{\Lcal} \tilde{\Lcal} \\
& = \left [\left ( \ddx \right )^{m-l} +\sum_{j \neq q} \lambda_j \left ( \ddx \right )^{m-l-1} + \dots + \prod_{j \neq q} \lambda_j \right ] 
\left [ \left ( \ddx \right )^l + r(x) \left ( \ddx \right )^{l-1} + \dots + s(x) \right ] \\
& = \left ( \ddx \right )^m + \left ( \sum_{j \neq q} \lambda_j + r(x) \right ) \left ( \ddx \right )^{m-1} + \dots
\end{align*}
but then $\Lcal$ is the operator for 
\begin{itemize}
\item $\set{\Poly{k} e^{\lambda_j x} } \cup \set{\hat{\Lcal} \left ( \Poly{k} e^{\lambda_q x} \right )}_{k \neq n}$ or
\item $\set{\tilde{\Lcal} \left ( \Poly{k} e^{\lambda_j x} \right )}_{k,j \neq q} \cup \set{\Poly{k} e^{\lambda_q x}}_{k \neq n}$.
\end{itemize}

\begin{align*}
E & = \set{P_k(x)}_{k=1}^m \\
& = \set{\Poly{j} e^{\lambda_i x}}_{j=0, i=1}^{m_i-1,M} \\
& = \cup_{i=1}^M \set{\Poly{j} e^{\lambda_i x}}_{j=0}^{m_i-1} \\
& = \cup_{i=1}^M E_i
\end{align*}
\begin{itemize}
\item $E \to \Lcal$ such that $\forall f \in E$ $\Lcal f = 0$.
\item $\cup_{i \neq k} E_i \to \tilde{\Lcal}$ in the same way: $\tilde{\Lcal} f = f^{(m-m_k)}(x)  + \sum_{i \neq k} \lambda_i^{m_i} f^{(m - m_k - 1)}(x) + \dots$.
\item $E_k \setminus \set{\Poly{k} e^{\lambda_i x} } \to \hat{\Lcal}$, $\hat{\Lcal}f = f^{(m_k-1)}(x) + r(x) f^{(m_k-2)}(x) + \dots$.
\item $E \setminus \set{\Poly{k} e^{\lambda_i x} } \to \bar{\Lcal}$, $\bar{\Lcal} f = f^{(m-1)}(x) + \left ( r(x) + \sum_{i \neq k} \lambda_i^{m_i} \right ) f^{(m-2)}(x) + \dots$.
\end{itemize}
Need $W(\hat{\Lcal} \left ( E_k \setminus \set{\Poly{k} e^{\lambda_i x}} ; x\right)$.

\begin{align*}
\hat{\Lcal} \left ( E_k \setminus \set{\Poly{n} e^{\lambda_k x}} \right ) & = \prod_{i \neq k} \left ( \ddx - \lambda_i \right )^{m_i} \Poly{j} e^{\lambda_k x}, \quad j \neq n \\
& \neq \prod_{i \neq k} \left ( \ddx - \lambda_i \right )^{m_i - 1} \left ( \poly{j-1}+ \lambda_k \Poly{j} - \lambda_i \poly{j} \right ) e^{\lambda_k x}
\end{align*}

\begin{align*}
\tilde{\lambda}_l = \begin{cases} \lambda_1 & 1 \leq l \leq m_1 \\ \lambda_i & \sum_{j < i, j \neq k} m_j < l \leq \sum_{j \leq i,j \neq k} m_j \end{cases}
\end{align*}

\begin{align*}
\prod_{l=1}^{m-m_k} & \left ( \ddx - \tilde{\lambda}_l \right ) \Poly{j} e^{\lambda_k x} = \\
 \prod_{l=2}^{m-m_k} & \left ( \ddx - \tilde{\lambda}_l \right ) \left ( \poly{j-1} + (\lambda_k - \tilde{\lambda}_l ) \Poly{j} \right ) e^{\lambda_k x} = \\
\prod_{l=3}^{m-m_k} & \left ( \ddx - \tilde{\lambda}_l \right ) \left ( \poly{j-2} + (2 \lambda_k - \tilde{\lambda}_1 - \tilde{\lambda}_2 ) \poly{j-1} + (\lambda_k^2 - \lambda_k \tilde{\lambda}_1 - \tilde{\lambda}_2 \lambda_k + \tilde{\lambda}_1 \tilde{\lambda}_2 ) \Poly{j} \right ) e^{\lambda_k x}
\end{align*}

\begin{align*}
\Lcal P_k(x) & = P_k^{(m)}(x) - \sum_{i=1}^{M} m_i \lambda_i P_k^{(m-1)}(x) + \dots = 0 \\
\hat{\Lcal} P_k(x) & = P_k^{(m-1)}(x) + r(x) P_k^{(m-2)}(x) + \dots = 0 & \forall k \neq j \\
\tilde{\Lcal} P_j(x) & = P_j'(x) + q(x) P_j(x) = 0 \\
\bar{\Lcal} P_k(x) & = P_k^{(m-1)}(x) + p(x) P_k^{(m-2)}(x) + \dots & \text{such that } \Lcal P_k(x) = \bar{\Lcal} \tilde{\Lcal} P_k(x) \\
\breve{\Lcal} P_k(x) & = P_k'(x) + s(x) P_k(x) & \text{such that } \Lcal P_k(x) = \breve{\Lcal} \hat{\Lcal} P_k(x) \\
\implies -\sum_{i=1}^M m_i \lambda_i & = q(x) + p(x) = r(x) + s(x)
\end{align*}

$\Omega$ (in this document, pretty sure $\Lambda$ in the current version) has $\sum_{k=1}^M \left [ \frac{m_k (m_k+1)}{2} + (m-m_k)^2 \right ]$ elements.

To make $\Omega$ make each piece separately.
Separate into coefficients and roots.
\begin{algorithm}
$\lambda_k$, $m_k$

$\Omega_{\lambda_k} = \text{spdiags} \left ( 0:-1:1-m, \lambda_k^{(0:m-1)}, m, m_k \right )$;

$P_T$ (Pascal's triangle in lower triangular form, size $m \times m$)

$\Omega_k = P_T(:,1:m_k) \otimes \Omega_{\lambda_k}$; ($\otimes$ is the Hadamard matrix product)

$\Omega = \begin{bmatrix} \Omega_1 & \Omega_2 & \dots & \Omega_M \end{bmatrix}$;

\caption{Algorithm to construct $\Omega$}
\end{algorithm}

\end{document}
