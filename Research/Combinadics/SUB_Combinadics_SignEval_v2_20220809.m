function pm = SUB_Combinadics_SignEval_v2_20220809(J)
% SIGNEVAL gives a sign value to a combination J
%   pm = SignEval(J) gives a value of 0 or 1 to the combination J. This is
%   done according to an artificial example problem.

%   J's which return 1: 0, 1, 2, 03, 04, 05, 13, 023, 123, 024, 134, 0124,
%   0134, 01245
%   J's which return 0: 3, 4, 5, 14, 15, 23, 24, 25, 014, 015, 025, 135,
%   0235, 0245, 1235, 1345, 01345
%   Other J's are not part of this example and represent an error in
%   calculation

m = length(J);
if isrow(J)
    if m==1
        if J<3
            pm=1;
        else
            pm=0;
        end
    elseif m==2
        if J(1)==0
            pm=1;
        elseif J(1)==1
            if J(2)==3
                pm=1;
            elseif J(2)==4 || J(2)==5
                pm=0;
            else
                disp('J out of range')
                disp(J)
            end
        elseif J(1)==2 && J(2)>2 && J(2)<6
            pm=0;
        else
            disp('J out of range')
                disp(J)
        end
    elseif m==3
        if J(1)==0
            if J(2)==1
                if J(3)==4 || J(3)==5
                    pm=0;
                else
                    disp('J out of range')
                disp(J)
                end
            elseif J(2)==2
                if J(3)==3 || J(3)==4
                    pm=1;
                elseif J(3)==5
                    pm=0;
                else
                    disp('J out of range')
                disp(J)
                end
            else
                disp('J out of range')
                disp(J)
            end
        elseif J(1)==1
            if J(2)==2 && J(3)==3
                pm=1;
            elseif J(2)==3
                if J(3)==4
                    pm=1;
                elseif J(3)==5
                    pm=0;
                else
                    disp('J out of range')
                disp(J)
                end
            else
                disp('J out of range')
                disp(J)
            end
        else
            disp('J out of range')
                disp(J)
        end
    elseif m==4
        if J(1)==0
            if J(2)==1
                if J(3)==2
                    if J(4)==4
                        pm=1;
                    else
                        disp('J out of range')
                disp(J)
                    end
                elseif J(3)==3
                    if J(4)==4
                        pm=1;
                    else
                        disp('J out of range')
                disp(J)
                    end
                else
                    disp('J out of range')
                disp(J)
                end
            elseif J(2)==2 && J(4)==5
                if J(3)==3 || J(3)==4
                    pm=0;
                else
                    disp('J out of range')
                disp(J)
                end
            else
                disp('J out of range')
                disp(J)
            end
        elseif J(1)==1 && J(4)==5
            if J(2)==2 && J(3)==3
                pm=0;
            elseif J(2)==3 && J(3)==4
                pm=0;
            else
                disp('J out of range')
                disp(J)
            end
        else
            disp('J out of range')
                disp(J)
        end
    elseif m==5
        if J(1)==0 && J(2)==1 && J(4)==4 && J(5)==5
            if J(3)==2
                pm=1;
            elseif J(3)==3
                pm=0;
            else
                disp('J out of range')
                disp(J)
            end
        else
            disp('J out of range')
                disp(J)
        end
    elseif m==6
        pm=1;
    else
        disp('J out of range')
                disp(J)
    end
else
    disp('J must be a row vector')
end