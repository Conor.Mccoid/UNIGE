function [NJK,JK] = SUB_Combinadics_NJK_v2_20220810(NJ,NK,K,P,n,m)
% NJK finds the combinadic of J union K using only the combinadics of J and
% K

% because we're only feeding this subfunction adjacent NJ and NK we really
% don't need to also feed it the adjacency matrix; this is doubling up on
% our check

% there is essentially an unranking process going on behind the scenes
% here, and it appears to be faster than the greedy algorithm used later;
% adapt a version 2 which does this unranking, taking sets J and K as input
% -> this second version is significantly faster

if NJ<NK
    disp('NJ must be larger than NK')
elseif NJ<=m+1
    NJK=1; JK=0:m; % possibly 0:m...
elseif m==1
    NJK=NK+P(NJ-2,3); JK=[NK-1,NJ-1];
else % the largest element lies in J
    a = P(n-m,m+1); %nchoosek(n-1,m)
    if a>NJ-1 % n-1 is not in J
        [NJK,JK]=SUB_Combinadics_NJK_v2_20220810(NJ,NK,K,P,n-1,m);
    else % n-1 is in J
        if a<=NK-1 % n-1 is in K
            [NJK,JK]=SUB_Combinadics_NJK_v2_20220810(NJ-a,NK-a,K(1:end-1),P,n-1,m-1);
        else % n-1 is not in K
            c=P(n-m,m);     %nchoosek(n-1,m-1)
            d=P(n-m-1,m+1); %nchoosek(n-2,m)
            if c>NJ-1-a % n-2 is not in J
                if d<=NK % n-2 is in K
                    NJK=NK; JK=K;
                else % n-2 is not in K
                    [NJK,JK]=SUB_Combinadics_NJK_v2_20220810(NJ-a+d,NK,K,P,n-1,m);
                    if n-2>=m+1
                        NJK=NJK-P(n-m-2,m+2);
                    end
                    JK=JK(1:end-1);
                end
            else % n-2 is in J
                [NJK,JK]=SUB_Combinadics_NJK_v2_20220810(NJ-a,NK-d,K(1:end-1),P,n-1,m-1);
            end
        end
        NJK=NJK+P(n-m-1,m+2); JK=[JK,n-1];
    end
end