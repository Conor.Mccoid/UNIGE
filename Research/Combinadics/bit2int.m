function k=bit2int(v,n)
% BIT2INT converts binary string to integer

k=0;
for i=1:n
    if v(i)==1
        k=k + 2^(n-i);
    end
end
end