function NJK = SUB_Combinadics_NJK_v1_20220809(NJ,NK,P,n,m)
% NJK finds the combinadic of J union K using only the combinadics of J and
% K

% because we're only feeding this subfunction adjacent NJ and NK we really
% don't need to also feed it the adjacency matrix; this is doubling up on
% our check

% there is essentially an unranking process going on behind the scenes
% here, and it appears to be faster than the greedy algorithm used later;
% adapt a version 2 which does this unranking, taking sets J and K as input

% rewrite version in paper

if NJ<NK
    NJK=SUB_Combinadics_NJK_v1_20220809(NK,NJ,P,n,m);
elseif NJ<=m+1
    NJK=1;
elseif m==1
    NJK=NK+P(NJ-2,3);
else % the largest element lies in J
    a = P(n-m,m+1); %nchoosek(n-1,m)
    if a>NJ-1 % n-1 is not in J
        NJK=SUB_Combinadics_NJK_v1_20220809(NJ,NK,P,n-1,m);
    else % n-1 is in J
        if a<=NK-1 % n-1 is in K
            NJK=SUB_Combinadics_NJK_v1_20220809(NJ-a,NK-a,P,n-1,m-1);
        else % n-1 is not in K
            c=P(n-m,m);     %nchoosek(n-1,m-1)
            d=P(n-m-1,m+1); %nchoosek(n-2,m)
            if c>NJ-1-a % n-2 is not in J
                if d<=NK % n-2 is in K
                    NJK=NK;
                else % n-2 is not in K
                    NJK=SUB_Combinadics_NJK_v1_20220809(NJ-a+d,NK,P,n-1,m)-P(n-m-2,m+2);
                end
            else % n-2 is in J
                NJK=SUB_Combinadics_NJK_v1_20220809(NJ-a,NK-d,P,n-1,m-1);
            end
        end
        NJK=NJK+P(n-m-1,m+2);
    end
end