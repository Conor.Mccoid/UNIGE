function [J_array,N_array,S_array] = ALGO_Combinadics_ComboRep_v3_20220814(n,S_master,P,list_A)

ind    =0:n-1;
J_list = ind';      % list of J for given level
N_list = J_list+1;  % list of N(J) for given level
S_list = NaN(n,1);  % list of signs for given level
for k=1:n
    J = J_list(k,:);
    S_list(k) = SUB_Combinadics_SignEval_v3_20220814(J,S_master);
end
J_array = cell(n,1); % list of all J
N_array = J_array;
S_array = J_array;
J_array{1}=J_list;
N_array{1}=N_list;
S_array{1}=S_list;
num_J=n;            % current number of J

% P      = pascal(n); P=[P,eye(n,1)];
% list_A = SUB_simplices_Combinadics_v1_20220304(n-1,P);

for i=1:n-1
    A=list_A{i};
    ind=1:num_J;
    flag_N=zeros(P(n-i,i+2),1);
    J_list=zeros(P(n-i,i+2),i+1); J_array{i+1}=J_list;
    for j=ind
        NJ=N_list(j);
        for k=ind(A(NJ,N_list)==1)
            NK=N_list(k);
            if S_list(NJ)~=S_list(NK)
                J=J_array{i}; J=J(NJ,:);
                [N_new,J_new]=SUB_Combinadics_NJK_v2_20220810(NK,NJ,J,P,n,i);
                if flag_N(N_new)==0
                    J_list(N_new,:)=J_new;
                    N_array{i+1}=[N_array{i+1}; N_new];
                    S_array{i+1}=[S_array{i+1}; SUB_Combinadics_SignEval_v3_20220814(J_new,S_master)];
                    flag_N(N_new)=1;
                end
            end
        end
    end
%     J_list=J_array{i+1};
    J_array{i+1}=J_list;
    N_list=N_array{i+1};
    S_list=sparse(N_list,1,S_array{i+1});
    num_J=length(N_list);
end