% Comparisons in the efficiency of the various algorithms for combination
% trees

% Step 1: generate initial combinations and storage solutions
% Step 2: assign +/- values randomly
% Step 3: determine adjacent combinations
% Step 4: compare +/- values
% Step 5: create new combinations
% repeat from Step 2 until levels exhausted

% still to do: cost reduction measures, incl. Pascal's triangle
%   flag for lex. rep. to avoid double counting
%   visual rep. of a given tree
%   statistical analysis

n=6; indices=0:n-1;

%% Binary representation

signs = NaN(2^n,1);
list_J = eye(n);
[~,list_cols]=size(list_J);
list_N2J = zeros(1,list_cols);
for k=1:list_cols
    list_N2J(k) = bit2int(list_J(:,k),n);
    J = indices(list_J(:,k)==1);
    signs(list_N2J(k)) = SUB_Combinadics_SignEval_v1_20220804(J);
end

start_index=1;
end_index=n;

for i=1:n
    number_intersections=0;
    for k=start_index:end_index
        for j=k+1:end_index
            diff=sum(xor(list_J(:,k),list_J(:,j)));
            if diff==2
                if signs(list_N2J(k))~=signs(list_N2J(j))
                    J_new=or(list_J(:,k),list_J(:,j));
                    N2J_new=bit2int(J_new,n);
                    if isnan(signs(N2J_new))
                        list_J=[list_J,J_new];
                        list_N2J=[list_N2J,N2J_new];
                        J=indices(J_new==1);
                        signs(N2J_new)=SUB_Combinadics_SignEval_v1_20220804(J);
                        number_intersections=number_intersections+1;
                    end
                end
            end
        end
    end
    start_index=end_index+1;
    end_index=end_index+number_intersections;
end

% repeated combinations appearing, often with different signs, massive
% multiplier on number of intersections
% need to check if new combination already exists

%% Lexicographical representation

% any way to make the same check on new combinations?

list_J = indices';
signs = NaN(n,1);
for k=1:n
    J = list_J(k,:);
    signs(k) = SUB_Combinadics_SignEval_v1_20220804(J);
end
superlist_J=cell(n,1);
superlist_signs=cell(n,1);
list_rows=n;

list_J_new=[];
signs_new=[];

for i=1:n
    superlist_J{i}=list_J;
    superlist_signs{i}=signs;
    for j=1:list_rows
        J=list_J(j,:);
        for k=j+1:list_rows
            K=list_J(k,:);
            S=SUB_Combinadics_AdjLexo_v1_20220803(J,K);
            if length(S)==1
                if signs(j)~=signs(k)
                    J_new=unique([J,S]);
                    list_J_new=[list_J_new;J_new];
                    signs_new=[signs_new;SUB_Combinadics_SignEval_v1_20220804(J_new)];
                end
            end
        end
    end
    list_J=list_J_new;
    signs=signs_new;
    [list_rows,~]=size(list_J);
    list_J_new=[]; signs_new=[];
end
   
%% Combinadics

list_J = indices';
list_N = list_J+1;
signs = NaN(n,1);
for k=1:n
    J = list_J(k,:);
    signs(k) = SUB_Combinadics_SignEval_v1_20220804(J);
end
superlist_J = cell(n,1);
superlist_N = superlist_J;
superlist_signs=superlist_J;
list_rows=n;

list_J_new=[];
list_N_new=[];
signs_new=[];

list_A = SUB_simplices_Combinadics_v1_20220304(n-1);

for i=1:n-1
    superlist_J{i}=list_J;
    superlist_N{i}=list_N;
    superlist_signs{i}=signs;
    A=list_A{i};
    indices=1:list_rows;
    flag_N=zeros(nchoosek(n,i));
    for j=indices
        NJ=list_N(j);
        for k=indices(A(NJ,list_N)==1)
            NK=list_N(k);
            if signs(NJ)~=signs(NK)
                J_new=unique([list_J(j,:),list_J(k,:)]);
                N_new=SUB_simplices_ConvertCombinadics_v1_20220309(J_new');
                if flag_N(N_new)==0
                    list_J_new=[list_J_new;J_new];
                    list_N_new=[list_N_new;N_new];
                    signs_new=[signs_new;SUB_Combinadics_SignEval_v1_20220804(J_new)];
                    flag_N(N_new)=1;
                end
            end
        end
    end
    list_J=list_J_new;
    list_N=list_N_new;
    signs=sparse(list_N,1,signs_new);
    list_rows=length(list_N);
    list_J_new=[]; list_N_new=[]; signs_new=[];
end