function [J_list,N_list,S_list] = ALGO_Combinadics_BitRep_v1_20220809(n)

ind    = 0:n-1;     % corresponding elements of J
S_list = NaN(2^n,1); % signs assoc. to each J
J_list = eye(n);    % list of all J found
num_J  = n;         % number of J found so far
N_list = zeros(1,num_J); % binary number assoc. to each J
for k=1:num_J
    N_list(k) = bit2int(J_list(:,k),n); % find assoc. N_2(J)
    J         = ind(J_list(:,k)==1);    % find explicit J
    S_list(N_list(k)) = SUB_Combinadics_SignEval_v2_20220809(J); % find assoc. sign(J)
end

start_index=1;
end_index=n;

for i=1:n-1
    num_J=0;    % number of new J found
    for k=start_index:end_index-1
        J=J_list(:,k);
        for j=k+1:end_index
            K=J_list(:,j);
            diff=sum(xor(J,K)); % are J and K adjacent?
            if diff==2
                if S_list(N_list(k))~=S_list(N_list(j)) % are they on different sides of the hyperplane?
                    J_new=or(J,K);
                    N_new=bit2int(J_new,n);
                    if isnan(S_list(N_new)) % has J+K already been found?
                        J_list=[J_list,J_new];
                        N_list=[N_list,N_new];
                        J_new=ind(J_new==1);
                        S_list(N_new)=SUB_Combinadics_SignEval_v2_20220809(J_new);
                        num_J=num_J+1;
                    end
                end
            end
        end
    end
    start_index=end_index+1;
    end_index=end_index+num_J;
end