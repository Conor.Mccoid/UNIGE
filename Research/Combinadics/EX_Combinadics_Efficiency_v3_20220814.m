% Comparisons in the efficiency of the various algorithms for combination
% trees

% Step 1: generate initial combinations and storage solutions
% Step 2: assign +/- values randomly
% Step 3: determine adjacent combinations
% Step 4: compare +/- values
% Step 5: create new combinations
% repeat from Step 2 until levels exhausted

% still to do: cost reduction measures, incl. Pascal's triangle
%   flag for lex. rep. to avoid double counting
%   visual rep. of a given tree
%   statistical analysis


%%
nmax=12;

t_complete=zeros(100,3*nmax-6);

for n=3:nmax
    for iter=1:100

        % Set-up
        [~,~,S_master]=ALGO_Combinadics_ComboRep_Setup_20220814(n);

        % Binary representation
        tic
        ALGO_Combinadics_BitRep_v2_20220814(n,S_master);
        t_complete(iter,3*n-8)=toc;

        % Combinadics
        tic
        P      = pascal(n); P=[P,eye(n,1)];
        list_A = SUB_simplices_Combinadics_v1_20220304(n-1,P);
        t_complete(iter,3*n-7)=toc;
        ALGO_Combinadics_ComboRep_v3_20220814(n,S_master,P,list_A); % compare v1 and v2
        t_complete(iter,3*n-6)=toc;
    end
end

%%
figure(1)
clf
% boxplot([t_bit,t_lexi,t_combo])
labels=3:nmax; labels=[labels; labels; labels]; labels=labels(:);
boxplot(t_complete,'labels',labels,'colors','bmr')
set(gca,'yscale','log')

figure(2)
semilogy(3:nmax,mean(t_complete(:,1:3:end),1),'bs--',...
    3:nmax,mean(t_complete(:,2:3:end),1),'mo--',...
    3:nmax,mean(t_complete(:,3:3:end),1),'r^--','markersize',10,'linewidth',2)
xlabel('n')
ylabel('mean time')
legend('Binary','Combinadic set-up','Combinadics')
set(gca,'xlim',[3,nmax],'fontsize',26,'linewidth',2)

% %%
% t_bit=t_complete(:,1:3:end);
% t_lex=t_complete(:,2:3:end);
% t_com=t_complete(:,3:3:end);
% figure(3)
% clf
% boxplot(t_bit,3:nmax,'colors','b')
% hold on
% boxplot(gca,t_lex,3:nmax,'colors','m')
% boxplot(gca,t_com,3:nmax,'colors','r')
% set(gca,'yscale','log','ylim',[1e-5,1e-1])
% hold off