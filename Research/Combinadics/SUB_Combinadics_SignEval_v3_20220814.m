function pm = SUB_Combinadics_SignEval_v3_20220814(J,S)
% SIGNEVAL gives a sign value to a combination J
%   pm = SignEval(J,S) gives a value of 0 or 1 to the combination J. This is
%   done according to a previously determined list of sign values, S.

m = length(J); n=max(J);
if isrow(J)
    P=[pascal(n),eye(n,1)];
    N=SUB_simplices_ConvertCombinadics_v1_20220309(J,P,m);
    pm=S{m}; pm=pm(N);
else
    disp('J must be a row vector')
end