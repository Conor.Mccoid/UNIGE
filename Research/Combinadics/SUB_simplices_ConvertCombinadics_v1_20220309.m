function N = SUB_simplices_ConvertCombinadics_v1_20220309(J,P,m)
% CONVERTCOMBINADICS converts a set of integers into its combinadic
% representation
%   N=ConvertCombinadics(J,P,m) converts the combination of length m into
%   its combinatorial ranking, using Pascal's triangle P to speed up
%   computation
N=0;
for j=1:m
    if J(j)>=j
        N=N + P(J(j)-j+1,j+1);
    end
end
N=N+1;