function A = SUB_simplices_Combinadics_v1_20220304(n,P)
% COMBINADICS constructs the adjacency matrix for combinadic
% representations of combinations of vertices/hyperplanes

% nb: appears to be in working order; would be ideal to get a sparse
% construction / true-false / fold m=2 case into the main loop

n_mid = ceil((n+1)/2);
A = cell(n,1);
A{1} = triu(ones(n+1),1);

A_prev = A{1};
A{2} = A_prev(1:3,1:3);
A_step = [1,1];
A_store = cell(n+1,1); % storage for A_step
for k=4:n+1
  A_step = [ A_step, zeros(P(k-3,3),1); eye(k-2), ones(k-2,1) ];
  A_store{k} = A_step; % storage of step pieces for next level
  A{2} = [ A{2}, A_step; zeros(size(A_step')), A_prev(1:(k-1),1:(k-1)) ];
end

for m=3:n_mid
  A_prev = A{m-1}; M = P(m,m+1);
  A{m} = flip(flip(A_prev(1:M,1:M)',1),2);
  A_step = flip(flip(A_store{2*m-1}',1),2);
  A_new = A_store{2*m-1};
  for k=2*m : n+1
    A_temp = [ A_step, zeros(P(k-1-m,m+1),P(k-m+1,m-1)); eye(P(k-m,m)), A_new ];
    A_step = A_temp;
    A_new  = A_store{k};
    A_store{k} = A_step;
    M = P(k-m+1,m);
    A{m} = [ A{m}, A_step; zeros(size(A_step')), A_prev(1:M,1:M) ];
  end
end

for m=1:n_mid-1
  A{n+1-m} = flip(flip(A{m}',1),2);
end