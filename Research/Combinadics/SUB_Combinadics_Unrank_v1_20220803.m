function J = SUB_Combinadics_Unrank_v1_20220803(N,n,m,P,J_array)
% UNRANK finds the m-combination associated with rank N
%   J=Unrank(N,n,m) assumes N represents an m-combination with largest
%   possible element n

% use of J_array is taking a lot of time but has sped things up

J_list=J_array{m}; J=J_list(N+1,:);
if prod(J)==0
    temp=P(n-m+1,m+1); % nchoosek(n-1,m)
    if N==0
        J = 0:(m-1);
    elseif temp>N
        J = SUB_Combinadics_Unrank_v1_20220803(N,n-1,m,P,J_array);
    else
        J = SUB_Combinadics_Unrank_v1_20220803(N-temp,n-1,m-1,P,J_array);
        J = [J, n];
    end
end