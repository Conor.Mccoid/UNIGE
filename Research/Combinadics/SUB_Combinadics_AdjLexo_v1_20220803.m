function S = SUB_Combinadics_AdjLexo_v1_20220803(J,K)
% ADJLEXO computes K\J of two lexicographically ordered combos
%   S = AdjLexo(J,K) finds which elements of K are not in J and stores them
%   in S

if isempty(J)
    S=K;
elseif isempty(K)
    S=[];
elseif J(1)==K(1)
    S=SUB_Combinadics_AdjLexo_v1_20220803(J(2:end),K(2:end));
elseif J(1)<K(1)
    S=SUB_Combinadics_AdjLexo_v1_20220803(J(2:end),K);
else
    S=SUB_Combinadics_AdjLexo_v1_20220803(J,K(2:end));
    S=[S,K(1)];
end