function pm = SUB_Combinadics_SignEval_v1_20220804(J)
% SIGNEVAL gives a sign value to a combination J
%   pm = SignEval(J) gives a value of 0 or 1 to the combination J. This
%   first version does so randomly, but must take a row vector as input.

if isrow(J)
    pm = randi([0,1]);
else
    disp('J must be a row vector')
end