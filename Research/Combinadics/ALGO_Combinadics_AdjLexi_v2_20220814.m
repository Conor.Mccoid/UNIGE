function [J_array,S_array] = ALGO_Combinadics_AdjLexi_v2_20220814(n,S_master)

ind    = 0:n-1;     % corresponding elements of J
P      = pascal(n); P=[P,eye(n,1)];
J_list = ind';      % list of J
S_list = NaN(n,1);  % list of signs assoc. to each J
for k=1:n
    J = J_list(k,:);
    S_list(k) = SUB_Combinadics_SignEval_v3_20220814(J,S_master);
end
J_array=cell(n,1);  % collection of all J for all levels
S_array=cell(n,1);  % collection of all signs
J_array{1}=J_list;
S_array{1}=S_list;
num_J=n;            % number of J to check

for i=1:n-1
    flag_N=zeros(P(n-i,i+2),1);
    for j=1:num_J-1
        J=J_list(j,:);
        for k=j+1:num_J
            K=J_list(k,:);
            S=SUB_Combinadics_AdjLexo_v1_20220803(J,K);
            if length(S)==1
                if S_list(j)~=S_list(k)
                    J_new=unique([J,S]);
                    N_new=SUB_simplices_ConvertCombinadics_v1_20220309(J_new,P,i+1);
                    if flag_N(N_new)==0
                        J_array{i+1}=[J_array{i+1}; J_new];
                        S_array{i+1}=[S_array{i+1}; SUB_Combinadics_SignEval_v3_20220814(J_new,S_master)];
                        flag_N(N_new)=1;
                    end
                end
            end
        end
    end
    J_list=J_array{i+1};
    S_list=S_array{i+1};
    [num_J,~]=size(J_list);
end