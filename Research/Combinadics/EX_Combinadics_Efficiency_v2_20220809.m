% Comparisons in the efficiency of the various algorithms for combination
% trees

% Step 1: generate initial combinations and storage solutions
% Step 2: assign +/- values randomly
% Step 3: determine adjacent combinations
% Step 4: compare +/- values
% Step 5: create new combinations
% repeat from Step 2 until levels exhausted

% still to do: cost reduction measures, incl. Pascal's triangle
%   flag for lex. rep. to avoid double counting
%   visual rep. of a given tree
%   statistical analysis

profile on
n=6;

t_bit=zeros(100,1);
t_lexi=t_bit;
t_combo=t_bit;

for iter=1:100
% Binary representation
tic
ALGO_Combinadics_BitRep_v1_20220809(n);
t_bit(iter)=toc;

% repeated combinations appearing, often with different signs, massive
% multiplier on number of intersections
% need to check if new combination already exists

% Lexicographical representation
% any way to make the same check on new combinations?
tic
ALGO_Combinadics_AdjLexi_v1_20220809(n);
t_lexi(iter)=toc;

% Combinadics
tic
ALGO_Combinadics_ComboRep_v2_20220810(n); % compare v1 and v2
t_combo(iter)=toc;
end

profile report

%%
figure(1)
subplot(1,3,1)
hist(t_bit,20)
subplot(1,3,2)
hist(t_lexi,20)
subplot(1,3,3)
hist(t_combo,20)

figure(2)
boxplot([t_bit,t_lexi,t_combo])