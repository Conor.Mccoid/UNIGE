function [measure_L,NN] = SUB_optsin_Functional_v1_20220412(coeffs,x1,x2,D21,D22,J1BC,J2BC,nonlinsolves,suppress)
% FUNCTIONAL takes coefficients of a Fourier series and returns a measure
% of how chaotic the corresponding ASPN iteration will be
%   measure_L = Functional(coeffs,...) takes coefficients coeffs and
%   creates a function with corresponding Fourier series (only
%   antisymmetric portion) and its derivative. It then applies ASPN to the
%   problem u''+f(u)=0 and retrieves a measure of chaos in the system.
%
%   Additionally requires several objects global to the example:
%       - x1 and x2, the discretizations of the two subdomains;
%       - D21 and D22, the 2nd order diff mat for these subdomains;
%       - J1BC and J2BC, which discretize the boundary conditions;
%       - nonlinsolves, the number of nonlinear solves for the inner
%       iterations.

f = @(x) sin(pi*x*(1:length(coeffs))) * coeffs;
fp= @(x) (pi*(1:length(coeffs)).*cos(pi*x*(1:length(coeffs)))) * coeffs;
% f=@(x) -sin(3.6*x);
% fp=@(x) - 3.6*cos(3.6*x);

l1=length(x1);
l2=length(x2);
a=x2(1); b=x1(end);

h=0.01;
yy=-2:h:0;
% measure_L = 0;

% if nargout==2
    NN=zeros(size(yy));
% end

for k=1:length(yy)
    y=yy(k);
    u1=ones(l1,1);
    u2=ones(l2,1);

    for i=1:nonlinsolves
        F1=D21*u1 + f(u1);
        J1=D21    + spdiags(fp(u1),0,l1,l1);
        J1=[J1BC(1,:); J1(2:end-1,:); J1BC(2,:)];
        F1(1)=J1BC(1,:)*u1; F1(end)=J1BC(2,:)*u1-y;
        u1=u1 - J1\F1;
    end
    u1a=u1(x1==a);
    g1 =J1 \ [zeros(l1-1,1); 1];
    g1a=g1(x1==a);
    
    for i=1:nonlinsolves
        F2=D22*u2 + f(u2);
        J2=D22    + spdiags(fp(u2),0,l2,l2);
        J2=[J2BC(1,:); J2(2:end-1,:); J2BC(2,:)];
        F2(1)=J2BC(1,:)*u2 - u1a; F2(end)=J2BC(2,:)*u2;
        u2=u2 - J2\F2;
    end
    u2b=u2(x2==b);
    g2 =J2\[ g1a; zeros(l2-1,1)];
    g2b=g2(x2==b);
    
%     G=u2b;
%     Gp=g2b;
    N=y - (u2b-y)/(g2b-1);
    
%     measure_L = measure_L + h*(N+y);

%     if nargout==2
        NN(k)=N;
%     end
end

measure_L = norm(NN + yy);

if nargin==8 || suppress~=1
    figure(1)
    plot([yy,-yy],[NN,-NN],'k.',[yy,-yy],[-yy,yy],'r--','markersize',10)
    axis([-2,2,-2,2])
    axis square
    xlabel('\gamma')
    ylabel('g(\gamma)')
%     legend('ASPN','y=-\gamma')
    pause(0)
end
end