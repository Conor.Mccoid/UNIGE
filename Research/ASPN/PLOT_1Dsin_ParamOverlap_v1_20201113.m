%===PLOT_1Dsin_ParamOverlap_v1_20201113===%
% Graph of parameter at which cycling begins vs size of the overlap
%   Cycling in ASPN begins when the curve of NR crosses the line 2x-x*.
%   Once this occurs, there exists a basin of initial conditions that
%   converges to a stable cycle. The parameter at which this occurs changes
%   based on the size of the overlap.
%
%   Also of interest is the size of the basin as a function of parameter
%   and/or overlap.

overlap=0.01:0.01:0.4;
n=length(overlap);
A=zeros(1,n);
a=1.8; y0=-2.5; y=y0; ymax=-1;

% figure(1)
% plot(-2.5:0.5:2.5,2.5:-0.5:-2.5,'k--')
% hold on
for i=1:n
    [x1,x2,J1BC,J2BC,D21,D22]=SUB_Init2ndDirichlet_v1_20201116(1001,overlap(i));
    while A(i)==0
        [~,~,NR]=SUB_1Dsin_iterate_v1_20201116(x1,x2,a,J1BC,J2BC,D21,D22,y,10);
        
%         figure(1)
%         plot(y,NR,'b.')
        if NR>=-y
%             EX_1Dsin_v1_20201113(overlap(i),a,2.5)
%             pause(0)

            A(i)=a;
            y0=y; ymax=y0+0.1;
        end
        y=y+0.01;
        if y>ymax
%             figure(1)
%             clf
%             plot(-2.5:0.5:2.5,2.5:-0.5:-2.5,'k--')
%             hold on
            
            y=y0;
            a=a+0.01;
        end
    end
end
% hold off

figure(2) % MTLB_1Dsin_ParamOverlap_v1
plot(overlap,A,'r.--')
xlabel('Overlap')
ylabel('\mu at cycling')