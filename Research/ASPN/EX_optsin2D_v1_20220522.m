% OptSin example in 2D
%   Rather than plot the resulting NR method over the entire nD space, we
%   will limit ourselves to a single 1D pathway through this space. This
%   pathway will begin in a given location then march tangential to the
%   direction indicated by the derivative of the fixed point iteration by a
%   preset value so that the norm of the result may be plotted uniformly.
%   Prop: the norm of the result is symmetric.

% notes 16.06.2022: nx=20, ny=20 has established chaotic behaviour, but not
% cycling behaviour. The graphs show we don't have the right slope
% approaching the cycle, meaning the cycle is unstable. Moreover, I haven't
% checked if symmetry is preserved, or that the method remains on this path
% at all times. If the method shifts along the path in an unexpected way,
% it will be significantly harder to predict where the cycle's basin is.
% Important note: using as a start the nonlinearity from the 1D case has
% been key in getting results. Consider continuation methods for better
% results.

nx = 20;
ny = 20;
[x1,x2,y,DDX1,DDX2,DDY1,DDY2,indx1,indx2,indy1,indy2,intx1,intx2] = SUB_Init2D_v1_20201130(nx,ny,0.5);
nonlinsolves=10;

Lap1=DDX1 + DDY1; Lap1(diag(indy1))=1; Lap1(diag(indx1))=1;
Lap2=DDX2 + DDY2; Lap2(diag(indy2))=1; Lap2(diag(indx2))=1;
II=speye(length(y)); II(1)=0; II(end)=0;

coeffs=zeros(5,1);
N=length(coeffs);
% x=unique([x1,x2]);
x=linspace(-1,1,101)';
f_test=@(coeffs) sin(pi*x*(1:N)) * coeffs;

% coeffs_start=0.5.^(1:N); coeffs_start=-coeffs_start';
% load('DATA_optsin_Result_v1_20220417.mat');
load('DATA_optsin_Result_v2_20220704.mat'); coeffs_end=coeffs;
N_end=length(coeffs_end);
if N_end>N
    coeffs_start=coeffs_end(1:N);
else
    coeffs_start=zeros(N,1); coeffs_start(1:N_end)=coeffs_end;
end
[grad_f,f_obj]=SUB_optsin2D_FunctionalFD_v1_20220523(coeffs_start,x1,x2,y,Lap1,Lap2,II,indx1,indx2,indy1,indy2,intx1,intx2,nonlinsolves);
iter=0;
while abs(f_obj)>1e-3 && iter < 100
    h = f_obj / norm(grad_f)^2;
    coeffs=coeffs_start - h*grad_f;
%     figure(1)
    f_obj_new=SUB_optsin2D_Functional_v1_20220522(coeffs,x1,x2,y,Lap1,Lap2,II,indx1,indx2,indy1,indy2,intx1,intx2,nonlinsolves,1);
    

%     figure(3)
% %     plot(x,f_test(coeffs),'r',x,-sin(pi*x),'b')
%     plot(x,f_test(coeffs),'r',x,sin(pi*x*(1:N_end)) * coeffs_end,'b')
%     title(['Obj. fun. val: ', num2str(f_obj_new)])
%     pause(0)
    
    iter=iter+1;
    
    while f_obj_new > f_obj && h>eps
        h = 0.5*h;
        coeffs=coeffs_start - h*grad_f;
%         figure(1)
        f_obj_new=SUB_optsin2D_Functional_v1_20220522(coeffs,x1,x2,y,Lap1,Lap2,II,indx1,indx2,indy1,indy2,intx1,intx2,nonlinsolves,1);
        
%         figure(3)
%         plot(x,f_test(coeffs),'r',x,sin(pi*x*(1:N_end)) * coeffs_end,'b')
%         title(['Obj. fun. val: ', num2str(f_obj_new)])
%         pause(0)
        
        iter=iter+1;
        if h<=eps
            break
        end
    end
    [grad_f,f_obj]=SUB_optsin2D_FunctionalFD_v1_20220523(coeffs,x1,x2,y,Lap1,Lap2,II,indx1,indx2,indy1,indy2,intx1,intx2,nonlinsolves);
    coeffs_start=coeffs;
end

%%
figure(2)
subplot(1,2,1)
% plot(x,f_test(coeffs),'r',x, -sin(pi*x),'b')
plot(x,f_test(coeffs_start),'r',x,sin(pi*x*(1:N_end)) * coeffs_end,'b')
xlabel('x')
ylabel('f(x)')
legend('Fourier series','f(x)')
subplot(1,2,2)
% plot(x,(pi*(1:length(coeffs)).*cos(pi*x*(1:length(coeffs)))) * coeffs,'r',x,-pi*cos(pi*x),'b')
plot(x,(pi*(1:N).*cos(pi*x*(1:N))) * coeffs_start,'r',x,(pi*(1:N_end).*cos(pi*x*(1:N_end))) * coeffs_end,'b')
xlabel('x')
ylabel('f''(x)')
legend('Fourier series','f''(x)')

%%
f=@(x) sin(pi*x*(1:N)) * coeffs_start;
fp=@(x) pi*(1:N).*cos(pi*x*(1:N)) * coeffs_start;
l1=length(x1); l2=length(x2);
y0=-1.3*sin(pi*y')/norm(sin(pi*y));
NR=zeros(size(y0));
a=x2(1); b=x1(end);

iter=0;
    
figure(1)
clf
for j=1:3
    subplot(2,2,j)
    f_obj=SUB_optsin2D_Functional_v1_20220522(coeffs_start,x1,x2,y,Lap1,Lap2,II,indx1,indx2,indy1,indy2,intx1,intx2,nonlinsolves);
    hold on
    axis square
end
for k=1:50
    u1=zeros(l1,ny);
    u2=zeros(l2,ny);
    
    for i=1:nonlinsolves
        J1=fp(u1(:)); J1(indx1)=0; J1(indy1)=0; % set-up nonlinearity
        J1=Lap1 - spdiags(J1,0,l1*ny,l1*ny);    % Jacobian
        F1=Lap1*u1(:) - f(u1(:));
        BCx1=[zeros(1,ny-2);u1(end,2:end-1)-y0(2:end-1)]; F1(indx1)=BCx1(:); F1(indy1)=0; % BCs
        u1(:)=u1(:) - J1 \ F1;
    end
    u1a=u1(x1==a,2:end-1);
    g1=J1 \ kron(II,[ zeros(l1-1,1); 1]);
    g1a=g1(intx1,2:end-1);
    
    for i=1:nonlinsolves
        J2=fp(u2(:)); J2(indx2)=0; J2(indy2)=0; % set-up nonlinearity
        J2=Lap2 - spdiags(J2,0,l2*ny,l2*ny);    % Jacobian
        F2=Lap2*u2(:) - f(u2(:));
        BCx2=[u2(1,2:end-1)-u1a;zeros(1,ny-2)]; F2(indx2)=BCx2(:); F2(indy2)=0; % BCs
        u2(:)=u2(:) - J2 \ F2;
    end
    u2b=u2(x2==b,2:end-1);
    g2=J2 \ kron(II,[ 1; zeros(l2-1,1)]);
    g2b=g2(intx2,2:end-1);
    g2b=g2b*g1a-II(2:end-1,2:end-1);
    
    NR(2:end-1)=y0(2:end-1) - (g2b \ (u2b - y0(2:end-1))')';
    
    figure(1)
    for j=1:3
        subplot(2,2,j)
        plot([-norm(y0),-norm(y0),-norm(NR)],[norm(y0),norm(NR),norm(NR)],'bo--','Linewidth',2,'Markersize',10)
    end
    y0=NR;
    
    u=[u1(x1<=0,:); u2(x2>0,:)];
    xx=unique([x1,x2]);
    if k<=24
        iter=iter+1;
        figure(2)
        subplot(6,8,2*iter-1)
        contourf(xx,y,u')
        title(['Iteration ', num2str(k)])
        axis square
        
        subplot(6,8,2*iter)
        plot(y,[0,u2b,0],'b.-',y,NR,'k')
        if k<=13
            hold on
            plot(y,(-1)^(k+1) * 0.5*sin(pi*y),'r--')
            hold off
        end
        axis([-1,1,-1,1])
        axis square
    end
end
figure(1)
for j=1:3
    subplot(2,2,j)
    hold off
    if j==1
        axis([-9,0,0,9])
    elseif j==2
        axis([-1.6,0,0,1.6])
    elseif j==3
        axis([-1.5,-1.4,1.4,1.5])
    end
    axis square
    set(gca,'Fontsize',13,'Linewidth',2)
end

subplot(2,2,4)
plot(x,f_test(coeffs_start),'r',x,sin(pi*x*(1:N_end)) * coeffs_end,'b','linewidth',2)
xlabel('x')
ylabel('f(x)')
legend('Final','Initial')
axis square
set(gca,'Fontsize',13,'Linewidth',2)