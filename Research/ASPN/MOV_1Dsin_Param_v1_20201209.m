% MOV_1Dsin_Param_v1_20201209
% Movie of G(y) and NR for varying parameter at fixed overlap

overlap=2; nonlinsolves=35; n=4;
[x1,x2,J1BC,J2BC,D21,D22] = SUB_Init2ndDirichlet_v1_20201116(n,overlap);
L=2.5;
% yy=-L:0.01:L;
yy=0;
% for A=2:0.1:4
for A=3.3
    G=zeros(size(yy)); Gp=G; N=G;
    for k=1:length(yy)
        [G(k),Gp(k),N(k),u1norm]=SUB_1Dsin_iterate_v1_20201116(x1,x2,A,J1BC,J2BC,D21,D22,yy(k),nonlinsolves);
        
        figure(2)
        if A==2
            clf
        else
            hold on
        end
        semilogy(1:nonlinsolves,u1norm,'.--','DisplayName',['a=', num2str(A)])
        xlabel('Iterate')
        ylabel('Norm of iterate of NR')
    end
    figure(1)
    plot(yy,G,'b',yy,N,'r',yy,yy,'k--',yy,-yy,'k--','Linewidth',2)
    axis([-L,L,-L,L])
    axis square
    xlabel('\gamma')
    ylabel('G(\gamma)')
    legend('G(\gamma)','NR')
    set(gca,'linewidth',2,'fontsize',26)
    pause(0)
end

figure(2)
legend('show')