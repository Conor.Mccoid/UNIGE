function [gradf,f] = SUB_optsin_FunctionalFD_v1_20220416(coeffs,x1,x2,D21,D22,J1BC,J2BC,nonlinsolves)
% FUNCTIONALFD computes a finite difference approximation of the Jacobian
% of the functional for the optsin experiment

f = SUB_optsin_Functional_v1_20220412(coeffs,x1,x2,D21,D22,J1BC,J2BC,nonlinsolves,1);
gradf = zeros(size(coeffs));

h=1e-1;
for k=1:length(coeffs)
%     coeffs_up=coeffs; coeffs_up(k)=coeffs(k)+h;
    coeffs_down=coeffs; coeffs_down(k)=coeffs(k)-h;
    gradf(k)=f - SUB_optsin_Functional_v1_20220412(coeffs_down,x1,x2,D21,D22,J1BC,J2BC,nonlinsolves,1);
    %         SUB_optsin_Functional_v1_20220412(coeffs_down,x1,x2,D21,D22,J1BC,J2BC,nonlinsolves,1);
    gradf(k)=gradf(k)/h;
    h=h*0.1;
end
% gradf=gradf/(2*h);
end