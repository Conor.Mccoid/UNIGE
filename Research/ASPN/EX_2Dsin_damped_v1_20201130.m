% EX_2Dsin_damped_v1_20201130
%   This example looks at the 2D sin example with damping in the
%   y-direction, such that complete damping results in the 1D sin example.
%   Since we know the 1D sin example has cycles and this behaviour should
%   be continuous, there must be an amount of damping that results in
%   cycles in 2D.
%
%   This is in essence a copy-paste from 'Experiment15.m' and requires
%   editing to bring it up to current standards.

% Grid
nx = 21; ny = 21;
[x1,x2,DDX1,DDX2,DDY1,DDY2,ind1x,ind2x,ind1y,ind2y] = SUB_Init2D_v1_20201130(nx,ny,0.4);

b = x1(end);
a = x2(1);
l1 = length(x1); l2 = length(x2);
ind1 = 1:l1; ind1 = ind1(abs(x1-a)<=(h+2*eps));
ind2 = 1:l2; ind2 = ind2(abs(x2-b)<=(h+2*eps));

% Diff. mat.
X1 = [ [x1(2:end) ; 0], x1, [0 ; x1(1:end-1)] ];
X2 = [ [x2(2:end) ; 0], x2, [0 ; x2(1:end-1)] ];

I1 = speye(l1); I1int = I1; I1int(1) = 0; I1int(end) = 0;
D1 = ones(l1,1);
D1 = dx^2 * [ D1, -2*D1, D1 ];
D1 = spdiags(D1,[-1,0,1],l1,l1);
I2 = speye(l2); I2int = I2; I2int(1) = 0; I2int(end) = 0;
D2 = ones(l2,1);
D2 = dx^2 * [ D2, -2*D2, D2 ];
D2 = spdiags(D2,[-1,0,1],l2,l2);

pert    = 0;
epsilon = 1e-9; %1e-3 gives period doubling
II = speye(ny); IIint = II; IIint(1) = 0; IIint(end) = 0;

% BC parameters
a1 = 0; b1 = 1;
a2 = 0; b2 = 1;
c1 = 0; d1 = 1; BC1 = sparse([1,1,1],ind1,[-c1*dx, d1, c1*dx],1,l1);
c2 = 0; d2 = 1; BC2 = sparse([1,1,1],ind2,[-c2*dx, d2, c2*dx],1,l2);
BCL= zeros(1,ny-2);
BCR= zeros(1,ny-2);

% Jacobian BCs
J1BC = sparse([1,1,1,2,2,2],[1,2,3,l1-2,l1-1,l1],...
    [-3*a1/(2*h) + b1,4*a1/(2*h),-a1/(2*h),c1/(2*h),-4*c1/(2*h),3*c1/(2*h)+d1],2,l1);
J2BC = sparse([1,1,1,2,2,2],[1,2,3,l2-2,l2-1,l2],...
    [-3*c2/(2*h) + d2,4*c2/(2*h),-c2/(2*h),a2/(2*h),-4*a2/(2*h),3*a2/(2*h)+b2],2,l2);
% DDBC = sparse([1,1,1,2,2,2],[1,2,3,nx-2,nx-1,nx],...
%     0.5 * dx * [-3, 4, -1, 1, -4, 3],2,nx); % Neumann BCs in y
DDBC = [II(1,:) ; II(end,:)]; % Dirichlet BCs in y

% Laplacian
Lap1=DDX1 + epsilon*DDY1; Lap1(diag(ind1y))=1;
Lap2=DDX2 + epsilon*DDY2; Lap2(diag(ind2y))=1;

% Plotting bifurcation region
strpt = 3.4;
endpt = 3.8;
res   = 0.1;

% fx  = [11.86,37.6,37.6,11.86];
fx  = 1.6*ones(1,ny-2);
ind = round((endpt - strpt) / res);
gam = zeros(ind,64);
gamnorm = gam;
nonlinsolves = 10;
stab= 50;

for k = 1:ind
    C = strpt + k*res;
    F = @(x)   sin(C*x);
    Fp= @(x) C*cos(C*x);
    
    u1 = zeros(l1,ny);
    u2 = zeros(l2,ny);
    u2b= fx;
    u2bold = u2b;
    for iter = 1:(stab+64)
        % Step 1: solve u in first domain
        for i = 1:nonlinsolves
            J1        = Fp(u1(:));
            J1(ind1x) = 0; J1(ind1y) = 0;
            J1        = Lap1 - spdiags(J1,0,l1*ny,l1*ny);
            F1        = Lap1*u1(:) - F(u1(:));
            BCy1      =(DDBC * u1'- pert)';
            BCx1      = J1BC * u1(:,2:end-1) - [ BCL ; u2b ];
            F1(ind1x) = BCx1(:); F1(ind1y) = BCy1(:);
            u1(:)     = u1(:) - J1 \ F1;
        end
        u1a= BC1 * u1; u1a = u1a(2:end-1);
        
        % Step 2: solve u in second domain
        for i = 1:nonlinsolves
            J2        = Fp(u2(:));
            J2(ind2x) = 0; J2(ind2y) = 0;
            J2        = Lap2 - spdiags(J2,0,l2*ny,l2*ny);
            F2        = Lap2*u2(:) - F(u2(:));
            BCy2      =(DDBC * u2' - pert)';
            BCx2      = J2BC * u2(:,2:end-1) - [ u1a ; BCR ];
            F2(ind2x) = BCx2(:); F2(ind2y) = BCy2(:);
            u2(:)     = u2(:) - J2 \ F2;
        end
        u2b = BC2 * u2; u2b = u2b(2:end-1);

        % Preconditioning with Newton
        % Step 3: solve g in first domain
        dF1= Fp(u1(:)); dF1(ind1x) = 0; dF1(ind1y) = 0;
        dF1= Lap1 - spdiags(dF1,0,l1*ny,l1*ny);
        g1 = dF1 \ kron(IIint,[ zeros(l1-1,1); 1]);
        g1a= kron(IIint,BC1) * g1;

        % Step 4: solve g in second domain
        dF2= Fp(u2(:)); dF2(ind2x) = 0; dF2(ind2y) = 0;
        dF2= Lap2 - spdiags(dF2,0,l2*ny,l2*ny);
        g2 = dF2 \ kron(IIint,[ 1; zeros(l2-1,1)]);
        g2b= kron(IIint,BC2) * g2; g2b = g2b * g1a - II; g2b = g2b(2:end-1,2:end-1);

        % Step 5: update u2b
        u2b = u2bold - ( g2b \ (u2b' - u2bold') )';
        u2bold= u2b;
        
        if iter > stab
            gamnorm(k,iter-stab) = norm(u2b)./norm(fx);
            gam(k,iter-stab) = u2b(1);
        end
        
    end
end

figure(1)
aa = strpt + (1:ind)*res;
plot(aa,gam,'k.')
xlabel('a')
ylabel('$$\Vert \gamma \Vert$$','interpreter','latex')
% title(['Perturbation = ', num2str(pert)])
set(gca,'fontsize',26,'linewidth',2)

figure(2)
plot(aa,gamnorm,'k.')
xlabel('a')
ylabel('$$\Vert \gamma \Vert$$','interpreter','latex')
% axis([min(aa),max(aa),23.524,23.533])
set(gca,'fontsize',26,'linewidth',2)