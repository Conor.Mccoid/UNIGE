function EX_1Dsin_v1_20201113(overlap,A,L)
% 1DSIN_v1 plots G(y) and NR with variable overlap and parameter
%   1Dsin_v1(overlap,A,L) takes y in [-L,L] with overlap of size overlap
%   and problem parameter A and produces G(y), the fixed point function,
%   and its Newton-Raphson-accelerated counterpart.

[x1,x2,J1BC,J2BC,D21,D22] = SUB_Init2ndDirichlet_v1_20201116(1001,overlap);
if nargin<3
    L=2.5;
end
yy=-L:0.01:L;
G=zeros(size(yy)); Gp=G; N=G;

for k=1:length(yy)
    [G(k),Gp(k),N(k)] = SUB_1Dsin_iterate_v1_20201116(x1,x2,A,J1BC,J2BC,D21,D22,yy(k),50);
end

figure(1)
plot(yy,G,'b',yy,N,'r',yy,yy,'k--',yy,-yy,'k--','Linewidth',2)
axis([-L,L,-L,L])
axis square
xlabel('\gamma')
ylabel('G(\gamma)')
legend('G(\gamma)','NR')
set(gca,'linewidth',2,'fontsize',26)

%==Secant method==%
S=zeros(length(yy));
for i=1:length(yy)
    for j=1:length(yy)
        S(i,j) = yy(i) - (G(i) - yy(i))*(yy(i) - yy(j))/(G(i)-G(j) + yy(j) - yy(i));
        S(i,j) = S(i,j)/abs(yy(i));
    end
end
figure(2)
contourf(yy,yy,S,-1:1)
axis([-L,L,-L,L])
axis square
xlabel('x_n')
ylabel('x_{n-1}')
title('Secant method applied to altS')
set(gca,'linewidth',2,'fontsize',26)
end