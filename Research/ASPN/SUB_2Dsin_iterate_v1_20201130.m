function results = SUB_2Dsin_iterate_v1_20201130(x1,x2,y,Lap1,Lap2,A)
% 2DSIN_ITERATE computes one iteration of the 2D sin example

ny=length(y);
l1=length(x1); u1=zeros(l1,ny); b=x1(end);
l2=length(x2); u2=zeros(l2,ny); a=x2(1);
II=speye(ny); IIint=II; IIint(1)=0; IIint(end)=0;

for i=1:nonlinsolves
    F1=Lap1*u1(:) - sin(A*u1(:));
    J1=A*cos(A*u1(:)); J1(ind1x)=0; J1(ind1y)=0; % zero out BC rows
    J1=Lap1       - spdiags(J1,0,l1*ny,l1*ny);
    BCy1=(yBC*u1')'; BCx1=J1BC*u1(:,2:end-1) - [BCL;u2b]; % BCs
    F1(ind1x)=BCx1(:); F1(ind1y)=BCy1(:);
    u1(:)=u1(:)-J1\F1;
end
u1a=u1(:,x1==a); u1a=u1a(2:end-1);
g1=J1 \ kron(IIint,[ zeros(l1-1,1); 1]);
g1a=

for i=1:nonlinsolves
    F2=Lap2*u2(:) - sin(A*u2(:));
    J2=A*cos(A*u2(:)); J2(ind2x)=0; J2(ind2y)=0;
    J2=Lap2       - spdiags(J2,0,l2*ny,l2*ny);
    BCy2=(yBC*u2')'; BCx2=J2BC*u2(:,2:end-1) - [u1a;BCR];
    F2(ind2x)=BCx2(:); F2(ind2y)=BCy2(:);
    u2(:)=u2(:)-J2\F2;
end
u2b=u2(x2==b); u2b=u2b(2:end-1);