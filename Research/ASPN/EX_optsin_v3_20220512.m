% OptSin example, using gradient descent with a line search

n_disc=100; % size of discretization, must be divisible by 2
            % half this is the discretization in Fourier space
[x1,x2,J1BC,J2BC,D21,D22]=SUB_Init2ndDirichlet_v1_20201116(n_disc+1,0.4);
nonlinsolves=10;

% N = n_disc/2;
coeffs=zeros(5,1);
N = length(coeffs);
x=unique([x1,x2]);
alpha=pi;
f=@(x) -sin(alpha*x); fp=@(x) -alpha*cos(alpha*x);
for k=1:length(coeffs)
    coeffs(k) = f(x') * sin(pi*k*x) * (x(2)-x(1));
end
f_test=@(coeffs) sin(pi*x*(1:N)) * coeffs;

coeffs_start=0.5.^(1:N); coeffs_start=-coeffs_start';
[grad_f,f_obj]=SUB_optsin_FunctionalFD_v1_20220416(coeffs_start,x1,x2,D21,D22,J1BC,J2BC,nonlinsolves);
iter=0;
while abs(f_obj)>1e-3 && iter < 100
    y = f_obj / norm(grad_f)^2;
    coeffs=coeffs_start - y*grad_f;
    figure(1)
    f_obj_new=SUB_optsin_Functional_v1_20220412(coeffs,x1,x2,D21,D22,J1BC,J2BC,nonlinsolves,1);
    

    figure(3)
    plot(x,f_test(coeffs),'r',x,f(x),'b')
    title(['Obj. fun. val: ', num2str(f_obj_new)])
    pause(0)
    
    iter=iter+1;
    
    while f_obj_new > f_obj && y>eps
        y = 0.5*y;
        coeffs=coeffs_start - y*grad_f;
        figure(1)
        f_obj_new=SUB_optsin_Functional_v1_20220412(coeffs,x1,x2,D21,D22,J1BC,J2BC,nonlinsolves,1);
        
%         figure(3)
%         plot(x,f_test(coeffs),'r',x,f(x),'b')
%         title(['Obj. fun. val: ', num2str(f_obj_new)])
%         pause(0)
        
        iter=iter+1;
        if y<=eps
            return
        end
    end
    [grad_f,f_obj]=SUB_optsin_FunctionalFD_v1_20220416(coeffs,x1,x2,D21,D22,J1BC,J2BC,nonlinsolves);
    coeffs_start=coeffs;
end

%%
figure(2)
subplot(1,2,1)
plot(x,f_test(coeffs),'r',x, f(x),'b')
xlabel('x')
ylabel('f(x)')
legend('Fourier series','f(x)')
subplot(1,2,2)
plot(x,(pi*(1:length(coeffs)).*cos(pi*x*(1:length(coeffs)))) * coeffs,'r',x,fp(x),'b')
xlabel('x')
ylabel('f''(x)')
legend('Fourier series','f''(x)')

%%
f=@(x) sin(pi*x*(1:N)) * coeffs;
fp=@(x) pi*(1:N).*cos(pi*x*(1:N)) * coeffs;
y=-1.22;
l1=length(x1); l2=length(x2);
a=x2(1); b=x1(end);
    u1=ones(l1,1);
    u2=ones(l2,1);
    
figure(1)
clf
subplot(1,2,1)
f_obj=SUB_optsin_Functional_v1_20220412(coeffs,x1,x2,D21,D22,J1BC,J2BC,nonlinsolves);
hold on
for k=1:50

    for i=1:nonlinsolves
        F1=D21*u1 + f(u1);
        J1=D21    + spdiags(fp(u1),0,l1,l1);
        J1=[J1BC(1,:); J1(2:end-1,:); J1BC(2,:)];
        F1(1)=J1BC(1,:)*u1; F1(end)=J1BC(2,:)*u1-y;
        u1=u1 - J1\F1;
    end
    u1a=u1(x1==a);
    g1 =J1 \ [zeros(l1-1,1); 1];
    g1a=g1(x1==a);
    
    for i=1:nonlinsolves
        F2=D22*u2 + f(u2);
        J2=D22    + spdiags(fp(u2),0,l2,l2);
        J2=[J2BC(1,:); J2(2:end-1,:); J2BC(2,:)];
        F2(1)=J2BC(1,:)*u2 - u1a; F2(end)=J2BC(2,:)*u2;
        u2=u2 - J2\F2;
    end
    u2b=u2(x2==b);
    g2 =J2\[ g1a; zeros(l2-1,1)];
    g2b=g2(x2==b);
    
    NR=y - (u2b-y)/(g2b-1);
    plot([y,y,NR],[y,NR,NR],'r.-','Linewidth',1.5)
    y=NR;

end
plot(-2:2,-2:2,'b--')
set(gca,'Fontsize',26,'Linewidth',2)
hold off

subplot(1,2,2)
plot(x,f_test(coeffs),'r','Linewidth',2)
set(gca,'Fontsize',26,'Linewidth',2)
axis([-1,1,-2,2])
axis square
xlabel('x')
ylabel('f(x)')