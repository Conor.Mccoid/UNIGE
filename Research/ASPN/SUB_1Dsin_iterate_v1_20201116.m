function [G,Gp,N,u1norm]=SUB_1Dsin_iterate_v1_20201116(x1,x2,A,J1BC,J2BC,D21,D22,y,nonlinsolves)
% 1DSIN_ITERATE performs one iteration of ASPN for the 1Dsin example
%   [G,Gp,N] = 1Dsin_iterate(x1,x2,A,J1BC,J2BC,D21,D22,y) computes the
%   fixed point function of AltS, its derivative, and the Newton-Raphson
%   acceleration for the 1Dsin example with domains x1 and x2, parameter A,
%   BCs defined in J1BC and J2BC, and 2nd derivative matrices D21 and D22,
%   with initial value y. The number of nonlinear solves used is given in
%   'nonlinsolves'.

l1=length(x1); u1=ones(l1,1);
l2=length(x2); u2=ones(l2,1);
a=x2(1); b=x1(end);

if nargout>3
    u1norm=zeros(1,nonlinsolves);
end

for i=1:nonlinsolves
    F1=D21*u1 - sin(A*u1);
    J1=D21    - spdiags(A*cos(A*u1),0,l1,l1);
    J1=[J1BC(1,:); J1(2:end-1,:); J1BC(2,:)];
    F1(1)=J1BC(1,:)*u1; F1(end)=J1BC(2,:)*u1-y;
    u1=u1 - J1\F1;
    
    if nargout>3
        u1norm(i)=norm(u1);
        
        figure(3)
        hold on
        plot(u1(2),u1(3),'.')
        text(u1(2),u1(3),num2str(i))
        xlabel('u(-1/3)')
        ylabel('u(1/3)')
    end
end
u1a=u1(x1==a);
g1 =J1 \ [zeros(l1-1,1); 1];
g1a=g1(x1==a);

for i=1:nonlinsolves
    F2=D22*u2 - sin(A*u2);
    J2=D22    - spdiags(A*cos(A*u2),0,l2,l2);
    J2=[J2BC(1,:); J2(2:end-1,:); J2BC(2,:)];
    F2(1)=J2BC(1,:)*u2 - u1a; F2(end)=J2BC(2,:)*u2;
    u2=u2 - J2\F2;
end
u2b=u2(x2==b);
g2 =J2\[ g1a; zeros(l2-1,1)];
g2b=g2(x2==b);

G=u2b;
Gp=g2b;
N=y - (u2b-y)/(g2b-1);
end