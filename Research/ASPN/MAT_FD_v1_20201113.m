function [D,D2] = MAT_FD_v1_20201113(n,h)
% FD builds the finite difference matrix
%   D = FD(n,h) builds the matrix that calculates the finite difference
%   approximation of the derivative for a set of n equidistant points each 
%   separated by h using the midpoint rule.
%
%   [D,D2] = FD(n,h) builds both the first order and second order FD
%   matrices.

D=ones(n,1)/(2*h);
D=[-D, zeros(n,1), D];
D=spdiags(D,[-1,0,1],n,n);

if nargout>1
    D2=ones(n,1)/h^2;
    D2=[D2,-2*D2,D2];
    D2=spdiags(D2,[-1,0,1],n,n);
end
end