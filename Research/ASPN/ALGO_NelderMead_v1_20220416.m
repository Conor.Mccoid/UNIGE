function [x_end,f_end] = ALGO_NelderMead_v1_20220416(f,list_x,list_f,tol)
% NELDERMEAD runs the Nelder-Mead optimization algorithm on a given
% multivariate scalar-valued objective function

al=1;
gm=2;
ro=0.5;
if nargin<4
    tol=1e-3;
    itermax=500;
end

N=length(list_f);
[list_f,ind] = sort(list_f);
list_x = list_x(:,ind);
x0 = mean(list_x,2);
stop_test = vecnorm(list_x - x0,1); stop_test=sqrt(stop_test*stop_test'/N);
iter=1;
while stop_test>tol && iter<itermax
    xr = x0 + al*(x0-list_x(:,end));
    fr = f(xr);
    if list_f(1)<=fr && fr<list_f(end-1)
        list_x(:,end) = xr;
        list_f(end) = fr;
    elseif fr<list_f(1)
        xe = x0 + gm*(xr-x0);
        fe = f(xe);
        if fe<fr
            list_x(:,end) = xe;
            list_f(end) = fe;
        else
            list_x(:,end) = xr;
            list_f(end) = fr;
        end
    elseif fr>=list_f(end-1)
        if fr<list_f(end)
            xc = x0 + ro*(xr-x0);
            fc = f(xc);
            if fc<fr
                list_x(:,end) = xc;
                list_f(end) = fc;
            else
                [list_x,list_f] = SUB_NelderMead_Shrink(f,list_x,list_f);
            end
        else
            xc = x0 + ro*(list_x(:,end) - x0);
            fc = f(xc);
            if fc<list_f(end)
                list_x(:,end) = xc;
                list_f(end) = fc;
            else
                [list_x,list_f] = SUB_NelderMead_Shrink(f,list_x,list_f);
            end
        end
    end
    [list_f,ind] = sort(list_f);
    list_x = list_x(:,ind);
    x0 = mean(list_x,2);
    stop_test = vecnorm(list_x - x0,1); stop_test=sqrt(stop_test*stop_test'/N);
    iter=iter+1;
    disp(['Current chaos: ' num2str(list_f(1))])
end
x_end=list_x(:,1); f_end=list_f(1);
end

function [list_x,list_f] = SUB_NelderMead_Shrink(f,list_x,list_f)
% SHRINK performs the shrinking portion of Nelder-Mead

sg=0.5;
x1 = list_x(:,1);
for k=2:length(list_f)
    list_x(:,k) = x1 + sg*(list_x(:,k) - x1);
    list_f(k) = f(list_x(:,k));
end
end