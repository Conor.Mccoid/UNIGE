% PLOT_1Dsin_NRConvergence_n4_v1_20201211
% Vector field of NR for 1D sin example, no DDM, n=4 (2D input/output)
%   The vector field showing where NR points given an input of 2 variables
%   is plotted to see if/where cycles/divergence may occur for n=4 points,
%   2 of which are boundary points.
%
%   Makes use of SUB_1Dsin_iterate_v2_20201211 which has been modified to
%   provide additional outputs.

overlap=2; nonlinsolves=1; n=4;
[x1,x2,J1BC,J2BC,D21,D22] = SUB_Init2ndDirichlet_v1_20201116(n,overlap);
L=2.5; x=-L:0.1:L; y=x;
xx=repmat(x,length(y),1); yy=repmat(y',1,length(x));

for A=0:0.1:4
    NRx=zeros(length(x),length(y)); NRy=NRx;
    for i=1:length(x)
        for j=1:length(y)
            [NRx(i,j),NRy(i,j)]=SUB_1Dsin_iterate_v2_20201211(x1,A,J1BC,D21,[0;x(i);y(j);0]);
        end
    end

    figure(1)
    contourf(xx,yy,sqrt(NRx.^2 + NRy.^2)./sqrt(xx.^2 + yy.^2),[0,1,2]) % contour of relative movement, more than 1 being bad
    hold on
    quiver(xx,yy,NRx./sqrt(NRx.^2 + NRy.^2),NRy./sqrt(NRx.^2 + NRy.^2),'linewidth',2) % quivers show where the method points
    hold off
    xlabel('u(-1/3)')
    ylabel('u(1/3)')
    axis([-L,L,-L,L])
    title(['a=', num2str(A)])
    pause(0.5)
end