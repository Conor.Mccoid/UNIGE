function f = SUB_optsin2D_f_v1_20220522(x,coeffs)
% F is the nonlinearity in the ODE in 2D
%   A function in nD can be decomposed into a Fourier series with each
%   element of the series identified by n indices.
%   In this case, the function is antisymmetric, and so only about half of
%   the elements are required. Each element of interest contains an odd
%   number of sines and the remaining dimensions cosines.

%   In this first version, we will allow each index to run from 0 to 4. We
%   will also assume a minimum number of cosines. This will then relate to
%   the number of 0's in the set of indices.

%   Question I seem to be asking quite late: what is the dimension this
%   function lives in? Surely it's only 2D... why did I think it was
%   higher? Then I need sin(k pi x) cos(j pi y) and cos(k pi x) sin(j pi y)

%   In fact, if u, the solution, is scalar-valued, then the nonlinearity
%   must be single variate. This means I could use the same (albeit
%   possibly with more coefficients) function as previously...