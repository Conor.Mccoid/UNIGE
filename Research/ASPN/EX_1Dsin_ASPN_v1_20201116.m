function N=EX_1Dsin_ASPN_v1_20201116(overlap,A,domain,itermax,plotinput)
% 1DSIN_ASPN plots results of ASPN for the 1Dsin example
%   1Dsin_ASPN(overlap,A,domain,itermax) takes y in 'domain' with overlap
%   of size 'overlap' and parameter A and performs at most itermax
%   iterations of ASPN.
%
%   Produces MTLB_1Dsin_NewtonGamma_v1

[x1,x2,J1BC,J2BC,D21,D22] = SUB_Init2ndDirichlet_v1_20201116(1001,overlap);
N=zeros(size(domain));

for k=1:length(domain)
    error=1; iter=0; Nold=domain(k);
    while error > 1e-5 && iter < itermax
        [~,~,N(k)] = SUB_1Dsin_iterate_v1_20201116(x1,x2,A,J1BC,J2BC,D21,D22,Nold,50);
        error = abs(N(k)-Nold);
        Nold  = N(k);
        iter  = iter+1;
    end
end

if nargin<5 || plotinput~=0
    figure(1)       % MTLB_1Dsin_NewtonGamma_v1
    plot(domain,N,'r.',domain,domain,'k--',domain,-domain,'k--','Linewidth',2,'markersize',10)
    axis([min(domain),max(domain),min(domain),max(domain)])
    axis square
    xlabel('\gamma')
    ylabel('NR accelerated alt Schwarz')
    set(gca,'linewidth',2,'fontsize',26)
end
end