function [gradf, f] = SUB_optsin2D_FunctionalFD_v1_20220523(coeffs,x1,x2,y,Lap1,Lap2,II,indx1,indx2,indy1,indy2,intx1,intx2,nonlinsolves)

f = SUB_optsin2D_Functional_v1_20220522(coeffs,x1,x2,y,Lap1,Lap2,II,indx1,indx2,indy1,indy2,intx1,intx2,nonlinsolves,1);
gradf = zeros(size(coeffs));

h=1e-1;
for k=1:length(coeffs)
%     coeffs_up=coeffs; coeffs_up(k)=coeffs(k)+h;
    coeffs_down=coeffs; coeffs_down(k)=coeffs(k)-h;
    gradf(k)=f - SUB_optsin2D_Functional_v1_20220522(coeffs_down,x1,x2,y,Lap1,Lap2,II,indx1,indx2,indy1,indy2,intx1,intx2,nonlinsolves,1);
    gradf(k)=gradf(k)/h;
    h=h*0.1;
end
end