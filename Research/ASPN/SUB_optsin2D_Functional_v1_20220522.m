function [measure_L,NN] = SUB_optsin2D_Functional_v1_20220522(coeffs,x1,x2,y,Lap1,Lap2,II,indx1,indx2,indy1,indy2,intx1,intx2,nonlinsolves,suppress)
% FUNCTIONAL for the optsin example in 2D

f = @(u) sin(pi*u*(1:length(coeffs))) * coeffs;
fp= @(u) (pi*(1:length(coeffs))).*cos(pi*u*(1:length(coeffs))) * coeffs;

l1=length(x1);
l2=length(x2);
ly=length(y);
a=x2(1); b=x1(end);

h=0.01;
y0=-0.5*sin(pi*y');
N=1/h;
% N=100;
NN=zeros(N,ly);
yy=zeros(N,ly);

for k=1:N
    u1=zeros(l1,ly);
    u2=zeros(l2,ly);
    yy(k,:)=y0;
    
    for i=1:nonlinsolves
        J1=fp(u1(:)); J1(indx1)=0; J1(indy1)=0; % set-up nonlinearity
        J1=Lap1 - spdiags(J1,0,l1*ly,l1*ly);    % Jacobian
        F1=Lap1*u1(:) - f(u1(:));
        BCx1=[zeros(1,ly-2);u1(end,2:end-1)-y0(2:end-1)]; F1(indx1)=BCx1(:); F1(indy1)=0; % BCs
        u1(:)=u1(:) - J1 \ F1;
    end
    u1a=u1(x1==a,2:end-1);
    g1=J1 \ kron(II,[ zeros(l1-1,1); 1]);
    g1a=g1(intx1,2:end-1);
    
    for i=1:nonlinsolves
        J2=fp(u2(:)); J2(indx2)=0; J2(indy2)=0; % set-up nonlinearity
        J2=Lap2 - spdiags(J2,0,l2*ly,l2*ly);    % Jacobian
        F2=Lap2*u2(:) - f(u2(:));
        BCx2=[u2(1,2:end-1)-u1a;zeros(1,ly-2)]; F2(indx2)=BCx2(:); F2(indy2)=0; % BCs
        u2(:)=u2(:) - J2 \ F2;
    end
    u2b=u2(x2==b,2:end-1);
    g2=J2 \ kron(II,[ 1; zeros(l2-1,1)]);
    g2b=g2(intx2,2:end-1);
    g2b=g2b*g1a-II(2:end-1,2:end-1);
    
    NN(k,2:end-1)=y0(2:end-1) - (g2b \ (u2b - y0(2:end-1))')'; 
    y0=(1-k*h)*y0/(1-(k-1)*h);
%     y0=(1-h)*y0 + h*[0,u2b,0];
    
%     figure(2)
% %     surf(y,x1(x1<=a),u1(x1<=a,:))
% %     hold on
% %     surf(y,x2,u2)
% %     hold off
%     plot(y,y0,'k-',y,NN(k,:),'r--')
%     xlabel('y')
%     ylabel('Iterations')
%     legend('Alt Schwarz','ASPN')
%     pause
end

measure_L= norm(NN + yy);

if nargin<15
    suppress=0;
end
if suppress~=1
    figure(1)
    plot(-vecnorm(yy'),vecnorm(NN'),'k',-vecnorm(yy'),vecnorm(yy'),'r--','Linewidth',2);%,-vecnorm(yy(1:end-1,:)'),-vecnorm(yy(2:end,:)'),'b*-')
%     axis([-2,2,-2,2])
%     axis square
    xlabel('Path')
    ylabel('Norm of NR')
    pause(0)
    
%     figure(2)
%     plot(1:ly,yy)
%     xlabel('x')
%     ylabel('\gamma')
%     pause
end
end