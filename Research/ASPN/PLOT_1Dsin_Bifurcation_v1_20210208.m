%% Experiment 14 - plotting the bifurcation diagram for u''-sin(au)=0
% Having found a period doubling example, we plot out its bifurcation
% diagram. We may change the overlap and transmission condition to see how
% this bifurcation changes.

%% Bifurcation diagram

% Grid
[x1,x2,J1BC,J2BC,D1,D2] = SUB_Init2ndDirichlet_v1_20201116(1001,0.4);

% Plotting bifurcation region
strpt = 3.52;
% endpt = 3.75;
endpt = 3.82;
res   = 0.0001;
% strpt = 9.10;
% endpt = 9.155;
% res   = 0.001;
start=20; N=64;

ind = round((endpt - strpt) / res);
gam = zeros(ind,N);
gam_neg = zeros(size(gam));
nonlinsolves = 10;

for k = 1:ind
    C = strpt + k*res;
%     u2b= 1.6;
    u2b= 1.65;
    for iter = 1:(start+N)
        [~,~,u2b]=SUB_1Dsin_iterate_v1_20201116(x1,x2,C,J1BC,J2BC,D1,D2,u2b,nonlinsolves);
        if iter > start
            gam(k,iter-start) = u2b;
        end
    end
end

for k = 1:ind
    C = strpt + k*res;
%     u2b= -1.6;
    u2b= -1.65;
    for iter = 1:(start+N)
        [~,~,u2b]=SUB_1Dsin_iterate_v1_20201116(x1,x2,C,J1BC,J2BC,D1,D2,u2b,nonlinsolves);
        if iter > start
            gam_neg(k,iter-start) = u2b;
        end
    end
end

%%
figure(1)
aa = strpt + (1:ind)*res;
subplot(2,1,1)
plot(aa,gam,'k.',aa,gam_neg,'r.')
axis([strpt,endpt,1.35,1.75])
% axis([strpt,endpt,1.4,1.5])
xlabel('\mu')
ylabel('\gamma')
set(gca,'fontsize',26,'linewidth',2)

subplot(2,1,2)
plot(aa,gam,'k.',aa,gam_neg,'r.')
axis([strpt,endpt,-1.75,-1.35])
% axis([strpt,endpt,-1.5,-1.4])
xlabel('\mu')
ylabel('\gamma')
set(gca,'fontsize',26,'linewidth',2)

%%
figure(2)
aa = strpt + (1:ind)*res;
plot(aa,gam,'k.',aa,gam_neg,'r.')
axis([strpt,endpt,1.3,1.75])
xticks(strpt + (0:300:ind)*res)
xlabel('C')
ylabel('\gamma')
legend('+1.65','-1.65')
set(gca,'fontsize',20,'linewidth',2)