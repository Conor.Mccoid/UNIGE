% OptSin example, using a quasi-Newton method

% The objective function produces a norm which we want to minimize, ideally
% set to 0. We therefore employ nonlinear minimization techniques.

n_disc=100; % size of discretization, must be divisible by 2
            % half this is the discretization in Fourier space
[x1,x2,J1BC,J2BC,D21,D22]=SUB_Init2ndDirichlet_v1_20201116(n_disc+1,0.4);
nonlinsolves=10;

% N = n_disc/2;
coeffs=zeros(5,1);
N = length(coeffs);
x=unique([x1,x2]);
alpha=pi;
f=@(x) -sin(alpha*x); fp=@(x) -alpha*cos(alpha*x);
for k=1:length(coeffs)
    coeffs(k) = f(x') * sin(pi*k*x) * (x(2)-x(1));
end
f_test=@(coeffs) sin(pi*x*(1:N)) * coeffs;

f_obj=@(coeffs) SUB_optsin_FunctionalFD_v1_20220416(coeffs,x1,x2,D21,D22,J1BC,J2BC,nonlinsolves);
% X = zeros(N,2); X(:,1)=coeffs; X(1,2)=0.5;
% F = [ f_obj(X(:,1)), f_obj(X(:,2)) ];
% iter=2;
X = zeros(N,N+1); % list_x(:,1)=coeffs_end;
F = zeros(N,N+1); F(:,1) = f_obj(X(:,1));
for k=2:N+1
    X(k-1,k) = -0.9^(k-1);
    F(:,k) = f_obj(X(:,k));
end
iter=N+1;
norms_F=vecnorm(F);
[norms_F,ind]=sort(norms_F);
F=F(:,ind); X=X(:,ind);
while norm(F(:,end))>1e-3 && iter < 100+N
    [~,k] = size(F);
%     B = F(:,2:end)-F(:,1:end-1);
    B = eye(N);
    u = [ ones(1,k) ; B'*F ] \ [1 ; zeros(k-1,1) ];
    
    X_new = X*u;
    if X_new(1)>0
        X_new=-X_new;
    end
    
    F_new = f_obj(X_new);
    norm_new = norm(F_new);
    ind=1;
    while ind<=N && norm_new<norms_F(ind+1)
        ind=ind+1;
    end
    
    X = [ X(:,1:ind), X_new, X(:,ind+1:end) ];
    F = [ F(:,1:ind), F_new, F(:,ind+1:end) ];
    norms_F = [norms_F(1:ind), norm_new, norms_F(ind+1:end)];
    iter=iter+1;
    if k>N
        X=X(:,2:end);
        F=F(:,2:end);
        norms_F=norms_F(2:end);
    end
    
    figure(2)
    plot(x,f_test(X_new),'r',x,f(x),'b')
    pause(0)
    
end
coeffs_end=X(:,end);

%%
figure(2)
subplot(1,2,1)
plot(x,f_test(X),'r',x, f(x),'b')
xlabel('x')
ylabel('f(x)')
legend('Fourier series','f(x)')
subplot(1,2,2)
plot(x,(pi*(1:length(coeffs_end)).*cos(pi*x*(1:length(coeffs_end)))) * coeffs_end,'r',x,fp(x),'b')
xlabel('x')
ylabel('f''(x)')
legend('Fourier series','f''(x)')