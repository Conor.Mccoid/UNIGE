%===PLOT_1Dsin_ParamOverlap_v1_20201113===%
% Graph of basin size vs size of the overlap
%   As the parameter and overlap changes so too does the size of the basin.
%   While it is challenging to present a 3D visualization of the basin size
%   as a function of two parameters (though in writing this a contour plot
%   would make sense), one can present the size of the basin in both
%   parameter space and initial condition space as a function of overlap.

overlap=0.2:0.01:0.4;
n=length(overlap);
Abasin=zeros(1,n); ybasin=Abasin;
a=2.2; y0=-2.5; y=y0; ymax=-1;
yy=-0.5:0.05:0.5;

for i=1:n
    [x1,x2,J1BC,J2BC,D21,D22]=SUB_Init2ndDirichlet_v1_20201116(1001,overlap(i));
    while Abasin(i)==0
        [~,~,NR]=SUB_1Dsin_iterate_v1_20201116(x1,x2,a,J1BC,J2BC,D21,D22,y,10);
        if NR>=-y
            y0=y; ymax=y0+0.1;
            param=a; basin=1;
            while ~isempty(basin)
                basin=abs(basin(end)-basin(1)); ybasin(i)=max(ybasin(i),basin);
                N=EX_1Dsin_ASPN_v1_20201116(overlap(i),param,y+yy,20,0);
                param=param+0.01;
                basin=yy(abs(N)>1e-1);
            end
            Abasin(i)=param-a;
        end
        y=y+0.01;
        if y>ymax
            y=y0;
            a=a+0.01;
        end
    end
end

figure(1)
plot(overlap,ybasin,'bs',overlap,Abasin,'ks')
xlabel('Overlap')
ylabel('Basin of cycling')
legend('Basin of initial conditions','Basin of parameter')