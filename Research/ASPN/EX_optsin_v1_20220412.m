% example for finding the optimal sinusoidal (antisymmetric) nonlinearity for maximum chaos, using Nelder-Mead

n_disc=100; % size of discretization, must be divisible by 2
            % half this is the discretization in Fourier space
[x1,x2,J1BC,J2BC,D21,D22]=SUB_Init2ndDirichlet_v1_20201116(n_disc+1,0.4);
nonlinsolves=10;
f_obj = @(coeffs) SUB_optsin_Functional_v1_20220412(coeffs,x1,x2,D21,D22,J1BC,J2BC,nonlinsolves);

% N = n_disc/2;
coeffs=zeros(10,1);
N = length(coeffs);
x=unique([x1,x2]);
alpha=pi;
f=@(x) -sin(alpha*x);
fp=@(x) -alpha*cos(alpha*x);
% f=@(x) sin(pi*x*(1:N)) * coeffs_end;
% fp=@(x) pi*(1:N).*cos(pi*x*(1:N)) * coeffs_end;
for k=1:N
    coeffs(k) = f(x)' * sin(pi*k*x) * (x(2)-x(1));
end

list_x = zeros(N,N+1); % list_x(:,1)=coeffs_end;
list_f = zeros(N+1,1); list_f(1) = f_obj(list_x(:,1));
for k=2:N+1
    list_x(k-1,k) = -0.5^(k-1);
    list_f(k) = f_obj(list_x(:,k));
end

%%
[coeffs_end,~] = ALGO_NelderMead_v1_20220416(f_obj,list_x,list_f);
[f_end,NN] = SUB_optsin_Functional_v1_20220412(coeffs_end,x1,x2,D21,D22,J1BC,J2BC,nonlinsolves);

% function returned is smooth and looks reasonable, but the NR iteration
% is a mess of points
% should try either adding more points to final product or adding
% smoothness as a constraint in the optimization

% update: changed objective function so it tries to match NR to -gamma ->
% much better results, everything is smooth and we have some definitely
% stable cycles, albeit small

% should also try quasi-Newton optimization, using finite differences -
% simplex method requires about same amount of computation anyway

% I found good results when using only the first 10 coefficients

%%
figure(2)
subplot(1,2,1)
plot(x,sin(pi*x*(1:length(coeffs_end))) * coeffs_end,'r',x, f(x),'b')
xlabel('x')
ylabel('f(x)')
legend('Fourier series','f(x)')
subplot(1,2,2)
plot(x,(pi*(1:length(coeffs_end)).*cos(pi*x*(1:length(coeffs_end)))) * coeffs_end,'r',x,fp(x),'b')
xlabel('x')
ylabel('f''(x)')
legend('Fourier series','f''(x)')

%%
f=@(x) sin(pi*x*(1:N)) * coeffs_end;
fp=@(x) pi*(1:N).*cos(pi*x*(1:N)) * coeffs_end;
y=-1.22;
l1=length(x1); l2=length(x2);
a=x2(1); b=x1(end);
    u1=ones(l1,1);
    u2=ones(l2,1);
    
figure(3)
clf
hold on
for k=1:50

    for i=1:nonlinsolves
        F1=D21*u1 + f(u1);
        J1=D21    + spdiags(fp(u1),0,l1,l1);
        J1=[J1BC(1,:); J1(2:end-1,:); J1BC(2,:)];
        F1(1)=J1BC(1,:)*u1; F1(end)=J1BC(2,:)*u1-y;
        u1=u1 - J1\F1;
    end
    u1a=u1(x1==a);
    g1 =J1 \ [zeros(l1-1,1); 1];
    g1a=g1(x1==a);
    
    for i=1:nonlinsolves
        F2=D22*u2 + f(u2);
        J2=D22    + spdiags(fp(u2),0,l2,l2);
        J2=[J2BC(1,:); J2(2:end-1,:); J2BC(2,:)];
        F2(1)=J2BC(1,:)*u2 - u1a; F2(end)=J2BC(2,:)*u2;
        u2=u2 - J2\F2;
    end
    u2b=u2(x2==b);
    g2 =J2\[ g1a; zeros(l2-1,1)];
    g2b=g2(x2==b);
    
    NR=y - (u2b-y)/(g2b-1);
    plot([y,y,NR],[y,NR,NR],'r.-')
    y=NR;

end
hold off