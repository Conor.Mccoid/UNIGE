function [x1,x2,J1BC,J2BC,D21,D22] = SUB_Init2ndDirichlet_v1_20201116(nx,overlap)
% INIT2NDDIRICHLET generates components for 2nd order Dirichlet problems on
% the domain [-1,1]
%   [x1,x2,J1BC,J2BC,D21,D22] = Init2ndDirichlet returns two domains x1 and
%   x2, Dirichlet BCs J1BC and J2BC and 2nd order derivative matrices D21
%   and D22 for the domain [-1,1] with nx equidistant points and overlap
%   of size 'overlap'.

a=-overlap/2;
b=-a;
x=linspace(-1,1,nx)';
h=x(2)-x(1);
x1=x(x<=b); l1=length(x1);
x2=x(x>=a); l2=length(x2);

[~,D21]=MAT_FD_v1_20201113(l1,h);
[~,D22]=MAT_FD_v1_20201113(l2,h);
J1BC=sparse([1,2],[1,l1],[1,1],2,l1);
J2BC=sparse([1,2],[1,l2],[1,1],2,l2);
end