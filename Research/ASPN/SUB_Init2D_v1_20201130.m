function [x1,x2,y,DDX1,DDX2,DDY1,DDY2,indx1,indx2,indy1,indy2,intx1,intx2] = SUB_Init2D_v1_20201130(nx,ny,overlap)
% INIT2D provides primitives for 2nd order 2D Dirichlet ODEs
% Outputs:
%   x1, x2: two subdomains in the x-direction
%   y: domain in y-direction
%   DDX1: 2nd order differentiation matrix in x-direction for 1st subdomain
%   DDY1: same as DDX1, in y-direction
%   DDX2: same as DDX1, for 2nd subdomain
%   DDY2: same as DDY1, for 2nd subdomain
%   indx1: indices of those rows related to the x-boundary in 1st subdomain
%   indy1: same as indx1, for x-boundary
%   indx2: same as indx1, in 2nd subdomain
%   indy2: same as indy1, in 2nd subdomain

a=-overlap/2; b=-a;                         % boundaries of two domains
x=linspace(-1,1,nx)'; y=linspace(-1,1,ny)'; % x and y coordinates
hx=x(2)-x(1); hy=y(2)-y(1);
x1=x(x<=b); b=x1(end);                      % leftmost domain
x2=x(x>=a); a=x2(1);                        % rightmost domain
l1=length(x1); l2=length(x2);               % domain sizes

[~,ddx1]=MAT_FD_v1_20201113(l1,hx);         % 2nd order differentiation
[~,ddx2]=MAT_FD_v1_20201113(l2,hx);         % matrices (finite differences)
[~,ddy] =MAT_FD_v1_20201113(ny,hy);         % for both domains, in both directions

Ix1=speye(l1); Ix2=speye(l2); Iy=speye(ny); % identity matrices in x and y
DDX1=kron(Iy,ddx1);                         % 2D 2nd order differentiation
DDX2=kron(Iy,ddx2);                         % matrices for both domains,
DDY1=kron(ddy,Ix1);                         % in both directions
DDY2=kron(ddy,Ix2);
indx1=kron([0;ones(ny-2,1);0],[1;zeros(l1-2,1);1])==1;   % index of x-boundary in domain 1
indx2=kron([0;ones(ny-2,1);0],[1;zeros(l2-2,1);1])==1;   % "" in domain 2
indy1=kron([1;zeros(ny-2,1);1],ones(l1,1))==1;   % index of y-boundary ""
indy2=kron([1;zeros(ny-2,1);1],ones(l2,1))==1;   % "" in domain 2
% NB: for the four corners the BCs in y are used and not the BCs in x

intx1=kron([0;ones(ny-2,1);0],x1==a)==1;    % interface in domain 1
intx2=kron([0;ones(ny-2,1);0],x2==b)==1;    % interface in domain 2

DDX1(indy1,:)=0; DDX2(indy2,:)=0;
DDY1(indx1,:)=0; DDY2(indx2,:)=0;
II1=kron(Iy,Ix1); II2=kron(Iy,Ix2);
DDX1(indx1,:)=II1(indx1,:);
DDX2(indx2,:)=II2(indx2,:);
DDY1(indy1,:)=II1(indy1,:);
DDY2(indy2,:)=II2(indy2,:);

end