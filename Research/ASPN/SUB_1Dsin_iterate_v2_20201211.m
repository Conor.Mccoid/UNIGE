function [ux,uy]=SUB_1Dsin_iterate_v2_20201211(x1,A,J1BC,D21,u1)
% 1DSIN_ITERATE performs one iteration of ASPN for the 1Dsin example
%   [G,Gp,N] = 1Dsin_iterate(x1,x2,A,J1BC,J2BC,D21,D22,y) computes the
%   fixed point function of AltS, its derivative, and the Newton-Raphson
%   acceleration for the 1Dsin example with domains x1 and x2, parameter A,
%   BCs defined in J1BC and J2BC, and 2nd derivative matrices D21 and D22,
%   with initial value y. The number of nonlinear solves used is given in
%   'nonlinsolves'.

l1=length(x1);

F1=D21*u1 - sin(A*u1);
J1=D21    - spdiags(A*cos(A*u1),0,l1,l1);
J1=[J1BC(1,:); J1(2:end-1,:); J1BC(2,:)];
F1(1)=J1BC(1,:)*u1; F1(end)=J1BC(2,:)*u1;
NR=-J1 \ F1;
ux=NR(2); uy=NR(3);
end