function B = ContactStructure(N)
b = rand(N*(N-1)/2,1);
B = zeros(N,N);
for k=1:N
    B(k:end,k) = [1;b(1:N-k)];
    B(k,(k+1):end) = B((k+1):end,k)';
    b = b(N-k+1:end);
end