function s = ProbIP(v,w)
[n,m] = size(v);
[mc,nc] = size(w);
if n>1 || nc>1
    disp('First input must be a row vector, second input must be a column vector')
    return
end
if m~=mc
    disp('Input vectors must have same length')
    return
end

s = v.*w';
while m>1
    r = zeros(1,ceil(m/2));
    for i=1:floor(m/2)
        r(i) = ProbSum(s(2*i-1),s(2*i));
    end
    if mod(m,2)
        r(end) = s(end);
    end
    s = r; m = ceil(m/2);
end
end

function s = ProbSum(p,q)
s = p + q - p*q;
end