%% Vertices for graph - represents population, distance related to probability of contact
N = 100;             % number of people
V = N*rand(N,2);    % relative locations

figure(1)
plot(V(:,1),V(:,2),'bo')

%% Contact structure matrix - indicates maximum probability of transmission (should improve this to be more valid)
C = zeros(N);
for i=1:N
    for j=1:N
        C(i,j) = norm(V(i,:)-V(j,:));
    end
end
F = @(d) normpdf(d,0,sqrt(N)/2)/normpdf(0,0,sqrt(N)/2);
C = F(C);

figure(1)
G = graph(C.*(C>1e-2),'omitselfloops');
plot(G,'XData',V(:,1),'YData',V(:,2),'Linewidth',10*G.Edges.Weight,'Linestyle','-')

%% Propagation of disease and probability
T = 20;             % number of days
P = zeros(N,1); P(1) = 1; % initial infection
Q = P;              % initial probabilities
for t=1:T
    B = C.*ContactStructure(N);  % contact structure for day t
    A = rand(N)<B;    % transmissions for the day
    At= A*diag(P);
    P = ProbMat(A,P);
    Q = ProbMat(B,Q); % propagate probabilities
    
    figure(1)
    subplot(1,2,1)
    plot(digraph(At','omitselfloops'),'XData',V(:,1),'YData',V(:,2))
    hold on
    scatter(V(P==1,1),V(P==1,2),'ro')
    hold off
    title(['Infection at t=',num2str(t)])
    
    subplot(1,2,2)
    G = graph(B.*(B>1e-2),'omitselfloops');
    plot(G,'XData',V(:,1),'YData',V(:,2),'Linewidth',10*G.Edges.Weight)
    hold on
    scatter(V(:,1),V(:,2),100*Q + eps,'mo')
    hold off
    title(['Probabilities at t=',num2str(t)])
    pause
end