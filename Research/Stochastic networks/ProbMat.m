function v = ProbMat(A,u)
[n,m] = size(A);
[nu,mu] = size(u);
if m~=nu
    disp('Dimension mismatch between matrix and vector')
    return
end

v = zeros(n,mu);
for i = 1:n % should alter to take advantage of sparsity in A
    for j = 1:mu
        v(i,j) = ProbIP(A(i,:),u(:,j));
    end
end