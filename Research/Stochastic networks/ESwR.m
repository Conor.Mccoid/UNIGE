function ESwR(N,T)
% ESWR produces a random epidemic spreading through a stochastic newtork
%   ESwR(N,T) creates a set of N nodes and a set of possible edges based on
%   the position of the nodes. The first node (index 1) begins with the
%   virus. The infection then spreads randomly for T time frames.

V = N*rand(N,2); n = N*(N-1)/2;
E = zeros(n,3); k=0;
for i = 1:N-1
    for j = (i+1):N
        k=k+1;
        E(k,:) = [i,j,norm(V(i,:) - V(j,:))];
    end
end
E = E(E(:,3)<2*sqrt(N),1:2);

P = [1,1,1];
PT= sparse(1,1,1,N,1);

figure(1)
G = graph(sparse(E(:,1),E(:,2),1,N,N),'upper');
plot(G,'Xdata',V(:,1),'Ydata',V(:,2))

for t=1:T
    [B,BT] = contact(E);
    BT= [BT;BT(:,2),BT(:,1),BT(:,3)];
    BT= sparse(BT(:,1),BT(:,2),BT(:,3),N,N);
    P = update(B,P);
    PT= ProbMat(BT+speye(N),PT);
    Q = prob(P);
    
    figure(1)
    G = graph(sparse(B(:,1),B(:,2),B(:,3),N,N),'upper');
    plot(G,'Xdata',V(:,1),'Ydata',V(:,2),'Linewidth',10*G.Edges.Weight)
    hold on
    scatter(V(Q(:,1),1),V(Q(:,1),2),100*Q(:,2)+eps,'mo')
    hold off
    title(['Probabilities at time t=', num2str(t)])
    
    figure(2)
    G = graph(BT);
    plot(G,'Xdata',V(:,1),'Ydata',V(:,2))
    hold on
    scatter(V(PT==1,1),V(PT==1,2),'ro')
    hold off
    title(['Infection at time t=', num2str(t)])
    
    i = input('Node to test:');
    P = testing(Q,P,PT,i);
    Q = prob(P);
    
%     figure(3)
%     G = graph(sparse(B(:,1),B(:,2),B(:,3),N,N),'upper');
%     plot(G,'Xdata',V(:,1),'Ydata',V(:,2),'Linewidth',10*G.Edges.Weight)
%     hold on
%     scatter(V(Q(:,1),1),V(Q(:,1),2),100*Q(:,2)+eps,'mo')
%     hold off
%     title(['Probabilities at time t=', num2str(t), ' after testing node ', num2str(i)])
end
end

function Q = update(B,P)
% ESWR_UPDATE calculates the spread of the probabilities P through the
% contact structure B
%   Q = ESwR_update(B,P) takes the sparse symmetric matrix B and the list
%   of triples P (node index, probabilities and path index) to
%   calculate the next list of triples Q. The path index is unordered so
%   that paths depend only on the nodes they traverse, not the order.

Q = [P, P(:,1)];
while ~isempty(P)
    i = P(1,1); p = P(1,2); A = P(1,3:end); P = P(2:end,:);
    Bb= [B(B(:,1)==i,2:3); B(B(:,2)==i,[1,3])];
    while ~isempty(Bb)
        j = Bb(1,1); b = Bb(1,2); Bb = Bb(2:end,:);
        if isempty(find(A==j,1))
            Q = [ Q ; j, b*p, A, j ];
        end
    end
end
end

function I = indexA(Q,j,A)
I = find(Q(:,1)==j); Q = Q(I,3:end);
while ~isempty(Q)
    i = Q(:,1)==A(1);
    I = I(i); Q = Q(i,2:end); A = A(2:end);
end
end

function Q = prob(P)
Q=[];
while ~isempty(P)
    i = P(1,1); p = P(P(:,1)==i,2); P = P(P(:,1)~=i,:);
    Q = [Q ; i, ProbIP(ones(size(p')),p)];
end
end

function [B,BT] = contact(E)
% CONTACT returns a list of random edges for a given network
%   B = contact(E) takes a list of edges and returns a probability for
%   randomly selected edges in B.

N = length(E); n = round(0.1*N);
B = [E(randsample(N,n),:),rand(n,1)];
BT= rand(n,1); BT= [B(:,1:2), B(:,3)>BT]; BT=BT(BT(:,3)==1,:);
end

function P = testing(Q,P,PT,i)

PB=P(:,2);
Pflag= 1:length(PB);
pi= Q(Q(:,1)==i,2);
pA= P(P(:,1)==i,2);
bundle = P(P(:,1)==i,3:end); [K,L]=size(bundle);

% node i along path A
if PT(i)==1
    PB(P(:,1)==i) = PB(P(:,1)==i)/pi;
elseif PT(i)==0
    PB(P(:,1)==i) = 0;
end
Pflag(P(:,1)==i) = [];

% nodes k and l along paths C and D
for node=(L-1):-1:1
    paths=1:K;
    while ~isempty(paths)
        C = bundle(paths(1),:); C((node+1):end) = C(node);
        pkc = P(indexA(P,C(end),C),2);
        
        % paths A containing C
        index = indexB(bundle,C(1:node)); 
        paths = paths(~ismember(paths,index));
        logic=false(1,K); logic(index)=true;
        y = ProbIP(logic/pkc + ~logic,pA);
        
        % paths D containing C
        index = indexB(P(Pflag,3:end),C(1:node));
        if PT(i)==1
            PB(Pflag(index)) = PB(Pflag(index))*y/pi;
        elseif PT(i)==0
            PB(Pflag(index)) = PB(Pflag(index))*(1-y)/(1-pi);
        end
        Pflag(index)= []; % flag entries of P so as not to double update
    end
end
P(:,2) = PB;
P = P(P(:,2)~=0,:);
end

function I = indexB(P,A)
I = find(P(:,1)==A(1)); P=P(I,2:end); A=A(2:end);
while ~isempty(A)
    ind = P(:,1)==A(1);
    I = I(ind); P=P(ind,2:end); A=A(2:end);
end
end