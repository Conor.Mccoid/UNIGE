%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%     phd.cls : A class file for a Ph.D. thesis                           %
%               Universit� de Gen�ve, Facult� des Sciences                %
%               Section de math�matiques                                  %
%                                                                         %
%               Version 3.0, 20/06/2022, Conor McCoid
%               Version 2.0, 29/04/2008, Martin J. Gander                 %
%               (Version 1.0 by Julio Rodriguez in 2007)                  %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{phd}

% All options are passed to the book class.
%		I've changed article to book, which is more in keeping with international standards for a thesis

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{book}}
\ProcessOptions
\LoadClass{book}

% use standard graphics and math packages

\RequirePackage{amsmath,amssymb,amsthm}
\RequirePackage{psfrag}
\RequirePackage{graphicx}

% to make the numero symbol work, add textcomp package to get the \textdegree symbol
\RequirePackage{textcomp}

% modify page sizes and indentation

\oddsidemargin 0in \evensidemargin 0in
\topmargin -25pt \headheight 9pt \headsep 25pt 
\setlength{\textheight}{235mm}
\setlength{\textwidth}{155mm}

% provide standard mathematics environments

\newtheorem{theorem}{\bfseries Theorem}[chapter]
\newtheorem{lemma}[theorem]{\bfseries Lemma}
\newtheorem{corollary}[theorem]{\bfseries Corollary}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{definition}{\bfseries Definition}[chapter]
\newtheorem{algorithm}{\bfseries Algorithm}[chapter]
\newcommand{\QED}{\hspace*{\fill}\rule{2.5mm}{2.5mm}}
\renewenvironment{proof}{\noindent{\bfseries Proof\ }}{\QED\\}

\newtheorem{remark}{\bfseries  Remark}[chapter]
\newtheorem{problem}{\bfseries Problem}[chapter]
\newtheorem{example}{\bfseries Example}[chapter]

% Variables for the titlepage

\newcommand{\Professor}[1]{\gdef\@Professor{#1}}
\newcommand{\Place}[1]{\gdef\@Place{#1}}
\newcommand{\NumThesis}[1]{\gdef\@NumThesis{#1}}

\renewcommand{\maketitle}{
    \thispagestyle{empty}
    \setcounter{page}{0}
    \noindent\large UNIVERSIT\'E DE GEN\`EVE \hfill FACULT\'E DES SCIENCES \\
    Section de math\'ematiques \hfill {\@Professor }  \\
    \hrule
    \vfill
    \begin{center}
    {\large\bfseries\@title}
    \vfill

    {\large Ph.D. THESIS} \\
    \vskip 7mm

    presented to the Faculty of Science, University of Geneva \\
    for obtaning the degree of Doctor of Mathematics.  %distinction { \@distinct }.
    \vfill

    by \\
    \vskip0.2cm
    {\large\rm\ {\@author} } \\
    \vskip0.2cm
    from \\
    \vskip0.2cm
    { \@Place } \\
    \vfill

    {Ph.D. N\textdegree { \@NumThesis } }
    \vfill

    {\footnotesize
    Atelier d'impression ReproMail \\
    Geneva, \ \@date \\[-2mm]}
    \hrulefill

    \end{center}
    \newpage}


\@addtoreset{equation}{chapter}   % Makes \section reset 'equation' counter.
\def\theequation{\thechapter.\arabic{equation}}

\@addtoreset{theorem}{chapter}
\def\thetheorem{\thechapter.\@arabic\c@theorem}
\def\thelemma{\thechapter.\@arabic\c@theorem}
\def\thecorollary{\thecorollary.\@arabic\c@theorem}
\def\theproposition{\theproposition.\@arabic\c@theorem}

\@addtoreset{figure}{chapter}
\def\thefigure{\thechapter.\@arabic\c@figure}

\@addtoreset{table}{chapter}
\def\thetable{\thechapter.\@arabic\c@table}
