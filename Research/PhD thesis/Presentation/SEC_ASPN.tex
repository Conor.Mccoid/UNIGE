\section{Cycling in Schwarz methods accelerated by Newton's method}

\begin{frame}{Alternating Schwarz (AltS) for a 1D BVP}
\begin{equation*}
\begin{aligned}
(1) & \begin{cases} F(x,u_1,u_1',u_1'') = 0 \\ u_1(a) = A \\ u_1(\beta) = \gamma_n \end{cases} &
(2) & \begin{cases} F(x,u_2,u_2',u_2'') = 0 \\ u_2(\alpha) = u_1(\alpha) \\ u_2(b) = B \end{cases} \\
(3) & \gamma_{n+1} = u_2(\beta) = G(\gamma_n) & &
\end{aligned}
\end{equation*}
AltS can be thought of as a fixed point iteration, $\gamma_{n+1} = G(\gamma_n)$.
\end{frame}

\begin{frame}{Newton-Raphson Accelerated AltS / Nonlinear Preconditioning}
\begin{equation*}
\begin{aligned} \label{alg:ASPN}
(1) & \begin{cases} F(x,u_1,u_1',u_1'') = 0 \\ u_1(a) = A \\ u_1(\beta) = \gamma_n \end{cases} &
(2) & \begin{cases} F(x,u_2,u_2',u_2'') = 0 \\ u_2(\alpha) = u_1(\alpha) \\ u_2(b) = B \end{cases} \\
(3) & \begin{cases} J(u_1) \cdot (g_1,g_1',g_1'') = 0 \\ g_1(a) = 0 \\ g_1(\beta) = 1 \end{cases} &
(4) & \begin{cases} J(u_2) \cdot (g_2,g_2',g_2'') = 0 \\ g_2(\alpha) = 1 \\ g_2(b) = 0 \end{cases} \\
(5) & \gamma_{n+1} = \gamma_n - \frac{ u_2(\beta) - \gamma_n }{g_1(\alpha) g_2(\beta) - 1} & = & \gamma_n - \frac{G(\gamma_n) - \gamma_n}{G'(\gamma_n) - 1}
\end{aligned}
\end{equation*}
% Superficially this is ASPIN but with an exact Newton method
\end{frame}

%===Background===%

\begin{frame}{When does a fixed point iteration (FP) converge in 1D?}
\begin{columns}
\begin{column}{0.5\textwidth}
Convergence of the iteration $x_{n+1} = g(x_n)$ depends on which region $(x,g(x))$ lies.

\begin{description}
    \item[1:] Monotonic divergence
    \item[2:] Monotonic convergence
    \item[3:] Oscillatory convergence
    \item[4:] Oscillatory divergence
\end{description}
\end{column}
\begin{column}{0.5\textwidth}
    \begin{figure}
        \centering
        \begin{tikzpicture}[scale=0.6]
            \draw[black,thick] (-4,0) -- (4,0);
            \draw[black,thick] (0,-4) -- (0,4);
            \draw[black,dashed] (-4,-4) -- (4,4);
            \draw[black,dashed] (-4,4) -- (4,-4);
            
            \filldraw[black] (-1,-2) circle (3pt);
            \draw[red,->] (-1,-2) -- (-2,-2) -- (-2,-4);
            
            \filldraw[black] (-2,-1) circle (3pt);
            \draw[red,->] (-2,-1) -- (-1,-1) -- (-1,-0.5);
            
            \filldraw[black] (-2,1) circle (3pt);
            \draw[red,->] (-2,1) -- (1,1) -- (1,0.5);
            
            \filldraw[black] (-1,2) circle (3pt);
            \draw[red,->] (-1,2) -- (2,2) -- (2,4);
            
            \draw (-1,-3) node {1};
            \draw (-3,-1.5) node {2};
            \draw (-3,1.5) node {3};
            \draw (-1,3) node {4};
            \draw(1,3) node {1};
            \draw (3,1.5) node {2};
            \draw (3,-1.5) node {3};
            \draw (1,-3) node {4};
        \end{tikzpicture}
        \caption{Behaviour of FP; the origin is the fixed point of the function}
        \label{fig:FP}
    \end{figure}
\end{column}
\end{columns}
\end{frame}

\begin{frame}{When does Newton-Raphson (NR) converge in 1D?}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            For Newton-Raphson there are no regions.
            Instead, convergence at a given point is determined by where the slope points.
            These 'regions' correspond to those for FPI.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \centering
                \begin{tikzpicture}[scale=0.7]
                    \draw[black,thick] (-4,0) -- (4,0);
                    \draw[black,thick] (0,-2) -- (0,4);
                    \filldraw[black] (-2,2) circle (3pt);
                    \draw[red,->] (-4,2) -- (4,2);
                    \draw[red,->] (-2,2) -- (-2,0);
                    \draw[red,->] (-2,2) -- (0,0);
                    \draw[red,->] (-2,2) -- (2,0);
                    
                    \draw (-1.25,0.5) node {2};
                    \draw (-3,1) node {1};
                    \draw (0.25,0.5) node {3};
                    \draw (1,1.25) node {4};
                \end{tikzpicture}
                \caption{Regions of NR; the origin is the root of the function}
                \label{fig:NR1}
            \end{figure}
        \end{column}
    \end{columns}
    % Repeat the definitions of each region
\end{frame}

\newcommand{\faxis}{\clip (-4,-4) rectangle (4,4);
		\draw[black,thick,->] (-4,0) -- (4,0);
		\draw[black,thick,->] (0,-4) -- (0,4);}
\newcommand{\gaxis}{\clip (-4,-4) rectangle (4,4);
			\draw[black,thick,->] (-4,-4) -- (4,4);
			\draw[black,thick,->] (0,-4) -- (0,4);}
\begin{frame}
We can trace out the boundaries between these `regions'.
If a function is parallel to these boundaries then the Newton-Raphson method arrives at stable points.

\begin{figure}
\centering
	\begin{tikzpicture}
	\matrix[column sep=0.5cm, row sep=0.5cm, every cell/.style={scale=0.4}, ampersand replacement=\&]
	{
	\faxis
	\foreach \a in {0,1,2,3} {
		\foreach \s in {-1,1} {
			\draw[black,dashed] (\s*\a,-4) -- (\s*\a,4);
			}
		} \&
	\faxis
	\foreach \a in {0,0.5,1,2,4} {
		\foreach \s in {-1,1} {
			\draw[black,dashed,domain=-4:4,smooth,variable=\x] plot ({\x},{\s*\a*\x + \x});
			}
		} \&
		
%	\gaxis
%	\foreach \a in {1,-1,2,-2,0.5,-0.4,1.5,-1.5} {
%                    \draw[domain=-4:4, smooth, samples=200, variable=\x, black, dashed] plot ({\x},{\a * sqrt(abs(\x)) + \x});
%                    } &

	\faxis
           \foreach \a in {0,1,2,3} {
		\foreach \s in {-1,1} {
			\draw[black,dashed] (-4,\s*\a) -- (4,\s*\a);
			}
		} \\
	};
	\end{tikzpicture}
\end{figure}
\end{frame}

\begin{frame}
What about tracing the border between regions 3 and 4?

Let $x^*$ be a root of $f(x)$ and let $f_C(x)$ trace the border between regions 3 and 4.
Then
    \begin{align*}
    	2 x^* - x & = x - \frac{f_C(x)}{f_C'(x)}, \\
    	\implies f_C'(x) & = - \frac{f_C(x)}{2 (x^* - x)}, \\
    	\implies f_C(x) & = C \sqrt{\abs{x - x^*}}
    \end{align*}
for any value of $C \in \bbr$.
\end{frame}

\begin{frame}{When does NR converge in 1D?}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            A function must never be parallel to these lines for NR to converge.
            Any time it is parallel, a cycle exists.
            
            ~
            
            NR applied to a fixed point iteration has a modified set of lines that it can't run parallel to.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \centering
                \begin{tikzpicture}[scale=0.6]
			\matrix[column sep=0.5cm, row sep=0.5cm, every cell/.style={scale=0.36}, ampersand replacement=\&]{
                \clip (-4,-4) rectangle (4,4);
                    \draw[black,thick,->] (-4,0) -- (4,0) node[above left] {$x$};
                    \draw[black,thick,->] (0,-4) -- (0,4) node[below right] {$f(x)$};
                    \foreach \a in {1,-1,2,-2,0.5,-0.4,1.5,-1.5} {
                    \draw[domain=-4:4, smooth, samples=200, variable=\x, black, dashed] plot ({\x},{\a * sqrt(abs(\x))});
                    }
                    \draw[domain=-4:4,samples=200,variable=\x,red,thick] plot({\x},{-rad(atan(\x))-\x});
                    \\
			\clip (-4,-4) rectangle (4,4);
                    \draw[black,thick,->] (-4,-4) -- (4,4) node[below left] {$y=x$};
                    \draw[black,thick,->] (0,-4) -- (0,4) node[below left] {$g(x)$};
                    \foreach \a in {1,-1,2,-2,0.5,-0.4,1.5,-1.5} {
                    \draw[domain=-4:4, smooth, samples=200, variable=\x, black, dashed] plot ({\x},{\a * sqrt(abs(\x)) + \x});
                    }
                    \draw[domain=-4:4, smooth, samples=200, variable=\x, red, thick] plot ({\x},{-rad(atan(\x))});
                    \\
                 };
                \end{tikzpicture}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{When does AltS converge in 1D?}
Conditions on the problem are known to guarantee convergence of AltS, and that this convergence is monotonic \footnote[frame]{\cite{lui1999schwarz}}.
This puts the fixed point iteration in region 2.
\\~\\
NR accelerated AltS doesn't have such conditions.
Instead, it has conditions on the FPI.
\begin{table}
\centering
    \begin{tabular}{c|c|c}
        $G(\gamma)$ lies in & Necessary condition & Sufficient condition \\ \hline
        1 & $G'(\gamma) >1$ \\
        2 & $G'(\gamma) < 1$ & $G'(\gamma) < 1/2$ \\
        3 & $G'(\gamma) < 1/2$ & $G'(\gamma) < 0$ \\
        4 & $G'(\gamma) < 0$
    \end{tabular}
\end{table}
\end{frame}

% 1D algorithm, infallible
\begin{frame}{Possible algorithm}
	Combining this knowledge we can write an algorithm with guaranteed convergence, if $G(\gamma)$ lies in FP region 2.
    \begin{enumerate}
        \item Select $\gamma_0 \in \bbr$ and set $n=0$.
        \item Calculate $G(\gamma_n)$ and $G'(\gamma_n)$.
        If $G'(\gamma_n)=1$ set $\gamma_{n+1} = G(\gamma_n)$, increment $n$ and repeat this step.
        If not, proceed to the next step.
        \item Calculate the Newton iteration for $G(\gamma_n)$ (using the Davidenko-Branin trick), denoted $\tilde{\gamma}_n$.
        If $\abs{G'(\gamma_n) - 1} \leq 1/2$ then set $\gamma_{n+1} = \tilde{\gamma}_n$, increment $n$ and return to step 2.
        If not, calculate the average of $\gamma_n$ and $\tilde{\gamma}_n$, denoted $\hat{\gamma}_n$, and proceed to the next step.
        \item Calculate $G(\hat{\gamma}_n)$.
        If $G(\hat{\gamma}_n) - \hat{\gamma}_n$ has the same sign as $G(\gamma_n) - \gamma_n$ then set $\gamma_{n+1} = \tilde{\gamma}_n$.
        If not, set $\gamma_{n+1} = G(\gamma_n)$.
        In either case, increment $n$ and return to step 2.
    \end{enumerate}
\end{frame}
% rewrite as decision tree?

%===1D EXAMPLES===%

\begin{frame}{Example}
\begin{columns}
	\begin{column}{0.5\textwidth}
\begin{equation*}
\begin{cases} u''(x) - \sin(a u(x)) = 0 \\
	u(-1) = u(1) = 0 \end{cases}
\end{equation*}
The equation is nonsingular and admits only $u(x) = 0$.
\\~\\
Let $a=3.6$ with 0.4 overlap.
Figure to the right shows solution after several iterations as a function of initial condition.
\end{column}

\begin{column}{0.5\textwidth}
    \begin{figure}
        \centering
        		\includegraphics[height=0.7\textheight]{ASPN/MTLB_1Dsin_NewtonGamma_v1_20210208.eps}
        \label{fig:1D G}
    \end{figure}
\end{column}
\end{columns}
\end{frame}

\begin{frame}{Example}
	\begin{figure}
		\centering
		        \includegraphics[width=\textwidth]{ASPN/exp9_02.eps}
	\end{figure}
\end{frame}

\begin{frame}{Example}
    \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{ASPN/bifurcation_all.eps}
        \caption{Period doubling bifurcation in the example caused by NR acceleration.}
        \label{fig:1D bifurcation}
    \end{figure}
\end{frame}

\note{
AltS will converge for this example since $G(\gamma)$ lies entirely within region 2.

~

However, Newton-Raphson accelerated AltS won't converge for all initial conditions as it crosses the line between regions 3 and 4.
There will be a (small) domain where the method converges to a stable oscillation.
}

%===2D===%
% 2D background - NR equation (simplification impossible), 'monotonicity' of FP
\begin{frame}{2D}
The line between regions 3 and 4 in 2D is now defined by all possible rotations around the root.
\begin{equation*}
x_{n+1}-x^* = R (x_n - x^*), \quad R^\top R=I
\end{equation*}
If a function $f(x)$ satisfies
\begin{equation*}
f(x) = J_f(x) (I-R) (x - x^*)
\end{equation*}
at some point for any rotation matrix $R$ then that point lies on the boundary between regions 3 and 4 and therefore represents a cycle.
\end{frame}

\begin{frame}{2D cycling}
\begin{columns}
\begin{column}{0.5\textwidth}
Using the 1D example as a starting point, we can `optimize' the nonlinearity to find similar cycling and chaos in higher dimensions.

First, we make the 1D example as bad as possible.
This becomes an initial guess for the nonlinearity in 2D.
\end{column}

\begin{column}{0.5\textwidth}
\begin{figure}
\centering
	\adjincludegraphics[trim={{0.5\width} 0 0 {0.5\width}},clip,width=\textwidth]{../FIG_ASPN/MTLB_optsin2D_f_v1_20220705-eps-converted-to.pdf}
\end{figure}
\end{column}
\end{columns}
\end{frame}

\begin{frame}
\begin{figure}
\centering
	\includegraphics[trim={2.9cm 1.5cm 2.3cm 1.5cm},clip,height=\textheight]{../FIG_ASPN/MTLB_optsin2D_iterates_v1_20220727-eps-converted-to.pdf}
\end{figure}
\end{frame}