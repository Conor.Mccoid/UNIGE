\documentclass{beamer}

\usetheme{Berlin}
\usecolortheme{dolphin}
%\setbeamertemplate{footline}{}
\setbeamertemplate{navigation symbols}{}

\usepackage{preamble}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{setspace}
\usepackage{animate}
\usepackage{tikz}
\usetikzlibrary{calc,intersections,positioning}

\usepackage{booktabs} % Allows the use of \toprule, \midrule and \bottomrule in tables
\usepackage[backend=bibtex]{biblatex}
\ExecuteBibliographyOptions{doi=false, eprint=false, url=false}
\bibliography{references.bib}

\renewcommand{\insertnavigation}[1]{} % Remove navigation header to sections

\usepackage{tkz-graph}
\GraphInit[vstyle = Shade]
\tikzset{
  LabelStyle/.style = { rectangle, rounded corners, draw,
                        minimum width = 2em, fill = yellow!50,
                        text = red, font = \bfseries },
  VertexStyle/.append style = { inner sep=5pt,
                                font = \normalsize\bfseries},
  EdgeStyle/.append style = {->, bend left} }
\definecolor{processblue}{cmyk}{0.96,0,0,0}

\usepackage{pgfpages}
\setbeameroption{hide notes}

\title[Cycles in ASPN]{Cycles in Newton-Raphson-accelerated Alternating Schwarz}
\author{Conor McCoid, Martin J. Gander}
\institute[UNIGE]{University of Geneva}
\date{10.12.20}

\begin{document}

\frame{\titlepage}

\begin{frame}{Alternating Schwarz (AltS) for a 1D BVP}
\begin{equation}
\begin{aligned}
(1) & \begin{cases} F(x,u_1,u_1',u_1'') = 0 \\ u_1(a) = A \\ u_1(\beta) = \gamma_n \end{cases} &
(2) & \begin{cases} F(x,u_2,u_2',u_2'') = 0 \\ u_2(\alpha) = u_1(\alpha) \\ u_2(b) = B \end{cases} \\
(3) & \gamma_{n+1} = u_2(\beta) = G(\gamma_n) & &
\end{aligned}
\end{equation}
AltS can be thought of as a fixed point iteration, $\gamma_{n+1} = G(\gamma_n)$.
\end{frame}

\begin{frame}{Newton-Raphson Accelerated AltS / Nonlinear Preconditioning}
\begin{equation}
\begin{aligned} \label{alg:ASPN}
(1) & \begin{cases} F(x,u_1,u_1',u_1'') = 0 \\ u_1(a) = A \\ u_1(\beta) = \gamma_n \end{cases} &
(2) & \begin{cases} F(x,u_2,u_2',u_2'') = 0 \\ u_2(\alpha) = u_1(\alpha) \\ u_2(b) = B \end{cases} \\
(3) & \begin{cases} J(u_1) \cdot (g_1,g_1',g_1'') = 0 \\ g_1(a) = 0 \\ g_1(\beta) = 1 \end{cases} &
(4) & \begin{cases} J(u_2) \cdot (g_2,g_2',g_2'') = 0 \\ g_2(\alpha) = 1 \\ g_2(b) = 0 \end{cases} \\
(5) & \gamma_{n+1} = \gamma_n - \frac{ u_2(\beta) - \gamma_n }{g_1(\alpha) g_2(\beta) - 1} & = & \gamma_n - \frac{G(\gamma_n) - \gamma_n}{G'(\gamma_n) - 1}
\end{aligned}
\end{equation}
% Superficially this is ASPIN but with an exact Newton method
\end{frame}

\begin{frame}{Example}
    Consider the following second order nonlinear differential equation with homogeneous Dirichlet boundary conditions on the domain (-1,1):
    \begin{equation*}
        u''(x) - \sin(a u(x)) = 0.
    \end{equation*}
    This equation is nonsingular and admits only the trivial solution $u(x) = 0$.
\end{frame}

\begin{frame}{Example}
	\begin{figure}
		\centering
		\includegraphics[height=0.7\textheight]{MTLB_1Dsin_NewtonGamma_v1_20210208.eps}
		\caption{Newton-Raphson accelerated AltS on 1D sine example, $a=3.6$ with overlap 0.4.
		Also plotted is $y=x$ and $y=-x$.}
	\end{figure}
\end{frame}

\begin{frame}{Example}
    \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{bifurcation_all.eps}
        \caption{Period doubling bifurcation in the example caused by NR acceleration.}
        \label{fig:1D bifurcation}
    \end{figure}
\end{frame}

%===Background===%

\begin{frame}{When does a fixed point iteration (FP) converge in 1D?}
\begin{columns}
\begin{column}{0.5\textwidth}
Convergence of the iteration $x_{n+1} = g(x_n)$ depends on which region $(x,g(x))$ lies.

\begin{description}
    \item[1:] Monotonic divergence
    \item[2:] Monotonic convergence
    \item[3:] Oscillatory convergence
    \item[4:] Oscillatory divergence
\end{description}
\end{column}
\begin{column}{0.5\textwidth}
    \begin{figure}
        \centering
        \begin{tikzpicture}[scale=0.6]
            \draw[black,thick] (-4,0) -- (4,0);
            \draw[black,thick] (0,-4) -- (0,4);
            \draw[black,dashed] (-4,-4) -- (4,4);
            \draw[black,dashed] (-4,4) -- (4,-4);
            
            \filldraw[black] (-1,-2) circle (3pt);
            \draw[red,->] (-1,-2) -- (-2,-2) -- (-2,-4);
            
            \filldraw[black] (-2,-1) circle (3pt);
            \draw[red,->] (-2,-1) -- (-1,-1) -- (-1,-0.5);
            
            \filldraw[black] (-2,1) circle (3pt);
            \draw[red,->] (-2,1) -- (1,1) -- (1,0.5);
            
            \filldraw[black] (-1,2) circle (3pt);
            \draw[red,->] (-1,2) -- (2,2) -- (2,4);
            
            \draw (-1,-3) node {1};
            \draw (-3,-1.5) node {2};
            \draw (-3,1.5) node {3};
            \draw (-1,3) node {4};
            \draw(1,3) node {1};
            \draw (3,1.5) node {2};
            \draw (3,-1.5) node {3};
            \draw (1,-3) node {4};
        \end{tikzpicture}
        \caption{Behaviour of FP; the origin is the fixed point of the function}
        \label{fig:FP}
    \end{figure}
\end{column}
\end{columns}
\end{frame}

\begin{frame}{When does Newton-Raphson (NR) converge in 1D?}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            For Newton-Raphson there are no regions.
            Instead, convergence at a given point is determined by where the slope points.
            These 'regions' correspond to those for FPI.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \centering
                \begin{tikzpicture}[scale=0.7]
                    \draw[black,thick] (-4,0) -- (4,0);
                    \draw[black,thick] (0,-2) -- (0,4);
                    \filldraw[black] (-2,2) circle (3pt);
                    \draw[red,->] (-4,2) -- (4,2);
                    \draw[red,->] (-2,2) -- (-2,0);
                    \draw[red,->] (-2,2) -- (0,0);
                    \draw[red,->] (-2,2) -- (2,0);
                    
                    \draw (-1.25,0.5) node {2};
                    \draw (-3,1) node {1};
                    \draw (0.25,0.5) node {3};
                    \draw (1,1.25) node {4};
                \end{tikzpicture}
                \caption{Regions of NR; the origin is the root of the function}
                \label{fig:NR1}
            \end{figure}
        \end{column}
    \end{columns}
    % Repeat the definitions of each region
\end{frame}

\begin{frame}
\begin{figure}
\centering
	\begin{tikzpicture}[scale=0.7]
	\draw[black,thick] (-4,0) -- (4,0);
	\draw[black,thick] (0,-4) -- (0,4);
	\foreach \a in {0,1,2,3} {
		\foreach \s in {-1,1} {
			\draw[black,dashed] (-4,\s*\a) -- (4,\s*\a);
			}
		}
	\end{tikzpicture}
	\caption{Tracing border between regions 1 and 4.}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}
\centering
	\begin{tikzpicture}[scale=0.7]
	\draw[black,thick] (-4,0) -- (4,0);
	\draw[black,thick] (0,-4) -- (0,4);
	\foreach \a in {0,1,2,3} {
		\foreach \s in {-1,1} {
			\draw[black,dashed] (\s*\a,-4) -- (\s*\a,4);
			}
		}
	\end{tikzpicture}
	\caption{Tracing border between regions 1 and 2.}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}
\centering
	\begin{tikzpicture}[scale=0.7]
	\clip (-4,-4) rectangle (4,4);
	\draw[black,thick] (-4,0) -- (4,0);
	\draw[black,thick] (0,-4) -- (0,4);
	\foreach \a in {0,0.5,1,2,4} {
		\foreach \s in {-1,1} {
			\draw[black,dashed,domain=-4:4,smooth,variable=\x] plot ({\x},{\s*\a*\x});
			}
		}
	\end{tikzpicture}
	\caption{Tracing border between regions 2 and 3.}
\end{figure}
\end{frame}

\begin{frame}
What about tracing the border between regions 3 and 4?

Let $x^*$ be a root of $f(x)$ and let $f_C(x)$ trace the border between regions 3 and 4.
Then
    \begin{align*}
    	2 x^* - x & = x - \frac{f_C(x)}{f_C'(x)}, \\
    	\implies f_C'(x) & = - \frac{f_C(x)}{2 (x^* - x)}, \\
    	\implies f_C(x) & = C \sqrt{\abs{x - x^*}}
    \end{align*}
for any value of $C \in \bbr$.
\end{frame}

\begin{frame}{When does NR converge in 1D?}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            We transform from Cartesian coordinates $(x,y)$ to $(x,C)$, where $C = y/\sqrt{\abs{x-x*}}$ (with $x*$ being the root of the function).
            A function must be monotonic in this geometry for NR to converge.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \centering
                \begin{tikzpicture}[scale=0.6]
                \clip (-4,-4) rectangle (4,4);
                    \draw[black,thick,->] (-4,0) -- (4,0) node[above left] {$x$};
                    \draw[black,thick,->] (0,-4) -- (0,4) node[below right] {$f(x)$};
                    \foreach \a in {1,-1,2,-2,0.5,-0.4,1.5,-1.5} {
                    \draw[domain=-4:4, smooth, samples=200, variable=\x, black, dashed] plot ({\x},{\a * sqrt(abs(\x))});
                    }
                    \draw[domain=-4:4,samples=200,variable=\x,red,thick] plot({\x},{-rad(atan(\x))-\x});
                \end{tikzpicture}
                \caption{Geometry of NR; the origin is the root of the function}
                \label{fig:NR2}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{NR accelerated FP in 1D}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            Figure \ref{fig:NRFP} is the same as figure \ref{fig:NR2} tilted so that the $x$--axis is set to the line $y=x$.
            A function $g(x)$ must now be monotonic in this geometry if NR on $g(x)-x$ is to converge.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \centering
                \begin{tikzpicture}[scale=0.6]
                \clip (-4,-4) rectangle (4,4);
                    \draw[black,thick,->] (-4,-4) -- (4,4) node[below left] {$y=x$};
                    \draw[black,thick,->] (0,-4) -- (0,4) node[below left] {$g(x)$};
                    \foreach \a in {1,-1,2,-2,0.5,-0.4,1.5,-1.5} {
                    \draw[domain=-4:4, smooth, samples=200, variable=\x, black, dashed] plot ({\x},{\a * sqrt(abs(\x)) + \x});
                    }
                    \draw[domain=-4:4, smooth, samples=200, variable=\x, red, thick] plot ({\x},{-rad(atan(\x))});
                    \end{tikzpicture}
                \caption{Geometry of NR of FP; the origin is the root of the function}
                \label{fig:NRFP}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{When does AltS converge in 1D?}
    In 1D, AltS takes information at a point on the interface and produces an update at that point.
    This can be thought of as a fixed point function, $G(\gamma)$.
    \\~\\
    AltS then converges if (and only if) $G(\gamma)$ lies in regions 2 or 3 for $\gamma$ sufficiently close to $\gamma*$ (the fixed point).
    There are also a number of conditions that are sufficient for AltS to converge from any initial $\gamma$ (for example, see \footfullcite{lui1999schwarz}).
    Under such conditions, $G(\gamma)$ is necessarily in regions 2 or 3 everywhere.
    
    \printbibliography
\end{frame}

\begin{frame}{When does NR accelerated AltS converge in 1D?}
    Applying NR to $G(\gamma)-\gamma$ gives an accelerated AltS.
    Like any NR method, we need the slope to take on certain values in certain regions.
    \\~\\
    \centering
    \begin{tabular}{c|c|c}
        $G(\gamma)$ lies in & Necessary condition & Sufficient condition \\ \hline
        1 & $G'(\gamma) >1$ \\
        2 & $G'(\gamma) < 1$ & $G'(\gamma) < 1/2$ \\
        3 & $G'(\gamma) < 1/2$ & $G'(\gamma) < 0$ \\
        4 & $G'(\gamma) < 0$
    \end{tabular}
\end{frame}

% 1D monotonicity -> G in region 2 of FP, necessary and sufficient conditions for NR
\begin{frame}{Monotonicity of AltS in 1D}
\begin{lem}
    As long as the problem to be solved is nonsingular on all subdomains then $G(\gamma)$ is strictly monotonic.
\end{lem}
    Proof outline: If $G(\gamma_1) = G(\gamma_2)$ then $u_2(x)$ is the same for both $\gamma_1$ and $\gamma_2$.
    Therefore, $u_2(\alpha)$ is the same for both $\implies u_1(x)$ is the same for both $\implies u_1(\beta)$ is the same for both.
\end{frame}

%===1D EXAMPLES===%

% 1D examples of failure
\begin{frame}{1D example}
   Recall the 1D example with Dirichlet boundary conditions on (-1,1):
    \begin{equation*}
        u''(x) - \sin(a u(x)) = 0.
    \end{equation*}
    This equation is nonsingular and admits only the trivial solution $u(x) = 0$.
\end{frame}

\begin{frame}{1D example}
    \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{exp9_02.eps}
        \caption{Left: $G(\gamma)$ and NR accelerated AltS; also plotted are the lines $y=x-x_0$ and $y=2x_0 - x$.
        Right: $G(\gamma)$ with the geometry of figure \ref{fig:NRFP}.}
        \label{fig:1D G}
    \end{figure}
\end{frame}

\begin{frame}
AltS will converge for this example since $G(\gamma)$ lies entirely within region 2.

~

However, Newton-Raphson accelerated AltS won't converge for all initial conditions as it crosses the line between regions 3 and 4.
There will be a (small) domain where the method converges to a stable oscillation.
\end{frame}

\begin{frame}{1D example}
    \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{bifurcation_all.eps}
        \caption{Period doubling bifurcation in the example caused by NR acceleration.}
        \label{fig:1D bifurcation}
    \end{figure}
\end{frame}

% other 1D examples?

% add G and NR and bifurcation graphs for changing overlap and Robin BCs
% plot the collection of these objects together
% try to make graph of the size of the basin of divergence as a function of overlap

\begin{frame}
\begin{columns}
\begin{column}{0.5\textwidth}
Changing overlap changes where cycling occurs in parameter space.
\end{column}
\begin{column}{0.5\textwidth}
    \begin{figure}
	\centering
	\includegraphics[width=\textwidth]{MTLB_1Dsin_ParamOverlap_v1_20210208.eps}
	\caption{Parameter at which cycling starts as a function of overlap.}
    \end{figure}
\end{column}
\end{columns}
\end{frame}

\begin{frame}
\begin{columns}
\begin{column}{0.5\textwidth}
As overlap increases, the basin of cycling grows in parameter space but the number of initial conditions that converge to it dwindles.
\end{column}
\begin{column}{0.5\textwidth}
	\begin{figure}
		\centering
		\includegraphics[width=\textwidth]{MTLB_1Dsin_BasinOverlap_v1_20210208.eps}
		\caption{Basin of cycling as a function of overlap.
		The basin is in two dimensions: parameter and initial condition.}
	\end{figure}
\end{column}
\end{columns}
\end{frame}

% 1D algorithm, infallible
\begin{frame}{Possible algorithm}
    Suppose we know that $G(\gamma)$ lies in FP region 2.
    Then we can apply an algorithm guaranteed to converge.
    \begin{enumerate}
        \item Select $\gamma_0 \in \bbr$ and set $n=0$.
        \item Calculate $G(\gamma_n)$ and $G'(\gamma_n)$.
        If $G'(\gamma_n)=1$ set $\gamma_{n+1} = G(\gamma_n)$, increment $n$ and repeat this step.
        If not, proceed to the next step.
        \item Calculate the Newton iteration for $G(\gamma_n)$ (using the Davidenko-Branin trick), denoted $\tilde{\gamma}_n$.
        If $\abs{G'(\gamma_n) - 1} \leq 1/2$ then set $\gamma_{n+1} = \tilde{\gamma}_n$, increment $n$ and return to step 2.
        If not, calculate the average of $\gamma_n$ and $\tilde{\gamma}_n$, denoted $\hat{\gamma}_n$, and proceed to the next step.
        \item Calculate $G(\hat{\gamma}_n)$.
        If $G(\hat{\gamma}_n) - \hat{\gamma}_n$ has the same sign as $G(\gamma_n) - \gamma_n$ then set $\gamma_{n+1} = \tilde{\gamma}_n$.
        If not, set $\gamma_{n+1} = G(\gamma_n)$.
        In either case, increment $n$ and return to step 2.
    \end{enumerate}
\end{frame}
% rewrite as decision tree?

% 2D background - NR equation (simplification impossible), 'monotonicity' of FP
\begin{frame}{2D}
The line between regions 3 and 4 in 2D is now defined by all possible rotations around the root.
\begin{equation*}
x_{n+1}-x^* = R (x_n - x^*), \quad R^\top R=I
\end{equation*}
If a function $f(x)$ satisfies
\begin{equation*}
f(x) = J_f(x) (I-R) (x - x^*)
\end{equation*}
at some point for any rotation matrix $R$ then that point lies on the boundary between regions 3 and 4 and therefore represents a cycle.
\end{frame}

% 2D examples of failure
\begin{frame}{2D - damping in y--direction}
\begin{columns}
	\begin{column}{0.5\textwidth}
		\begin{equation*}
			u_{xx}(x,y) + \epsilon u_{yy}(x,y) - \sin(a u) = 0
		\end{equation*}
		If $\epsilon \to 0$ then we retrieve the 1D problem.
		If $\epsilon$ is sufficiently small, we see cycling again.
	\end{column}
	\begin{column}{0.5\textwidth}
		\begin{figure}
			\centering
			\includegraphics[width=\textwidth]{exp15_04.png}
			\caption{Bifurcation diagram for $\epsilon =1e-5$.}
		\end{figure}
	\end{column}
\end{columns}
\end{frame}

% 2D algorithm, equivalent of 1D but without infallibility

\begin{frame}{Future Work}
\begin{itemize}
\item Determine relation between operator parameters (overlap size, tangential diffusion, etc.) and basin of cycling
\item Find conditions under which such cycling is impossible
\item Improve algorithm for higher dimensions
\end{itemize}
\end{frame}

\end{document}