\section{Multisecant equations}

%===Specific commands for this talk===%
\newcommand{\rxi}[1]{\vec{r}(\vec{x}_{#1})}
\newcommand{\fxi}[1]{\vec{f}(\vec{x}_{#1})}
\newcommand{\Jxn}{J(\vec{x}_n)}
%===%

\begin{frame}
\begin{columns}
	\begin{column}{0.5\textwidth}
		\textbf{Linear (Krylov)}
		\\~\\
		$Ax = b$
		\\~\\
		\onslide<2-3>{$\hat{\vec{x}} \in \text{span} \set{\vec{q}, A\vec{q}, \dots, A^{k-1} \vec{q}}$}
		\\~\\
		\onslide<3>{$\langle \vec{q}_k, \vec{q}_j \rangle = 0$}
	\end{column}
	
	\begin{column}{0.5\textwidth}
		\textbf{Nonlinear (Extrapolation)}
		\\~\\
		$f(x)=b$
		\\~\\
		\onslide<2-3>{$\hat{\vec{x}} = \sum_{i=0}^k \alpha_i \vec{x}_{n+i}$}
		\\~\\
		\onslide<3>{$\sum_{i=0}^k \alpha_i ( \vec{x}_{n+i} - \vec{x}) = 0$}
	\end{column}
\end{columns}
\end{frame}

%===Multisecant equations===%

\begin{frame}
\frametitle{Multisecant equations -- where they come from}
Recall the secant method in 1D:
\begin{equation*}
\hat{x} = x_{n+1} -  \frac{x_{n+1} - x_n}{f(x_{n+1}) - f(x_n)}f(x_{n+1}),
\end{equation*}
which uses $f'(x_n) \approx (f(x_{n+1}) - f(x_n))/(x_{n+1} - x_n)$.
\begin{figure}
	\begin{tikzpicture}
		 \draw[->,thick] (-0.5,0) -- (1.5,0) node[below] {$x$}; \draw[->,thick] (0,-0.5) -- (0,1.5) node[right] {$y$};
		 \draw[domain=-0.5:1.15, smooth, variable=\x, blue] plot ({\x}, {\x*\x*\x + \x/2});
		 \draw[red] (0.5,0.5^3+0.25) circle (1pt); \draw[red] (1,1.5) circle (1pt);
		 \draw[thick,red] (0.5,0.375) -- (1,1.5);
		 \draw[->,thick,red] (0.5,0.5^3+0.25) -- (0.5 - 0.1667,0);
	\end{tikzpicture}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Multisecant equations -- where they come from}
In higher dimensions we need the Jacobian $J(\vec{x}_n)$:
\begin{equation*}
\fxi{n+i} - \fxi{n} \approx \Jxn ( \vec{x}_{n+i} - \vec{x}_n ).
\end{equation*}
Collect enough of these and you get the system
\begin{equation*}
\begin{bmatrix} \fxi{n+1} & \dots & \fxi{n+d} \end{bmatrix} - \fxi{n} \vec{1}^\top = \hat{J} \left ( \begin{bmatrix}
\vec{x}_{n+1} & \dots & \vec{x}_{n+d} \end{bmatrix} - \vec{x}_n \vec{1}^\top \right ) .
\end{equation*}
Then an approximate root of $f$ is
\begin{equation*}
\hat{\vec{x}} = \vec{x}_n - \hat{J}^{-1} \fxi{n}.
\end{equation*}
\end{frame}

\begin{frame}
\frametitle{Multisecant equations - Newtonian form}
\begin{align*}
\begin{bmatrix} \fxi{n} & \dots & \fxi{n+d} \end{bmatrix} - \fxi{n} \vec{1}^\top = & 
	\hat{J} \left ( \begin{bmatrix} \vec{x}_{n} & \dots & \vec{x}_{n+d} \end{bmatrix} - \vec{x}_n \vec{1}^\top \right ) \\
F_{n,k} - \fxi{n} \vec{1}^\top = &
	\hat{J} \left ( X_{n,k} - \vec{x}_n \vec{1}^\top \right ) \\
F_{n,k} \begin{bmatrix} -1 & \dots & -1 \\ 1 \\ & \ddots \\ & & 1 \end{bmatrix} = & 
	\hat{J} X_{n,k} \begin{bmatrix} -1 & \dots & -1 \\ 1 \\ & \ddots \\ & & 1 \end{bmatrix} \\
F_{n,k} \Delta_n = &
	\hat{J} X_{n,k} \Delta_n \\
\hat{J}^{-1} F_{n,k} \Delta_n = &
	X_{n,k} \Delta_n
\end{align*}
\end{frame}

\begin{frame}
\frametitle{Multisecant equations - Newtonian form}
Recall we are seeking the term $\hat{J}^{-1} \fxi{n}$.
If there exists some $\tilde{\vec{u}}$ such that $F_{n,k} \Delta_n \tilde{\vec{u}} = \fxi{n}$ then $\hat{J}^{-1} \fxi{n} = X_{n,k} \Delta_n \tilde{\vec{u}}$:
\begin{equation*}
F_{n,k} \Delta_n \tilde{\vec{u}} = \fxi{n}, \quad \hat{\vec{x}} = \vec{x}_n - X_{n,k} \Delta_n \tilde{\vec{u}}.
\end{equation*}

If the system to find $\hat{J}$ is underdetermined then the system to find $\tilde{\vec{u}}$ is overdetermined.
To solve it, multiply by some matrix $B^\top$:
\begin{equation*}
B^\top F_{n,k} \Delta_n \tilde{\vec{u}} = B^\top \fxi{n}, \quad \hat{\vec{x}} = \vec{x}_n - X_{n,k} \Delta_n \tilde{\vec{u}}.
\end{equation*}
\end{frame}

\begin{frame}
\frametitle{Multisecant equations - other forms}
\begin{align*}
B^\top F_{n,k} \Delta_n \tilde{\vec{u}} = & B^\top \fxi{n}, & \hat{\vec{x}} = & \vec{x}_n - X_{n,k} \Delta_n \tilde{\vec{u}} \\
\begin{bmatrix} \vec{1}^\top \\ B^\top F_{n,k} \end{bmatrix} \hat{\vec{u}} = & \begin{bmatrix} 0 \\ B^\top \fxi{n} \end{bmatrix}, & \hat{\vec{x}} = & \vec{x}_n - X_{n,k} \hat{\vec{u}} \\
\begin{bmatrix} \vec{1}^\top \\ B^\top F_{n,k} \end{bmatrix} (\vec{e}_1 - \hat{\vec{u}}) = & \begin{bmatrix} 1 \\ B^\top \fxi{n} \end{bmatrix} - \begin{bmatrix} 0 \\ B^\top \fxi{n} \end{bmatrix}, & \hat{\vec{x}} = & X_{n,k} (\vec{e}_1 - \hat{\vec{u}}) \\
\begin{bmatrix} \vec{1}^\top \\ B^\top F_{n,k} \end{bmatrix} \vec{u} = & \begin{bmatrix} 1 \\ \vec{0} \end{bmatrix}, & \hat{\vec{x}} = & X_{n,k} \vec{u} \\
\end{align*}
\end{frame}

\begin{frame}
\frametitle{How to solve the multisecant equations}
Using the base form and Cramer's rule, with the columns of $B$ equal to $\vec{v}_i$, the solution $\hat{\vec{x}}$ is equal to
\begin{equation} \label{eq:uqn}
\hat{\vec{x}}_{n+1} = \frac{ \vmat{
\vec{x}_n & \dots & \vec{x}_{n+k} \\
\vec{v}_1^\top \fxi{n} & \dots & \vec{v}_1^\top \fxi{n+k} \\
\vdots & & \vdots \\
\vec{v}_k^\top \fxi{n} & \dots & \vec{v}_k^\top \fxi{n+k} }}{ \vmat{
1 & \dots & 1 \\
\vec{v}_1^\top \fxi{n} & \dots & \vec{v}_1^\top \fxi{n+k} \\
\vdots & & \vdots \\
\vec{v}_k^\top \fxi{n} & \dots & \vec{v}_k^\top \fxi{n+k} }}.
\end{equation}
\end{frame}

\begin{frame}
\frametitle{General equivalence}
We're asking $\vec{v}_i^\top F_{n,k} \vec{u} = 0$ for all $i=1,\dots,k$.
If $\fxi{n}$ is a residual for a sequence then we're asking this residual to be orthogonal to the space spanned by the columns of $B$.
This is exactly an extrapolation method.
\\~\\
\pause
If $\fxi{i+1} = A \fxi{i}$ then the columns of $F_{n,k}$ lie in $\mathcal{K}_k(A, \fxi{n})$.
The method is then a Krylov subspace method.
\\~\\
\pause
If both are true then we have both an extrapolation method and an equivalent (ie. produces the same sequence of results) Krylov method.
Which ones depend on the choice of $B$.
\end{frame}

\begin{frame}
GMRES: $\hat{\vec{x}} =\vec{x}_n + Q_k \vec{y}_k$ where $Q_k$ is derived from the Arnoldi iteration on the Krylov subspace $\mathcal{K}_{k-1}(A,\fxi{n})$;
\begin{align*}
\norm{A \vec{x}_n + A Q_k \vec{y}_k - \vec{b}} = & \norm{\fxi{n} + Q_k H_{k+1} \vec{y}_k}.
\end{align*}

\pause Multisecant Newtonian form: $\hat{\vec{x}} = \vec{x}_n + X_{n,k} \Delta \tilde{\vec{u}}_k$;
\begin{align*}
\norm{A \vec{x}_n + A X_{n,k} \Delta \tilde{\vec{u}}_k - \vec{b}} = & \norm{ \fxi{n} + F_{n,k} \Delta \tilde{\vec{u}}_k}
\end{align*}

\pause MPE: $\hat{\vec{x}} = X_{n,k} \vec{u}_k$ such that $\vec{1}^\top \vec{u}_k=1$;
\begin{align*}
\norm{A X_{n,k} \vec{u}_k - \vec{b}} = & \norm{(A X_{n,k} - \vec{b} \vec{1}^\top) \vec{u}_k} \\
						     = & \norm{F_{n,k} \vec{u}_k}.
\end{align*}

%\item $\hat{\vec{x}} = \vec{x}_n + X_{n,k} \Delta \tilde{\vec{u}}_k$ where
%\begin{equation*}
%\Delta = \begin{bmatrix} -1 \\ 1 & \ddots \\ & \ddots & -1 \\ & & 1 \end{bmatrix};
%\end{equation*}
%\begin{align*}
%\norm{(A-I) \vec{x}_n + (A-I) X_{n,k} \Delta \tilde{\vec{u}}_k + \vec{b}} = & \norm{\fxi{n} + F_{n,k} \Delta \tilde{\vec{u}}_k} \\
%											 = & \norm{\fxi{n} + (A-I) F_{n,k-1} \tilde{\vec{u}}_k}.
%\end{align*}
\end{frame}

%\begin{frame}
%\frametitle{Table of equivalences}
%\begin{table}
%	\begin{tabular}{c | c | c}
%		Extrap. method & Residual is orthogonal to... & Associated Krylov \\ \hline
%		MPE & $\mathcal{K}_{k-1} (A,\fxi{n})$ (Arnoldi) & GMRES \\
%		RRE & $\mathcal{K}_{k-1} (A-I,\fxi{n})$ & GCR \\
%		MMPE & $\mathcal{K}_{k-1} (G,\vec{q}_0)$ & n/a
%	\end{tabular}
%	\caption{Connections between extrapolation methods and Krylov methods.}
%	\label{tab:KrylovExtrap}
%\end{table}
%% We discussed how MPE in fact needs to use the Krylov subspace of A-I to be equivalent to GMRES
%% This Krylov subspace is equal to that for A, so finding a basis for K(A,r) also finds a basis for K(A-I,r)
%% Hence the equivalence
%% But for RRE, the basis is found specifically for K(A-I,r)
%% The difference is in how the basis is found, but both are bases for the same spaces
%\end{frame}

\begin{frame}
\frametitle{Map of equivalences}
\begin{figure}
  \centering
  \begin{tikzpicture}
    \matrix (m) [column sep=0.8cm, row sep=1em, ampersand replacement=\&]{
%      \& \& \& \& \node[align=center] (Anderson) {Anderson\\mixing}; \\
%      \node[align=center] (multisecant) {$\begin{bmatrix} \vec{1}^\top \\ F_{n,k} \end{bmatrix} \vec{u} = \begin{bmatrix} 1 \\ 0 \end{bmatrix}$,\\ $X_{n,k} \vec{u} = \hat{\vec{x}}$}; \&
      \node (overdetermined) {$\begin{bmatrix} \vec{1}^\top \\ B^\top F_{n,k} \end{bmatrix} \vec{u} = \begin{bmatrix} 1 \\ 0 \end{bmatrix}$}; \& \&
      \node[coordinate] (preMPE) {}; \& \node (MPE) {MPE}; \\
      \& \& \& \node (GMRES) {GMRES}; \\
%      \node[align=center] (preBroyden) {$\begin{bmatrix} \vec{1}^\top \& \vec{1}^\top \\ F_{n,k} \& B \end{bmatrix} \vec{u} = \begin{bmatrix} 1 \\ 0 \end{bmatrix}$,\\ $\begin{bmatrix} X_{n,k} \& C \end{bmatrix} \vec{u} = \hat{\vec{x}}$};
      \node[coordinate] (prepreRRE) {}; \& \& \node[coordinate] (preRRE) {}; \& \node (RRE) {RRE}; \\
      \node[coordinate] (prepreMMPE) {}; \& \& \node[coordinate] (preMMPE) {}; \& \node (MMPE) {MMPE}; \\
%      \& \node[coordinate] (preTEA) {}; \& \& \node[coordinate] (preBiCG) {}; \& \node (BiCG) {BiCG}; \\
%      \node (Broyden) {Generalized Broyden};
%      \& \& \& \& \node (TEA) {TEA*}; \\
      };
    \path[->,very thick]
%      (multisecant) edge node[above] {$k < d$} (overdetermined) edge node[right] {$k < d$} (preBroyden)
%      (preBroyden) edge node[right,align=left] {$(C \Delta)^\top (X_{n,k} \Delta) = 0$, \\ $B = \hat{J}_{n-1,k} C$} (Broyden)
      (overdetermined) edge node[above] {$B=F_{n,k-1}$} (preMPE)
      (preMPE) edge[red] (MPE)
      (prepreRRE) edge node[above] {$B=F_{n,k} \Delta$} (preRRE)
      (preRRE) edge[red] (RRE)
      (prepreMMPE) edge node[above] {$B=\begin{bmatrix} \vec{q}_1 & \dots & \vec{q}_k \end{bmatrix}$} (preMMPE)
      (preMMPE) edge[red] (MMPE);
%      (preTEA) edge node[above] {$B=\begin{bmatrix} \vec{q} & A^\top \vec{q} & \dots \end{bmatrix}$} (preBiCG)
%      (preBiCG) edge[blue] (BiCG)
%      (BiCG) edge[red] (TEA);
    \draw[-, very thick] (overdetermined) edge (prepreMMPE);
    \draw[->,very thick,blue] (preMPE) |- (GMRES);
    \draw[->,very thick,blue] (preRRE) |- (GMRES);
%    \draw[->,very thick] (preMPE) |- node[above] {+relaxation} (Anderson);
  \end{tikzpicture}
  \caption{Interconnectivity of extrapolation and acceleration methods.
  Red arrows indicated $\fxi{n} = \vec{x}_{n+1}-\vec{x}_n$ while blue arrows indicate $\fxi{n} = A \vec{x}_n - \vec{b}$ and $\fxi{n+1} = (A+I) \fxi{n}$.}
\end{figure}
\end{frame}