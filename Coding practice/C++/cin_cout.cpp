#include <iostream> // for std::cout and std::cin

int main()
{
	std::cout << "Enter a number: "; // ask for input
	
	int x{0}; // define and initialize a variable x
	std::cin >> x; // receive input from user
	
	std::cout << "You entered " << x << '\n';
	return 0;
}
