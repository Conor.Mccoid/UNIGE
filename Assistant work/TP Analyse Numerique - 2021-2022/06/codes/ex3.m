clear all;
n = 100;
kappa = [1.0E2, 1.0E6, 1.0E10, 1.0E18]; 

for i = 1:length(kappa)
    % Data 
    A = matgen(n, kappa(i));
    x = randn(n,1);
    b = A*x;

    % Invert and multiply
    M = inv(A);
    x1 = M*b;

    % LU fac
    x2 = A\b;

    R = [ norm(x1-x)/norm(x); norm(A*x1-b)/(norm(A)*norm(x1)+norm(b));
    norm(x2-x)/norm(x); norm(A*x2-b)/(norm(A)*norm(x2)+norm(b))];
   fprintf('kappa=%2.1E ', kappa(i)) ;
   fprintf('%2.1E ', R) 
   fprintf('\n');
end

