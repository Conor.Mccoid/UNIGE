function [L,U,p] = LUPivot_ibrahim(A)
% LUPIVOT computes LU factorization of A with partial pivoting
%   [L,U,p] = LUPivot(A) computes the LU factorization of A with partial
%   pivoting. L is a unit lower triangular matrix, U is an upper triangular
%   matrix, and p is a vector of permutations where p(i) is the row of A
%   from which the i-th pivot is selected. In other words, p is computed such
%   that solving A*x = b is equivalent to solving L*U*x = b(p).
%  ~this algorithm is the one found in the polycpie.
[m,n]=size(A);
if m~=n
    disp('Error: Please enter a square matrix!');
    return;
end

U=A; L=eye(m); p=1:m;
for k=1:m-1
    [pivot,i]=max(abs(U(k:m,k))); % serching for pivot
    i=i+k-1; % to get the correct index
    if pivot~=0
        U([k,i],k:m)=U([i,k],k:m); % swaping rows
        L([k,i],1:k-1)=L([i,k],1:k-1);
        p([k,i])=p([i,k]);
        for j=k+1:m
            L(j,k)=U(j,k)/U(k,k);
            U(j,k:m)=U(j,k:m)-L(j,k)*U(k,k:m);
        end
    end
end