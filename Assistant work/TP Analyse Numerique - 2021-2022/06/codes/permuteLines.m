function M = permuteLines(M, i, k)
    % Fonction qui permute les lignes i et k dans la matrice M
    % Input :   - Matrice M dont on veut permuter les lignes
    %           - i et k : indices des lignes à permuter
    % Output :  - Matrice M changée
    temp = M(i, :); 
    M(i, :) = M(k, :);
    M(k, :) = temp;
end