
clear all;
%% PArt 1
n=9;
A=rand(n);
det(A);
d = mydet(A);


%% Part 2
n=100;
A=randn(n,n);

[L,U]=LUNoPivot(A);
prod(diag(L))*prod(diag(U))
det(A)
