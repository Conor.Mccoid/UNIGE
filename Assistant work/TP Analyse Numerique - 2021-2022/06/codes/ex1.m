%% Part 1

U = triu(rand(5,5),0);
x_exact = rand(5,1);
b = U*x_exact;
y = usol(U,b);

x_exact-y

L = tril(rand(5,5),0);
x_exact1 = rand(5,1);
b1 = L*x_exact1;
y1 = lsol(L,b1);
x_exact1-y1


%% Part 2
A=[4 ,-3, 1;0.5, 3,-1;0,1,0];
[L,U] = LUNoPivot(A);
[L1,U1,P]=lu(A);

b2=rand(3,1);
x_exact2=A\b2;
y2=lsol(L,b2);
x2=usol(U,y2);

x2-x_exact2

%% Part 3
n=3;
A=rand(n,n);
[L,U,p] = LUPivot(A);
P=zeros(n,n);
for i=1:n
    P(i,p(i))=1;
end
[L1,U1,P1]=lu(A);

b3=rand(n,1);
x_exact3=P*A\b3;
y3=lsol(L,b3);
x3=usol(U,y3);

x3-x_exact3


%% Part 4
% a)
A = [1 5 2; 0 0 8; 2 4 1];
b = [9;8;9];
[L,U,p] = LUPivot(A);
P=zeros(3,3);
for i=1:3
    P(i,p(i))=1;
end

x_exact3=P*A\b;                               
y3=lsol(L,b);
x3=usol(U,y3);
 
x3-x_exact3

%b) 
N=12;
for i=1:N
    for j=1:N
        A(i,j)=((j/N)^(i-1))^N;
    end
    b(i)=1/i;
end
[L,U,p] = LUPivot(A);
[L1,U1,P1]=lu(A);
P=zeros(N,N);
for i=1:N
    P(i,p(i))=1;
end
x_exact4=P*A\b;                               
y4=lsol(L,b);
x4=usol(U,y4);
 
x4-x_exact4

%% Part 5
n=10;
A=rand(n,n);
b=randn(n,1);
tic
x_exact5=A\b;
toc
tic
[L,U,p] = LUPivot(A);
b3=b;
for i=1:n
    b5(i)=b(p(i));
end
y5=lsol(L,b5);
x5=usol(U,y5);
toc
x5-x_exact5


%% Part 6
n=100;
h=1/n;
A=(1/h)*(gallery('tridiag',n,1,4,1));
[L,U,p] = LUPivot(A);
[L1,U1] = LUNoPivot(A);
%check if p=(1,2,......,n)
