function d = mydet(A)
% d = mydet(A) compute the determinant of A by Laplace's formula 

n = length(A);
if n == 1
    d = A;
    return;
end

d = 0;
for i = 1 : n
    d = d + (-1)^(i+1)*A(1,i)*mydet(A(2:end, [1:i-1, i+1:n]));
end
return;
