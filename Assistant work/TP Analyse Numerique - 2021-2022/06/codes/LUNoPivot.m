function [L,U] = LUNoPivot(A)
% LUNOPIVOT computes LU factorization of A without pivoting
% [L,U] = LUNoPivot(A) computes the LU factorization of A using
% Gaussian elimination without pivoting. L is a unit lower triangular
% matrix and U is an upper triangular matrix.

n = length(A(:,1));
L = diag(ones(n,1));
U = A;
for j = 1 : n-1
    for i = j+1:n
        l = U(i,j)/U(j,j);
        L(i,j) = l;
        U(i,j:n) = U(i,j:n) - U(j,j:n)*l;
    end
end

    
