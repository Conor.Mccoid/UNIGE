    clear all;
    n = 100;
    h = 1/n;
    x = h:h:1-h;
    nOnes = ones(n-1, 1) ;
    A = sparse(diag(2 * nOnes, 0) - diag(nOnes(1:n-2), -1) - diag(nOnes(1:n-2), 1));
    f = (h^2)*(-2*ones(length(x)))';
    sol = A\f;
    solution = x.^2-x;
    
    figure(1)
    plot(x,solution,'b') 
    hold on
    plot(x,sol,'ro')
    title('n=100')
    xlabel('x')
    ylabel('u(x)')
    legend('Using LU decomposition','Exact solution')
    
