function [position, value] = findPivot(vector, r)
    % Fonction qui calcule la position et la valeur du plus grand terme
    % d'un vecteur.
    % Input :   - vector : le vecteur
    %           - r : on effectue la recherche entre (r+1) et la fin
    % Output :  - [position, value] : la position et la valeur
    position = r+1;
    value = vector(r+1);
    for i = r+1 : length(vector)
       if (vector(i) > value)
           position = i;
           value = vector(i);
       end
    end
end