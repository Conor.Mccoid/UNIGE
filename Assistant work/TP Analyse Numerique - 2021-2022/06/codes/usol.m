function y = usol(U,b)
% USOL solves the system Ux = b for upper triangular U
% y = usol(U,b) solves the system Ux = b, where U is a
% upper triangular matrix.
n = length(b);
y = zeros(n,1);

for i = n:-1:1
    s = 0;
    for j = i+1:n
        s = s + U(i,j)*y(j);
    end
    y(i) = (b(i)-s)/U(i,i);
end

