clear all;
n = 2;
format long
kappa = [1.0E2, 1.0E6, 1.0E10, 1.0E18]; 

for i = 1:length(kappa)
    % Data 
    kappa(i)
    A = matgen(n, kappa(i));
    x = randn(n,1);
    b = A*x;

    % Cramer's rule
    d=A(1,1)*A(2,2)-A(2,1)*A(1,2); 
    x_cramer = [(b(1)*A(2,2)-b(2)*A(1,2))/d ; (b(2)*A(1,1)-b(1)*A(2,1))/d];
    
    % Gauss
    [L,U,p]=LUPivot(A);
    P=zeros(n,n);
    for i=1:n
        P(i,p(i))=1;
    end
    y=lsol(L,b);
    x_Gauss=usol(U,y);
    
    % Backslash 
    x_back = A\b;
    
   
    R_cramer = [ norm(x_cramer-x)/norm(x), norm(A*x_cramer-b)/(norm(A)*norm(x)+norm(b))]
    R_Gauss = [ norm(x_Gauss-x)/norm(x), norm(A*x_Gauss-b)/(norm(A)*norm(x)+norm(b))]
    R_back = [ norm(x_back-x)/norm(x), norm(A*x_back-b)/(norm(A)*norm(x)+norm(b))]

   
end

