X1=[15.5,14.7,13.8,17.0,16.5,15.5,17.0,16.4,14.8];
X2=[14.6,13.0,14.5,16.9,14.1,15.6,16.9,16.6,14.8];

D=X2-X1;
mu=mean(D);
sigma=std(D);
T=mu*sqrt(9)/sigma;

disp(['mean: ', num2str(mu)])
disp(['std: ', num2str(sigma)])
disp(['test: ', num2str(T)])
