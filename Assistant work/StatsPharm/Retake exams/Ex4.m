y=[91;94;82;88;81;81;82]; X=[ones(7,1),(1:7)'];
alpha=(X'*X) \ (X'*y);

disp(['intercept: ',num2str(alpha(1))]);
disp(['slope: ',num2str(alpha(2))]);