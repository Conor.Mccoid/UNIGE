function y = Lagrange_Interpolation(x, data)
% The function y = Langrange_Interpolation(x, data) evaluates
%   y = p_n(x) at x, where p_n(x) is the Lagrange polynomial defined
%   by the interpolation points given in data.

if size(data,1) < size(data,2), data = data.'; end;

np1 = size(data,1);
y = 0;
for i = 1:np1
    l = ones(size(x));
    for j = 1:np1
        if j == i, continue; end;
        l = l.*(x - data(j,1))/(data(i,1) - data(j,1));
    end
    y = y + data(i,2) * l;
end

return
