function y = Lagrange_Interpolation_Barycentric(x, data)
% The function y = Lagrange_Interpolation_Barycentric(x, data) evaluates
%   y = p_n(x) at x, where p_n(x) is the Lagrange polynomial defined
%   by the interpolation points given in data.
%   Using barycentric formula

if size(data,1) < size(data,2), data = data.'; end;

np1 = size(data,1);

w = zeros(np1,1);
for i = 1:np1
    d = data(i,1) - data(:,1);
    w(i) = 1/(prod(d(1:i-1)) * prod(d(i+1:end)));
end

y = zeros(size(x));
z = zeros(size(x));
for j = 1 : length(x)
   d = x(j) - data(:,1); 
   ind=(d==0);
   if (any(ind))
	   y(j)=sum(data(ind,2))/nnz(ind);
	   z(j)=1;
	   continue;
   end
   t = data(:,2).*w./d;
   y(j) = sum(t(t>0))+sum(t(t<0));
   t = w./d;
   z(j) = sum(t(t>0))+sum(t(t<0));
end

y=y./z;

return
