% Exercise 2
clear all;
close all;

a = 0;
b = 1;

N = 200;

x = linspace(a,b,N);

nodes = linspace(0,1,5);
points= 3*nodes.^2-4;
derivs= [ 1 1 1 1 1 ];

data = [ nodes ; points ];
pn_Lag = Lagrange_Interpolation(x, data);
pn_bar = Lagrange_Interpolation_Barycentric(x, data);

data = [ nodes ; points ; derivs ];
pn_Her = Lagrange_Interpolation_Hermite(x,data');

plot(x,pn_Her,'b',x,pn_Lag,'r')
grid on;
hold on;

e = 0.05;
for k = 1 : 5
    plot([nodes(k)-e,nodes(k)+e],[points(k)-e*derivs(k),points(k)+e*derivs(k)],'k-','LineWidth',1.2);
end
grid on;
title('Comparaison de Lagrange et Hermite','interpreter','latex','FontSize',16);
xlabel('$$x$$','interpreter','latex','FontSize',16);
ylabel('$$y$$','interpreter','latex','FontSize',16);
legend('Hermite','Lagrange','Location','NorthWest')

