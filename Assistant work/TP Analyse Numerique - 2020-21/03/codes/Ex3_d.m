% Exercise 3
clear all;
close all;

% Part (a)-(b)
fprintf('----------\n')
clear all
x = 2;
while ( x + 1 > 1 )
    x = x / 2;
end
2*x
eps('double')
epsMach=2^(-52)
pause

fprintf('----------\n')
x = single(2);
while ( x + 1 > 1 )
    x = x / 2;
end
2*x
eps('single')
epsMach=single(2^(-23))
pause


% Part (c) 
fprintf('----------\n')
% Denormalized number;
s=single(1);
i=0;
while ( s > 0 )
    s = s / 2;
    i = i + 1;
end
rmin = 2^( -i + 1 );
rmin
denorm_min = 2^(-23)*2^(-126)
realmin('single')


fprintf('----------\n')
% Denormalized number;
s=1;
i=0;
while ( s > 0 )
    s = s / 2;
    i = i + 1;
end
rmin = 2^( -i + 1 );
rmin
denorm_min = 2^(-52)*2^(-1022)
realmin



