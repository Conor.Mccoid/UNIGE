function y = Lagrange_Interpolation_Modified_with_weights(x, data, w)
% The function y = Langrange_Interpolation(x, data) evaluates
%   y = p_n(x) at x, where p_n(x) is the Lagrange polynomial defined
%   by the interpolation points given in data.
%   Using modified formula

if size(data,1) < size(data,2), data = data.'; end;
np1 = size(data,1);
y = zeros(size(x));
l = ones(size(x));
for j = 1 : length(x)
    for i = 1:np1
        if x(j) == data(i,1)
            y(j) = data(i,2); 
            l(j) = 1; 
            break; 
        end
        y(j) = y(j) + data(i,2)*w(i)/(x(j)-data(i,1));
        l(j) = l(j)* (x(j) - data(i,1));
    end
end

y=y.*l;
return
