function c = differences_mod(data)
%Returns the differences divisees as defined in eq. 1.14 of the polycopie
n=length(data);
M=zeros(2*n);
%Creates the vectors x0 x0 x1 x1 and y0 y0 y1 y1 ...
y=[data(2,:);data(2,:)];
y=y(:);
x=[data(1,:);data(1,:)];
x=x(:);
M(:,1)=y;
%Sets the first column with derivatives of the matrix M
for i=2:2:2*n
    M(i,2)=data(3,i/2);
end
%Sets the rest of the first column
for i=3:2:2*n
    M(i,2)=(M(i,1)-M(i-1,1))/(x(i)-x(i-1));
end
%Sets the rest of the matrix
for i=1:2*n
    for j=3:i
        M(i,j)=(M(i,j-1)-M(i-1,j-1))/(x(i)-x(i-j+1));
    end
end
c=diag(M);
end