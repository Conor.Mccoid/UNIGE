function y = Interpolation_Lagrange(x,data)
%Returns the Lagrange interpolation polynomial
n=length(data);
m=length(x);
M=zeros(n);
for k=1:m
    for j=1:n
        for i=1:n
            if i==j
                M(j,i)=1;
            else
            M(j,i)=(x(k)-data(1,j))/(data(1,i)-data(1,j));
            end
        end
    end
    l=prod(M);
    y(k)=sum(data(2,1:n).*l);
end
end