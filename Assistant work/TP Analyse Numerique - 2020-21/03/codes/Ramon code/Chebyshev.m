function x = Chebyshev(n)
for i=0:n
x(i+1)=cos((2*i+1)*pi/(2*n+2));
end
end

