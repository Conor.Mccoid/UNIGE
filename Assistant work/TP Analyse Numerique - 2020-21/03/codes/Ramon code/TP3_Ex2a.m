clf

%Number of points used to draw the plots and compute the maximum of
%functions
N=1000;
%Interval where we interpolate the function
I_i=-1;
I_f=1;
%Range of n, where n is the number of interpolation points
n_i=3;
n_f=14;
nx=n_i:n_f;
MAX=zeros(1,n_f-n_i+1);
x=linspace(I_i,I_f,N);
%Function to interpolate
f=@(x) exp(x);

syms y;
df=@(x,n) subs(diff(f(y),y,n),x);

%Compute the expected max error of the interpolation
for n=nx
    b(n-n_i+1)=max(abs(df(x,n+1)))*((I_f-I_i)/n)^(n+1)/4/(n+1);
end
%Compute the error MAX for each number of interpolation points n
for n=1:length(MAX)
    clear data;
    data(1,:)=linspace(I_i,I_f,n-1+n_i);
    %data(1,:)=Chebyshev(n-1+n_i); %Uncomment this line and comment the one
    %above to obtain the plot for Chebyshev interpolation points
    data(2,:)=f(data(1,:));
    MAX(n)=max(abs(Interpolation_Lagrange(x,data)-f(x)));
end
semilogy(nx,MAX,'r')
hold;
semilogy(nx,b,'b') %Comment this line if you don't want to show the expected max error of interpolation