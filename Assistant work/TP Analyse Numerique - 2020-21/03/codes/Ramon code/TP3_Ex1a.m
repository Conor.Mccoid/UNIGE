clf
clear data;

data(1,:)=linspace(0,1,5);
data(2,:)=3*data(1,:).^2-4;
data(3,:)=ones(5,1);

%Creates the vector x0 x0 x1 x1 ...
points=[data(1,:);data(1,:)];
points=points(:)';

c=differences_mod(data)
x=linspace(0,1,100);

%Hermite polynomial
plot(x,p_newton(points,c,x))
hold

%Lagrange polynomial
plot(x,Interpolation_Lagrange(x,data(1:2,:)))

%data points
plot(data(1,:),data(2,:),'o')