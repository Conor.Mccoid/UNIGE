function p = p_newton(points,c,x)
%Returns the Newton polynomial defined in theorem 1.3 of the polycopie
n=length(points);
p=0;
for i=1:n
    monomial=c(i);
    for j=1:i-1
        monomial=monomial.*(x-points(j));
    end
    p = p + monomial;
end
end

