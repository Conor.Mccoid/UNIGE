% Exercise 1d
clear all;
close all;

f1 = @(y) abs(y);
f2 = @(y) abs(sin(5*y)).^3;

a = -1;
b = 1;

n_max = 100;
err1 = zeros(n_max,1);
err2 = zeros(n_max,1);
for n = 1 : n_max

vec = n:-1:0;
nodes = 0.5*(a+b)+0.5*(b-a)*cos(pi*(2*vec+1)./(2*n+2));
points1 = f1(nodes);
points2 = f2(nodes);

x = linspace(a,b,200);

data1 = [ nodes ; points1 ];
data2 = [ nodes ; points2 ];
pn_bar1 = Lagrange_Interpolation_Barycentric(x, data1);
pn_bar2 = Lagrange_Interpolation_Barycentric(x, data2);
fn1 = f1(x);
fn2 = f2(x);

err1(n) = max(abs( fn1 - pn_bar1 ));
err2(n) = max(abs( fn2 - pn_bar2 ));
end

figure(1);
loglog([1:n_max]',err1,'*r',[1:n_max]',[1:n_max]'.^(-1),'b');    
grid on;
title('Erreur d interpolation pour $$f_1$$','interpreter','latex','FontSize',16);
xlabel('$$n$$','interpreter','latex','FontSize',16);
ylabel('$$\max|p_n(x)-f(x)|$$','interpreter','latex','FontSize',16);
hL = legend({'Erreur','$$n^{-?}$$'},...
                            'interpreter','latex','FontSize',16,...
                            'FontWeight','bold',...
                            'Location','NorthEast');
set(hL,'Orientation','vertical');

figure(2);
loglog([1:n_max]',err2,'*r',[1:n_max]',[1:n_max]'.^(-3),'b');
grid on;
title('Erreur d interpolation pour $$f_2$$','interpreter','latex','FontSize',16);
xlabel('$$n$$','interpreter','latex','FontSize',16);
ylabel('$$\max|p_n(x)-f(x)|$$','interpreter','latex','FontSize',16);
hL = legend({'Erreur','$$n^{-?}$$'},...
                            'interpreter','latex','FontSize',16,...
                            'FontWeight','bold',...
                            'Location','NorthEast');
set(hL,'Orientation','vertical');

