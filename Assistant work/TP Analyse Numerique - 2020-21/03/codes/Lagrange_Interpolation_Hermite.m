function y = Lagrange_Interpolation_Hermite(x,data)
% The function y = Lagrange_Interpolation_Hermite(x, data) evaluates
%   y = p_n(x) at x, where p_n(x) is the Hermite polynomial defined
%   by the interpolation points given in data.

    %data = data';
    ind = size(data,2);
    n = size(data,1);
    % Rearrange "data" if necessary;
    if ind > 2
        data = kron(data,[1;1]);
        n = 2*n;
    end
    
    % Construct the finite-differences table;
    M = zeros(n,n);
    M(:,1) = data(:,2);
    for j = 2 : n
        for k = j : n
            if ind > 2 && j == 2 && mod(k,2) == 0
                M(k,j) = data(k,3);
            else
                M(k,j) = ( M(k,j-1)-M(k-1,j-1) ) / ( data(k,1)-data(k-(j-1),1) );
            end
        end
    end
    
    % Assemble the polynomial by the Newton formula;
    m = length(x);
    pr = ones(1,m);
    y = M(1,1)*ones(1,m);
    for j = 2 : n
        pr = pr.*(x-data(j-1,1));
        y = y + M(j,j)*pr;
    end
end
