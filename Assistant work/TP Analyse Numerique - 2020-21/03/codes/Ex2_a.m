% Exercise 1a
clear all;
close all;

f = @(y) sin(y);

a = -1;
b = 1;

n_max = 100;
err1 = zeros(n_max,1);
err2 = zeros(n_max,1);
bound= zeros(n_max,1);
for n = 3 : n_max

nodes = linspace(a,b,n+1);
points = f(nodes);

x = linspace(a,b,200);

data = [ nodes ; points ];
pn_Lag = Lagrange_Interpolation_Modified(x, data);
pn_bar = Lagrange_Interpolation_Barycentric(x, data);
fn = f(x);

err1(n) = max(abs( fn - pn_Lag ));
err2(n) = max(abs( fn - pn_bar ));
bound1(n)= exp(1)/(4*n)*((b-a)/(n-1))^n;

end
% Smaller "n";
figure(3)
vv = 3:1:14;
p1 = semilogy(vv,err1(3:14),'*r');
axis([3 14 0 2])
hold on
p2 = semilogy(vv,err2(3:14),'ob');
p3 = semilogy(vv,bound1(3:14),'k');
grid on
title('Erreur pour points equidistants','interpreter','latex','FontSize',16);
xlabel('n','interpreter','latex','FontSize',16);
ylabel('$$\max|p_n(x)-f(x)|$$','interpreter','latex','FontSize',16);
hL = legend([p1,p2,p3],{'$$Lagrange$$','$$Barycentrique$$','$$b(n)$$'},...
                            'interpreter','latex','FontSize',16,...
                            'FontWeight','bold',...
                            'Location','NorthEast');
set(hL,'Orientation','vertical');

% Higher "n";
figure(4)
p1 = semilogy(err1,'*r');
hold on
p2 = semilogy(err2,'ob');
grid on
title('Erreur pour points equidistants','interpreter','latex','FontSize',16);
xlabel('n','interpreter','latex','FontSize',16);
ylabel('$$\max|p_n(x)-f(x)|$$','interpreter','latex','FontSize',16);
hL = legend([p1,p2],{'$$Lagrange$$','$$Barycentrique$$'},...
                            'interpreter','latex','FontSize',16,...
                            'FontWeight','bold',...
                            'Location','NorthEast');
set(hL,'Orientation','vertical');