\documentclass[11pt,a4paper]{article}
\usepackage{amsthm}
\usepackage{amscd}
\usepackage{fancyvrb}
\usepackage{amsmath} % Pour les symboles complementaire comme les matrices !
\usepackage{amssymb}
\usepackage{verbatim}
\usepackage{epsfig}
%\usepackage[french]{babel}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage{float}

%\usepackage[T1]{fontenc} % Pour la bonne cesure du francais


\newtheorem{theorem}{Theorem}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{example}{Example}
%\newcommand{\qed}{\hfill$\qedsquare$\goodbreak\bigskip}

\advance\voffset by -35mm \advance\hoffset by -25mm
\setlength{\textwidth}{175mm} \setlength{\textheight}{260mm}
\pagestyle{empty}

\newdimen\figcenter  
\newdimen\figlarge
\newdimen\figheight  
\def\figinsert#1{
\figcenter=\hsize\relax
\advance\figcenter by -\figlarge
\divide\figcenter by 4
\vskip +1.truecm
\vskip\figheight\noindent\hskip\figcenter
\special{psfile=#1}
\vskip -.7truecm}

\usepackage{bm}
\newcommand{\vect}[1]{\bm{#1}}




\begin{document}

\noindent {\large UNIVERSITÉ DE GENÈVE} \hfill Section de mathématiques \\
\noindent Faculté des Sciences 
\hfill A rendre avant le Jeudi 27 mai 2021. \\
\hrule
\bigskip
\bigskip

\begin{center} \textbf{\Large Analyse Numérique} \end{center}
\begin{center} \textbf{\Large Travaux Pratiques - Série 9} \end{center}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\paragraph{Exercice 1 : Préservation de la norme.}

Dans cet exercice, nous considérons les équations linéarisées de Volterra-Lotka (voir l'exercice 4, partie 2 du TP8). \`A une affinit\'e près, la linéarisation du système de Volterra-Lotka au voisinage du point d’équilibre intérieur s’écrit:
\begin{equation}\label{PN}
\left\{
  \begin{array}{lr}
    \dot{x}_1 = x_2 \\
    \dot{x}_2 =-x_1.
  \end{array}
\right.
\end{equation}
Les trajectoires de \eqref{PN} sont des cercles d’équation $x^2_1 + x^2_2 = c^2$ avec $c\in\mathbb{R}$. On souhaite v\'erifier si les schémas numériques conservent cette propriété.

\begin{enumerate}
\item[(a)] Ecrire le problème ci-dessus
sous la forme matricielle $\dot{\textbf{x}}=A\textbf{x}$.
\item[(b)] Il est facile de montrer que la solution $\textbf{x}(t)=[x_1(t),x_2(t)]^T$ du problème ci-dessus est telle que la norme de la solution est préservée,
c.à.d. $\| \textbf{x}(t) \|_2 = c$ pour chaque $t \in [0,T]$. Cela reste vrai pour tous les problèmes
EDO linéaires $\dot{\textbf{x}}=A\textbf{x}$ où la matrice $A$ est antisymétrique ($A^T = -A$). (Pourquoi?)

Nous considérons la méthode d'Euler implicite, Crank Nicolson et Runge Kutta d'ordre 4. Vérifier laquelle de ces méthodes préserve la norme pour les solutions numériques : $\| \textbf{x}(t_n) \|_2 = \| \textbf{x}(0) \|_2$ pour chaque $n$. On a fixé la condition initiale à : $\textbf{x}(0)=[1;0]^T$ et calcul\'e la solution exacte sur l’intervalle $[0, T]$ avec $T = 500$; le pas de discrétisation est constant $\Delta t = 0.05$. Étudier cette propriété à l'aide de vos codes, obtenir les résultats de la Figure \ref{fig:1} et les expliquer.

\begin{figure}[H]
\centering
\includegraphics[width=.45\linewidth]{Ex1_fig1_IE.eps}
\includegraphics[width=.45\linewidth]{Ex1_fig1_CN_RK4.eps}
\caption{Exercice 1b: Solutions discrètes obtenues
par les méthodes: Euler Implicite, Crank-Nicolson (CN) et Runge Kutta d'ordre 4 (RK4).
(Nous vous rappellons que $x_1$ et $x_2$ sont le composantes de la solution discrète $\textbf{x}$.)}
\label{fig:1}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[width=.45\linewidth]{Ex1_fig2_h_005.eps}
\includegraphics[width=.45\linewidth]{Ex1_fig2_h_080.eps}
\caption{Exercice 1c: Normes des solutions discrètes obtenues
par les méthodes: Euler Implicite (IE), Crank--Nicolson (CN) et Runge Kutta d'ordre 4(RK4) pour différentes valeurs de $h$.}
\label{fig:2}
\end{figure}

\item[(c)] Repondre l'exercice (b) en utilisant $h=0.8$ et comparer les résultats obtenus par
les méthodes de Crank-Nicolson et Runge Kutta d'ordre 4 pour obtenir la Figure \ref{fig:2}.
Qu'observez-vous ?
Commenter.
\end{enumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\paragraph{Exercice 2 : La méthode de Newton pour le calcul des valeurs et vecteurs propres.}
Dans cet exercice, nous vous demandons d'utiliser la méthode de Newton pour calculer
les valeurs et vecteurs propres d'une matrice $A$. 
Pour faire cela, vous pouvez écrire le problème de valeurs propres $A {\bf v} = \lambda {\bf v}$
comme l'équation non-linéaire suivante :
\begin{equation*}
f\left(\begin{bmatrix}
{\bf v} \\
\lambda \\
\end{bmatrix} \right)
:=
\begin{bmatrix}
(A - \lambda I){\bf v} \\
\frac{1}{2}{\bf v}^T {\bf v} - 1 \\
\end{bmatrix}
= 0.
\end{equation*}
En calculant les racines de $ f $, on résout la paire valeur/vecteur propre normalisée $A{\bf v}=\lambda {\bf v}$, avec $\|{\bf v}\|_2^2 = 2$.
\begin{itemize}
\item [(a)] Écrire un code MATLAB pour résoudre le problème ci-dessus au moyen de la méthode de Newton (exacte).
\item [(b)] Tracer les courbes des erreurs $|\lambda^{(k)}(A) - \lambda_{min}(A)|$ et $| \sin \theta\bigl( {\bf v}^{(k)} , {\bf v}_{min} \bigr)|$. (Suggestion: utiliser le commande 'subspace'.)
Vous devez obtenir des résultats similaires à la Figure \ref{fig:2}.
Commenter les résultats obtenus.
 Utiliser les matrices de test $A$ symétrique et $B$ non-symétrique  et les valeurs initiales suivantes:
\begin{Verbatim}
n = 100;
D = diag(linspace(10,1,n));
[Q,~] = qr(rand(n));
A = Q*D*Q';
P = rand(n);
B = P*(D/P);
x0A = v_exactA + rand(n,1)/n;
x0B = v_exactB + rand(n,1)/n;
e0 = lam_exact + 0.04;
\end{Verbatim}
\textbf{Indication}: $x0A$, $x0B$ et $e0$ sont les conditions initiales pour la méthode de Newton. Quelles sont les paires exactes valeur/vecteur propres de A et B?
\end{itemize}

\begin{figure}[h]
\centering
\includegraphics[width=.45\textwidth]{Newton1.eps}
\includegraphics[width=.45\textwidth]{Newton2.eps}
\caption{Erreurs de la méthode de Newton.
En particulier, ``Newton val'' et ``Newton vec'' représentent
les erreurs $|\lambda^{(k)}-\lambda|$ et $|\sin \theta({\bf v}^{(k)},{\bf v})|$, 
où ${\bf v}$ est le vecteur propre correspondant à $\lambda$.
À gauche, résultats pour $A$; à droite, résultats pour $B$.}
\label{fig:2}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\paragraph{Exercice 3 : Itération du quotient de Rayleigh.}
Dans les graphiques de convergence pour les méthodes de Newton de l'Exercice 2 (Figure \ref{fig:2}, lignes rouges), on observe que l'erreur des valeurs propres $\lambda^{(k)}$ et des vecteurs propres ${\bf v}^{(k)}$ est du même ordre de grandeur.
Par contre, on remarque que, pour un vecteur propre approximé ${\bf v}^{(k)}$, on pourrait obtenir une valeur propre approximée de ``précision double'' en utilisant le quotient de Rayleigh:
\[
\widehat\lambda^{(k)} = \frac{ ({\bf v}^{(k)})^TA{\bf v}^{(k)}}{({\bf v}^{(k)})^T{\bf v}^{(k)}}.
\]  
Comme $\widehat\lambda^{(k)}$ est une approximation meilleure que $\lambda^{(k)}$, on peut continuer la méthode de Newton avec $\begin{bmatrix}{\bf v}^{(k)} \\ \widehat\lambda^{(k)} \end{bmatrix}$ au lieu de $\begin{bmatrix}{\bf v}^{(k)} \\ \lambda^{(k)} \end{bmatrix}$ et consid\'erer
\begin{equation*}
\begin{bmatrix}
{\bf v}^{(k+1)} \\ \lambda^{(k+1)}
\end{bmatrix}
=
\begin{bmatrix}
{\bf v}^{(k)} \\ \widehat\lambda^{(k)}
\end{bmatrix}
- 
\begin{bmatrix}
(A - \widehat\lambda^{(k)} I) & {\bf v}^{(k)}  \\
({\bf v}^{(k)})^T  &  {\bf 0} \\
\end{bmatrix}^{-1} 
\begin{bmatrix}
(A - \widehat\lambda^{(k)} I){\bf v}^{(k)} \\
\frac{1}{2}({\bf v}^{(k)})^T {\bf v}^{(k)} - 1 \\
\end{bmatrix}.
\end{equation*}

Écrire un programme pour l'itération du quotient de Rayleigh ci dessous. Comparer avec la méthode de Newton en utilisant les mêmes matrices de test de l'Exercice 2.

\begin{figure}[h]
	\centering
	\includegraphics[width=.45\textwidth]{RQI1.eps}
		\includegraphics[width=.45\textwidth]{RQI2.eps}
	\caption{Erreurs de la méthode de Newton (rouge) et de la méthode de RQI (magenta).
		En particulier, ``Newton val'' et ``Newton vec'' représentent
		les erreurs $|\lambda^{(k)}-\lambda|$ et $|\sin \theta({\bf v}^{(k)},{\bf v})|$, 
		où ${\bf v}$ est le vecteur propre correspondant à $\lambda$.
		Le même c'est pour ``RQI val'' et ``RQI vec''.
		À gauche, résultats pour $A$; à droite, résultats pour $B$.}
	\label{fig:3}
\end{figure}

\end{document}