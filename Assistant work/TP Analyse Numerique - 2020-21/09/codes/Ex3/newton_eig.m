function [xs,ls] = newton_eig(A,x0,lambda0,tol,max_its)


[n,m] = size(A);

x_newt = x0;
lambda_newt = lambda0;

new = [x0; lambda0];

I = eye(n);

for i = 2:max_its
    J = [A - lambda_newt(i-1)*I, -x_newt(:,i-1); 
        x_newt(:,i-1)', 0];
    
    % Compute the change in x and lambda
    fx = [(A - lambda_newt(i - 1)*I)*x_newt(:,i-1); 
              0.5*x_newt(:,i-1)'*x_newt(:,i-1) - 1];

    if norm(fx)<tol, break, end

    new = new - J\fx;
    x_newt = [x_newt, new(1:n)];
    lambda_newt = [lambda_newt, new(end)];        
end

xs = x_newt;
ls = lambda_newt;


end

