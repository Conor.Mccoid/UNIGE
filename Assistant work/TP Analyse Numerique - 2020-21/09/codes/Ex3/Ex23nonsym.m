% Exercise 5
clear all;
close all;

rng(0)

maxit = 10;
tol = 1e-14;

n = 100;
Q = rand(n);
%D = diag([1 linspace(1.15,11,n-1)]);
D = diag(linspace(10,1,n));
A = Q*(D/Q);

x_exact = Q(:,end); e_exact = D(end,end);

x0 = x_exact + rand(n,1)/n;
e0 = e_exact + 0.04;

[v_N,l_N] = newton_eig(A,x0,e0,tol, maxit);
%[v,l_II,v_II] = power_inv_iteration(A,e0,x0,tol,maxit);
[v_R,l_R] = rqi2_eig(A,x0,e0,tol,maxit);

err_N = abs(l_N - e_exact);
err_R = abs(l_R - e_exact);
for i=1:length(l_N)
    err_N_vec(i) = subspace(v_N(:,i),x_exact);
end
% for i=1:length(l_II)
%     err_II_vec(i) = subspace(v_II(:,i),x_exact);
% end
for i=1:length(l_R)
    err_R_vec(i) = subspace(v_R(:,i),x_exact);
end



figure
semilogy(err_N, 'ro-')
hold on
semilogy(err_N_vec, 'rx-')
semilogy(err_R, 'mo-')
semilogy(err_R_vec, 'mx-')
ylim([1.0E-16,1.0E0])

%
legend('Newton val', 'Newton vec', 'RQI val', 'RQI vec','Location','NorthEast')
xlabel('iteration','Fontsize',16)
ylabel('error','Fontsize',16)
set(gca,'Fontsize',16)
grid on;
