function [xs,ls] = rqi_eig(A,x0,lambda0,tol,max_its)


[n,m] = size(A);
x_newt = x0;
lambda_newt = lambda0;

I = eye(n);

for i = 2:max_its
    
    v = (A-lambda_newt(i-1)*eye(n))\ x_newt(:,i-1);
    v = v/norm(v);

    x_newt = [x_newt, v];
    lambda_newt(i) = v'*A*v;

    if norm(A*v - lambda_newt(i)*v) < tol , break, end;
       
end

xs = x_newt;
ls = lambda_newt;


end

