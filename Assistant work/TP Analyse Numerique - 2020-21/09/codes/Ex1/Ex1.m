%% Ex1

A = [0,1;-1,0];
F = @(~,x) A*x;
dF= @(~,~) A;
T = 500;
Y = [1;0];
N = T/0.05 - 1;

[xEE,xRK,xIE,xCN] = NonlinODE(F,T,Y,N,dF);

figure(1)
subplot(1,3,1)
plot(xEE(1,:),xEE(2,:))
xlabel('x_1')
ylabel('x_2')
title('Explicit Euler')
subplot(1,3,2)
plot(xIE(1,:),xIE(2,:))
xlabel('x_1')
ylabel('x_2')
title('Implicit Euler')
subplot(1,3,3)
plot(xCN(1,:),xCN(2,:),'r--',xRK(1,:),xRK(2,:),'b--')
xlabel('x_1')
ylabel('x_2')
legend('Crank-Nicholson','Runge Kutta')

N = T/0.8 - 1;

[xEE,xRK,xIE,xCN] = NonlinODE(F,T,Y,N,dF);

figure(2)
subplot(1,3,1)
plot(xEE(1,:),xEE(2,:))
xlabel('x_1')
ylabel('x_2')
title('Explicit Euler')
subplot(1,3,2)
plot(xIE(1,:),xIE(2,:))
xlabel('x_1')
ylabel('x_2')
title('Implicit Euler')
subplot(1,3,3)
plot(xCN(1,:),xCN(2,:),'r--',xRK(1,:),xRK(2,:),'b--')
xlabel('x_1')
ylabel('x_2')
legend('Crank-Nicholson','Runge Kutta')