function g = g_fun(x,t)
    g=x(1)*exp(x(2).*t).*cos(x(3).*t);