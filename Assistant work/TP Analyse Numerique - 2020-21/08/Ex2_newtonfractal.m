x = -1.1:0.006:1.1;
y = -1.1:0.006:1.1;
Z = zeros(length(x),length(y));
x1 = [1;0]; x2=[-1/2;sqrt(3)/2];
x3 = [-1/2;-sqrt(3)/2];
F=@(x)[x(1)^3-3*x(1)*x(2)^2-1 ; 3*x(1)^2*x(2)-x(2)^3];
dF=@(x)[3*x(1)^2-3*x(2)^2 -6*x(1)*x(2);6*x(1)*x(2) 3*x(1)^2-3*x(2)^2];

for j=1:length(x)
    %disp(sprintf('x=%f',x(j)));
    for k=1:length(y)
        sol = NewtonSolve(F,dF,[x(j);y(k)],100,1e-8);
        Z(k,j) = (norm(sol-x1)<0.01) + 2*(norm(sol-x2)<0.01) + 3*(norm(sol-x3)<0.01);
    end
end
imagesc(x,y,Z);
colorbar;
hold on;
plot([x1(1) x2(1) x3(1)],[x1(2) x2(2) x3(2)],'r*');
hold off;
