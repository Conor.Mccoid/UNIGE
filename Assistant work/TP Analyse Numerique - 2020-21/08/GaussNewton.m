function [x,X] = GaussNewton(r,dr,x0,tol,maxiter)
%r - function
%dr - Jacobian matrix of function g
%x0 - initial value
x = x0;
for k = 1:maxiter
    G = dr(x)'*dr(x);
    b = dr(x)'*(-r(x));
    p = G\b;
    y = x;
    x = x+p;
    if norm(x-y) < tol
        break
    end;
    X(:,k) = x;
end;
end