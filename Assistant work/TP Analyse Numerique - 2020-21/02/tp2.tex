\documentclass[12pt,a4paper]{article}
\usepackage{amsmath,amsthm,amssymb,epsfig,amscd,verbatim,fancyvrb}
\usepackage{graphicx}
%\usepackage[cmtip,arrow]{xy}
%\usepackage{pb-diagram,pb-xy}
\usepackage[french]{babel}
\usepackage[utf8]{inputenc}
\addtolength{\textwidth}{4cm} \addtolength{\textheight}{4cm}
\addtolength{\hoffset}{-2cm} \addtolength{\voffset}{-2cm}
\usepackage{color}

\usepackage{multicol}
\setlength{\columnseprule}{1pt}

\begin{document}
\noindent UNIVERSITÉ DE GENÈVE \hfill Section de Mathématiques \\
\noindent Faculté des Sciences 
\hspace*{5cm}\`A rendre sur Moodle pour le jeudi 15 octobre 2020
\hspace*{8.0cm} et \`a pr\'esenter oralement d\`es vendredi 16 octobre 2020.
  \\[-3mm]
\hrule

\begin{center}
\textbf{Analyse Numérique}  \\[1mm]\bigskip
\textbf{Travaux Pratiques - Série 2} \\[1mm]\bigskip

\textbf{\huge{Interpolation polynomiale}}
\bigskip
\end{center}




Le but de l'interpolation polynomiale est de construire un polynôme $p_n(x)$ de degré au maximum $n$ satisfaisant
\[
  p_n(x_i) = y_i\quad \text{pour} \quad i = 0,1,2,\dots,n,
\]
où les $(x_i,y_i)$ sont les points d'interpolation donnés avec
$a\leq x_0< x_1<\dots<x_{n}\leq b$.
Dans les exercices suivants, on discutera de la construire le polynôme d'interpolation.

\paragraph{1. Polynômes de Lagrange.}
\begin{enumerate}
  \item[a.] 
    En utilisant la formule de Lagrange, on construit le polynôme d'interpolation avec
    \[
      p_n(x) = \sum_{i=0}^{n} y_i \ell_i(x)
      \quad\text{avec}\quad
      \ell_i(x) = \prod_{\substack{j=0\\ j\neq i}}^{n} \frac{x-x_j}{x_i-x_j}.
    \]
    Écrire une fonction Matlab pour évaluer le polynôme $p_n(x)$, 
    étant donnés les points d'interpolation $(x_i,y_i)$ et les points 
    d'évaluation $x$.
    \begin{figure}[h]
\begin{center}
  \includegraphics[width=\textwidth]{fig1}
  \caption{Exercice 1.c: La fonction exacte $f(x)$ (ligne noire, solide) et son polynôme d'interpolation $p_n(x)$
  (ligne rouge, discontinue) calculé en utilisant la formule barycentrique, avec $n=3,6,9$.}\label{fig:lagrange}
\end{center}
\end{figure}\\
    Par exemple,
    \begin{verbatim}
      function y = Interpolation_Lagrange(x, data)
      %  La fonction y = Interpolation_Lagrange(x, data) donne
      %       y = p_n(x), 
      %     où p_n(x) est le polynôme de Lagrange défini par les 
      %     points d'interpolation: 
      %       data =[ x_0, x_1, x_2, .., x_n;
      %               y_0, y_1, y_2, .., y_n ].
      %  REMARQUE: si x est un vecteur, alors y(i) = p_n( x(i) ).
      \end{verbatim}
      {\bf Indice}: Utiliser la fonction de MATLAB \texttt{prod}. Taper \texttt{help prod} dans
      la fenêtre de commande de MATLAB pour en avoir une description.

 \medskip 
  \item[b.]
   On peut réécrire la formule de Lagrange dans la forme modifiée suivante :
    \[
      p_n(x) = \ell(x)\sum_{i=0}^{n} y_i\frac{\omega_i}{x-x_i} 
      \quad\text{avec}\quad
      \ell(x) = \prod_{j=0}^{n} {x-x_j},\quad
      \omega_i = \left(\prod_{\substack{j=0\\ j\neq i} }^{n} {x_i-x_j}\right)^{-1},
    \]
    ainsi que dans la forme barycentrique :
    \[
      p_n(x) = \left({\displaystyle \sum_{i=0}^{n}
      y_i\frac{\omega_i}{x-x_i}}\right)
      \left( {\displaystyle \sum_{i=0}^{n}
      \frac{\omega_i}{x-x_i}}\right)^{-1}.
    \]
    Les fonctions ci-dessus sont valables seulement si $x\neq x_j$, autrement 
    $p_n(x_j) = y_j$.
    Comme dans l'exercice (1.a), écrire des fonctions pour évaluer $p_n(x)$ en
    utilisant la formule de Lagrange modifiée et la formule barycentrique.
   % Please explain the advantage, or disadvantage, of each formula.

    {\bf Indice}: Il faut explicitement vérifier si $x= x_j$.
    \medskip
  \item[c.]
    Utiliser votre code pour interpoler la fonction
    \[f(x) = e^{x/4}\text{sin}(2x)\]
    avec $n+1$ points équidistants dans l'intervalle $[-\pi,\pi]$, c.-à-d.,
    \[
      x_i = -\pi  + \frac{2\pi}{n}i
      \quad\text{et}\quad y_i = f(x_i),\quad
      \text{pour}\quad i=0,\dots,n.
    \]
    Essayer différents $n$ ; par exemple $3$, $6$ et $12$. 
    Tracer le graphique des fonctions $p_n(x)$ et $f(x)$, puis comparer les résultats.
    Essayer de reproduire la Figure~\ref{fig:lagrange}.

\end{enumerate}




\bigskip

\paragraph{2. Points d'interpolation.}
Dans les exercices précédents, on a utilisé des points d'interpolation équidistants.
Dans la pratique, les abscisses de Chebyshev sont souvent utilisées.
Les abscisses de Chebyshev dans l'intervalle $(-1,1)$ 
sont définies par
\[
  x_i = \cos \left(\frac{2i+1}{2n+2}\pi\right), \quad\text{pour}\quad
  i = 0,\dots, n.
\]
\begin{enumerate}

\item[a.] Calculer les poids $w_i$ pour $n=50$ pour les points \'equidistants et les abscisses de Chebyshev. Vérifier numériquement les formules suivantes:
$$w_i = \frac{(-1)^{n-i} }{h^n\,n!} \binom{n}{i}, \quad \text{où} \quad h=2/n ~~\text{(pas de discr\'etisation)}$$
pour les points \'equidistants, et
$$ w_i = (-1)^i\frac{2^{n}}{n+1}\sin\theta_i, \quad \text{où} \quad \theta_i=\frac{(2i+1)\pi}{2n+2}, \quad \text{pour} \quad i=0,...,n $$
pour les abscisses de Chebyshev.

\item[b.] Utiliser les abscisses de Chebyshev comme points d'interpolation pour les deux fonctions suivantes sur l'intervalle $(-1,1)$
\[f(x) = |x| + x/2 - x^2\quad\text{et}\quad g(x) = \frac{1}{1 + 25x^2}.\]

Comparer le résultat avec le cas de points équidistants -- 
voir par exemple la Figure~\ref{fig:chebyshev}. ({\bf Indice}: Utiliser un repère semi-logarithmique \texttt{semilogy} pour tracer le graphique de l'erreur, car autrement dans une échelle normale l'erreur serait trop petite pour être discernée.)

\end{enumerate}




\paragraph{3. Formule de Newton.}
\begin{enumerate}
  \item[a.] 
    On peut aussi construire le polynôme d'interpolation en utilisant la formule de Newton
    \begin{equation}\label{eq:newton}
      p_n(x) = c_0 + c_1(x-x_0) +
      c_2(x-x_0)(x-x_1)+\cdots+c_n(x-x_0)\cdots(x-x_{n-1}),
    \end{equation}
    où 
    \[
      c_k = \delta^k y[x_0,x_1,\dots,x_k],\quad \text{pour}\quad  k=0,\dots,n,
    \]
    sont les différences divisées (voir la Définition~1.2 dans les polycopiés du cours).

    \item[(i)] Écrire une fonction Matlab pour générer les coefficients $[c_0,c_1,\dots,c_n]$ pour des noeuds différents deux \`a deux.
    \begin{verbatim}
    function c = differences_divisees(data)
    %  La fonction y = differences_divisees(x, data) 
    % input: data=[x_0, x_1,...,x_n;
    %              y_0, y_1,...,y_n].
    % output: les valeurs différences divisées c_k
    \end{verbatim}
    
%    \item[(ii)] Vérifier que les coefficients $[c_0,c_1,...,c_n]$ ne dépendent pas de l'ordre des couples $(x_i,y_i)$.
    
    %{\bf Indice}: Utiliser le tableau des différences divisées décrit à la page~3 des polycopiés.
\medskip

  \item[b.]

    Écrire une fonction Matlab pour évaluer $p_n(x)$ de l'équation~\eqref{eq:newton},
    étant donnés $\{x_i\}_{i=0}^{n}$, $\{c_i\}_{i=0}^n$, et le point d'évaluation 
    \texttt{x}. Assurez-vous que \texttt{x} peut être un vecteur.
    
%    Set both ends $a_0=-\pi$ and $a_{n}=\pi$ as Hermite interpolation
%    points.

    \medskip
  \item[c.]
    Comme dans l'exercice (1.c), essayer d'interpoler la fonction $f(x)$ en utilisant 
    votre programme.
    
    {\bf Bonus}: 
    ({\Large$\star$}) Si on ajoute un nouveau point d'interpolation $(x_{n+1}, y_{n+1})$,
    comment peut-on calculer efficacement $p_{n+1}(x)$ à partir de $p_n(x)$?

    \item[d.] Est-il possible de traiter le cas où $x_{n-1}=x_{n}$
    et $y_{n}=f'(x_n)$ ?
    Dans ce cas $p_n(x)$ est un polynôme de Hermite de $f(x)$,
    comme expliqué dans la Section~1.5 des polycopiés.
\end{enumerate}
     
     \begin{figure}
\begin{center}
  \includegraphics[width=\textwidth]{fig2}\\
  \includegraphics[width=\textwidth]{fig3}
  
   \caption{ 
    {\bf En haut}: La fonction $f(x) = |x| + x/2 - x^2$ (ligne noire, solide) et son polynôme d'interpolation $p_n(x)$
  (ligne rouge, discontinue). Les points d'interpolation sont dessinés sous forme de
  \texttt{o}. En haut à droite: erreur absolue $|f(x)-p_n(x)|$.
  \quad {\bf En bas}: La fonction de Runge $g(x) = (1 + 25x^2)^{-1}$ (ligne noire, solide) et son polynôme d'interpolation $p_n(x)$
  (ligne rouge, discontinue). En bas à droite: erreur absolue $|g(x)-p_n(x)|$.
}\label{fig:chebyshev}
\end{center}
\end{figure}
\bigskip



\end{document}
