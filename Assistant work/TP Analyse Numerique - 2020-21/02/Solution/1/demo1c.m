% demo for interpolation
close all; clc;

f = @(x) exp(x/4).*sin(2*x);
%f = @(x) 1./(1+25*x.^2);
%f = @(x) cos(2*x) + sin(x);

%a = -1; b = 1;
a = -pi; b = pi;


xx = linspace(a,b,100);

n = 3;
x = linspace(a,b,n+1);
data3 = [x; f(x)];
y3 = Lagrange_Interpolation_Barycentric(xx, data3);

n = 6;
x = linspace(a,b,n+1);
data6 = [x; f(x)];
y6 = Lagrange_Interpolation_Barycentric(xx, data6);

n = 12;
x = linspace(a,b,n+1);
data12 = [x; f(x)];
y12 = Lagrange_Interpolation_Barycentric(xx, data12);

figure(1)
subplot(1,3,1)
plot(xx, f(xx), '-k', 'linewidth', 2); hold on
plot(xx, y3, '--r','linewidth', 2);
plot(data3(1,:),data3(2,:), 'o');
xlim([a,b]);
xlabel('x'); ylabel('y');
title('n=3');

subplot(1,3,2)
plot(xx, f(xx), '-k', 'linewidth', 2); hold on
plot(xx, y6, '--r','linewidth', 2);
plot(data6(1,:),data6(2,:), 'o');
xlim([a,b]);
xlabel('x'); ylabel('y');
title('n=6');

subplot(1,3,3)
plot(xx, f(xx), '-k', 'linewidth', 2); hold on
plot(xx, y12, '--r','linewidth', 2);
plot(data12(1,:),data12(2,:), 'o');
xlim([a,b]);
xlabel('x'); ylabel('y');
title('n=12');

return

