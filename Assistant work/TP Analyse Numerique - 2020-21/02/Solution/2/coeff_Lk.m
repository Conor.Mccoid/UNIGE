function c = coeff_Lk(k,nodes)
% Return the coefficients of the polynomial L_k(x).
%   nodes are the nodes that define L_k
%   k is an integer from 0 to n, with n+1 = length(nodes).


% MATLAB indices start from 1, but k starts from 0. Add 1 to adjust.
n = length(nodes)-1;
these_points = nodes([0:(k-1) (k+1):n]+1);
c = poly(these_points);
the_scale_factor = prod(nodes(k+1)-these_points);
c = c/the_scale_factor;