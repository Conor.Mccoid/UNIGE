%exercice 2 question 1
%on commence par calculer les poids wi pour des points equidistand

m=10; %nombre de points = n+1
X=ones(1,m);
%on crée la subdivision equdiatant dans le vecteur X
for i=1:m
    X(i)=-1 + 2*(i-1)/(m-1);   %replace m by m-1          %
end

%on calcul les poids wi que dans le vecteur W

W=ones(1,m);
for i=1:m
    prod=1;
    for j =1:m
        if i~=j
            prod=prod*(X(i)-X(j));
        end
    end
    W(i)=prod^(-1);
end

%on calcul les Wi a laide de la formule dans le vecteur F

F=ones(1,m);
h=2/(m-1);
for i =1:m
    F(i)=(-1)^(m-i)/(h^(m-1)*factorial(m-i)*factorial(i-1));
end
Result=ones(1,m);

% on verifie que les deux forumles sont identique
%on crée un vecteur result tel que Result(i) contienne la difference w(i)-
%
for i=1:m
    Result(i)=F(i)-W(i);
end
Result
%%