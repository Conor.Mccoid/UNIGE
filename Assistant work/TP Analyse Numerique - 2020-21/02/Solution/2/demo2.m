% demo for interpolation
close all; clc;
f = @(x) abs(x) +x/2 -x.^2;
a = -1; b = 1;
n = 30;
ii = [0:1:n];
x = cos(pi*(2*ii+1)/(2*n+2));
x2 = linspace(a, b, n);

xx = linspace(a,b,100);


data = [x; f(x)];
p1= lagrange_poly(x,data(2,:)');
y=polyval(p1,xx);

data2 = [x2; f(x2)];

p2= lagrange_poly(x2,data2(2,:)');
y2=polyval(p2,xx);


figure(1)
subplot(1,3,1)
plot(xx, f(xx), '-k', 'linewidth', 2); hold on
plot(xx, y, '--b');
plot(data(1,:),data(2,:), 'o');
xlim([a,b]);
ylim([-.6,.7]);
xlabel('x'); ylabel('y');
title('Chebyshev nodes, n=30');

subplot(1,3,2)
plot(xx, f(xx), '-k', 'linewidth', 2); hold on
plot(xx, y2, '--r');
plot(data2(1,:),data2(2,:), 'o');
xlim([a,b]);
ylim([-.6,.7]);
xlabel('x'); ylabel('y');
title('Equally distant nodes, n=30');

subplot(1,3,3)
semilogy(xx, abs(f(xx)-y), 'xb' ); hold on
semilogy(xx, abs(f(xx)-y2), '+r' );
xlabel('x'); ylabel('|f(x) - p_n(x)|');
title('Error plot');
legend('Chebyshev', 'Eq. Distant');
return

