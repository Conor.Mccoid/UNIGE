function p = lagrange_poly(nodes,data)
p = 0; % zero coefficients
for k=0:length(nodes)-1
  p = p + coeff_Lk(k,nodes)*data(k+1);
end