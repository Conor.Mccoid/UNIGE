function [c,TDD] = differences_divisees(data)
% La fonction y = differences_divisees(x, data) donne 
% input: data=[x_0, x_1,...,x_n;
%              y_0, y_1,...,y_n].
% output: les valeurs differences divisees c_k
    x=data(1,:);
    y=data(2,:);
    n=size(data,2);
    TDD=zeros(n,n);
    c = zeros(1,n);
    TDD(:,1) = y';
    for j=2:n
    for i=j:n
        TDD(i,j)= ( TDD(i-1,j-1)-TDD(i,j-1)) / (x(i-j+1)-x(i));
    end
    end
    c=diag(TDD);
end