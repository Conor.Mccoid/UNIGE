function y = Newton_Interpolation(x,data)
c=differences_divisees(data);
n=size(data,2);
xi=data(1,:);
y=c(1)*ones(1,size(x,2));
for j=1:size(x,2)
for i=1:n-1
    y(j)=y(j)+c(i+1)*prod(x(j)-xi(1:i));
end
end