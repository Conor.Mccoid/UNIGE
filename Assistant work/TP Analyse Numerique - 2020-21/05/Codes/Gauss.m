function [I]=Gauss(f,a,b,n)

[pts,w]=GaussCoefficients(n);

h=b-a;

I=h*sum(w.*f(a+h*pts));