function [y,fe]=Adapt(f,a,b,abstol,fe)
% ADAPT should not be called by itself, it is used by Integrate
% [y,fe]=Adapt(f,a,b,abstol) integrates f in the interval [a,b].
% It is the estimate of the integral using Monte Carlo, abstol is the
% absolute tolerance, and fe is the number of function evaluations needed.

I1=Simpson(f,a,b);
I2=Gauss(f,a,b,5);


err=abs(I1-I2);
if(err<=abstol)
    y=I2; fe=fe+5;
else
    m=(a+b)/2;
    [y1,fe]=Adapt(f,a,m,abstol,fe);
    [y2,fe]=Adapt(f,m,b,abstol,fe);
    y=y1+y2;
end