function [y,fe] = Integrate(f,a,b,tol)
%INTEGRATE Adaptive integration of f using Simpson's rule and Gauss' rule with 5 nodes
% [y,fe]=Integrate(f,a,b,tol) integrates f in the interval [a,b] using
% Simpson and Gauss' rule. The relative tolerance is specified by tol. fe returns
% the number of function evaluations needed.

%Verify tol
if(tol>=1)
    disp('Choose a smaller tolerance');
elseif(tol<eps)
    disp('Choose a bigger tolerance');
else
   %Monte Carlo estimation
   x=rand(1,5)*(b-a) +a; m=(a+b)/2;
   I=(b-a)/8*(f(a)+f(b)+f(m)+sum(f(x)));
   [y,fe]=Adapt(f,a,b,abs(I)*tol,0);
end

