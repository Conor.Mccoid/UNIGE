f1=@(x) 4./(1+x.^2); 
f2=@(x) 4.*sqrt(1-x.^2);

for i=1:15
    tol(i)=10^(-i); 
    [y1(i),fe1(i)] = Integrate(f1,0,1,tol(i));
    err1(i)=abs(pi-y1(i));
    
    [y2(i),fe2(i)] = Integrate(f2,0,1,tol(i));
    err2(i)=abs(pi-y2(i));
end
 
figure(1)

semilogy(fe1,tol,'b-');
hold on
semilogy(fe1,err1,'r.-');
title('f=4/(1+x^2)')
legend('Estimated error','Real error')

figure(2)

semilogy(fe2,tol,'b.-');
hold on
semilogy(fe2,err2,'r.-');
title('f=4*sqrt(1-x^2)');
legend('Estimated error','Real error')
 
 
 
 
 