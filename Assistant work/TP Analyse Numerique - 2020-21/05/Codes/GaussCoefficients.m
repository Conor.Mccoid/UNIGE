function [pts,w]=GaussCoefficients(s)

for i=1:s-1
    gamma(i)=i/sqrt((2*i-1)*(2*i+1));
end

A=diag(gamma,-1)+diag(gamma,1);

[W,P]=eig(A); 

pts=diag(P); pts=(1+pts')/2; 

w=W(1,:).^2;