function y=xn_modN(x,n,N)
    p=x - int(x/N)*N;
    y=p; counter=1;
    while counter<n
        while y<N && counter<n
            y=y*p;
            counter=counter+1;
        end
        y=y-int(y/N)*N;
    end
endfunction
