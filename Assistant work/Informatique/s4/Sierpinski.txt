<---One step--->
{
{L(1),Midpoint(L(1),L(2)),Midpoint(L(1),L(3))},
{Midpoint(L(2),L(1)),L(2),Midpoint(L(2),L(3))},
{Midpoint(L(3),L(1)),Midpoint(L(3),L(2)),L(3)}}

<---Iterative step--->
Join(
Sequence(
{
{Element(L(k),1),				Midpoint(Element(L(k),1),Element(L(k),2)),	Midpoint(Element(L(k),1),Element(L(k),3))},
{Midpoint(Element(L(k),2),Element(L(k),1)),	Element(L(k),2),				Midpoint(Element(L(k),2),Element(L(k),3))},
{Midpoint(Element(L(k),3),Element(L(k),1)),	Midpoint(Element(L(k),3),Element(L(k),2)),	Element(L(k),3)}
},
k,1,length(L)
)
)

<---This one works! Complete list of points--->
L=
Iteration(
Join(
Sequence(
{
{Element(M,k,1),				Midpoint(Element(M,k,1),Element(M,k,2)),	Midpoint(Element(M,k,1),Element(M,k,3))},
{Midpoint(Element(M,k,2),Element(M,k,1)),	Element(M,k,2),				Midpoint(Element(M,k,2),Element(M,k,3))},
{Midpoint(Element(M,k,3),Element(M,k,1)),	Midpoint(Element(M,k,3),Element(M,k,2)),	Element(M,k,3)}
},
k,1,length(M)
)
),
M,
{{{A,B,C}}},
n
)

<---Connect the dots--->
Sequence(Polygon(Element(L,k)),k,1,length(L))