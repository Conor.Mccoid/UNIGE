# -*- coding: utf-8 -*-
"""
Created on Thu Apr 28 18:27:29 2022

@author: conmc
"""
import numpy as np
import matplotlib.pyplot

import data1
import data2

x=data1.x
y=data1.y
x1=data2.x1
y1=data2.y1

#%%
L = np.array([np.power(x,2),
     np.multiply(x,y),
     np.power(y,2),
     x,
     y])
f = -np.ones(len(x))
ABCDE = np.linalg.solve(np.matmul(L,np.matrix.transpose(L)),np.matmul(L,f))

x_grid = np.linspace(-5,5,100)
y_grid = np.linspace(-5,5,100)
x_grid, y_grid = np.meshgrid(x_grid,y_grid)
Conic = ABCDE[0]*x_grid**2 + ABCDE[1]*x_grid*y_grid + ABCDE[2]*y_grid**2 + ABCDE[3]*x_grid + ABCDE[4]*y_grid
matplotlib.pyplot.contour(x_grid,y_grid,Conic,[-1])
matplotlib.pyplot.plot(x,y,'b*')
matplotlib.pyplot.show()

#%%
L = np.array([np.power(x1,2),
     np.multiply(x1,y1),
     np.power(y1,2),
     x1,
     y1])
f = -np.ones(len(x1))
ABCDE = np.linalg.solve(np.matmul(L,np.matrix.transpose(L)),np.matmul(L,f))

x_grid = np.linspace(-5,5,100)
y_grid = np.linspace(-5,5,100)
x_grid, y_grid = np.meshgrid(x_grid,y_grid)
Conic = ABCDE[0]*x_grid**2 + ABCDE[1]*x_grid*y_grid + ABCDE[2]*y_grid**2 + ABCDE[3]*x_grid + ABCDE[4]*y_grid
matplotlib.pyplot.contour(x_grid,y_grid,Conic,[-1])
matplotlib.pyplot.plot(x1,y1,'b*')
matplotlib.pyplot.show()