# -*- coding: utf-8 -*-
"""
Created on Thu Apr 28 17:41:06 2022

@author: conmc
"""
import numpy as np
import matplotlib.pyplot as plt

import data1
import data2

x=data1.x
y=data1.y
x1=data2.x1
y1=data2.y1

#%%
L = [ [ 8*np.dot(x,x), 8*np.dot(x,y), -4*sum(x)],
      [ 8*np.dot(x,y), 8*np.dot(y,y), -4*sum(y)],
      [ -4*sum(x),     -4*sum(y),     2*len(x)]]
f = np.power(x,2)+np.power(y,2)
f = [ 4*np.dot(x,f), 4*np.dot(y,f), -2*sum(f) ]

abu = np.linalg.solve(L,f)
r = np.sqrt(abu[0]**2 + abu[1]**2 - abu[2])
x_result=r*np.sin(np.linspace(0,2*np.pi,100)) + abu[0]
y_result=r*np.cos(np.linspace(0,2*np.pi,100)) + abu[1]
plt.plot(x,y,'bo',x_result,y_result,'r--')
plt.show(block=True)

#%%
L = [ [ 8*np.dot(x1,x1), 8*np.dot(x1,y1), -4*sum(x1)],
      [ 8*np.dot(x1,y1), 8*np.dot(y1,y1), -4*sum(y1)],
      [ -4*sum(x1),     -4*sum(y1),     2*len(x1)]]
f = np.power(x1,2)+np.power(y1,2)
f = [ 4*np.dot(x1,f), 4*np.dot(y1,f), -2*sum(f) ]

abu = np.linalg.solve(L,f)
r = np.sqrt(abu[0]**2 + abu[1]**2 - abu[2])
x_result=r*np.sin(np.linspace(0,2*np.pi,100)) + abu[0]
y_result=r*np.cos(np.linspace(0,2*np.pi,100)) + abu[1]
plt.plot(x1,y1,'bo',x_result,y_result,'r--')
plt.show(block=True)