# -*- coding: utf-8 -*-
"""
Created on Wed Apr  6 16:06:01 2022

@author: conmc
"""
import numpy as np
def JacobiSquares(n,p):
    q=np.floor(np.sqrt(n))
    p.append(q)
    m=n-q**2
    if m!=0:
        [m,p]=JacobiSquares(m,p)
    return(m,p)

print(JacobiSquares(5436,[]))