# -*- coding: utf-8 -*-
"""
Created on Wed Apr 27 17:39:36 2022

@author: conmc
"""
import numpy as np

def LagrangeSquares(n,p,p_num,k,kmax):
    # n is the number to decompose into squares
    # p is the current decomposition of the larger number
    
    # produce q and m
    # append p with q and p_num with k
    
    # if m==0
    #   return this p
    # if m!=0
    #   reduce m into a sum of squares
    # if the number of squares for m plus those in p is greater than kmax
    #   reduce n into a sum of squares equal in size to kmax, but with k-1 q's
    
    q=np.floor(np.sqrt(n/k))
    m=n-k*q**2
    lp=sum(p_num)+k
    q=[q]
    q_num=[k]
    if m!=0:
        kmin=max(kmax-lp,1)
        [m,r,r_num]=LagrangeSquares(m,[],[],kmin,kmin)
        q=q+r
        q_num=q_num+r_num
        lp=lp+sum(r_num)
    if lp>kmax and k>1:
        [m,p,p_num]=LagrangeSquares(n,p,p_num,k-1,kmax)
    else:
        p=p+q
        p_num=p_num+q_num
    return(m,p,p_num)

print(LagrangeSquares(5463,[],[],4,4))
print(LagrangeSquares(5436,[],[],4,4))

for k in range(1,21):
    print(LagrangeSquares(k,[],[],4,4))