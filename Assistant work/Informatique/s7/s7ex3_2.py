import turtle

def DrawLsystem(w,t,l):
    n=len(w)
    for i in range(n):
        if w[i]=='f':
            t.fd(l)
        elif w[i]=='g':
            t.rt(120)
        elif w[i]=='d':
            t.lt(60)
    return

def main():
    s = turtle.getscreen()
    t = turtle.Turtle()

    # Initial word
    l=100
    w='fgfgfg'
    DrawLsystem(w,t,l)
    counter=1
    while counter<5:
        t.penup()
        t.forward(150)
        t.pendown()
        w=w.replace('f','fdfgfdf') # the L-system rewrite
        l=l/3 # re-normalize the side lengths
        DrawLsystem(w,t,l)
        counter=counter+1
    s.exitonclick()
    return

if __name__=='__main__':
    main()