import turtle
import numpy as np

def DrawLsystem(w,t,l):
    n=len(w)
    for i in range(n):
        if w[i]=='f':
            t.fd(l)
        elif w[i]=='g':
            t.rt(120)
        elif w[i]=='d':
            t.lt(60)
        elif w[i]=='h':
            t.rt(60)
        elif w[i]=='e':
            t.lt(30)
        elif w[i]=='j':
            t.rt(30)
    return

def main():
    s = turtle.getscreen()
    t = turtle.Turtle()

    # Initial word
    l=100
    w='fgfgfg'
    DrawLsystem(w,t,l)
    counter=1

    # Mapping
    #WordMap1={'f': 'gg', 'g': 'fd', 'j': 'h', 'h': 'j'}
    while counter<5:
        t.penup()
        t.forward(150)
        t.pendown()

        w=w.replace('f','efhfe')
        l=l/np.sqrt(3) # re-normalize the side lengths
        t.begin_fill()
        t.color("red",)
        DrawLsystem(w,t,l)
        t.end_fill()

        w=w.replace('f','jfdfj')
        l=l/np.sqrt(3) # re-normalize the side lengths
        t.begin_fill()
        t.color("blue")
        DrawLsystem(w,t,l)
        t.end_fill()

        counter=counter+1
    s.exitonclick()
    return

if __name__=='__main__':
    main()