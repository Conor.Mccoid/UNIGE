# Conway's game of life
#   Cells are either alive or dead at time t
#   At time t+1, if a dead cell had three living neighbours it becomes alive
#   At time t+1, if a living cell had two or three living neighbours it stays alive, otherwise it dies
import numpy as np
import matplotlib.pyplot as plt

#%%   Simplest idea: take a large grid and scan it for changes

def TimeStep(grid,N):

    step=grid.copy()
    for i in range(N):
        for j in range(N):
            jup=min(N-1,j+1)
            jdown=max(0,j-1)
            iup=min(N-1,i+1)
            idown=max(0,i-1)
            # jup=(j+1)%N
            # jdown=(j-1)%N
            # iup=(i+1)%N
            # idown=(i-1)%N
            testval=grid[i,jup] + grid[i,jdown] + grid[iup,j] + grid[idown,j] + grid[iup,jup] + grid[iup,jdown] + grid[idown,jup] + grid[idown,jdown]

            if grid[i,j]==1:
                if (testval<2) or (testval>3):
                    step[i,j]=0
            else:
                if testval==3:
                    step[i,j]=1
    return(step)

def GliderGrid(i,j,N):

    i=int(i)
    j=int(j)
    grid=np.zeros(shape=[N,N])
    glider=np.array([[0,0,1],
                     [1,0,1],
                     [0,1,1]])
    grid[i:i+3,j:j+3]=glider
    return(grid)

def main():

    # initialize a grid
    N=100 # size of grid
    grid=np.random.rand(N,N)>0.25
    grid=grid.astype(int)
    # grid=GliderGrid(N/2,N/2,N)
    plt.spy(grid)
    plt.show(block=False)
    plt.pause(0.5)
    for t in range(100):
        grid=TimeStep(grid,N)
        plt.spy(grid)
        plt.show(block=False)
        plt.pause(0.5)

main()