function N=blabla(x,n,m)
r=1;
p=x;
e=n;
while e>0
if mod(e,2)==1 then
r=mod(r*p,m);
e=e-1;
else
p=mod(p^2,m);
e=div(e,2);
end;
end
N = r;
endfunction