# -*- coding: utf-8 -*-
"""
Created on Mon Mar  7 19:00:48 2022

@author: conmc
"""
def pgcd(m,n):
    r=1
    R=[]
    Q=[]
    while r!=0 and m>0:
        q=0
        while m>=n:
            m=m-n
            q=q+1
        r=m
        m=n
        n=r
        R.append(r)
        Q.append(q)
    return(R[-2],R,Q)