\documentclass{article}

\usepackage[francais]{babel}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{subcaption}
\usepackage{float}
\usepackage{tikz}
\usetikzlibrary{decorations.pathreplacing,positioning,calc,intersections,3d,shapes.geometric,math,external}
\tikzexternalize[prefix=tikz/]
\usepackage{colortbl}
\usepackage{enumitem}

\newcommand{\dxdy}[2]{\frac{d #1}{d #2}}
\newcommand{\dxdyk}[3]{\frac{d^{#3} #1}{d {#2}^{#3}}}
\newcommand{\pdxdy}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\liminfty}[1]{\lim_{#1 \to \infty}}
\newcommand{\limab}[2]{\lim_{#1 \to #2}}

\newcommand{\abs}[1]{\left \vert #1 \right \vert}
\newcommand{\norm}[1]{\left \Vert #1 \right \Vert}
\newcommand{\order}[1]{\mathcal{O} \left ( #1 \right )}
\newcommand{\set}[1]{\left \{ #1 \right \}}
\newcommand{\Set}[2]{\left \{ #1 \ \middle \vert \ #2 \right \}}
\newcommand{\vmat}[1]{\begin{vmatrix} #1 \end{vmatrix}}
\DeclareMathOperator{\sign}{sign}

\newcommand{\bbn}{\mathbb{N}}
\newcommand{\bbz}{\mathbb{Z}}
\newcommand{\bbq}{\mathbb{Q}}
\newcommand{\bbr}{\mathbb{R}}
\newcommand{\bbc}{\mathbb{C}}
\newcommand{\bbf}{\mathbb{F}}

\title{Rattrapage}

\begin{document}

\maketitle

\section{Equation diff\'erentielle ordinaire}

R\'esoudre les deux probl\`emes suivantes:

\begin{enumerate}[label=\alph*)]
\item $y'(t) = \ln(t) y(t), \quad y(1) = 2$

\begin{proof}
\begin{align*}
\frac{dy}{y} = & \ln(t) dt \\
\ln(y) = & x \ln(x) \mid_1^t - \int_1^t \frac{1}{x} x \, dx \\
	= & t \ln(t) - (t-1) + C \\
	= & \ln(t^t) + 1 - t + C \\
y(t) = & t^t \exp(1-t) \exp(C) \\
y(1) = & 1 \exp(0) \exp(C) = \exp(C) = 2 \\
\implies y(t) = & 2 t^t \exp(1-t)
\end{align*}
\end{proof}

\item $y'(t) = \begin{bmatrix} -12 & -30 \\ 5 & 13 \end{bmatrix} y(t), \, y(0) = \begin{bmatrix} -1 \\ 3 \end{bmatrix}$

\begin{proof}
\begin{align*}
\begin{vmatrix} -12 - \lambda & -30 \\ 5 & 13 - \lambda \end{vmatrix} = & (-12 - \lambda) (13 - \lambda) + 60 \\
	= & -156 - 13 \lambda + 12 \lambda + \lambda^2 + 150 \\
	= & \lambda^2 - \lambda - 6 \\
\implies \lambda = & \frac{1 \pm \sqrt{1 + 4 \times 6}}{2} \\
	= & \frac{1 \pm 5}{2} \\
	= & -\frac{4}{2}, \, \frac{6}{2} \\
	= & -2, \, 3
\end{align*}

\begin{align*}
\begin{bmatrix} -12 + 2 & -30 \\ 5 & 13 + 2 \end{bmatrix} = & \begin{bmatrix} -10 & -30 \\ 5 & 15 \end{bmatrix} \\
	\implies & \vec{v}_1 = \begin{bmatrix} 3 \\ -1 \end{bmatrix} \\
\begin{bmatrix} -12 - 3 & -30 \\ 5 & 13 - 3 \end{bmatrix} = & \begin{bmatrix} -15 & -30 \\ 5 & 10 \end{bmatrix} \\
	\implies & \vec{v}_2 = \begin{bmatrix} 2 \\ -1 \end{bmatrix}
\end{align*}

\begin{align*}
\begin{bmatrix} 3 & 2 \\ -1 & -1 \end{bmatrix} \begin{bmatrix} c_1 \\ c_2 \end{bmatrix} = & \begin{bmatrix} -1 \\ 3 \end{bmatrix} \\
\begin{bmatrix} 3 & 2 & -1 \\ -1 & -1 & 3 \end{bmatrix} \sim & \begin{bmatrix} 1 & 0 & 5 \\ -1 & -1 & 3 \end{bmatrix} \\
	\sim & \begin{bmatrix} 1 & 0 & 5 \\ 0 & -1 & 8 \end{bmatrix} \\
	\sim & \begin{bmatrix} 1 & 0 & 5 \\ 0 & 1 & -8 \end{bmatrix}
\end{align*}

\begin{equation*}
y(t) = 5 \begin{bmatrix} 3 \\ -1 \end{bmatrix} \exp(-2 t) - 8 \begin{bmatrix} 2 \\ -1 \end{bmatrix} \exp(3 t)
\end{equation*}
\end{proof}

\end{enumerate}

\section{Int\'egrales multiples}

Calculer
\begin{equation*}
\int_A z \,dx\, dy\, dz
\end{equation*}
sur le domaine
\begin{equation*}
A = \Set{(x,y,z) \in \bbr^3}{0 \leq z \leq 1, \quad 4 x^2 + 9 y^2 \leq 1}.
\end{equation*}

\begin{proof}
\begin{align*}
x = & 3 r \sin(\theta), \\
y = & 2 r \cos(\theta), \\
4 x^2 + 9 y^2 = & 4 \times 9 r^2 \sin(\theta)^2 + 9 \times 4 r^2 \cos(\theta)^2 \\
	= & 36 r^2 \leq 1 \\
	\implies & r \leq \frac{1}{6}
\end{align*}

\begin{align*}
\det(dg) = & \begin{vmatrix} \pdxdy{x}{r} & \pdxdy{x}{\theta} \\ \pdxdy{y}{r} & \pdxdy{y}{\theta} \end{vmatrix} \\
	= & \begin{vmatrix} 3 \sin(\theta) & 3 r \cos(\theta) \\ 2 \cos(\theta) & -2 r \sin(\theta) \end{vmatrix} \\
	= & 6 r (-\sin(\theta)^2 - \cos(\theta)^2) = - 6 r
\end{align*}

\begin{align*}
D = & \int_0^1 z \, dz \int_0^{1/6} \int_0^{2 \pi} 6 r \, d\theta \, dr \\
	= & z^2/2 \mid_0^1 \, 6 \frac{r^2}{2} \mid_0^{1/6} \, \theta \mid_0^{2 \pi} \\
	= & \frac{3 \pi}{36} = \frac{\pi}{12}
\end{align*}
\end{proof}

\end{document}