function [u,res]=GMRES(A,f,tol,max_iters)
% GMRES: Solves linear system by minimizing residuals in Krylov spaces
% [u,res]=GMRES(A,f,tol,max_iter) computes u in the Krylov space
% (f,Af...,A^(k-1)f) by minimizing the norm ||f-A*u||_2 where k is
% the smallest integer such that ||f-A*u||_2<tol. In the worst case
% GMRES is stopped if k<max_iter. The vector res contains the
% history of the residuals.
% [u,res]=GMRES(A,f,tol) is equivalent to GMRES(A,f,tol,length(f))
% [u,res]=GMRES(A,f) is equivalent to GMRES(A,f,1e-6,length(f))
if nargin<2 % Check number of input arguments
    error('Warning: not enough input arguments!')
end
if nargin<3 % Set a tolerance if not defined
tol=1e-6;
end
if nargin<4 % Set max_iters if not defined
    max_iters=length(f);
end
k=1; % Initialize the iteration index
res=norm(f); % Initialize the vercor of residuals
rhs=res; % Initialize the right-hand side
Q(:,1)=f/res; % First column of Q
while res(end)>tol && k<=max_iters % GMRES iterations
    Q(:,k+1)=A*Q(:,k); % PART 1: Arnoldi: (A Q_k = Q_{k+1} H_k)
    for j=1:k % Gram-Schmidt on v=A*Q(:,k)
        H(j,k)=Q(:,k+1)'*Q(:,j);
        Q(:,k+1)=Q(:,k+1)-H(j,k)*Q(:,j);
    end
    H(k+1,k)=norm(Q(:,k+1)); % Lower-diagonal element
    Q(:,k+1)=Q(:,k+1)/H(k+1,k); % Compute the vector Q(:,k+1)
    R(k+1,k)=0; % PART 2: QR by Givens rotations
    R(:,k)=H(:,k); % New column of R
    for j=1:k-1 % Former Givens rotations on last column
        Rk=c(j)*R(j,k)+s(j)*R(j+1,k);
        R(j+1,k)=-s(j)*R(j,k)+c(j)*R(j+1,k);
        R(j,k)=Rk;
    end % Next apply the new Givens rotation
    c(k)=R(k,k)/sqrt(R(k,k)^2+R(k+1,k)^2);
    s(k)=R(k+1,k)/sqrt(R(k,k)^2+R(k+1,k)^2);
    R(k,k)=c(k)*R(k,k)+s(k)*R(k+1,k);
    R(k+1,k)=0;
    rhs(k+1)=-s(k)*rhs(k); % PART 3: Update rhs and residuals
    rhs(k)=c(k)*rhs(k); % New Givens rotation on rhs
    res(k+1)=abs(rhs(k+1)); % Compute the new norm of the residual
    k=k+1; % Update iteration index
end
y=R\rhs'; % Compute the coefficients y_j
u=Q(:,1:end-1)*y; % Compute u as sum_j y_j q_j
end