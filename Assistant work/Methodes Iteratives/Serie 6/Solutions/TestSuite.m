%% Laplacian convergence

n = 20;
A = FDLaplacianTrue(n,2); A = full(-A);
x = linspace(0,1,n+2); x = x(2:end-1);
u = exp(-x'*x) .* (sin(pi*x') * sin(pi*x)); u = u(:);
f = A*u;

C = cos(pi/(n+1));

% [rerange, imrange, radius] = nrange(A,n^2);
% figure(2)
% plot(rerange,imrange,'.')
% xlabel('Real')
% ylabel('Imaginary')
% 
% C = radius/(max(rerange) + min(rerange));

[v,error] = GMRES(A,f);
vcol = length(error);

figure(1)
slope = log(0.5*error(2:end)./error(1))./(2:vcol);
Cest  = exp(max(slope));
plot(1:vcol,log(error),'.-',1:vcol,log(2*error(1)*Cest.^(1:vcol)),'--',1:vcol,log(2*error(1)*C.^(1:vcol)),'-')
xlabel('Iteration')
ylabel('log(Error)')

%% Estimer C

N  = 4;
nn = round(logspace(1,2,N));
h  = 1./(nn+1);
C_num= zeros(size(nn));
C_30 = C_num;
C_28 = zeros(N,100);

for k = 1:N
    n = nn(k); h=1./(n+1);
    A = FDLaplacianTrue(n,2); A=-A;
    x = linspace(0,1,n+2); x = x(2:end-1);
    u = exp(-x'*x) .* (sin(pi*x') * sin(pi*x)); u = u(:);
    f = A*u;
    
    [v,res] = GMRES(A,f);
    vcol = length(res);
    
    slope = log(0.5*res(2:end)./res(1))./(2:vcol);
    C_num(k)= exp(max(slope));
    C_30(k) = cos(pi*h);
    C_28(k,1:vcol) = 1./abs(chebyshevT(1:vcol,1/C_30(k)));
        
    figure(1)
    subplot(2,2,k)
    semilogy(1:vcol,res,'.',1:vcol,2*res(1)*C_num(k).^(1:vcol),'--',...
        1:vcol,2*res(1)*C_30(k).^(1:vcol),'.-',1:vcol,res(1)*C_28(k,1:vcol),'*-',...
        'Linewidth',2,'MarkerSize',10)
    xlabel('Iteration')
    ylabel('Residus')
    legend('Residus','LS Numerique','LS Thm30','LS Thm28','Location','southwest')
    title(['n=',num2str(n)])
    set(gca,'Fontsize',20,'Linewidth',2)
end

% % C = (1-sin(pi*h))./cos(pi*h);
% C = cos(pi*h);
% figure(2)
% % loglog(h,1-Cest,'.',h,1-C,'-')
% plot(log(h),log(1-Cest),'.',log(h),log(1-C),'-')
% xlabel('h')
% ylabel('Slope')