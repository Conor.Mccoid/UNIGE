\documentclass[a4paper, 12pt]{article}

\usepackage{preamble}
\usepackage[french]{babel}
\usepackage[utf8]{inputenc}
\usepackage{tikz}
\usepackage[margin=2cm]{geometry}

\renewcommand{\vec}[1]{\mathbf{#1}}
\newcommand{\Span}[1]{\text{span} \left \{ #1 \right \}}

\def\lammax{\lambda_{\text{max}}}
\def\lammin{\lambda_{\text{min}}}
\def\FA{\mathcal{F}(A)}

\title{S\'erie 6, Exercice 3b)}
\date{}

\begin{document}

\maketitle

\section{Exercice 3b)}

La question demande \`a trouver une limite sup\'erieure exacte des r\'esidus r\'elatifs pour le syst\`eme
$$A\vec{u}= \vec{f},$$
avec $\vec{u}$ quelconque, $\vec{f}$ qui correspond avec $\vec{u}$ et $A$ le Laplacien en dimension deux,
en utilisant GMRES.
C'est possible \`a utiliser les th\'eor\`emes 28 et 30 pour calculer une limite sup\'erieure pour GMRES.
D'abord, je consid\`ere le th\'eor\`eme 30:

\begin{thm}[Th\'eor\`eme 30]
$\FA$ est contenu dans un disque de rayon $s$ et de centre $c$, alors
\begin{equation}
\frac{\norm{\vec{r}_k}}{\norm{\vec{r}_0}} \leq 2 \left ( \frac{s}{\abs{c}} \right )^k .
\end{equation}
\end{thm}

Premi\`erement, montrer que $\FA = \Span{\lambda_i}$ o\`u les $\lambda_i$ sont les valeurs propres de $A$.
$A$ est hermitienne (elle est sym\'etrique et r\'eelle) alors, par l'exercice 2, $\nu(A) \leq \rho(A) = \lammax$.
Il suffit de prouver que $\lammin = \min_{v \in \FA} \abs{v}$ et que $\FA \subset \bbr$.

Mais il y a une d\'emonstration plus facile.
Puisque $A$ est hermitienne, il existe une d\'ecomposition matricielle telle que $A = U \Lambda U^*$, o\`u $U$ est unitaire et $\Lambda$ est diagonale avec les \'el\'ements \'egaux aux vecteurs propres.
Consid\'erer une \'el\'ement $v \in \FA$: $v = \vec{y}^* A \vec{y} =\vec{y}^* U \Lambda U^* \vec{y} = \vec{z}^* \Lambda \vec{z} \in \mathcal{F}(\Lambda)$ parce que $\norm{\vec{z}}^2 = \norm{U^* \vec{y}}^2 = \vec{y}^* U U^* \vec{y} = \vec{y}^* \vec{y} = 1$.
Alors, $\FA \in \mathcal{F}(\Lambda) = \Span{\lambda_i}$.
Nous savons que $\Span{\lambda_i} \in \FA$ par le th\'eor\`eme 29 partie d), alors $\FA = \Span{\lambda_i}$.

\begin{figure}
	\centering
	\begin{tikzpicture}[scale=0.75]
		\draw[black,thick,->] (0,-4) -- (0,4) node[above] {$i \bbr$};
		\draw[black,thick,->] (-1,0) -- (10,0) node[right] {$\bbr$};

		\filldraw[black] (1,0) circle (3pt) node[above left] {$\lammin$};
		\filldraw[black] (9,0) circle (3pt) node [above right]{$\lammax$};

		\draw[red,thick,|-|] (0,-0.5) -- node[above] {$c$} ++(5,0) ;
		\draw[blue,thick] (5,0) -- node[above] {$s$} ++(2.828,-2.828);
		\draw[magenta,thick] (1,0) -- node[above] {$\FA$} (9,0);

		\draw (5,0) circle [radius=4];
	\end{tikzpicture}
	\caption{Le disque qui contient $\FA$.}
	\label{fig:disque}
\end{figure}

La Figure \ref{fig:disque} montre le disque qui contient $\FA$.
Les valeurs de $c$ et de $s$ sont \'evident:
$$c = \frac{\lammax + \lammin}{2}, \quad s = \frac{\lammax - \lammin}{2}, \implies \frac{s}{\abs{c}} = \frac{\kappa(A) - 1}{\kappa(A) + 1}.$$
Apr\`es la s\'erie 3 nous savons que cette fraction est \'egale $\cos(\pi h)$ pour le Laplacien en dimension deux avec un distance $h$ entre les points.

Cela implique que la convergence pour GMRES a le m\^eme comportement comme la m\'ethode de descent maximale.
Mais cette m\'ethode est plus lente que GMRES.
Nous pouvons essayer le th\'eor\`eme 28 \`a trouver une limite plus petite.

\begin{thm}[Th\'eor\`eme 28]
$A$ est diagonalizable telle que $A = U \Lambda U^{-1}$ et $\lambda_i \in E(c,d,a)$ pour tous les $i$,
o\`u $E(c,d,a)$ est une ellipse de centre $c$, de rayon $a$ et de distance entre centre et foyer $d$.
Alors
\begin{equation}
\frac{\norm{\vec{f} - A \vec{u}_k}}{\norm{\vec{f}}} \leq \kappa(U) \frac{T_k(\frac{a}{d})}{\abs{T_k(\frac{c}{d})}} .
\end{equation}
\end{thm}

Pour nous, $\vec{f} - A \vec{u}_k = \vec{r}_k$ et $\vec{f} = \vec{r}_0$.
Aussi, nous savons d\'ej\`a que $A = U \Lambda U^*$ o\`u $U$ est unitaire, alors $\kappa(U) = 1$.
De plus, par la Figure \ref{fig:disque}, $a =s$ et $c$ ne change pas.
La valeur de $d$ est aussi $s$ parce que le spectre est une intervalle, alors les foyers sont sur le p\'erim\`etre.
Donc, $a/d = 1$ et $c/d = 1/\cos(\pi h)$.
Finalement, le th\'eor\`eme nous donne la relation
$$
\frac{\norm{\vec{r}_k}}{\norm{\vec{r}_0}} \leq \frac{1}{\abs{\cos \left ( k \arccos \left (1/\cos(\pi h) \right ) \right )}} .
$$

\begin{figure}
	\includegraphics[width=\textwidth]{ex3b.png}
	\caption{Quatre graphs de la convergence des r\'esidus.}
	\label{fig:ex}
\end{figure}

Nous avons deux limites sup\'erieures exactes.
Je compare les r\'esidus avec cettes limites dans la Figure \ref{fig:ex}.
Il y a quatre graphs qui correspondent \`a quatre valeurs de $n$, le nombre des points.
En rouge il y a une estimation de la limite sup\'erieure.
Clairement, l'estimation n'est pas \'egale aux limites exactes.

Les limites exactes r\'epresentent un 'worst case scenario'.
Normalement, les m\'ethodes sont plus vites comme on peut estimer par les limites.
Mais, de temps en temps on trouve un probl\`eme qui est terrible pour une m\'ethode et on ne converge que la limite sup\'ereiure.

\end{document}