% SCRIPT EXERCISE 3a

% Partie 1 : test 

%Creation du Laplacian

n=20;

A=Laplacian2D(n);

%Creation de f 

x = linspace(0,1,n);
u = cos(x') * sin(x);
u_1=u(:);
f = A * u_1;

%Solution avec GMRES

tol=0.001 ; max_it = 1000 ; 

[u,res] = GMRES(A,f,tol,max_it);

%% Partie 2 : estimation de la borne sup pour la norme des residus. 

res_arr = 0;

for k=3:100

    B=Laplacian2D(k);
    
    x = linspace(0,1,k);
    u = cos(x') * sin(x); u_1 = u(:);
    f = B*u_1;

    tol=0.001 ; max_it = 1 ; 

    [u,res] = GMRES(B,f,tol,max_it);
    
    res_arr = [res_arr, res(1)];
      
end

% approximation 

res_arr(1)=[];

t=linspace(0,1,length(res_arr))';

X = [sqrt(t)];

a = X\res_arr';

t=linspace(0,5,5*length(res_arr))';

Y = [sqrt(t)]*a;


% plot

figure 

hold on 

plot(res_arr)

plot(Y,'--')

title('Residus vs approximation')

legend('resultats','approximation')

hold off

% REPONSE a): On peut voir sur la plot que le resultat le residu max est
% borné par a*sqrt(k) (a=9.6881). La taille du Laplacian étant k^2 x k^2 =
% n x n, alors on peut dire que la borne supérieur vaut environ 10*n.

%% REPONSE a): Deuxième version

%on calcule norm(rn)/norm(r0) pour tout les n

n=100;

A=Laplacian2D(n);

%Creation de f 

x = linspace(0,1,n);
u = cos(x') * sin(x);
u_1=u(:);
f = A * u_1;

%Solution avec GMRES

tol=0.001 ; max_it = 1000 ; 

[u,res] = GMRES(A,f,tol,max_it);

%calcule de la fraction

arr_res_rel=0;

for k=2:n
    arr_res_rel=[arr_res_rel,res(k)/res(1)]
    
end

arr_res_rel(1)=[];

%Exo 2 : comparaison avec le savoir de la matrice. 

%On a r_n=(((k(A)^2-1)/(k(A)^2))^(n/2)*r_0 ou k(A)=delt_max/delt_min (delt_max
%est la plus grande valeur propre, et delt_min la plus petite)

alpha=eig(A);

cond=abs(max(alpha))/abs(min(alpha));
    
cond_2=(cond^2-1)/(cond^2);
    
cond_2=sqrt(cond_2);
 
arr_bound=0;
 
for k=1:n
    
    arr_bound=[arr_bound,cond_2^k];
    
end

arr_bound(1)=[];
%%
figure

semilogy(1:n,arr_bound,'.-',2:n,arr_res_rel,'--')
xlabel('Iteration')
ylabel('Residus')

legend('Error bound','Found error')

hold off

%L'erreur bound est plus petit ou egal à 1, on a donc un bon résultat vu
%que GMRES converge. 



