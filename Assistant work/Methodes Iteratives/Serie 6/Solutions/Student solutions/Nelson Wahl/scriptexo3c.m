% SCRIPT EXERCISE 3c

n=20;

%Creation de f random

mu=1 ; vr=0;
u_e = mu+sqrt(vr)*abs(randn(n));
u_e=u_e(:);

%array entre 0.5 et 2

arr=linspace(0.5,2,20);

%creation de A

A=Laplacian2D(n);

%f

f=A*u_e;

%Boucle pour tester les différents resultat. 

err_gmres=[];

err_conjugate=[];

for k=1:20
    
    B=arr(k)*eye(n^2)+A;
    
    max_it=1000 ;
    
    tol=0.0001;
    
    [u,res]=GMRES(B,f,tol,max_it);
    
    [w,wk]=ConjugateGradient(B,f,zeros(1,n^2)',tol,max_it);
    
    err_gmres=[err_gmres,norm(u_e-u)];
    
    err_conjugate=[err_conjugate,norm(u_e-w)];
    
end

arr_2=[err_gmres ; err_conjugate];

plot(1:20,arr,1:20,arr_2);

legend('GMRES','ConjugateGradient')

%Conjugate Gradient à l'air meilleur dans ce cas là (pour le même nombre
%d'itération et la même tolerance).
