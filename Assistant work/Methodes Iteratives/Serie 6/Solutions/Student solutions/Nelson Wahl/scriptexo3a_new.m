% SCRIPT EXERCISE 3a

% Partie 1 : test 

%Creation du Laplacian

n=20;

A=Laplacian2D(n);

%Creation de f 

x = linspace(0,1,n);
u = cos(x') * sin(x);
u_1=u(:);
f = A * u_1;

%Solution avec GMRES

tol=0.001 ; max_it = 1000 ; 

[u,res] = gmres(A,f,zeros(n^2,1),tol,max_it);

% Partie 2 : estimation de la borne sup pour la norme des residus. 

%on calcule norm(rn)/norm(r0) pour tout les n

n=20;

A=Laplacian2D(n);

%Creation de f 

x = linspace(0,1,n);
u = cos(x') * sin(x);
u_1=u(:);
f = A * u_1;

%Solution avec GMRES

tol=0.001 ; max_it = 1000 ; 

[u,res] = gmres(A,f,zeros(n^2,1),tol,max_it);

%calcule de la fraction

arr_res_rel=0;

for k=2:n
    arr_res_rel=[arr_res_rel,res(k)/res(1)]
    
end

arr_res_rel(1)=[];

%Exo 2 : comparaison avec le savoir de la matrice. 

%Théorème 30 : r_n/r_0<=2*(s/|c|)^k

figure

hold on

[rerange, imrange, nradius] = nrange(full(A), 40, 'b'); % Convert to full(A)

center=((max(rerange)-min(rerange))/2)+min(rerange);

radius=((max(rerange)-min(rerange))/2);

plotellipse([center,0],radius,radius);

legend('Numerical range','Circle approximation');

hold off

arr_bound=0;
 
for k=1:n
    
    arr_bound=[arr_bound,2*((radius)/abs((center)))^k];
    
end

arr_bound(1)=[];

figure
semilogy(1:n,arr_bound,2:n,arr_res_rel)
legend('Error bound','Found error')
hold off

%GMRES converge beaucoup plus vite que ce que l'on pourrait attendre avec
%la borne pour la norme des résidus.



