function A = Laplacian2D(n) 
%return the Laplacian of size n^2 x n^2
internalPoints=n;
e   = ones(internalPoints,1);
spe = spdiags([e -2*e e], -1:1,internalPoints,internalPoints);
Iz  = speye(internalPoints);
A  = kron(Iz,spe)+kron(spe,Iz);
full(A);
A=-A;
end

