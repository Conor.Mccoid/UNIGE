function [u,res] = gmres(A,f,u0,tol,max_it)

%gmres implements the gmres algorithm with 
%INPUTS : 
%A,f,u0 : A,f,u0 - matrix, initial guess, vector in the problem.
%tol : tolerance
%max_it : maximum iterations
%OUTPUTS : 
%u : final approximation
%uk : matrice of all the approximation

%Algorithm from page 109 in handout chapter 3, with differents notes.

% initialisation -------------------------------------

k=1; % Initialize the iteration index
r=A*u0-f; % first error
res=norm(r); % Initialize the vector of residuals
rhs=res; % Initialize the right-hand side
Q(:,1)=r/res; %Initial guess

% GMRES ----------------------------------------------

while res(end)>tol && k<=max_it
    
    %--------- Part 1 : Arnoldi ---------- %
    
    Q(:,k+1)=A*Q(:,k); 
    
    for j=1:k % Gram-Schmidt on v=A*Q(:,k)

           H(j,k)=Q(:,k+1)'*Q(:,j);

           Q(:,k+1)=Q(:,k+1)-H(j,k)*Q(:,j);
           
    end
    
    H(k+1,k)=norm(Q(:,k+1)); % Lower-diagonal element

    Q(:,k+1)=Q(:,k+1)/H(k+1,k); % Compute the vector Q(:,k+1)
    
    % -------- Part 2 : Givens rotation ------ %

    R(k+1,k)=0; 

    R(:,k)=H(:,k); % New column of R

    for j=1:k-1 % Former Givens rotations on last column

        Rk=c(j)*R(j,k)+s(j)*R(j+1,k);

        R(j+1,k)=-s(j)*R(j,k)+c(j)*R(j+1,k);

        R(j,k)=Rk;
        
    end % Next apply the new Givens rotation
    
    c(k)=R(k,k)/sqrt(R(k,k)^2+R(k+1,k)^2);

    s(k)=R(k+1,k)/sqrt(R(k,k)^2+R(k+1,k)^2);

    R(k,k)=c(k)*R(k,k)+s(k)*R(k+1,k);

    R(k+1,k)=0;
    
    % -------- Part 3 : uptade res and rhs ------- %

    rhs(k+1)=-s(k)*rhs(k); 
    
    rhs(k)=c(k)*rhs(k); % New Givens rotation on rhs

    res(k+1)=abs(rhs(k+1)); % Compute the new norm of the residual

    k=k+1; % Update iteration index

end

y=R\rhs'; % Compute the coefficients y_j

u=Q(:,1:end-1)*y; % Compute u as sum_j y_j q_j

end




