function res = plotellipse(center,xRadius,yRadius)
%plot an ellipse from the input
%center is (x,y)
xCenter=center(1);
yCenter=center(2);

theta = 0 : 0.01 : 2*pi;
x = xRadius * cos(theta) + xCenter;
y = yRadius * sin(theta) + yCenter;
plot(x, y);
res=0;

end
