% Series 1 Test Suite

%%
n=11; d=1;
T=FDLaplacianTrue(n-2,d);
A=FDLaplacian(n-2,d);
disp(normest(A-T,2));
disp(normest((n-1)^2 * A-T,2));
% x=linspace(0,1,n)'; f=sin(x(2:end-1));
% error=abs(A*f + f);
% disp(norm(error(2:end-1),2))


%%
n=11; d=2;
T=FDLaplacianTrue(n-2,d);
A=FDLaplacian(n-2,d);
disp(normest(A-T,2));
disp(normest((n-1)^2 * A - T,2));
% x=linspace(0,1,n);
% y=linspace(0,1,n)';
% f=sin(y(2:end-1)) * sin(x(2:end-1));
% error=abs(A*f(:) + f(:));
% disp(norm(error(2:end-1),2));


%%
n=7; d=3;
T=FDLaplacianTrue(n-2,d);
A=FDLaplacian(n-2,d);
disp(normest(A-T,2));
disp(normest((n-1)^2 * A - T,2));