\documentclass[a4paper, 12pt]{article}

\usepackage{preamble}
\usepackage[french]{babel}
\usepackage[utf8]{inputenc}
\usepackage{tikz}
\usepackage[margin=2cm]{geometry}

\title{S\'erie 1, Exercices 4 et 5}
\date{}

\begin{document}

\maketitle

D'abord, qu'est-ce que c'est l'ordre d'un schéma des différences finies (SDF)?

Un SDF est une approximation num\'erique de la dérivée.
L'ordre définit le comportement de l'erreur quand la distance entre les points devient plus petite.

Un SDF est défini par les points utilisés à construire l'approximation, en anglais appelé le 'stencil'.
Pour les deuxièmes dérivées, vous devriez conna\^itre deux en dimension un.
\begin{figure}[h!]
	\begin{subfigure}{0.4\textwidth}
	\centering
		\begin{tikzpicture}
			\filldraw[black] (-1,0) circle (3pt) node[above] {$u_{i-1}$};
			\filldraw[black] (1,0) circle (3pt) node[above] {$u_{i+1}$};
			\draw[black, thick] (-1,0) -- (1,0);
			\draw[black,thick] (0,0) circle (3pt) node[above] {$u_i$};
		\end{tikzpicture}
	\caption{Centr\'ee}
	\end{subfigure}
	\begin{subfigure}{0.4\textwidth}
	\centering
		\begin{tikzpicture}
			\filldraw[black] (-2,0) circle (3pt) node[above] {$u_{i-2}$};
			\filldraw[black] (-1,0) circle (3pt) node[above] {$u_{i-1}$};
			\draw[black,thick] (0,0) circle (3pt) node[above] {$u_i$};
			\draw[black,thick] (-2,0) -- (0,0);
		\end{tikzpicture}
	\caption{D\'ecentr\'ee}
	\end{subfigure}
\caption{Deux SDF en dimension une.
Les deux approximent la deuxi\`eme d\'eriv\'ee de $u(x)$ au point $x_i$ ($u(x_i) = u_i$).}
\end{figure}

En dimension plus \'el\'ev\'ee, les 'stencils' deviennent plus compliqu\'es. 
\begin{figure}[h!]
	\begin{subfigure}{0.4\textwidth}
	\centering
		\begin{tikzpicture}
			\filldraw[black] (-1,0) circle (3pt) node[above] {$u_{i-1,j}$};
			\filldraw[black] (0,-1) circle (3pt) node[right] {$u_{i,j-1}$};
			\filldraw[black] (1,0) circle (3pt) node[above right] {$u_{i+1,j}$};
			\filldraw[black] (0,1) circle (3pt) node[right] {$u_{i,j+1}$};
			\draw[black,thick] (0,0) circle (3pt) node[above right] {$u_{i,j}$};
			\draw[black,thick] (-1,0) -- (1,0);
			\draw[black,thick] (0,-1) -- (0,1);
		\end{tikzpicture}
	\caption{Centr\'ee, en dimension deux}
	\end{subfigure}
	\begin{subfigure}{0.4\textwidth}
	\centering
		\begin{tikzpicture}
			\filldraw[black] (-1,0,0) circle (3pt);
			\filldraw[black] (0,-1,0) circle (3pt);
			\filldraw[black] (0,0,-1) circle (3pt);
			\filldraw[black] (1,0,0) circle (3pt);
			\filldraw[black] (0,1,0) circle (3pt);
			\filldraw[black] (0,0,1) circle (3pt);
			\draw[black,thick] (0,0,0) circle (3pt);
			\draw[black,thick] (-1,0,0) -- (1,0,0);
			\draw[black,thick] (0,-1,0) -- (0,1,0);
			\draw[black,thick] (0,0,-1) -- (0,0,1);
		\end{tikzpicture}
	\caption{Centr\'ee, en dimension trois}
	\end{subfigure}
\caption{Les SDF centr\'ees en dimensions deux et trois.
Celui en dimension deux est appel\'e l'\'etoile \`a cinque points et est le SDF du Laplacien discret en dimension deux (des diff\'erences finies; il y a d'autres representations du Laplacien discret, qui ne nous concernent pas).}
\end{figure}

Rappeler en dimension une avec un maillage uniforme:
\begin{align*}
u(x_{i+1})		&& = u(x_i)   & + u'(x_i) (x_{i+1} - x_i) &&+ u''(x_i) \frac{(x_{i+1}-x_i)^2}{2} && + u^{(3)}(x_i) \frac{(x_{i+1}-x_i)^3}{6} && + \order{h^4}\\
	   		&& = u(x_i)   & + u'(x_i) h 		      && + u''(x_i) \frac{h^2}{2} 		    && + u^{(3)}(x_i) \frac{h^3}{6}		&& + \order{h^4}, \\
u(x_{i-1}) 		&& = u(x_i)   & + u'(x_i) (x_{i-1} - x_i)  && + u''(x_i) \frac{(x_{i-1}-x_i)^2}{2}  && + u^{(3)}(x_i) \frac{(x_{i-1}-x_i)^3}{6}	  && + \order{h^4}\\
			&& = u(x_i)   & - u'(x_i) h 		      && + u''(x_i) \frac{h^2}{2} 		   && - u^{(3)}(x_i) \frac{h^3}{6} 		&& + \order{h^4}, \\ \hline
u(x_{i+1}) + u(x_{i-1})   && = 2u(x_i) & 				      && + u''(x_i) h^2 			   && 							 && + \order{h^4},
\end{align*}
parce que dans en maillage uniforme $x_{i+1} - x_i = -(x_{i-1} - x_i) = h$ pour tous les $i$.
Donc, on peut \'ecrire la deuxi\`eme d\'eriv\'ee de $u$ sur $x_i$ comme:
\begin{equation}
u''(x_i) = \frac{u(x_{i+1}) - 2 u(x_i) + u(x_{i-1})}{h^2} + \frac{\order{h^4}}{h^2}
\end{equation}
qui est un SDF d'ordre deux.

En dimension deux, le Laplacien est le somme de deux deuxi\`emes d\'eriv\'ees: $\Delta u = u_{xx} + u_{yy}$.
On peut utiliser le SDF pr\'ec\'edent dans les deux directions, $x$ et $y$:
\begin{equation}
\begin{split}
\Delta u & = \frac{u_{i+1,j} - 2u_{i,j} + u_{i-1,j}}{h^2} + \order{h^2} + \frac{u_{i,j+1} - 2 u_{i,j} + u_{i,j-1}}{h^2} + \order{h^2} \\
	   & = \frac{u_{i+1,j} + u_{i,j+1} - 4 u_{i,j} + u_{i-1,j} + u_{i,j-1}}{h^2} + \order{h^2}.
\end{split}
\end{equation}
En dimension trois:
\begin{equation}
\Delta u = \frac{u_{i+1,j,k} + u_{i,j+1,k} + u_{i,j,k+1} - 6u_{i,j,k} + u_{i-1,j,k} + u_{i,j-1,k} + u_{i,j,k-1}}{h^2} + \order{h^2}.
\end{equation}

Le Laplacien discret en dimension une ($D^2$) est juste une matrice.
On consid\'ere un vecteur $u$ qui represent une fonction $u(x)$:
\begin{equation*}
D^2 u = \frac{1}{h^2} \begin{bmatrix} -2 & 1 & & \\ 1 & -2 & 1 & \\ & & \ddots & \end{bmatrix} \begin{bmatrix} u_1 \\ u_2 \\ \vdots \\ u_n \end{bmatrix} = \begin{bmatrix} \frac{-2u_1 + u_2}{h^2} \\ \frac{u_1 - 2u_2 + u_3}{h^2} \\ \vdots \\ \frac{u_{n-1} - 2u_n}{h^2} \end{bmatrix}.
\end{equation*}

Pour cr\'eer le Laplacien discret en dimension deux en Matlab, on veut premi\`erement \'ecrire une fonction $u(x,y)$ comme un vecteur.
\begin{equation*}
u = \begin{bmatrix} u_{1,1} & \dots & u_{1,n} \\ \vdots & \ddots & \vdots \\ u_{n,1} & \dots & u_{n,n} \end{bmatrix} \to
\begin{bmatrix} u_{1,1} \\ \vdots \\ u_{n,1} \\ \hline u_{1,2} \\ \vdots \\ u_{n,2} \\ \hline \vdots \\ \hline u_{1,n} \\ \vdots \\ u_{n,n} \end{bmatrix} = u(:) .
\end{equation*}
Consid\'erer le Laplacien discret comme somme de deux matrices qui representent les operateurs de la deux\`ieme d\'eriv\'ee dans les deux directions ($x$ et $y$): $L = D_{xx} + D_{yy}$.

La matrice $D_{xx}$ agit juste sur les colonnes de $u$, $u(:,i)$ pour $i=1,...,n$.
Ca veut dire que:
\begin{equation*}
D_{xx} u(:) = \begin{bmatrix} \frac{-2u_{1,1} + u_{2,1}}{h^2} \\ \frac{u_{1,1} - 2u_{2,1} + u_{3,1}}{h^2} \\ \vdots \\ \frac{u_{n-1,1} - 2u_{n,1}}{h^2} \\ \hline \\
				       \frac{-2u_{1,2} + u_{2,2}}{h^2} \\ \vdots \\ \frac{u_{n-1,2} - 2u_{n,2}}{h^2} \\ \hline \vdots \\ \hline
				       \frac{-2u_{1,n} + u_{2,n}}{h^2} \\ \vdots \\ \frac{u_{n-1,n} - 2u_{n,n}}{h^2}  \end{bmatrix}
		= \begin{bmatrix} D^2 & & \\ & \ddots & \\ & & D^2 \end{bmatrix} u(:) = (I \otimes D^2) u(:).
\end{equation*}
o\`u $I$ est la matrice identit\'e et $\otimes$ est la multiplication de Kronecker.

Pour la matrice $D_{yy}$, noter que la deuxi\`eme matrice dans le produit de Kronecker agit sur les colonnes de $u$ et la premi\`ere sur les lignes.
Alors, $D_{yy} = D^2 \otimes I$.
On peut confirmer par un calcul direct que $D_{yy} u(:)$ donne les deuxi\`emes d\'eriv\'ees correctes.
En fin, $L = (I \otimes D^2) + (D^2 \otimes I)$.

En dimension trois, on peut \'etendre la formule:
$L = (I \otimes I \otimes D^2) + (I \otimes D^2 \otimes I) + (D^2 \otimes I \otimes I)$.
Chaque endroit dans les produits de Kronecker signifie une direction: $x$, $y$ et $z$.

%On peut aussi consid\'erer $D^2 u$:
%\begin{equation*}
%D^2 u = \begin{bmatrix} \frac{-2u_{1,1} + u_{2,1}}{h^2} & \frac{-2u_{1,2} + u_{2,2}}{h^2} & \dots & \frac{-2u_{1,n} + u_{2,n}}{h^2} \\
%			  \frac{u_{1,1} - 2u_{2,1} + u_{3,1}}{h^2} & \vdots & & \vdots \\
%			\vdots & \vdots & & \vdots \\
%			  \frac{u_{n-1,1} - 2u_{n,1}}{h^2} & \frac{u_{n-1,2} - 2u_{n,2}}{h^2} & \dots & \frac{u_{n-1,n} - 2u_{n,n}}{h^2} \end{bmatrix}.
%\end{equation*}
%Chaque \'el\'ement est la deuxi\`eme d\'eriv\'ee (appoximation) en direction $x$.
%
%Pour la matrice $D_{yy}$, noter qu'on peut consid\'erer $D^2 u^\top$, et chaque \'el\'ement de ce produit est la deuxi\`eme d\'eriv\'ee (appoximation) en direction $y$.

\end{document}

