\documentclass[12pt,a4paper]{article}
\usepackage{amsmath,amssymb,amsthm,epsfig,amscd,verbatim}
\usepackage[french]{babel} 
\usepackage{fancyvrb}
%\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\addtolength{\textwidth}{3cm} 
\addtolength{\textheight}{4cm}
\addtolength{\hoffset}{-2cm} 
\addtolength{\voffset}{-2cm}

\usepackage{hyperref}
\usepackage{float}

%% commandes utiles %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\R}{\mathbb{R}}
\newcommand{\C}{\mathbb{C}}
\renewcommand{\vec}[1]{\mathbf{#1}}
\newcommand{\tr}{\mbox{tr}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\begin{document}
\noindent UNIVERSIT\'E DE GEN\`EVE \hfill Section de Math\'ematiques \\
\noindent Facult\'e des Sciences \hfill \`A rendre le mercredi 3 mai \\[-3mm] \hrule
\bigskip

\begin{center}
\textbf{\Large{Méthodes Itératives}}  \\[1mm]\bigskip
\textbf{Série 4}
\end{center}
\bigskip


\textbf{Partie I: L'inégalité de Kantorovitch}\\
Dans cette partie nous vous demandons de démontrer l'inégalité de Kantorovitch au moyen de
l'inégalité de Wielandt.
Considérez une matrice hermitienne et définie positive $A \in \C^{n \times n}$ avec valeurs propres
$0<\lambda_1\leq\lambda_2\leq\cdots\leq\lambda_n$.
L'inégalité de Wielandt est
\begin{equation}\label{eq:Wielandt}
| \vec{u}^H A \vec{v} |^2 \leq \left( \frac{ \lambda_1 - \lambda_n }{ \lambda_1 + \lambda_n } \right)^2
(\vec{u}^H A \vec{u}) (\vec{v}^H A \vec{v}) \, \text{ pour tous $\vec{u},\vec{v} \in\C^n$ orthogonaux},
\end{equation}
et l'inégalité de Kantorovitch est
\begin{equation}\label{eq:Kantorovitch}
(\vec{u}^H A \vec{u}) (\vec{u}^H A^{-1} \vec{u}) \leq \frac{ ( \lambda_1 + \lambda_n )^2 }{ 4 \lambda_1 \lambda_n }
\| \vec{u} \|_2^4 \, \text{ pour tous $\vec{u} \in\C^n$}.
\end{equation}
L'inégalité de Kantorovitch peut être obtenue au moyen des étapes suivantes:
\begin{enumerate}\itemsep0em
  \item[(a)] (3 pts) Démontrer que \eqref{eq:Wielandt} et \eqref{eq:Kantorovitch} 
  sont tous les deux  satisfaits si $\vec{u} \in \C^n$ est un vecteur propre de $A$.
  \textit{Indication :} utiliser l'inégalité arithmético-géométrique $\sqrt{ab} \leq \frac{1}{2} (a+b)$
  pour tout $a,b>0$.
  
  \item[(b)] (3 pts) Démontrer que si $\vec{u} \in \C^n$ n'est pas un vecteur propre de $A$,
  alors $A^{-1} \vec{u} - (\vec{u}^H A^{-1} \vec{u}) \vec{u} \neq 0$
  et $\vec{u} - (\vec{u}^H A^{-1} \vec{u}) A \vec{u} \neq 0$.
  
  \item[(c)] (3 pts) Démontrer que si $\vec{u} \in \C^n$ est un vecteur unitaire, c.-à-d. $\|\vec{u}\|_2=1$, 
  alors $(\vec{u}^H A \vec{u}) (\vec{u}^H A^{-1} \vec{u}) \geq 1$, avec une inégalité stricte si $\vec{u}$ 
  n'est pas un vecteur propre de $A$.\\
  \textit{Indication:} commencer avec $1 = (\vec{u}^H \vec{u})^2 = (\vec{u}^H A^{1/2} A^{-1/2} \vec{u})^2$
  et utiliser l'inégalité de Cauchy-Schwarz.
  
  \item[(d)] (3 pts) Considérer un vecteur unitaire $\vec{u} \in \C^n$ qui n'est pas un vecteur propre de $A$.
  Définir $\vec{v} := A^{-1} \vec{u} - (\vec{u}^H A^{-1} \vec{u}) \vec{u}$ et montrer que :
  \begin{itemize}\itemsep0em
  \item[(1)] $\vec{v}^H \vec{u} = 0$.
  \item[(2)] $A\vec{v} \neq 0$.
  \item[(3)] $\vec{u}^H A \vec{v} = 1 - (\vec{u}^H A \vec{u}) (\vec{u}^H A^{-1} \vec{u}) < 0$.
  \item[(4)] $\vec{v}^H A \vec{v} = - (\vec{u}^H A^{-1} \vec{u}) (\vec{v}^H A \vec{u})$.
  \end{itemize}
  \textit{Indication :}  utiliser les résultats démontrés dans (b) et (c).
  
  \item[(e)] (4 pts) Utiliser l'inégalité de Wielandt \eqref{eq:Wielandt} et les résultats démontrés
  ci-dessus pour obtenir l'inégalité de Kantorovitch \eqref{eq:Kantorovitch}.
  
\end{enumerate}

$\;$\\

\textbf{Partie II: Résumé des méthodes itératives stationnaires}
\begin{enumerate}
  \item (2 pts) Démontrer les équivalences suivantes :
  \begin{itemize}\itemsep0em
  \item Jacobi:
  \begin{equation*}
  \begin{split}
  &(\vec{u}_{k+1})_i = \frac{1}{a_{ii}}\left[\vec{f}_i - \sum_{j=1}^{i-1} a_{ij} (\vec{u}_{k})_j - \sum_{j=i+1}^{n} a_{ij} (\vec{u}_{k})_j \right] \\
  &\Leftrightarrow \;
  \vec{u}_{k+1} = - D^{-1} (L+U) \vec{u}_{k} + D^{-1} \vec{f}.
  \end{split}
  \end{equation*}
  \item Gauss-Seidel: 
  \begin{equation*}
  \begin{split}
  &(\vec{u}_{k+1})_i = \frac{1}{a_{ii}}\left[\vec{f}_i - \sum_{j=1}^{i-1} a_{ij} (\vec{u}_{k+1})_j - \sum_{j=i+1}^{n} a_{ij} (\vec{u}_{k})_j \right] \\
  &\Leftrightarrow \;
  \vec{u}_{k+1} = - (D+L)^{-1} U \vec{u}_{k} + (D+L)^{-1} \vec{f}.
  \end{split}
  \end{equation*}
  \item SOR:
  \begin{equation*}
  \begin{split}
  &(\vec{u}_{k+1})_i = (1-\omega)(\vec{u}_{k})_i + \frac{\omega}{a_{ii}}\left[\vec{f}_i - \sum_{j=1}^{i-1} a_{ij} (\vec{u}_{k+1})_j - \sum_{j=i+1}^{n} a_{ij} (\vec{u}_{k})_j \right] \\
  &\Leftrightarrow \;
  \vec{u}_{k+1} = - (D+\omega L)^{-1} \left[ \bigl( (1-\omega)D - \omega U \bigr) \vec{u}_{k} + \omega \vec{f} \right].
  \end{split}  
  \end{equation*}
  \end{itemize}
  
  \item (3 pts) Considérer le Laplacien discret $A$ de $-\Delta$ en dimension 2.
  Démontrer que la méthode de Jacobi et la méthode de Richardson (avec le paramètre optimal $\alpha_{opt}$
  obtenu dans la Série 3) ont le même comportement de convergence.
  
  \item (3 pts) Considérer une matrice $A \in \R^{2 \times 2}$ et le splitting $A = D + L + U$ avec $D$ inversible.
  Démontrer que si la méthode de Jacobi converge, alors la méthode de Gauss-Seidel converge aussi et plus rapidement.
  
  \item (4 pts) Considérer le Laplacien discret $A\in \R^{n\times n}$ de $-\Delta$ en dimension 2
  et soit $h=\frac{1}{n+1}$ la taille de la grille.
  Dénoter avec $\rho(G_{\rm J};h)$, $\rho(G_{\rm GS};h)$, et $\rho(G_{\rm SOR};h)$ les rayons spectraux
  des méthodes de Jacobi, Gauss-Seidel, et SOR en fonction de $h$.
  Démontrer que :
  \begin{itemize}\itemsep0em
  \item $\rho(G_{\rm J};h) = 1 - \frac{\pi^2}{2} h^2 + O(h^4)$.
  \item $\rho(G_{\rm GS};h) = 1 - \pi^2 h^2 + O(h^4)$.
  \item $\rho(G_{\rm SOR};h) = 1 - 2 \pi h + O(h^2)$.
  \end{itemize}
  
\end{enumerate}

$\;$\\
\\

\textbf{Partie III: Méthodes de descente maximale (ou du gradient) et du gradient conjugué}
\begin{enumerate}\itemsep0em

\item (4 pts) Considérer une fonction $f : \R^n \rightarrow \R$ et on dénote avec $\nabla f(\vec{v})$ le
gradient de $f$ en $\vec{v} \in \R^n$ obtenu au moyen du produit scalaire habituel pour $\R^n$.
Démontrer que $\nabla f(\vec{v})$ est orthogonal à la ligne de niveau de $f$ en $\vec{v}$ et que
$-\nabla f(\vec{v})$ est la direction de descente maximale de $f$ en $\vec{v}$.

\item (4 pts) Implémenter une fonction Matlab qui résout un système linéaire $A \vec{u} = \vec{f}$ à l'aide de la méthode
du gradient.

\item (4 pts) Implémenter une fonction Matlab qui résout un système linéaire $A \vec{u} = \vec{f}$ à l'aide de la méthode
du gradient conjugué.

\item (1 pts) Tester vos codes en résolvant l'équation de Laplace (comme dans la Série 3).

\end{enumerate}


$\;$\\
\\

{\bf \'Evaluation:}
\begin{itemize}
\item[$\bullet$] Les exercices
\item[$\bullet$] Un examen oral durant la session d'examens sur le cours.
\end{itemize}
La note finale est de : $30 \%$ exercices et $70\%$ examen oral.\\ 
Les exercices sont notés $15\%$ pour la présentation de séries et
$15\%$ pour la présentation au tableau. Il faut passer au tableau au
moins 3 fois dans le semestre.
 

\end{document}
