function u=Heat2D(f,g,p)
% HEAT2D solves the stationary heat equation over a square 
%   u=Heat2D(f,g,p) solves the heat equation over a square of size one
%   by one. The source function is given by f, the boundary condition is
%   given by b, the number of grid points for each direction is a quarter
%   of the length(g) and one chooses the iterative solver with p,
%   i.e. p=0 uses Jacobi, p=1 uses Gauss-Seidel.  
