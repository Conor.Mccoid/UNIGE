% test suite for serie 2

n = 10;
x = linspace(0,1,n+2);
u = sin(pi*x')*sin(pi*x)+1;

% II= eye(n+2); I0 = [zeros(1,n+2); II(2:end-1,:) ; zeros(1,n+2)]; II = II-I0;
% II= kron(II,I0) + kron(I0,II); II = sum(II,2);
% g = u(:); g = g(II==1);
% f = -2*pi^2 * ( u(2:end-1,2:end-1) - 1 ); f = f(:);

x = x(2:end-1);
g = ones(4*n,1);
f = -2*pi^2 * sin(pi*x')*sin(pi*x); f = f(:);
% g = @(x,y) 1;
% f = @(x,y) -2*pi^2 * sin(pi*x)*sin(pi*y);

uu = Heat2D(f,g,0,100);
uu = reshape(uu,n,n);
surf(x,x',uu - u(2:end-1,2:end-1))

pause

uu = Heat2D(f,g,1,100);
uu = reshape(uu,n,n);
surf(x,x',uu - u(2:end-1,2:end-1))