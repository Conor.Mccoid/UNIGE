function u=Heat2D(f,g,p)
% HEAT2D solves the stationary heat equation over a square 
%   u=Heat2D(f,g,p) solves the heat equation over a square of size one
%   by one. The source function is given by f, the boundary condition is
%   given by g, the number of grid points for each direction is a quarter
%   of the length(g) and one chooses the iterative solver with p,
%   i.e. p=0 uses Jacobi, p=1 uses Gauss-Seidel.  

n = length(g); n = n/4; ind=1:n;        % taille du systeme
I = eye(n);
G = kron(g(ind),  I(:,1)) + ...         % bord a y=0
    kron(g(ind+n),I(:,end)) + ...       % bord a y=1
    kron(I(:,1),  g(ind+2*n)) + ...     % bord a x=0
    kron(I(:,end),g(ind+3*n));          % bord a x=1
F = f - (n+1)^2 * G;                    % cote droit
D = (n+1)^2*spdiags(ones(n,1)*[1 -2 1],[-1 0 1],n,n); % Lap. en dim. 1
D = kron(I,D) + kron(D,I);              % Lap. en dim. 2

if p==0                                 % Jacobi
    N = triu(D,1) + tril(D,-1);
    M = 1 / (4 * (n+1)^2);
elseif p==1                             % Gauss-Seidel
    N = triu(D,1);
    M = D - N; M = -M \ kron(I,I);
end

u0 = kron(ones(n,1),ones(n,1));         % estime initiale
err= 1; iter = 1;                       % initialisation
tol= eps; itermax = 1e3;                % parametres d'iteration
while err > tol && iter < itermax
    u  = M*N*u0 - M*F;
    u0 = u;
    err= norm(D*u - F);
    iter = iter+1;
end