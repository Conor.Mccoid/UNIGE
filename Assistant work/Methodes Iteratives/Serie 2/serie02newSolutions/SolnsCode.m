%% Espace propre (ex4)
G = [5/2, -1; 1, 0];                        % matrice G
v0= [1;2]; v1= G*v0; v2= G*v1;              % comportement de v0
u0= [2;1]; u1= G*u0; u2= G*u1;              % comportement de u0

x0= linspace(0,2*pi,1000); x0= [x0, x0(:,1)];
x0= sqrt(5)*[cos(x0);sin(x0)];              % anneau
x1= G*x0; x2= G*x1;                         % comportement de l'anneau

figure(1)
clf
hold on
plot(x0(1,:),x0(2,:),'b--',...              % anneau
     x1(1,:),x1(2,:),'r--',...              % anneau apres 1 appl. de G
     x2(1,:),x2(2,:),'m--')                 % anneau apres 2 appl. de G
quiver(zeros(1,4),zeros(1,4),...
    [v0(1),u0(1),-v0(1),-u0(1)],...         % v0 et u0
    [v0(2),u0(2),-v0(2),-u0(2)],'b')
quiver(zeros(1,4),zeros(1,4),...
    [v1(1),u1(1),-v1(1),-u1(1)],...         % v0 et u0 apres 1 appl. de G
    [v1(2),u1(2),-v1(2),-u1(2)],'r')
quiver(zeros(1,4),zeros(1,4),...
    [v2(1),u2(1),-v2(1),-u2(1)],...         % v0 et u0 apres 2 appl. de G
    [v2(2),u2(2),-v2(2),-u2(2)],'m')
hold off
axis equal
line([0 0], ylim,'color','k')               % les axes
line(xlim, [0 0],'color','k')

%% Vecteurs propres en dimension une (ex5)
n = 100;                                % discretization
x = linspace(0,1,n+2)';                 % interval unitaire

k = 5;                                  % nombre des vecteurs propres
Dex = -(1:k).^2 * pi^2;                 % valeurs propres exactes
Vex = sin(x * sqrt(-Dex));              % vecteurs propres exacts

L = Laplacian1D(n);                     % Laplacien discret en dim. 1
[V,D] = eigs(L,k,'smallestabs');        % val./vect. propres discrets
V = V./abs(max(V)).*sign(V(1,:));       % normalization

figure(1)                               % comparison de valeurs propres
plot(1:k,diag(D),'r*',1:k,Dex,'bo')
xlabel('# de valeur propre')
ylabel('Valeur propre')
legend('Lap. discret','Lap. exact')

figure(2)                               % comparison de vecteurs propres
plot(x(2:end-1),V,'r.',x,Vex,'b--')
xlabel('x')
ylabel('Vecteurs propres')
axis([0,1,-1,1])

%% Vecteurs propres en dimension deux (ex5)
n = 10;                                 % discretization
x = linspace(0,1,n+2)';                 % carr'e unitaire

k = 2;                                  % nombre des vecteurs propres
Dex = -(1:k).^2 * pi^2;                 % valeurs propres en dim. 1
Vex = sin(x * sqrt(-Dex));              % vecteurs propres en dim. 1
Dex = kron(Dex,ones(1,k)) + kron(ones(1,k),Dex);
[Dex,ind] = sort(Dex,'descend');        % valeurs propres exactes et tri'e
Vex = kron(Vex,Vex);
Vex = Vex(:,ind);                       % vecteurs propres exacts et tri'e

L = Laplacian1D(n); I = speye(n);
L = kron(I,L) + kron(L,I);              % Laplacien discret en dim. 2
[V,D] = eigs(L,k^2,'smallestabs');      % val./vect. propres discrets
V = V./abs(max(V)).*sign(V(1,:));       % normalization

figure(1)                               % comparison de valeurs propres
plot(1:k^2,diag(D),'r*',1:k^2,Dex,'bo')
xlabel('#')
ylabel('Valeur propre')
legend('Lap. discret','Lap. exact')

figure(2)                               % les vecteurs propres exacts
for i = 1:k^2
    subplot(k,k,i)
    surf(x,x',reshape(Vex(:,i),n+2,n+2))
end

figure(3)                               % les vecteurs propres discrets
for i = 1:k^2
    subplot(k,k,i)
    surf(x(2:end-1),x(2:end-1)',reshape(V(:,i),n,n))
end

%% Heat2D (ex6)
n = 10; x = linspace(0,1,n+2);          % discretisation
uex= -0.25*(x'-0.5).^2 * (x-0.5).^2;    % solution exacte
g  = [ uex(2:end-1,1)  ;...             % bord a y=0
       uex(2:end-1,end);...             % bord a y=1
       uex(1,2:end-1)' ;...             % bord a x=0
       uex(end,2:end-1)'];              % bord a x=1
   
x  = x(2:end-1);
f  = -0.5*( (x'-0.5).^2+(x-0.5).^2 );   % 'forcing' sur l'interieur
u  = Heat2D(f(:),g,0);                  % solution approx.

u  = reshape(u,n,n);                    % solution en dim. 2
uex= uex(2:end-1,2:end-1);              % l'interieur

figure(1)
subplot(1,3,1)                          % solution exacte sur l'interieur
surf(x,x',uex)
xlabel('x')
ylabel('y')
zlabel('Exact solution')
subplot(1,3,2)                          % solution approx. sur l'interieur
surf(x,x',u)
xlabel('x')
ylabel('y')
zlabel('Approximate solution')
subplot(1,3,3)                          % erreur
surf(x,x',abs(u-uex))
xlabel('x')
ylabel('y')
zlabel('Error')