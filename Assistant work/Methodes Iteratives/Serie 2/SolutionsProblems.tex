\documentclass{book}

\usepackage{hyperref}
\usepackage{amsthm}
\usepackage{amsmath}
\usepackage{amssymb}

\newtheorem{solution}{Solution}
\newcommand{\diag}{\text{diag}}

\newcommand{\R}{\mathbb{R}}
\newcommand{\C}{\mathbb{C}}
\renewcommand{\vec}[1]{\mathbf{#1}}
\newcommand{\tr}{\mbox{tr}}

\begin{document}

\chapter{Solutions of the problems}

\section{Problems in Chapter 1}

\begin{solution}[Problem \ref{prob:1.1}]
First, assuming sufficient regularity of $u$, we consider the following expansions
\begin{equation*}
\begin{split}
u(x_i+h,y_j) &= u(x_i,y_j) + h \partial_x u(x_i,y_j) + \frac{h^2}{2} \partial_{xx} u(x_i,y_j) 
+ \frac{h^3}{6} \partial_{xxx} u(x_i,y_j) + O(h^4), \\
u(x_i-h,y_j) &= u(x_i,y_j) - h \partial_x u(x_i,y_j) + \frac{h^2}{2} \partial_{xx} u(x_i,y_j) 
- \frac{h^3}{6} \partial_{xxx} u(x_i,y_j) + O(h^4). \\
\end{split}
\end{equation*}
Summing term-by-term, we obtain
\begin{equation*}
u(x_i+h,y_j) + u(x_i-h,y_j) = 2 u(x_i,y_j) + h^2 \partial_{xx} u(x_i,y_j) + O(h^4),
\end{equation*}
which is equivalent to
\begin{equation*}
\frac{ u(x_i+h,y_j) + u(x_i-h,y_j) - 2 u(x_i,y_j)}{h^2} = \partial_{xx} u(x_i,y_j) + O(h^2).
\end{equation*}
Repeating the same arguments for $u(x_i,y_j+h)$ and $u(x_i,y_j-h)$, we obtain
\begin{equation*}
\frac{ u(x_i,y_j+h) + u(x_i,y_j-h) - 2 u(x_i,y_j)}{h^2} = \partial_{yy} u(x_i,y_j) + O(h^2).
\end{equation*}
And summing the last two equalities, we obtain that
\begin{equation*}
D^2(u) = \frac{ u(x_i+h,y_j) + u(x_i-h,y_j) - 4 u(x_i,y_j) +u(x_i,y_j+h) + u(x_i,y_j-h)}{h^2},
\end{equation*}
which means that $D^2(u) = \Delta u + O(h^2)$. Hence $D^2(u)$ is a second-order approximation of the Laplacian.
\end{solution}

\section{Problems in Chapter 2} 

\begin{solution}[Problem \ref{prob:2.1}]
\underline{Proof of $\|A\|_1 = \max_{1\leq j \leq m} \sum_{i=1}^n |a_{ij}|$}.\\
We recall the definition
$\|A\|_1 := \max_{\vec{x} \neq 0} \frac{\|A \vec{x} \|_1}{\| \vec{x} \|_1}$,
and estimate $\|A \vec{x} \|_1$ for any $\vec{x} \neq 0$:
\begin{equation*}
\begin{split}
\|A \vec{x}\|_1 &= \sum_{i=1}^n | (A \vec{x})_i | = \sum_{i=1}^n \bigl| \sum_{j=1}^m a_{ij} x_j \bigr| \\
&\leq \sum_{i=1}^n \sum_{j=1}^m | a_{ij} | | x_j | 
\leq \Biggl( \max_{1\leq j \leq m} \sum_{i=1}^n | a_{ij} | \Biggr) \sum_{j=1}^m | x_j |
= \Biggl( \max_{1\leq j \leq m} \sum_{i=1}^n | a_{ij} | \Biggr) \| \vec{x} \|_1.
\end{split}
\end{equation*}
Therefore, for any $\vec{x} \neq 0$, we have that 
\begin{equation*}
\frac{\|A \vec{x} \|_1}{\| \vec{x} \|_1} \leq \max_{1\leq j \leq m} \sum_{i=1}^n | a_{ij} | .
\end{equation*}
Now, we need to find a vector $\vec{y} \neq 0$ such that the maximum in the definition is attained, that is
\begin{equation*}
\frac{\|A \vec{y} \|_1}{\| \vec{y} \|_1} = \max_{1\leq j \leq m} \sum_{i=1}^n | a_{ij} | .
\end{equation*}
To do so, we assume without loss of generality that the $k$th column of $A$ is such that
\begin{equation*}
\sum_{i=1}^n | a_{ik} | = \max_{1\leq j \leq m} \sum_{i=1}^n | a_{ij} |,
\end{equation*}
and we consider $\vec{y} = \vec{e}_k$, where $\vec{e}_k$ is the $k$th canonical vector.
Therefore, we notice that $\|\vec{e}_k\|_1 = 1$ and obtain  
\begin{equation*}
\frac{\|A \vec{y} \|_1}{\| \vec{y} \|_1} = \frac{\|A \vec{e}_k \|_1}{\| \vec{e}_k \|_1} = \|A \vec{e}_k \|_1
= \sum_{i=1}^n | a_{ik} | = \max_{1\leq j \leq m} \sum_{i=1}^n | a_{ij} | ,
\end{equation*}
and the claim follows.\\

\noindent
\underline{Proof of $\|A\|_\infty = \max_{1\leq i \leq n} \sum_{j=1}^m |a_{ij}|$}.\\
We recall the definition
$\|A\|_\infty := \max_{\vec{x} \neq 0} \frac{\|A \vec{x} \|_\infty}{\| \vec{x} \|_\infty}$,
and estimate $\|A \vec{x} \|_\infty$ for any $\vec{x} \neq 0$:
\begin{equation*}
\begin{split}
\|A \vec{x} \|_\infty &= \max_{1 \leq i \leq n} \Bigl| \sum_{j=1}^m a_{ij} x_j \Bigr|
\leq \max_{1 \leq i \leq n} \sum_{j=1}^m | a_{ij} | | x_j | \\
&\leq \max_{1 \leq j \leq n} | x_j | \max_{1 \leq i \leq n} \sum_{j=1}^m | a_{ij} |
= \| \vec{x} \|_{\infty} \max_{1 \leq i \leq n} \sum_{j=1}^m | a_{ij} |.
\end{split}
\end{equation*}
This implies that
\begin{equation*}
\frac{\|A \vec{x} \|_\infty}{\| \vec{x} \|_\infty} \leq \max_{1 \leq i \leq n} \sum_{j=1}^m | a_{ij} | .
\end{equation*}
Now, we need to find a vector $\vec{y} \neq 0$ such that the maximum in the definition is attained, that is
\begin{equation*}
\frac{\|A \vec{y} \|_\infty}{\| \vec{y} \|_\infty} = \max_{1 \leq i \leq n} \sum_{j=1}^m | a_{ij} | .
\end{equation*}
To do so, we write the matrix $A$ as
\begin{equation*}
A = \begin{bmatrix}
\vec{a}_1 \\
\vdots \\
\vec{a}_n \\
\end{bmatrix},
\end{equation*}
where $\vec{a}_j \in \C^{1 \times m}$, and we assume without loss of generality that
the $k$th vector is such that
\begin{equation*}
\| \vec{a}_k \|_1 = \sum_{j=1}^m |a_{kj}| 
= \max_{1 \leq i \leq n} \sum_{j=1}^m | a_{ij} | 
= \max_{1 \leq i \leq n} \| \vec{a}_i \|_1 .
\end{equation*}
We define the vector $\vec{y} \in \C^{m\times 1}$ such that
\begin{equation*}
y_j := \begin{cases}
0 & \text{if $a_{jk} = 0$}, \\
\frac{\overline{a_{jk}}}{|a_{jk}|} & \text{otherwise}, \\
\end{cases}
\end{equation*}
where $\overline{a_{jk}}$ is the complex conjugate of $a_{jk}$. Now, we have that
\begin{equation*}
A \vec{y} = 
\begin{bmatrix}
\vec{a}_1 \vec{y} \\
\vdots \\
\vec{a}_n \vec{y} \\
\end{bmatrix},
\end{equation*}
and notice that $\| \vec{y} \|_{\infty} = 1$ and
\begin{equation*}
| \vec{a}_k \vec{y} | = \Bigl| \sum_{j=1}^m \frac{a_{jk} \overline{a_{jk}}}{|a_{jk}|} \Bigr| = \sum_{j=1}^m |a_{jk}| = \| \vec{a}_k \|_1.
\end{equation*}
For $\ell=1,\dots,n$ we observe that
\begin{equation*}
| \vec{a}_\ell \vec{y} | = \Bigl| \sum_{j=1}^m \frac{a_{j\ell} \overline{a_{jk}}}{|a_{jk}|} \Bigr| 
\leq \sum_{j=1}^m |a_{j\ell}| = \| \vec{a}_\ell \|_1 \leq \| \vec{a}_k \|_1.
\end{equation*}
Therefore, we have obtained
\begin{equation*}
\frac{\|A \vec{y} \|_\infty}{\| \vec{y} \|_\infty} = \| \vec{a}_k \|_1 = \max_{1\leq i \leq n} \sum_{j=1}^m | a_{ij} | ,
\end{equation*}
and the claim follows.\\


\noindent
\underline{Proof of $\|A\|_2 = \sqrt{\rho(A^* A)}$}.\\

For any vector $\vec{x}$ we write
\begin{equation}
0 \leq \| A \vec{x} \|_2^2 = \vec{x}^* A^* A \vec{x},
\end{equation}
which implies that $A^* A$ is positive semi-definite.

Next, we prove the claim in two different ways.
For the first proof, we notice that $A^* A$ is Hermitian, and recalling the variational characterization
of eigenvalues of a Hermitian matrix, we obtain that
\begin{equation*}
\lambda_{max}(A^* A) = \max_{\vec{x} \neq 0} \frac{\vec{x}^* A^* A \vec{x}}{\vec{x}^* \vec{x}}
= \max_{\vec{x} \neq 0} \frac{\| A \vec{x} \|_2^2}{\| \vec{x} \|_2^2},
\end{equation*}
and noticing that the eigenvalues of $A^* A$ are non-negative, the claim follows.

The second proof is based on the Schur decomposition\index{Schur decomposition} of $A^* A$,
see, e.g., \cite[Theorem 1.3.6]{greenbaum1997iterative}.
Since $A^* A$ is Hermitian its Schur decomposition is $A^* A = U^* D U$, where $U$ is a unitary matrix
and $D$ is a diagonal matrix having on the diagonal the eigenvalues of $A^* A$: 
$D = \diag(\lambda_k(A^* A))$\footnote{Notice that this coincides with the fact that a Hermitian matrix is diagonalizable.}.
Using the Schur decomposition of $A^* A$ and the Cauchy-Schwarz inequality, we obtain
\begin{equation*}
\| A \vec{x} \|_2^2 = \vec{x}^* U^* D U \vec{x} \leq \| U \vec{x} \|_2 \| D U \vec{x} \|_2.
\end{equation*}
Next, we define $\vec{w} = U \vec{x}$ and compute
\begin{equation*}
\| D U \vec{x} \|_2^2 = \| D \vec{w} \|_2^2 = \sum_{j=1}^n | \lambda_j(A^* A) w_j|^2 \leq \rho(A^* A)^2 \| \vec{w} \|_2^2.
\end{equation*}
Now, since $U$ is unitary, it is easy to see that $\| \vec{w} \|_2 = \| U \vec{x} \|_2 = \| \vec{x} \|_2$.
Therefore, we obtain
\begin{equation*}
\| A \vec{x} \|_2^2 \leq \rho(A^* A) \| \vec{x} \|_2^2,
\end{equation*}
which implies that
\begin{equation*}
\frac{\| A \vec{x} \|_2}{\| \vec{x} \|_2} \leq \sqrt{\rho(A^* A)}.
\end{equation*}
Now, to show that the maximum in the definition is attained it is sufficient to recall that
$A^* A$ is positive semi-definite and choose a vector $\vec{y}$ as the eigenvector of $A^* A$ that
corresponds to $\lambda_{max}(A^* A) = \rho(A^* A)$.
Therefore, we have
\begin{equation*}
\| A \vec{y} \|_2^2 = \vec{y}^* ( A^* A ) \vec{y} = \rho(A^* A) \| \vec{y} \|_2^2,
\end{equation*}
and the claim follows.\\

\noindent
\underline{Proof of $\|A\|_F = \sqrt{\tr(A^* A)} = \sqrt{\tr(A A^*)}$}.\\

Notice that $(A^* A)_{jj} = \sum_{k=1}^n \overline{a_{kj}} a_{kj} = \sum_{k=1}^n | a_{kj} |^2$.
Now, we compute trace of $A^* A$ and obtain that
\begin{equation*}
\tr(A^* A) = \sum_{j=1}^n (A^* A)_{jj} = \sum_{j=1}^n \sum_{k=1}^n | a_{kj} |^2 = \|A\|_F^2.
\end{equation*}
Recalling the property of the trace $\tr(AB) = \tr(BA)$ for any $A$ and $B$, the claim follows.

\end{solution}

\begin{solution}[Problem \ref{prob:2.2}]
To show the sub-multiplicative property of the Frobenius norm, consider two matrices $A \in \C^{n \times m}$ and
$B \in \C^{m \times p}$. We write these matrices as
\begin{equation*}
A = \begin{bmatrix}
\vec{a}_1^\top   \\
\vdots \\
\vec{a}_n^\top   \\
\end{bmatrix}
\quad \text{and} \quad
B = \begin{bmatrix}
\vec{b}_1 & \cdots & \vec{b}_p \\
\end{bmatrix},
\end{equation*}
where $\vec{a}_j$ and $\vec{b}_j$ are column vectors in $\C^{m\times 1}$
Now, using the Cauchy-Schwarz inequality, we write
\begin{equation*}
\begin{split}
\| A B \|_F^2 &= \sum_{j=1}^n \sum_{k=1}^p | \vec{a}_j^\top   \vec{b}_k |^2
\leq \sum_{j=1}^n \sum_{k=1}^p \| \vec{a}_j \|_2^2 \| \vec{b}_k \|_2^2 \\
&= \Biggl( \sum_{j=1}^n \| \vec{a}_j \|_2^2 \Biggr) \Biggl( \sum_{k=1}^p \| \vec{b}_k \|_2^2 \Biggr) 
= \| A \|_F^2 \| B \|_F^2,
\end{split}
\end{equation*}
and the claim follows by taking the square root.

Notice that the Frobenius norm is not an induced $p$-norm.
To show it notice that any induced $p$-norm of the $n \times n$ identity equals $1$: $\|I\|_p=1$.
However we have that $\|I\|_F=n$.
\end{solution}

\begin{solution}[Problem \ref{prob:2.3}]
The inequality $\rho(A) \leq \|A\|$ is proved in Lemma \ref{rhosmallernorm} and holds for any matrix norm $\| \cdot \|$.

To prove the other bound, we consider the Jordan decomposition of $A$, that is $A = V J V^{-1}$,
where $V \in \C^{n \times n}$ is an invertible matrix and $J$ is a block-diagonal matrix.
The number of blocks in $J$ corresponds to the number $s$ of distinct eigenvalues of $A$, and
each block is given by
\begin{equation*}
J_k = \begin{bmatrix}
\lambda_k & 1 \\
& \lambda_k & 1 \\
& & \ddots & \ddots \\
& & & \lambda_k & 1 \\
& & & & \lambda_k \\
\end{bmatrix} \in \C^{m_k\times m_k},
\end{equation*}
where $m_k$ is the multiplicity of the eigenvalue $\lambda_k$.
Next, we consider a matrix $D_t = \diag(t,t^2,\dots,t^n) \in \R^{n\times n}$ where $t>0$ is arbitrary.
Now, we decompose the diagonal of $D$ in $s$ blocks as follows
\begin{equation*}
D_t = \begin{bmatrix}
D_1 \\
& D_2 \\
& & \ddots \\
& & & D_s \\
\end{bmatrix},
\end{equation*}
such that $D_k \in \R^{m_k \times m_k}$, and we notice that
\begin{equation*}
D_k J_k D_k^{-1} 
= \begin{bmatrix}
\lambda_k & \frac{1}{t} \\
& \lambda_k & \frac{1}{t} \\
& & \ddots & \ddots \\
& & & \lambda_k & \frac{1}{t} \\
& & & & \lambda_k \\
\end{bmatrix}
= \lambda_k I + \frac{1}{t} \widetilde{N},
\end{equation*}
where $\widetilde{N}$ is the following nilpotent matrix
\begin{equation*}
\widetilde{N} 
= \begin{bmatrix}
0 & 1 \\
& 0 & 1 \\
& & \ddots & \ddots \\
& & & 0 & 1 \\
& & & & 0 \\
\end{bmatrix}.
\end{equation*}
Now, it is easy to see that
\begin{equation*}
\| D_k J_k D_k^{-1} \|_1 = \| \lambda_k I + \frac{1}{t} \widetilde{N} \|_1 \leq |\lambda_k| + \frac{1}{t}.
\end{equation*}
Similarly, we have
\begin{equation*}
\| D_t J D_t^{-1} \|_1 \leq \max_k|\lambda_k| + \frac{1}{t} = \rho(A) + \varepsilon.
\end{equation*}
Next, we write
\begin{equation*}
\begin{split}
\rho(A) + \varepsilon &\geq \| D_t J D_t^{-1} \|_1 = \| D_t V^{-1} V J V^{-1} V D_t^{-1} \|_1 \\
&= \| D_t V^{-1} A V D_t^{-1} \|_1 = \| G A G^{-1} \|_1 = \| A \|_G, \\
\end{split}
\end{equation*}
where $G= D_t V^{-1}$ and $\| B \|_G := \| G B G^{-1} \|_1$ for any $B \in \C^{n \times n}$.
Therefore, we have obtained that $\| A \|_G \leq \rho(A) + \varepsilon$.
It remains to show that the matrix norm $\| \cdot \|_G$ is induced by a vector norm.
To do so, we first write that
\begin{equation*}
\| B \|_G := \| G B G^{-1} \|_1 = \max_{\vec{y} \neq 0} \frac{ \| G B G^{-1} \vec{y} \|_1}{\|\vec{y}\|_1}.
\end{equation*}
Then, since $G$ is invertible, we define $\vec{w}:=G^{-1} \vec{y}$ (and then $\vec{y} = G \vec{w}$) and write
\begin{equation*}
\| B \|_G = \max_{\vec{y} \neq 0} \frac{ \| G B G^{-1} \vec{y}\|_1}{\|\vec{y}\|_1}
= \max_{w \neq 0} \frac{ \| G B \vec{w} \|_1}{\|G \vec{w}\|_1}
= \max_{w \neq 0} \frac{ \| B \vec{w} \|_g}{\|\vec{w}\|_g},
\end{equation*}
where $\| \vec{v} \|_g := \| G \vec{v} \|_1$ is the vector norm that induces the matrix norm $\| \cdot \|_G$.
\end{solution}

\begin{solution}[Problem \ref{prob:2.4}]
  Let $\vec{u}$ and $\lambda$ be an eigenpair of $G$. Then 
\[
  G\vec{u} = \lambda\vec{u}\quad \Longrightarrow\quad
    G^k\vec{u} = \lambda^k\vec{u}
\]
and using $\|G^k\vec{u}\|\le \|G^k\| \, \|\vec{u}\|$, we find
\[
  |\lambda|^k\|\vec{u}\| \le \|G^k\|\, \|\vec{u}\|\quad \Longrightarrow  
  \quad |\lambda| \le \|G^k\|^{\frac{1}{k}}, 
\]
which implies that $\rho(G) \le \|G^k\|^{\frac{1}{k}}$. 

On the other hand, let $\varepsilon >0$ be given and consider the matrix
\[
   G(\varepsilon) = \frac{1}{\rho(G)+\varepsilon} G.
\]
If $\lambda$ is an eigenvalue of $G$, then $G(\varepsilon)$ has the 
eigenvalue  $\lambda/(\rho(G)+\varepsilon)$, and therefore 
\[
  \rho(G(\varepsilon)) = \frac{\rho(G)}{\rho(G)+\varepsilon} <1.
\]
Hence $\lim_{k\to\infty} \|G(\varepsilon)^k\| = 0$, which means that for $k$
sufficiently large, we have
\[
  \|G(\varepsilon)^k\| = \frac{\|G^k\|}{(\rho(G)+\varepsilon)^k} <1 \iff
  \|G^k\|^{\frac{1}{k}} < \rho(G)+\varepsilon\quad \mbox{for all $\varepsilon>0$}.
\]
Combining both bounds, we get for large $k$ and any $\varepsilon>0$
\[
  \rho(G) \le \|G^k\|^{\frac{1}{k}} <\rho(G)+\varepsilon
\]
which completes the proof. 
\end{solution}


\begin{solution}[Problem \ref{prob:2.5}]
First, we show that $\lim_{k \rightarrow \infty} \binom{k}{p}^{\frac{1}{k}} = 1$. 
To do so, we recall that (for $k\geq p$) we have $\binom{k}{p} = \frac{ k! }{ p! (k-p)! }$.
Now, we can write
\begin{equation*}
k! = (k-p)! \, ( k-p+1 ) \, ( k-p+2 ) \cdots ( k-p+(p-1) ) \, k
= (k-p)! \, \prod_{j=1}^p \bigl(k-(p-j)\bigr),
\end{equation*}
and we notice that
\begin{equation*}
\prod_{j=1}^p \bigl(k-(p-j)\bigr) = k^p + c_{p-1} k^{p-1} + \cdots + c_1 k + c_0 =: p_p(k),
\end{equation*}
where the leading term has coefficient $1$.
Therefore, using the previous two equations, we obtain
\begin{equation*}
\binom{k}{p}^{\frac{1}{k}} = \Biggl( \frac{ (k-p)! \, p_p(k) }{ p! \, (k-p)! } \Biggr)^{\frac{1}{k}} 
= \frac{ p_p(k)^{\frac{1}{k}} }{ (p!)^{\frac{1}{k}} }.
\end{equation*}
Now, we notice that $\lim_{k\rightarrow \infty} (p!)^{\frac{1}{k}} = 1$ and it remains to
study $p_p(k)^{\frac{1}{k}}$. To do so, we write the following
\begin{equation*}
\begin{split}
\lim_{k \rightarrow \infty} p_p(k)^{\frac{1}{k}} 
&= \lim_{k \rightarrow \infty} \Bigl( k^p + c_{p-1} k^{p-1} + \cdots + c_1 k + c_0 \Bigr)^{\frac{1}{k}} \\
&= \lim_{k \rightarrow \infty} \Bigl[ k^p \, \Bigl( 1 + c_{p-1} k^{-1} + \cdots + c_1 k^{-(p-1)} + c_0 k^{-p} \Bigr) \Bigr]^{\frac{1}{k}}\\
&= \lim_{k \rightarrow \infty} \Bigl[ k^\frac{p}{k} \, \Bigl( 1 + c_{p-1} k^{-1} + \cdots + c_1 k^{-(p-1)} + c_0 k^{-p} \Bigr)^{\frac{1}{k}} \Bigr].
\end{split}
\end{equation*}
It is clear that $\lim_{k \rightarrow \infty} \Bigl( 1 + c_{p-1} k^{-1} + \cdots + c_1 k^{-(p-1)} + c_0 k^{-p} \Bigr)^{\frac{1}{k}} = 1$. Therefore, it is sufficient to study the leading term $k^\frac{p}{k}$. We have
\begin{equation*}
k^\frac{p}{k} = \exp\Bigl( \frac{p}{k} \log(k) \Bigr).
\end{equation*}
Notice that $\frac{p}{k} \log(k) \leq \frac{2p}{k} \log(\sqrt{k}) \leq  \frac{2p}{k} \sqrt{k}$.
Now, we have that $\lim_{k \rightarrow \infty} \frac{p}{k} \log(k) = 0$ and hence
$\lim_{k \rightarrow \infty} k^\frac{p}{k} = \lim_{k \rightarrow \infty} \exp\Bigl( \frac{p}{k} \log(k) \Bigr) = 1$.
Therefore, we conclude that $\lim_{k \rightarrow \infty} \binom{k}{p}^{\frac{1}{k}} = 1$.

Next, we show that for any $\rho<1$ we have that $\lim_{k\rightarrow \infty} \binom{k}{p} \rho^k = 0$.
We will prove the claim in two different ways.
The first way uses the result obtained in the first part. First, we write
$\binom{k}{p}\rho^k = \bigl(\binom{k}{p}^{\frac{1}{k}}\rho)^k = \exp\bigl( k \log\bigl( \binom{k}{p}^{\frac{1}{k}}\rho \bigr) \bigr)$. Since $\lim_{k \rightarrow \infty} \binom{k}{p}^{\frac{1}{k}}\rho = \rho \in (0,1)$,
then $\lim_{k \rightarrow \infty} \log\bigl( \binom{k}{p}^{\frac{1}{k}}\rho \bigr) = \log( \rho ) < 0$.
Therefore, $\lim_{k \rightarrow \infty} k \log\bigl( \binom{k}{p}^{\frac{1}{k}}\rho \bigr) = -\infty$,
which implies that 
\begin{equation*}
\lim_{k \rightarrow \infty} \binom{k}{p}\rho^k = \lim_{k \rightarrow \infty} \exp\Bigl[ k \log\Bigl( \binom{k}{p}^{\frac{1}{k}}\rho \Bigr) \Bigr] = 0.
\end{equation*}

The second way works as follows.
As in the first part, we have that $\binom{k}{p} = \frac{ p_p(k) }{ p!}$.
Now, we define the sequence $\{ a_k \}_k$ as $a_k := \frac{ p_p(k) }{ p! } \rho^k$.
To study the convergence of $a_k$, we can use the ratio test and compute
\begin{equation*}
\frac{a_{k+1}}{a_k} = \frac{ p_p(k+1) }{ p! } \rho^{k+1} \frac{p!}{ p_p(k) } \frac{1}{\rho^{k}}
= \frac{ p_p(k+1) }{ p_p(k) } \rho,
\end{equation*}
and recalling the definition of $p_p(k)$, we have
\begin{equation*}
\frac{a_{k+1}}{a_k} 
= \frac{ k^p + \widetilde{c}_{p-1} k^{p-1} + \cdots + \widetilde{c}_1 k + \widetilde{c}_0 }{ k^p + c_{p-1} k^{p-1} + \cdots + c_1 k + c_0 } \rho.
\end{equation*}
It is then clear that 
\begin{equation*}
\lim_{k\rightarrow \infty} \frac{ k^p + \widetilde{c}_{p-1} k^{p-1} + \cdots + \widetilde{c}_1 k + \widetilde{c}_0 }{ k^p + c_{p-1} k^{p-1} + \cdots + c_1 k + c_0 } 
= 1 .
\end{equation*}
Therefore, $\lim_{k \rightarrow \infty} \frac{a_{k+1}}{a_k} = \rho < 1$. This implies that $a_k \rightarrow 0$, 
that is \linebreak $\lim_{k\rightarrow \infty} \binom{k}{p} \rho^k = 0$.
\end{solution}


\begin{solution}[Problem \ref{prob:2.6}]
Since $\rho(B) < 1$, then $1 \notin \sigma(B)$, where $\sigma(B)$ is the spectrum of $B$.
Hence, $\det(I-B)\neq 0$, which means that $(I-B)$ is invertible.

To show the Neumann series, we first consider the partial sum $\sum_{j=0}^n B^j$
and multiply it by $(I-B)$ to get
\begin{equation*}
(I-B) \sum_{j=0}^n B^j = \sum_{j=0}^n B^j - B^{j+1} = B^0 - B^{n+1} = I - B^{n+1},
\end{equation*}
where we used the fact that the second term is a telescoping sum.
Now, we multiply both terms by $(I-B)^{-1}$ to obtain
\begin{equation*}
\sum_{j=0}^n B^j = (I-B)^{-1} ( I - B^{n+1} ) = (I-B)^{-1} - (I-B)^{-1} B^{n+1}.
\end{equation*}
By subtracting $(I-B)^{-1}$, we get
\begin{equation*}
\sum_{j=0}^n B^j - (I-B)^{-1} = - (I-B)^{-1} B^{n+1}.
\end{equation*}
Recalling Problem \ref{prob:2.3}, for any arbitrary $\epsilon>0$ and $k$ large enough
we have that $\| B^k \| \leq ( \rho(B) + \epsilon )^k$.
Now, we write
\begin{equation*}
\| \sum_{j=0}^n B^j - (I-B)^{-1} \| = \| (I-B)^{-1} B^{n+1} \| \leq \| (I-B)^{-1} \| \| B^{n+1} \| \leq \| (I-B)^{-1} \| ( \rho(B) + \epsilon )^{n+1}.
\end{equation*}
Since $\rho(B)<1$, we can choose $\epsilon$ sufficiently small such
that $( \rho(B) + \epsilon ) < 1$ holds.\footnote{Notice also that $\rho(B)<1$, we can find a matrix norm $\| \cdot \|$
such that $\| B \| < 1$ (see Problem \ref{prob:2.3}), therefore we have $\|B^k\|\leq\|B\|^k$
and $\lim_{k\rightarrow\infty}\|B\|^k = 0$.}
Taking the limit for $n \rightarrow \infty$ we get
\begin{equation*}
\lim_{n \rightarrow \infty} \| \sum_{j=0}^n B^j - (I-B)^{-1} \| \leq
\lim_{n \rightarrow \infty} \| (I-B)^{-1} \| ( \rho(B) + \epsilon )^{n+1} = 0.
\end{equation*}
This means that
\begin{equation*}
\lim_{n \rightarrow \infty} \sum_{j=0}^n B^j = (I-B)^{-1}.
\end{equation*}
\end{solution}

\begin{solution}[Problem \ref{prob:2.7}]
Consider the discrete Laplacian in 1D:
\begin{equation*}
A = \begin{pmatrix}
2 & -1 \\
-1 & 2 & -1 \\
& -1 & 2 & -1 \\
& &  & \ddots & \\
\end{pmatrix}\in \R^{n\times n}.
\end{equation*}
Obviously, $a_{jj} > 0$ and $a_{ij} \leq 0$ for $i,j=1,\dots,n$ and $i \neq j$.
Hence, it remains to show that $A$ is invertible and $A^{-1} \geq 0$ (non-negative).
Since $A$ is tridiagonal, we can compute its eigenvalues, that are
\begin{equation*}
\lambda_k(A) = 2 + 2 \cos\Bigl( \frac{ k \pi }{ n+1 } \Bigr)
\end{equation*} 
for $k = 1 , \dots , n$. Since $0<\frac{k}{n+1}<1$, we have that $-1<\cos\Bigl( \frac{ k \pi }{ n+1 } \Bigr)<1$.
Therefore, $\lambda_k(A) > 0$, which means that $A$ is positive definite, hence invertible.
To show that $A^{-1} \geq 0$ we can use Theorem \ref{regsplitth}. To do so, we need to consider
a regular splitting $A = M - N$ and show that $\rho(M^{-1}N)<1$.
We consider the splitting matrices
\begin{equation*}
M = \begin{bmatrix}
2 & \\
& 2 & \\
& & 2 & \\
& &  & \ddots & \\
\end{bmatrix}
\quad \text{and} \quad
N = \begin{bmatrix}
0 & 1 \\
1 & 0 & 1 \\
& 1 & 0 & 1 \\
& &  & \ddots & \\
\end{bmatrix}.
\end{equation*}
Notice that $M^{-1}=\frac{1}{2}I \geq 0$ and $N \geq 0$, which means that the splitting $A=M-N$ is regular.
Now, we write
\begin{equation*}
M^{-1}N = M^{-1} ( M - A ) = I - M^{-1}A = I - \frac{1}{2}A.
\end{equation*}
Therefore, the eigenvalues of $M^{-1}N$ are, for $k=1,\dots,n$, given by
\begin{equation*}
\lambda_k(M^{-1}N) = 1 - \frac{1}{2}\lambda_k(A) = 1 - \frac{1}{2}\Bigl[ 2 + 2 \cos\Bigl( \frac{ k \pi }{ n+1 } \Bigr) \Bigr]
= - \cos\Bigl( \frac{ k \pi }{ n+1 } \Bigr),
\end{equation*}
and we easily see that $|\lambda_k(M^{-1}N)|<1$. Hence $\rho(M^{-1}N)<1$ and we can invoke Theorem \ref{regsplitth}
to obtain the claim.

Next, consider the Laplacian in 2D given by the following block-tridiagonal matrix
\begin{equation*}
 A = \begin{bmatrix}
        T & -I \\
        -I & T & \ddots \\
          & \ddots & \ddots & -I \\
          &        &  -I     & T \\
    \end{bmatrix}
    \quad \text{with} \quad
    T = \begin{bmatrix}
      4 & -1\\
        -1 & 4 & \ddots\\
               &  \ddots  & \ddots & -1\\
               &          & -1 & 4  
   \end{bmatrix}
\end{equation*}
and $I$ the identity matrix. It is clear that $a_{ii} > 0$ and $a_{ij} \leq 0$ for $i \neq j$.
Again we want to use Theorem \ref{regsplitth}, which means that we need to find a regular splitting
$A = M - N$ such that $\rho(M^{-1}N)<1$. We consider the splitting given by $M := \diag(A) = 4 I$
and $N := M-A$. It is easy to see that $M^{-1}=\frac{1}{4}I$ and $N \geq 0$, therefore the splitting is regular.
Now, similarly as before, we have 
\begin{equation*}
M^{-1}N = M^{-1} ( M - A ) = I - M^{-1}A = I - \frac{1}{4}A,
\end{equation*}
which means that $\lambda(M^{-1}N) = 1 - \frac{1}{4}\lambda(A)$. Therefore, we need to compute the eigenvalues of $A$.
To to so, we recall that $A$ can be written as $A = A_x \otimes I + I \otimes A_y$,
where $A_x$ and $A_y$ are two discrete 1D-Laplacian matrices. 
Now, we consider two eigenvectors $\vec{v}_x$ and $\vec{v}_y$
of $A_x$ and $A_y$, respectively, and we write
\begin{equation*}
\begin{split}
A ( \vec{v}_x \otimes \vec{v}_y ) &= ( A_x \otimes I + I \otimes A_y ) ( \vec{v}_x \otimes \vec{v}_y ) \\
&= A_x \vec{v}_x \otimes \vec{v}_y + \vec{v}_x \otimes A_y \vec{v}_y \\
&= \lambda(A_x) \vec{v}_x \otimes \vec{v}_y + \vec{v}_x \otimes \lambda(A_y) \vec{v}_y \\
&= (\lambda(A_x) + \lambda(A_y)) (\vec{v}_x \otimes \vec{v}_y),
\end{split}
\end{equation*}
where $\lambda(A_x)$ and $\lambda(A_y)$ are eigenvalues of $A_x$ and $A_y$.
This equality implies that $( \vec{v}_x \otimes \vec{v}_y )$ is an eigenvector of $A$ and its eigenvalues
are of the form $\lambda(A_x) + \lambda(A_y)$. In particular, let $A_x \in \R^{n\times n}$ and 
$A_y \in \R^{m\times m}$ we have
\begin{equation*}
\lambda_{j,k}(A) = \lambda_k(A_x) + \lambda_j(A_y) = 4 + 2 \cos\Bigl( \frac{ k \pi }{ n+1 } \Bigr)
+ 2 \cos\Bigl( \frac{ j \pi }{ m+1 } \Bigr),
\end{equation*}
for $k=1,\dots,n$ and $j=1,\dots,m$.
Therefore, we have
\begin{equation*}
\lambda_{j,k}(M^{-1}N) = 1 - \frac{1}{4}\lambda_{j,k}(A) = - \frac{1}{2} \cos\Bigl( \frac{ k \pi }{ n+1 } \Bigr)
- \frac{1}{2} \cos\Bigl( \frac{ j \pi }{ m+1 } \Bigr),
\end{equation*}
for $k=1,\dots,n$ and $j=1,\dots,m$.
Now, it is easy to see that $|\lambda_{j,k}(M^{-1}N)| < 1$ for all $k,j$.
Therefore, $\rho(M^{-1}N) < 1$ and we can invoke Theorem \ref{regsplitth} to obtain the claim.
\end{solution}

\begin{solution}[Problem \ref{prob:2.8}]
Recalling Theorem \ref{thm:iteration_forms}, for a splitting $A = M - N$ we have that 
\begin{eqnarray*}
	\vec{u}_{k+1} &=& M^{-1}N \vec{u}_k + M^{-1}\vec{f} \qquad \quad \; \; \: \text{(standard form)}, \nonumber \\
	\vec{u}_{k+1} &=& \vec{u}_k + M^{-1}\vec{r}_k \qquad \qquad \qquad \; \, \text{(correction form)}, \nonumber \\
    \vec{e}_{k+1} &=& M^{-1}N \vec{e}_k  \qquad \qquad \qquad \qquad \text{(error form)}, \nonumber \\
    \vec{d}_{k+1} &=& M^{-1}N \vec{d}_{k} \qquad \qquad \qquad \qquad \text{(difference form)}, \nonumber \\
    \vec{r}_{k+1} &=& (I-AM^{-1})\vec{r}_{k} = N M^{-1} \vec{r}_{k} \; \text{(residual form)}. \nonumber \\
\end{eqnarray*}
Now, it is sufficient to replace into these formulas the splitting matrices $M$ and $N$ corresponding to the methods
of Jacobi, Gauss-Seidel, and SOR. For the method of Jacobi, we have
\begin{eqnarray*}
	\vec{u}_{k+1} &=& -D^{-1}(L+U) \vec{u}_k + D^{-1}\vec{f} , \nonumber \\
	\vec{u}_{k+1} &=& \vec{u}_k + D^{-1}\vec{r}_k , \nonumber \\
    \vec{e}_{k+1} &=& -D^{-1}(L+U) \vec{e}_k , \nonumber \\
    \vec{d}_{k+1} &=& -D^{-1}(L+U) \vec{d}_{k} , \nonumber \\
    \vec{r}_{k+1} &=& (I-AD^{-1})\vec{r}_{k} = -(L+U)D^{-1} \vec{r}_{k} . \nonumber \\
\end{eqnarray*}
For the method of Gauss-Seidel, we have
\begin{eqnarray*}
	\vec{u}_{k+1} &=& -(D+L)^{-1}U \vec{u}_k + (D+L)^{-1}\vec{f} , \nonumber \\
	\vec{u}_{k+1} &=& \vec{u}_k + (D+L)^{-1}\vec{r}_k , \nonumber \\
    \vec{e}_{k+1} &=& -(D+L)^{-1}U \vec{e}_k , \nonumber \\
    \vec{d}_{k+1} &=& -(D+L)^{-1}U \vec{d}_{k} , \nonumber \\
    \vec{r}_{k+1} &=& (I-A(D+L)^{-1})\vec{r}_{k} = -U(D+L)^{-1} \vec{r}_{k} . \nonumber \\
\end{eqnarray*}
Finally, for the SOR method, we have
\begin{eqnarray*}
	\vec{u}_{k+1} &=& (\frac{1}{\omega}D+L)^{-1}(-U+(\frac{1}{\omega}-1)D) \vec{u}_k + (\frac{1}{\omega}D+L)^{-1}\vec{f}, \nonumber \\
	\vec{u}_{k+1} &=& \vec{u}_k + (\frac{1}{\omega}D+L)^{-1}\vec{r}_k , \nonumber \\
    \vec{e}_{k+1} &=& (\frac{1}{\omega}D+L)^{-1}(-U+(\frac{1}{\omega}-1)D) \vec{e}_k  , \nonumber \\
    \vec{d}_{k+1} &=& (\frac{1}{\omega}D+L)^{-1}(-U+(\frac{1}{\omega}-1)D) \vec{d}_{k} , \nonumber \\
    \vec{r}_{k+1} &=& (I-A(\frac{1}{\omega}D+L)^{-1})\vec{r}_{k} = (-U+(\frac{1}{\omega}-1)D)(\frac{1}{\omega}D+L)^{-1} \vec{r}_{k} , \nonumber \\
\end{eqnarray*}
that are equivalent to
\begin{eqnarray*}
	\vec{u}_{k+1} &=& (D+\omega L)^{-1}(-\omega U+(1-\omega)D) \vec{u}_k + \omega(D+\omega L)^{-1}\vec{f}, \nonumber \\
	\vec{u}_{k+1} &=& \vec{u}_k + \omega (D+\omega L)^{-1}\vec{r}_k , \nonumber \\
    \vec{e}_{k+1} &=& (D+\omega L)^{-1}(-\omega U+(1-\omega)D) \vec{e}_k  , \nonumber \\
    \vec{d}_{k+1} &=& (D+\omega L)^{-1}(-\omega U+(1-\omega)D) \vec{d}_{k} , \nonumber \\
    \vec{r}_{k+1} &=& (I-\omega A(D+\omega L)^{-1})\vec{r}_{k} = (-\omega U+(1-\omega)D)(D+\omega L)^{-1} \vec{r}_{k} . \nonumber \\
\end{eqnarray*}
\end{solution}

\begin{solution}[Problem \ref{prob:2.10}]
  The iteration matrix of the Jacobi method is $G_{\rm J} = -D^{-1}(L+U)$,
  and with Condition \ref{diagonallydominant}, we obtain that
  \[
     \|G_{\rm J}\|_{\infty}=\max_{i \in \{1,\ldots,n\}}\frac{1}{|a_{ii}|}
        \sum_{j\ne i}|a_{ij}|<1.
  \] 
  Now using Lemma \ref{rhosmallernorm}, we obtain 
  \[
     \rho(G_{\rm J})\le\|G_{\rm J}\|_{\infty}<1,
  \] 
  which together with Theorem \ref{StationaryMethodConvergence} concludes the proof.
\end{solution}

\begin{solution}[Problem \ref{prob:2.12a}]
If we compute the infinity norms we get
\begin{equation*}
\begin{split}
&\| \vec{v}_0 \|_\infty = 2, \quad \| \vec{v}_1 \|_\infty=\| G\vec{v}_0 \|_\infty = 1, \quad \| \vec{v}_2 \|_\infty=\| G^2\vec{v}_0 \|_\infty = 0.5, \\
&\| \vec{w}_0 \|_\infty = 2, \quad \| \vec{w}_1 \|_\infty=\| G\vec{w}_0 \|_\infty = 4, \quad \| \vec{w}_2 \|_\infty=\| G^2\vec{w}_0 \|_\infty = 8. \\
\end{split}
\end{equation*}
To understand this behavior it is sufficient to compute eigenvalues and eigenvectors of $G$, that are
$\lambda_1(G)=\frac{1}{2}$ with eigenvector $\vec{u}_1=\vec{v}_0$ and
$\lambda_2(G)=2$ with eigenvector $\vec{u}_2=\vec{w}_0$. 
Iterating starting from $\vec{v}_0$, we get
$\vec{v}_1=G\vec{v}_0=\frac{1}{2}\vec{v}_0$ and
$\vec{v}_2=G\vec{v}_1=\frac{1}{4}\vec{v}_0$, which explain why the vectors $\vec{v}_k$ decay in norm.
On the other hand, iterating starting from $\vec{v}_0$, we get
$\vec{w}_1=G\vec{w}_0=2\vec{w}_0$ and
$\vec{w}_2=G\vec{w}_1=4\vec{w}_0$, which explain why the vectors $\vec{w}_k$ grow in norm.
This means that $\rho(G)>1$ does not imply that the iterations induced by $G$ always diverge.
\end{solution}

\begin{solution}[Problem \ref{prob:2.10a}]
To prove the claim we use Theorem \ref{thm11.7_block}, hence we show that its hypotheses are satisfied.
To do so, consider the matrix $-T$, that is
\begin{equation*}
-T = \begin{bmatrix}
      4 & -1\\
        -1 & 4 & \ddots\\
               &  \ddots  & \ddots & -1\\
               &          & -1 & 4  
   \end{bmatrix} \in \mathbb{R}^{p \times p}.
\end{equation*}
Since it is tridiagonal, one can easily compute its eigenvalues, that are
\begin{equation*}
\lambda_k(-T) = \Bigl[ 4 + 2 \cos \Bigl( \frac{k \pi}{p+1} \Bigr) \Bigr]
\end{equation*}
for $k=1,\dots,p$. Since $-1 < \cos \Bigl( \frac{k \pi}{n+1} \Bigr)  < 1$ for $k=1,\dots,p$,
then $\lambda_k(-T) > 2$, which means that $-T$ is positive definite.

The matrix $A$ (recall Problem \ref{prob:2.7}) has eigenvalues
\begin{equation*}
\lambda_{j,k}(A) = \lambda_k(A_x) + \lambda_j(A_y) = 4 + 2 \cos\Bigl( \frac{ k \pi }{ n+1 } \Bigr)
+ 2 \cos\Bigl( \frac{ j \pi }{ m+1 } \Bigr),
\end{equation*}
for $k=1,\dots,n$ and $j=1,\dots,m$. Therefore $\lambda(A) >0$, that is $A$ is positive definite.

Finally, we need to show that the matrix $2D-A$, that is
\begin{equation*}
2D-A = 
\begin{bmatrix} 
-T & I\\ 
I & -T & \ddots\\
&\ddots & \ddots & I\\ 
& & I & -T \\
\end{bmatrix},
\end{equation*}
is positive definite. To do so, we define the block-diagonal matrix
\begin{equation*}
S = \begin{bmatrix}
I \\
& -I \\
& & I \\
& & & -I \\
& & & & \ddots \\
\end{bmatrix},
\end{equation*}
and we notice that 
\begin{equation*}
S A S^{-1} = S A S^\top  
=\begin{bmatrix} 
-T & I\\ 
I & -T & \ddots\\
&\ddots & \ddots & I\\ 
& & I & -T \\
\end{bmatrix}
= 2D-A.
\end{equation*}
Therefore, the matrix $2D-A$ is similar to $A$, hence positive definite.

Since all the hypotheses of Theorem \ref{thm11.7_block} are fulfilled,
the block-Jacobi method converges.
\end{solution}

\begin{solution}[Problem \ref{prob:2.11}]
According to Theorem \ref{thm:Young} the optimal parameter for the SOR method is given by
\begin{equation*}
\omega_{opt} = \frac{2}{1+\sqrt{1-\rho(G_{\rm J})}},
\end{equation*}
where $G_{\rm J}$ is the Jacobi iteration matrix: $G_{\rm J} = - D^{-1}(L+L^\top  )$.
Therefore, it is sufficient to compute the spectral radius of $G_{\rm J}$.
To do so, we first notice that $D^{-1}=\frac{1}{4} I$. Then, we write
\begin{equation*}
G_{\rm J} = - D^{-1}(L+L^\top  ) = - D^{-1}(A - D) = I - \frac{1}{4}A.
\end{equation*}
Hence, the eigenvalues of $G_{\rm J}$ are $\lambda(G_{\rm J}) = 1 - \frac{1}{4} \lambda(A)$,
where the eigenvalues of $A$ are given in Problem \ref{prob:2.7} and \ref{prob:2.11}\footnote{Notice that
in real implementations the discrete Laplacian is the matrix $A$ multiplied by $\frac{1}{h^2}$.
However, this factor will appear also in front of both $D$ and $L+L^\top  $, therefore it cancels in the
product $D^{-1}(L+L^\top  )$. This means that the matrix $G_{\rm J} = - D^{-1}(L+L^\top  )$ is not affected
by the factor $\frac{1}{h^2}$. However, a change in $h$ corresponds to a change in the number of discretization
points, hence in a change of the dimension of the matrices, and this affects the eigenvalues of $G_{\rm J}$.}. Using these results, we have that
\begin{equation*}
\rho(G_{\rm J})=\max_{k,j}\Bigl| 1- \frac{1}{4} \lambda_{k,j}(A)\Bigr|
=\frac{1}{2}\max_{k,j}\Bigl|\cos\Bigl(\frac{k\pi}{n+1}\Bigr)+\cos\Bigl(\frac{j\pi}{n+1}\Bigr)\Bigr|
=\cos\Bigl(\frac{\pi}{n+1}\Bigr).
\end{equation*}
Hence, the optimal SOR parameter is 
$$\omega_{\rm opt}=\frac{1}{1-\sqrt{1-\cos\bigl(\frac{\pi}{n+1}\bigr)^2}}
=\frac{1}{1-\sin\bigl(\frac{\pi}{n+1}\bigr)}.$$

Next, we compute the optimal parameter for the method of Richardson.
According to Theorem \ref{thm:Rich}, the optimal parameter is given by
\begin{equation*}
\alpha_{opt} = \frac{2}{\lambda_{max}(A) + \lambda_{min}(A)}.
\end{equation*}
Hence, it is sufficient to compute the maximum and minimum eigenvalues of $A$.
Recalling Problem \ref{prob:2.7} and \ref{prob:2.11} and assuming that we have that
\begin{equation*}
\lambda_{j,k}(A) = \lambda_k(A_x) + \lambda_j(A_y) = 4 + 2 \cos\Bigl( \frac{ k \pi }{ n+1 } \Bigr)
+ 2 \cos\Bigl( \frac{ j \pi }{ m+1 } \Bigr),
\end{equation*}
and we obtain that
\begin{equation*}
\begin{split}
\lambda_{max}(A) + \lambda_{min}(A) &= 4 
+ 2 \cos\Bigl( \frac{ \pi }{ n+1 } \Bigr) + 2 \cos\Bigl( \frac{ \pi }{ m+1 } \Bigr)\\
&\quad + 4 + 2 \cos\Bigl( \frac{ n \pi }{ n+1 } \Bigr) + 2 \cos\Bigl( \frac{ m \pi }{ m+1 } \Bigr) \\
&= 2\Bigl[ 1 + \cos\Bigl( \frac{ \pi }{ n+1 } \Bigr) \Bigr] 
 + 2\Bigl[ 1 + \cos\Bigl( \frac{ \pi }{ m+1 } \Bigr) \Bigr] \\
&\quad + 2\Bigl[ 1 + \cos\Bigl( \frac{ n \pi }{ n+1 } \Bigr) \Bigr] 
 + 2\Bigl[ 1 + \cos\Bigl( \frac{ m \pi }{ m+1 } \Bigr) \Bigr] . \\
\end{split}
\end{equation*}
Now, using the half-angle formula $1+\cos(\theta) = 2(\cos(\theta/2))^2$, we obtain that
\begin{equation*}
\begin{split}
\lambda_{max}(A) + \lambda_{min}(A) &= 4\Biggl[ \cos\Bigl( \frac{ \pi }{ 2n+2 } \Bigr)^2
 + \cos\Bigl( \frac{ \pi }{ 2m+2 } \Bigr)^2 \\
&\quad+ \cos\Bigl( \frac{ n \pi }{ 2n+2 } \Bigr)^2 
+ \cos\Bigl( \frac{ m \pi }{ 2m+2 } \Bigr)^2 \Biggr] . \\
\end{split}
\end{equation*}
Now, notice that $\frac{ n \pi }{ 2n+2 } + \frac{ \pi }{ 2n+2 } = \frac{\pi}{2}$
and $\frac{ m \pi }{ 2m+2 } + \frac{ \pi }{ 2m+2 } = \frac{\pi}{2}$, therefore we have that
\begin{equation*}
\cos\Bigl( \frac{ \pi }{ 2n+2 } \Bigr) = \sin\Bigl( \frac{\pi}{2} - \frac{ \pi }{ 2n+2 } \Bigr)
= \sin\Bigl( \frac{ n \pi }{ 2n+2 } \Bigr),
\end{equation*}
and similarly
\begin{equation*}
\cos\Bigl( \frac{ \pi }{ 2m+2 } \Bigr) = \sin\Bigl( \frac{\pi}{2} - \frac{ \pi }{ 2m+2 } \Bigr)
= \sin\Bigl( \frac{ m\pi }{ 2n+2 } \Bigr).
\end{equation*}
Consequently, we obtain that
\begin{equation*}
\begin{split}
\lambda_{max}(A) + \lambda_{min}(A) &= 4\Biggl[ \cos\Bigl( \frac{ \pi }{ 2n+2 } \Bigr)^2
 + \cos\Bigl( \frac{ \pi }{ 2m+2 } \Bigr)^2 + \cos\Bigl( \frac{ n \pi }{ 2n+2 } \Bigr)^2 
+ \cos\Bigl( \frac{ m \pi }{ 2m+2 } \Bigr)^2 \Biggr] \\
&= 4\Biggl[ \sin\Bigl( \frac{ n\pi }{ 2n+2 } \Bigr)^2
 + \sin\Bigl( \frac{ m\pi }{ 2m+2 } \Bigr)^2 + \cos\Bigl( \frac{ n \pi }{ 2n+2 } \Bigr)^2 
+ \cos\Bigl( \frac{ m \pi }{ 2m+2 } \Bigr)^2 \Biggr] = 8.
\end{split}
\end{equation*}
Hence, the optimal parameter is\footnote{Notice that in this case, the factor $\frac{1}{h^2}$ that must multiply
$A$ will affect directly the optimal parameter. In fact, the eigenvalues of $\frac{1}{h^2}A$ are scaled by this factor.
Therefore, the optimal parameter (that can be derived in the same way) is
$\alpha_{opt} = \frac{h^2}{4}$.}
$$\alpha_{opt} = \frac{2}{\lambda_{max}(A) + \lambda_{min}(A)} = \frac{1}{4}.$$
\end{solution}

\begin{solution}[Problem \ref{prob:2.12}]
We can use Theorem \ref{mue-lambda} and recall the expression
\begin{equation*}
(\lambda + \omega -1 )^2 = \lambda \omega^2 \mu^2,
\end{equation*}
where $\lambda$ and $\mu$ are eigenvalues of $G_{\rm SOR}$ and $G_{\rm J}$, respectively.
Since we want to study the relation between the Gauss-Seidel and Jacobi methods, we fix $\omega=1$ and obtain
\begin{equation*}
\lambda^2 = \lambda \mu^2 \Rightarrow \lambda = \mu^2,
\end{equation*}
which means that the eigenvalues of $G_{\rm GS}$ are the square of the ones of $G_{\rm J}$.
Therefore, we have that $\rho(G_{\rm GS}) = \rho(G_{\rm J})^2$, which means that
(if $A$ has the Property A) the method of Gauss-Seidel converges twice faster than the method of Jacobi.
\end{solution}

\begin{solution}[Problem \ref{prob:2.13}]
We recall from equation \eqref{eq:determ} in the proof of Theorem \ref{mue-lambda} that
$\lambda$ is an eigenvalue of $G_{\rm SOR} \in \R^{n \times n}$ if and only if
\begin{equation*}
\det\left((\lambda+\omega-1)I + \omega D^{-1}(\lambda L+U)\right) =0 .
\end{equation*}
Now, we set $\lambda=0$ and obtain
\begin{equation*}
\det\left((\omega-1)I + \omega D^{-1}U\right) =0 .
\end{equation*}
Notice that $(\omega-1)I + \omega D^{-1}U$ is a block upper-triangular matrix:
\begin{equation*}
(\omega-1)I + \omega D^{-1}U = 
\begin{bmatrix}
(\omega-1)I & \star \\
0 & (\omega-1)I \\
\end{bmatrix}.
\end{equation*}
Hence, we have that $0 = \det\left((\omega-1)I + \omega D^{-1}U\right) = (\omega -1)^n$,
which implies that $\omega = 1$.
\end{solution}

\end{document}