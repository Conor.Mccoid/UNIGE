function [u, k] = CGsimplifie(A, f, tol, kmax)
% Cette fonction CGsimplifie permet de r�soudre le syst�me lin�aire Au = f 
% en utilisant la m�thode du gradient conjug�, avec moins de prosuits dans
%la boucle while.

% INPUT
% Matrice A: Laplacien en dimension 2
% Vecteur f
% tol: tol�rance
% kmax: nombre maximum d'it�rations autoris�es.

% OUTPUT
% u : solution approximative
% k : le nombre d'it�rations avant la convergence

% INITIALISATION
k = 0; 
u = f; % premier essai pour u
r0 = f - A*u; % on calcule le r�sidu initial
p = r0; % on calcule la premi�re direction de recherche

% PROGRAMME
    while(k<kmax && tol<norm(r0, 2))       
        
        alpha =((norm(r0, 2))^2) /(p'*A*p); % on met � jour la valeur de alpha_k
        u = u + alpha * p; % on met � jour la valeur de u_k

        r1 = r0 - alpha*A*p;  % on met a jour le r�sidu
        beta = ((norm(r1, 2))^2) / ((norm(r0, 2))^2); % on calcule beta comme dans l'exercice 3
        p = r1 + beta * p; % on trouve la direction de recherche suivante
        k = k+1; % on incr�mente
        
    end
                                    
fprintf('La solution approximative est %d \n', u);
fprintf('La convergence est atteinte en %d pas \n', k);

end
