function [u, k] = CGmethod(A, f, tol, kmax)
% Cette fonction CGmethod permet de r�soudre le syst�me lin�aire Au = f 
% en utilisant la m�thode du gradient conjug� telle qu'elle est donn�e dans
% l'exercice 3.

% INPUT
% Matrice A: Laplacien en dimension 2
% Vecteur f
% tol: tol�rance
% kmax: nombre maximum d'it�rations autoris�es.

% OUTPUT
% u : solution approximative
% k : le nombre d'it�rations avant la convergence

% INITIALISATION
k = 0; 
u = f; % premier essai pour u
r0 = f - A*u; % on calcule le r�sidu initial
p = r0; % on calcule la premi�re direction de recherche

% PROGRAMME
    while(k<kmax && tol<norm(r0, 2))       
        
        alpha = (r0'*p) /(p'*A*p); % on met � jour la valeur de alpha_k
        u = u + alpha * p; % on met � jour la valeur de u_k

        r1 = f - A*u;  % on met a jour le r�sidu avec le nouveau u obtenu.
        beta = (r1'*A*p) / (p'*A*p); % on calcule beta.
        p = r1 + beta * p; % on trouve la direction de recherche suivante
        k = k+1; % on incr�mente.
        
    end
                                    
fprintf('La solution approximative est %d \n', u);
fprintf('La convergence est atteinte en %d pas \n', k);

end