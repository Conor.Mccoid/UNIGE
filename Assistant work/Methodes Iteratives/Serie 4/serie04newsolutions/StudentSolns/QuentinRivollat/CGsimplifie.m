function [u_k, k] = CGsimplifie(A, f, tol, kmax)

% Initialization of the variales
u_k = f;  % More or less random first estimation of u
k = 0;  
r0 = f - A*u_k;  % First residual
p = r0; % First direction

r_k = r0;

% Main loop
while((k<kmax) && (tol < norm(r_k, 2)))
        
    % Update alpha_k value, as given in the Exercice sheet
    alpha =((norm(r0, 2))^2) /(p'*A*p); 
    
    % Update of u_k
    u_k = u_k + alpha * p;

    % Update the residual
    r_k = r_k - alpha*A*p; 
    
    % Calculate beta as given in Ex. 3
    beta = ((norm(r_k, 2))^2) / ((norm(r0, 2))^2); 
    
    % Update the direction
    p = r_k + beta * p; 
    
    
    k = k+1; % on incrémente
        
end
                                    
fprintf('La solution approximative est %d \n', u_k);
fprintf('La convergence est atteinte en %d pas \n', k);

end
