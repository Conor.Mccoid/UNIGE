function [u_k, k] = CGmethod(A, f, tol, kmax)

% Initialization of the variables
u_k = f; % First estimation of uu
k = 0; 
r0 = f - A*u_k; % First residual estimation value
p_k = r0; % First direction

% Main Loop
while(k < kmax && tol < norm(r_k, 2))       

    % Update alpha_k value
    alpha = (r0'*p_k) /(p_k'*A*p_k); 
    
    % Update u_k
    u_k = u_k + alpha * p_k; 

    % Update the residual
    r_k = f - A*u_k;  
    
    % Calculate beta_k
    beta = (r_k'*A*p_k) / (p_k'*A*p_k); 
    
    % Update the direction
    p_k = r_k + beta * p_k; 
    
    k = k+1;

end
                                    
fprintf('La solution approximative est %d \n', u_k);
fprintf('La convergence est atteinte en %d pas \n', k);

end