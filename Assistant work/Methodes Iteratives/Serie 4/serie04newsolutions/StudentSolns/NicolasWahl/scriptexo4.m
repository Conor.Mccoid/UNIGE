% Excercice 4

% PARTIE 1

% Creation du laplacian en dimension 2 

n=20;

internalPoints=n;
e   = ones(internalPoints,1);
spe = spdiags([e -2*e e], -1:1,internalPoints,internalPoints);
Iz  = speye(internalPoints);
A  = kron(Iz,spe)+kron(spe,Iz);
full(A);

% Application du conjugate gradient

% --- condition initial : random vector for u0

mu=1 ; vr=0;
u0 = mu+sqrt(vr)*abs(randn(n));
u0=u0(:);

% --- f initial comme serie 3

u_ini=mat(n);

f_ini=A*u_ini;

% --- test de la fonction

maxiter=10000; tol =0.00000001;

[u,uk]=ConjugateGradient(A,f_ini,u0,tol,maxiter);

% estimation de la valeur:

%--- on calcule la norme d'energie a chaque pas

taille=size(uk); taille=taille(1);

for i=1:taille
    err_vect(i)=(u_ini-uk(:,i))'*A*(u_ini-uk(:,i));
    

end

%--- on calcule la norme d'energie par rapport à l'initialisation

norm_ini=(u_ini-u0)'*A*(u_ini-u0);

%--- on estime la valeur pour chaque iteration

for i=1:taille
    val(i)=((err_vect(i)/norm_ini)/2)^(1/i);
    
end

%--- plot


plot(val)

% text(4,0.5,txt)



% PARTIE 2

for i=1:5

    tol=0.00000001*10^(-2*i);
    
    maxiter=100^i;
    
    g = @() ConjugateGradient(A,f_ini,u0,tol,maxiter);

    time1(i)=timeit(g);

    f = @() ConjugateGradient2(A,f_ini,u0,tol,maxiter);

    time2(i)=timeit(f);
    
end

figure

hold on 

plot(time1)

plot(time2)

hold off

