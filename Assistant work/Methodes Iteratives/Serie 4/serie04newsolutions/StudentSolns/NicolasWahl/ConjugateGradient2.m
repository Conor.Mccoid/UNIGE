function [u,uk]=ConjugateGradient2(A,f,u0,tol,maxiter)
%Solves Au=f using Conjugate Gradient method (original) with u0 as an
%itinial guess, tol the tolerance and maxiter the number of maximum
%iteration. Output are [u,uk], where u is the last approx. and uk the
%matrix containing all the approximation


%initialisation

u=u0
r=f-A*u0;
uk(:,1)=u0;
k=0;
p=r;
while k<maxiter && norm(r)>tol
    a=(r'*p)/(p'*A*p);
    u=u+a*p;
    r=r-A*u;
    b=(r'*A*p)/(p'*A*p);
    p=r+b*p;
    uk(:,k+1)=u;
    k=k+1;
end