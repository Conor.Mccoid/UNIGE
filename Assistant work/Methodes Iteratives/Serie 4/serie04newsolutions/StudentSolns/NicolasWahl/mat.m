function vec=mat(n)
for i=1:n
    for j=1:n
        vec(i,j)=sin(pi*i/(n-1))*sin(pi*j/(n-1))+1
    end
end

vec=vec(:)
