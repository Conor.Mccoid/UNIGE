function D=Laplacian1D(n)
% LAPLACIAN1D compute a finite difference Laplacian
%   A=FDLaplacian(n,d) computes a finite difference Laplacian on the
%   unit interval using n interior points: 0 .. h .. 2h .. .. nh .. 1

  
h=1/(n+1);
D=1/h^2*spdiags(ones(n,1)*[1 -2 1],[-1 0 1],n,n);