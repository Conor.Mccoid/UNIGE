function [u,uk,rk,t]=Methode_Gradient_conjugue(A,f,u0,tol,maxiter)
% STEEPESTDESCENT solves Au=f using the steepest descent method
% [u,uk]=SteepestDescent(A,f,u0,tol,maxiter);
%  solves Au=f using steepest
% descent starting at the initial guess u0 up to a tolerance tol using
% at most maxiter iterations. A has to be symmetric positive definite. 
% uk contains all the iterates.

if nargin<5
    maxiter=100;
end
if nargin<4
    tol=0.0001;
end
r=f-A*u0;
p=r;
uk(:,1)=u0;
rk=zeros(length(r),maxiter);
t=zeros(maxiter);
k=0;
while norm(r)/norm(f)>tol && k<maxiter
    tic
    k=k+1;
    rk(:,k)=r;
    al=r'*p/(r'*A*p);
    uk(:,k+1)=uk(:,k)+al*p;
    r=f-A*uk(:,k+1);
    b=p'*A*r/(p'*A*p);
    p=r+b*p;
    t(k)=toc;
end
u=uk(:,k+1);
rk(:,k+1)=r;
rk=rk(:,1:k+1);
t=t(1:k+1);
end