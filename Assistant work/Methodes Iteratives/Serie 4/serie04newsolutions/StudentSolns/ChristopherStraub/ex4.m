%% a
n=20;
L = Laplacian1D(n);
I = speye(n);
L2 = kron(I,-L) + kron(-L,I); 
tol=0.0001;
maxiter=1000;
x = linspace(0,1,n);          % discretisation
uex= -0.25*(x'-0.5).^2 * (x-0.5).^2;    % solution exacte
r=ones(n^2,1);
u0 = uex(:)+r./norm(r);    
f=L2*uex(:);
[u1,uk1,rk,t1]=Methode_Gradient_conjugue(L2,f(:),u0,tol,maxiter);
nr0=norm(rk(:,1)'*(L2^-1)*rk(:,1));
nrk=norm(rk(:,end)'*(L2^-1)*rk(:,end));
rapport_conditonnemet1=(nrk/nr0)^(1/length(rk(1,:)))

%le (K(A)-1)/(K(A)+1) est plus grand ou egal a 0.9988

%% b
[u1,uk1,rk,t1]=Methode_Gradient_conjugue(L2,f(:),u0,tol,maxiter);
[u2,uk2,rk2,t2]=Methode_Gradient_conjugue_modif(L2,f(:),u0,tol,maxiter);
t_iter1= sum(t1)/length(t1);
t_iter2=sum(t2)/length(t2);
rapport_T2_sur_T1 = t2/t1 % le rapport entre les deux on trouve que t2 est presque deux fois plus rapide