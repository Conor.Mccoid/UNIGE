function [u,t] =gradient(uo,kmax,tol)
b=size(uo);
alpha=[];
r=[];
u=[];
p=[];
beta=[];
a=[],
u(:,1)=uo
f=[1     0     0     0     0     0     0     1     0     0     0     0     0     0     1     0     0     0     0     0     0     0     1     0     0     0     0     0     0     1 0     0     0     1     0     0]
f=f'

A=FDLaplacian(sqrt(b(1)),2)
r(:,1)=f-A*u
p(:,1)=r
k=1

tic
while k<kmax & norm(r(:,1),2)>tol
    a(:,k)=A*p(:,k)
    alpha(k)=norm(r(:,k),2)^2/(transpose(p(:,k))*a(:,k))
    u(:,k+1)=u(:,k)+alpha(k)*p(:,k)
    r(:,k+1)=r(:,1)-sum(alpha(i)*a(:,i))
    beta(k)=(norm(r(:,k+1),2)^2)/norm(r(:,k),2)^2
    p(:,k+1)=r(:,k+1)+beta(k)*p(:,k)
    k=k+1
end
t=toc
u=u(:,end)  
    
    
    
end