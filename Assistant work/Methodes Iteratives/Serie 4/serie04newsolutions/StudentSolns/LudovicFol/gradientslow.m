function [u,t]=gradientslow(uo,kmax,tol)
b=size(uo);
alpha=[];
r=[];
u=[];
p=[];
beta=[];
a=[],
u(:,1)=uo
f=[1     0     0     0     0     0     0     1     0     0     0     0     0     0     1     0     0     0     0     0     0     0     1     0     0     0     0     0     0     1 0     0     0     1     0     0]
f=f'

A=FDLaplacian(sqrt(b(1)),2)
r(:,1)=f-A*u
p(:,1)=r
k=1

tic
while k<kmax & norm(r(:,1),2)>tol
    alpha(k)=(r(:,k)'*p(:,k))/(p(:,k)'*A*p(:,k))
    u(:,k+1)=u(:,k)+alpha(k)*p(:,k)
    r(:,k+1)=f-A*u(:,k+1)
    beta(k)=(p(:,k)'*A*r(:,k+1))/(p(:,k)'*A*p(:,k))
    p(:,k+1)=r(:,k+1)+beta(k)*p(:,k)
    k=k+1
end
t=toc
u=u(:,end)  
    
    
    
end