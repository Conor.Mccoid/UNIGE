function [u,uk] = ConjugateGradient(A,f,u0,tol,maxiter)

if nargin<5, maxiter = 100; end
if nargin<4, tol =1e-3; end
Au= A*u0;    uk(:,1)= u0;
r = f - Au;  r1     = r'*r;
p = r;       k      = 1;
while sqrt(r1)>tol && k<maxiter
    r0 = r1;
    Ap = A*p;
    al = r0 / (p'*Ap);
    uk(:,k+1) = uk(:,k)+al*p;
    Au = Au + al*Ap;
    r  = f - Au; r1 = r'*r;
    be = r1/r0;
    p  = r+be*p;
    k  = k+1;
end
u=uk(:,k);