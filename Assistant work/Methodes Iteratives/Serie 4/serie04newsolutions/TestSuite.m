%% Laplacian convergence

n = 75;
A = FDLaplacianTrue(n,2); A = -A;
x = linspace(0,1,n);
u = cos(x') * sin(x); u = u(:);
f = A*u;

C = condest(A); C = (sqrt(C)-1)/(sqrt(C)+1);

[v,vk] = ConjugateGradient(A,f,ones(n^2,1));
[vrow,vcol] = size(vk);
error = zeros(vcol,1);
for i=1:vcol
    w = vk(:,i) - u;
    error(i) = sqrt(w'*A*w);
end

ind = 40:50;
P = fit(ind',log(error(ind)),'poly1');
plot(1:vcol,log(error),'.-',1:vcol,P(1:vcol),'--')
xlabel('Iteration')
ylabel('log(Error)')

Cest = exp(P.p1);

%% Estimer C

N  = 5;
nn = round(logspace(1,3,N));
h  = 1./(nn+1);
Cest = zeros(size(nn));

for k = 1:N
    n = nn(k);
    A = FDLaplacianTrue(n,2); A=-A;
    x = linspace(0,1,n);
    u = cos(x') * sin(x); u=u(:);
    f = A*u;
    
    [v,vk] = ConjugateGradient(A,f,ones(size(u)),1e-3,500);
    [vrow,vcol] = size(vk);
    error = zeros(vcol,1);
    for i = 1:vcol
        w = vk(:,i) - u;
        error(i) = sqrt(w'*A*w);
    end
    
    slope = log(0.5*error(2:end)./error(1))./(2:vcol)';
    Cest(k)= exp(max(slope));
    
    figure(1)
    plot(1:vcol,log(error),'.',1:vcol,log(2*error(1)*Cest(k).^(1:vcol)),'--')
    xlabel('Iteration')
    ylabel('log(Error)')
    pause(0)
end

C = (1-sin(pi*h))./cos(pi*h);
figure(2)
plot(h,Cest,'.',h,C,'-')
xlabel('h')
ylabel('Slope')

%% Temps

N  = 10;
nn = round(logspace(1,3,N));
tCG= zeros(size(nn)); tCGold = tCG;

for k = 1:N
    n = nn(k);
    A = FDLaplacianTrue(n,2); A=-A;
    x = linspace(0,1,n);
    u = cos(x') * sin(x); u=u(:);
    f = A*u;
    
    tic
    [v,vk] = ConjugateGradient(A,f,ones(size(u)));
    tCG(k) = toc;
    [vrow,vcol] = size(vk);
    tCG(k) = tCG(k)/vcol;
%     disp(['CG Error=', num2str(norm(v-u))])
    
    tic
    [v,vk] = ConjugateGradientOld(A,f,ones(size(u)));
    tCGold(k) = toc;
    [vrow,vcol] = size(vk);
    tCGold(k) = tCGold(k)/vcol;
%     disp(['CGold Error=', num2str(norm(v-u))])
end

loglog(nn,tCG,'.-',nn,tCGold,'--')
xlabel('n')
ylabel('t')
legend('CG, reduced','CG, old')