function [u,uk] = ConjugateGradientOld(A,f,u0,tol,maxiter)

if nargin<5, maxiter = 100; end
if nargin<4, tol =1e-3; end
uk(:,1)= u0;
r = f - A*u0;
p = r;
k = 1;
while norm(r)>tol && k<maxiter
    al = r'*p / (p'*A*p);
    uk(:,k+1) = uk(:,k)+al*p;
    r  = f - A*uk(:,k+1);
    be = -p'*A*r/(p'*A*p);
    p  = r+be*p;
    k  = k+1;
end
u=uk(:,k);