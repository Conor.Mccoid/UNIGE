function A=FDLaplacian(n,d)
% FDLAPLACIAN compute a finite difference Laplacian
%   A=FDLaplacian(n,d) computes a finite difference Laplacian on the
%   unit interval/square/cube (d=1,2,3) using n interior points

  
h=1/(n+1);
D=1/h^2*spdiags(ones(n,1)*[1 -2 1],[-1 0 1],n,n);

if d==1
    A=D;
elseif d==2
    I = eye(n);
    A=kron(I,D) + kron(D,I);
elseif d==3
    I = eye(n);
    D1=kron(I,D); D2=kron(D,I); II=kron(I,I);
    A=kron(D1,I) + kron(D2,I) + kron(II,D);
end

% if d==1
%   A=D;
% elseif d==2
%   X=spdiags(ones(n,1),0,n,n);
%   I2=1/h^2*spdiags(ones(n^2,1)*[1 1],[-n n],n^2,n^2);
%   A=kron(X,D)+I2;
% elseif d==3
%   X=spdiags(ones(n^2,1),0,n^2,n^2);
%   I2=1/h^2*spdiags(ones(n^3,1)*[1 1],[-n n],n^3,n^3);
%   I3=1/h^2*spdiags(ones(n^3,1)*[1 1],[-n^2 n^2],n^3,n^3);
%   A=kron(X,D)+I2+I3;
% end



% avec blkdiag

%if d==1
%  A=D;
%elseif d==2
%  A=[];
%  for i=1:n
%    A=blkdiag(A,D);
%  end
%  I2=1/h^2*spdiags(ones(n^2,1)*[1 1],[-n n],n^2,n^2);
%  A=A+I2;
%elseif d==3
%  C=[];
%  for i=1:n
%    C=blkdiag(C,D);
%  end
%  A=[];
%  for i=1:n
%    A=blkdiag(A,C);
%  end
%  I2=1/h^2*spdiags(ones(n^3,1)*[1 1],[-n n],n^3,n^3);
%  I3=1/h^2*spdiags(ones(n^3,1)*[1 1],[-n^2 n^2],n^3,n^3);
%  A=A+I2+I3;
%end
