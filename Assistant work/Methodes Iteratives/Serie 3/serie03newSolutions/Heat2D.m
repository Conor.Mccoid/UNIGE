function [u,pest]=Heat2D(uu,p)
% HEAT2D solves the stationary heat equation over a square 
%   u=Heat2D(f,g,p) solves the heat equation over a square of size one
%   by one. The source function is given by f, the boundary condition is
%   given by g, the number of grid points for each direction is a quarter
%   of the length(g) and one chooses the iterative solver with p,
%   i.e. p=0 uses Jacobi, p=1 uses Gauss-Seidel. 

[n,m] = size(uu);
if n~=m
    disp('uu is not square')
end
n = n-2;
I = speye(n);

% ind0 = [1 ; zeros(n,1) ; 1]; ind1 = [0 ; ones(n,1) ; 0];
% ind  = kron(ind0,ind1) + kron(ind1,ind0);
% g = uu(:); g = g(ind==1);
% 
% ind=1:n;
% 
% G = kron(g(ind),I(:,1)) + kron(g(ind+n),I(:,end)) + ...
%     kron(I(:,1),g(ind+2*n)) + kron(I(:,end),g(ind+3*n));
% G = (n+1)^2 * G;

D = (n+1)^2*spdiags(ones(n,1)*[1 -2 1],[-1 0 1],n,n);
D = kron(I,D) + kron(D,I);
F = uu(2:end-1,2:end-1); F = D*F(:);

if p==0
    N = triu(D,1) + tril(D,-1);
    M = -(4 * (n+1)^2);
elseif p==1
    N = triu(D,1);
    M = D - N;
end

u0 = kron(ones(n,1),ones(n,1));
itermax = 100;
err= zeros(itermax,1);
for k = 1:itermax
    u  = -M \ (N*u0 - F);
    u0 = u;
    err(k)= norm(reshape(u,n,n) - uu(2:end-1,2:end-1), 'fro');
end

P = fit((1:itermax)',log(err),'poly1');
pest = exp(P.p1);

% figure(2)
% plot(1:itermax,log(err),'b.',1:itermax,P(1:itermax),'r--')
% xlabel('Iteration')
% ylabel('log(error)')
% legend('log(e_k)',[num2str(P.p2) ' + (' num2str(P.p1) ') k'])
% pause