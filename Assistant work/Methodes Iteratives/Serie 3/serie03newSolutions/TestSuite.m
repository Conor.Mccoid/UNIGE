% Test suite for Serie 3

%% Heat2D, estimate spectral radius

% Jacobi
nn = 10:5:100;
pest = zeros(length(nn),1);
p  = 0;
for k=1:length(nn)
    n = nn(k);
    x = linspace(0,1,n+2);
    u = sin(pi*x')*sin(pi*x) + 1;

    [uu,pest(k)] = Heat2D(u,p);
end

P = fit(1./(nn'+1),pest,'poly2');
h = 1./(linspace(nn(1),nn(end),1000)+1);
figure(1)
subplot(1,2,1)
plot(1./(nn+1),pest,'b*',h,P(h),'r--',h,cos(pi*h).^(p+1),'k-',...
    'MarkerSize',10,'Linewidth',2)
    xlabel('h')
    ylabel('\rho(M^{-1}N)')
    legend('Taux de convergence asymptotique',...
        [num2str(P.p3) ' + (' num2str(P.p2) ') h + (' num2str(P.p1) ') h^2'],...
        '\rho(M^{-1}N) exacte')
    title('Jacobi')
    set(gca,'FontSize',20,'linewidth',2)

figure(2)
subplot(1,2,1)
loglog(1./(nn+1),1-pest,'b*',h,(pi^2/2)*h.^2,'k-',...
    'MarkerSize',10,'Linewidth',2)
    xlabel('h')
    ylabel('1-\rho')
    legend('1-\rho (calcule)','1-\rho (exacte)')
    title('Jacobi')
    set(gca,'Fontsize',20,'Linewidth',2)

% Gauss-Seidel
nn = 10:5:100;
pest = zeros(length(nn),1);
p  = 1;
for k=1:length(nn)
    n = nn(k);
    x = linspace(0,1,n+2);
    u = sin(pi*x')*sin(pi*x) + 1;

    [uu,pest(k)] = Heat2D(u,p);
end

P = fit(1./(nn'+1),pest,'poly2');
h = 1./(linspace(nn(1),nn(end),1000)+1);
figure(1)
subplot(1,2,2)
plot(1./(nn+1),pest,'b*',h,P(h),'r--',h,cos(pi*h).^(p+1),'k-',...
    'MarkerSize',10,'Linewidth',2)
    xlabel('h')
    ylabel('\rho(M^{-1}N)')
    legend('Taux de convergence asymptotique',...
        [num2str(P.p3) ' + (' num2str(P.p2) ') h + (' num2str(P.p1) ') h^2'],...
        '\rho(M^{-1}N) exacte')
    title('Gauss-Seidel')
    set(gca,'FontSize',20,'linewidth',2)

figure(2)
subplot(1,2,2)
loglog(1./(nn+1),1-pest,'b*',h,(pi^2)*h.^2,'k-',...
    'MarkerSize',10,'Linewidth',2)
    xlabel('h')
    ylabel('1-\rho')
    legend('1-\rho (calcule)','1-\rho (exacte)')
    title('Gauss-Seidel')
    set(gca,'Fontsize',20,'Linewidth',2)

%% Laplacian convergence

n = 20;
A = FDLaplacianTrue(n,2); A = -A;
x = linspace(0,1,n);
u = cos(x') * sin(x); u = u(:);
f = A*u;

[v,vk] = SteepestDescent(A,f,ones(n^2,1),1e-10,100);
[vrow,vcol] = size(vk);
error = zeros(vcol,1);
for i=1:vcol
    w = vk(:,i) - u;
    error(i) = sqrt(w'*A*w);
end

ind = round(0.8*vcol):vcol;
P0= fit((1:vcol)',log(error),'poly1');
P1= fit(ind',log(error(ind)),'poly1');
C0 = exp(P0.p1);
C1 = exp(P1.p1);
Cex= cos(pi/(n+1));
ind = (1-vcol):0;
plot(1:vcol,log(error),'b.',...
     1:vcol,log(error(end) * C0 .^ind),'m--',...
     1:vcol,log(error(end) * C1 .^ind),'r--',...
     1:vcol,log(error(end) * Cex.^ind),'k-')
xlabel('Iteration')
ylabel('log(Error)')
legend('|e_k|_A','Pente (entiere)','Pente (partielle)','Pente (exacte)')

%% Laplacian convergence - fonction de h
nn= 10:5:100;
C = zeros(1,length(nn));
for k = 1:length(nn)
    n = nn(k);
    A = FDLaplacianTrue(n,2); A = -A;
    x = linspace(0,1,n);
    u = cos(x') * sin(x); u = u(:);
    f = A*u;

    [v,vk] = SteepestDescent(A,f,ones(n^2,1),1e-10,1000);
    [vrow,vcol] = size(vk);
    error = zeros(vcol,1);
    for i=1:vcol
        w = vk(:,i) - u;
        error(i) = sqrt(w'*A*w);
    end
    
    ind = round(0.8*vcol):vcol;
    P   = fit(ind',log(error(ind)),'poly1');
    C(k)= exp(P.p1);
end

h = 1./(nn+1);
plot(h,C,'b*',h,cos(pi*h),'k--')
xlabel('h')
ylabel('$\frac{\kappa(A)-1}{\kappa(A)+1}$','interpreter','latex')
legend('Approx.','Exacte')