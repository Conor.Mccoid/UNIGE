function [u,uk]=SteepestDescent(A,f,u0,tol,maxiter)
% STEEPESTDESCENT solves Au=f using the steepest descent method
%   [u,uk]=SteepestDescent(A,f,u0,tol,maxiter); solves Au=f using steepest
%   descent starting at the initial guess u0 up to a tolerance tol using
%   at most maxiter iterations. A has to be symmetric positive definite.
%   uk contains all the iterates.
if nargin<5, maxiter = 100; end
if nargin<4, tol =0.0001; end
r=f-A*u0;
uk(:,1)=u0;
k=0;
while norm(r)/norm(f)>tol && k<maxiter
    k=k+1;
    al=r'*r/(r'*A*r);
    uk(:,k+1)=uk(:,k)+al*r;
    r=f-A*uk(:,k+1);
end
u=uk(:,k+1);