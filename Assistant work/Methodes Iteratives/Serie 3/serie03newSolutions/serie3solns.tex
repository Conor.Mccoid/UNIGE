\documentclass[a4paper, 12pt]{article}

\usepackage{preamble}
\usepackage[french]{babel}
\usepackage[utf8]{inputenc}
\usepackage{tikz}
\usepackage[margin=2cm]{geometry}

\renewcommand{\vec}[1]{\mathbf{#1}}

\title{S\'erie 3, Exercices 1c) et 2c)}
\date{}

\begin{document}

\maketitle

\section{Exercice 1c)}
\label{sec:ex1}

\newcommand{\diag}{\text{diag}}

Il est possible d'écrire les $\rho(M^{-1}N)$ pour Jacobi (que je note comme $\rho_J$) avec les m\'ethodes de l'exercice 1a).
Consid\'erer $A$, la matrice du Laplacien en dimension deux, sans les \'el\'ements sur la diagonale.
On sait que $A = I \otimes D + D \otimes I$ par les s\'eries pr\'ec\'edentes, o\`u $D$ est la matrice du Laplacien en dimension un.
Donc:
\begin{equation*}
\diag(A)  = I \otimes \diag(D) + \diag(D) \otimes I \implies \ N = I \otimes (D - \diag(D)) + (D - \diag(D)) \otimes I,
\end{equation*}
et alors les valeurs propres de $N$ sont $-\frac{2}{h^2} \left ( \cos \left ( i \pi h \right ) + \cos \left ( j \pi h \right ) \right )$.

La matrice $M^{-1}$ n'est qu'une valeur scalaire $-\frac{h^2}{4}$ fois l'identit\'e et la plus grande valeur propre de $N$ en valeur absolue est quand $i=j=1$.
Alors, $$\rho_J = \cos( \pi h) = 1 - \frac{\pi^2}{2} h^2 + \order{h^4}.$$
De plus, pour les matrices avec une propriet\'e specifique, $\rho_{GS} = \rho_J^2$ (o\`u $\rho_{GS} = \rho(M^{-1}N)$ pour Gauss-Seidel).
Le Laplacien discret a cette propriet\'e.
Alors, $$\rho_{GS} = \cos(\pi h)^2 = 1 - \pi^2 h^2 + \order{h^4}.$$

Mais comment les approximer num\'eriquement?
D'abord, on doit trouver une relation entre les objets d'it\'eration qu'on peut calculer et les $\rho_J$ et $\rho_{GS}$.
Par exemple, $$R_\infty(G) = \ln(\rho(G)). \quad \text{(eqn. 2.16 dans le polycopi\'e)}$$
$R_\infty(G)$ est le taux de convergence asymptotique.
\c{C}a veut dire qu'il est la pente $m$ de la ligne $$\ln\norm{e_k} = m k + \ln\norm{e_0},$$ quand $k$ tend vers l'infini.

(Peut-\^etre cette d\'efinition est un peu bizarre.
J'explique plus.
On sait que $\norm{e_k} \leq \norm{G^k} \norm{e_0}$ (la norme de la forme d'erreur, eqn. 2.6).
Si on prend le logarithme de cette relation on a
\begin{align*}
\ln \norm{e_k} & \leq \ln \norm{G^k} + \ln \norm{e_0} \\
		   & \leq k \ln \norm{G^k}^{1/k} + \ln \norm{e_0}. && \text{(une astuce du logarithme)}
\end{align*}
Par la s\'erie 1, quand $k$ tend vers l'infini, $\norm{G^k}^{1/k}$ tend vers $\rho(G)$.
Alors, la pente de cette ligne, qu'on note par $R_\infty$, devient $\ln \rho(G)$.
)

Donc, pour estimer la valeur de $\rho(G)$ on peut construire cette ligne.
Il est n\'ecessaire de calculer $e_k$, les erreurs exactes, pour beaucoup de $k$.
On consid\`ere une solution $u$, calcule une fonction $f$ par $f=Au$, et r\'eso\^ut le syst\`eme $Au = f$ par les m\'ethodes Jacobi et Gauss-Seidel.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.5\textwidth]{TauxAsymp.png}
	\caption{Jacobi, $n=10$.}
	\label{fig:taux}
\end{figure}

Regarder la figure \ref{fig:taux}.
Les erreurs $\ln \norm{e_k}$ cr\'eent une ligne.
La pente de cette ligne est -0.04135.
On peut la calculer par $$\frac{\ln \norm{e_{100}} - \ln \norm{e_0}}{100} = \frac{1}{100} \ln \left ( \frac{\norm{e_{100}}}{\norm{e_0}} \right )$$ ou r\'esoudre un syst\`eme d'\'equations normales:
\begin{equation*}
\begin{bmatrix} 0 & 1 \\ 1 & 1 \\ \vdots & \vdots \\ 100 & 1 \end{bmatrix} \begin{bmatrix} m \\ c \end{bmatrix} = \begin{bmatrix} \ln \norm{e_0} \\ \ln \norm{e_1} \\ \vdots \\ \ln \norm{e_{100}} \end{bmatrix} .
\end{equation*}
La derni\`ere m\'ethode est appell\'e 'a line of best fit'.
La valeur $m$ approxime $R_\infty$ et $c$ approxime $\norm{e_0}$.

\begin{figure}[h!]
	\centering
	\includegraphics[width=\textwidth]{ex1c.png}
	\caption{$\rho_J$ et $\rho_{GS}$ comme fonctions en $h$.}
	\label{fig:ex1c}
\end{figure}

On r\'ep\`ete pour plusieurs valeurs de $h$.
Apr\`es un certain nombre de $h$ on peut construire une figure comme figure \ref{fig:ex1c}.
Il y a une comparaison entre les taux de convergence asymptotiques (qui approximent $\rho(G)$) et $\rho(G)$ exacte.
Il y a aussi un autre 'fit', de polyn\^ome de d\'egree deux.
Si on ne veut pas utiliser un 'fit' on peut calculer la pente et la constante de la ligne
\begin{equation*}
\ln (1 - \rho(G)) = \ln \alpha + 2 \ln h.
\end{equation*}
\c{C}a veut dire que si on a les $y = \ln (1 - \rho(G))$ et les $x = \ln h$ alors on devrait avoir une ligne $y=mx + c$ avec $m = 2$ et $c = \ln \alpha$.

On peut aussi estimer $\alpha$ en prenant $(1 - \rho(G))/h^2$ pour un $h$ quelconque.
Par exemple, pour la m\'ethode de Jacobi et $n=10$ on sait que $\rho \approx \exp(-0.04135)$.
Alors, $$\alpha \approx (1-0.9595)(10+1)^2 = 4.9013.$$
La valeur exacte est $\pi^2/2 = 4.9348$, et nous avons une bonne estimation.

\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{ex1c_alt.png}
	\caption{Pr\'esentation alternative des r\'esultats, telle qu'ils forment des lignes droites.}
	\label{fig:ex1c droites}
\end{figure}

\newpage

\section{Exercice 2c)}
\label{sec:ex2}

Par exercice 1a) on sait que $$\kappa(A) = \frac{\lambda_{max}(A)}{\lambda_{min}(A)} = \frac{1 + \cos(\pi h)}{1 - \cos(\pi h)}.$$
Donc $$\frac{\kappa(A) - 1}{\kappa(A) + 1} = \frac{1 + \cos(\pi h) - (1 - \cos(\pi h))}{1 + \cos(\pi h) + (1 - \cos(\pi h))} = \cos(\pi h).$$
Alors, la valeur que nous devons estimer change avec $n$.
On choisit un $n$ sp\'ecifique (par exemple, $n=20$) et proc\'eder.

Par Th\'eor\`eme 19 dans chapitre 3 du polycopi\'e on sait que $$\norm{e_k}_A \leq \left ( \frac{\kappa(A) - 1}{\kappa(A) + 1} \right )^k \norm{e_0}_A = C^k \norm{e_0}_A$$ pour la m\'ethode de descente maximale.
Rappeler que $\norm{\vec{v}}_A = \sqrt{\vec{v}^\top A \vec{v}}$.
Alors, nous pouvons r\'ep\'eter l'id\'ee que nous avons utilis\'e pour l'exercice 1c):
Trouver la pente de la ligne $$\ln \norm{e_k}_A = mk + \ln \norm{e_0}_A.$$

On a besoin d'une solution exacte.
Je choisis $u(x,y) = \cos(y) \sin(x)$ et je construis le vecteur $\vec{u}_{ex} \in \bbr^{n^2}$ qui est la fonction $u(x,y)$ \'evalu\'ee sur les points int\'erieures et discr\`etes.
Je construis aussi la matrice $A \in \bbr^{n^2 \times n^2}$ le Laplacien discret en dimension deux et le vecteur $\vec{f} = A \vec{u}_{ex}$.
Je r\'esous le syst\`eme $A \vec{u} = \vec{f}$ avec la m\'ethode de descente maximale pour obtenir les it\'erations $\vec{u}_k$.
Je calcule $e_k = \vec{u}_k = \vec{u}_{ex}$ et $\norm{e_k}_A^2 = e_k^\top A e_k$.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.7\textwidth]{ex2c_iter.png}
	\caption{Convergence de $\vec{u}_k$ vers $\vec{u}_{ex}$ dans la norme d'\'energie.}
	\label{fig:ex2c iter}
\end{figure}

La figure \ref{fig:ex2c iter} montre les erreurs $\norm{e_k}_A$ (\textcolor{blue}{bleu}) pour $n=20$.
Si on prend le 'fit' sur toutes les erreurs (\textcolor{magenta}{rose}) on a une ligne qui approxime la convergence de toutes les erreurs.
Mais le relation que nous avons utilis\'e est une limite sup\'erieure.
Donc, $C$ approxime \textit{la plus petite} pente de convergence.
Pour cette m\'ethode, il appara\^it qu'elle arrive \`a la fin.
Alors, je prends le 'fit' juste pour les derni\`eres vingt erreurs (\textcolor{red}{rouge}) et voila, la pente approxime bien la valeur $C$ exacte (\textcolor{black}{noire}).

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.7\textwidth]{ex2c_kappa.png}
	\caption{Comparison entre $C$ approxim\'e comme fonction de $h$ et $\cos(\pi h)$, la fonction de $C$ exacte.}
	\label{fig:ex2c kappa}
\end{figure}

R\'ep\'eter pour plusiers valeurs de $n$.
L'estimation de $C$ change.
Nous connaissons la fonction $C(h)$ exacte et nous les comparons.
Quand $h$ tend vers 0 l'estimation de $C$ devient moins bien.
Pourquoi?
Noter l'erreur plus petite dans la figure \ref{fig:ex2c iter}; Est-ce que $\vec{u}_k$ a converg\'e?
Alors, que faut-il changer?
Essayer d'am\'eliorer l'estimation de $C$ pour tous les $h$.

\end{document}

