%% Laplacian convergence

n = 20;
A = FDLaplacianTrue(n,2); A = -A;
x = linspace(0,1,n);
u = cos(x') * sin(x); u = u(:);
f = A*u;

[v,vk] = SteepestDescent(A,f,ones(n^2,1),1e-10,100);
[vrow,vcol] = size(vk);
error = zeros(vcol,1);
for i=1:vcol
    w = vk(:,i) - u;
    error(i) = sqrt(w'*A*w);
end

ind = round(0.8*vcol):vcol;
P0= fit((1:vcol)',log(error),'poly1');
P1= fit(ind',log(error(ind)),'poly1');
C0 = exp(P0.p1);
C1 = exp(P1.p1);
Cex= cos(pi/(n+1));
ind = (1-vcol):0;
plot(1:vcol,log(error),'b.',...
     1:vcol,log(error(end) * C0 .^ind),'m--',...
     1:vcol,log(error(end) * C1 .^ind),'r--',...
     1:vcol,log(error(end) * Cex.^ind),'k-')
xlabel('Iteration')
ylabel('log(Error)')
legend('|e_k|_A','Pente (entiere)','Pente (partielle)','Pente (exacte)')

%% Laplacian convergence - fonction de h
nn= 10:5:100;
C = zeros(1,length(nn));
for k = 1:length(nn)
    n = nn(k);
    A = FDLaplacianTrue(n,2); A = -A;
    x = linspace(0,1,n);
    u = cos(x') * sin(x); u = u(:);
    f = A*u;

    [v,vk] = SteepestDescent(A,f,ones(n^2,1),1e-10,100);
    [vrow,vcol] = size(vk);
    error = zeros(vcol,1);
    for i=1:vcol
        w = vk(:,i) - u;
        error(i) = sqrt(w'*A*w);
    end
    
    ind = round(0.8*vcol):vcol;
    P   = fit(ind',log(error(ind)),'poly1');
    C(k)= exp(P.p1);
end

h = 1./(nn+1);
plot(h,C,'b*',h,cos(pi*h),'k--')
xlabel('h')
ylabel('$\frac{\kappa(A)-1}{\kappa(A)+1}$','interpreter','latex')
legend('Approx.','Exacte')