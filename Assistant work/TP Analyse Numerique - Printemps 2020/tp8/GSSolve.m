function [x,X,C] = GSSolve(A,f,x0,maxit,tol)
% GSSOLVE solves a linear system Ax=f using the Gauss-Seidel method
%   The function terminates when number of iterations exceeds maxit or
%   when the relative step size becomes smaller than tol.
%   Output X is a matrix with the kth column the kth iterate.

M = tril(A);
N = M - A;
C = max(abs(eig(inv(M)*N)));

X = x0;
x = M \ (N*x0 + f);
iter = 2;
while norm(x-x0)/norm(x) > tol && iter < maxit
    x0= x;
    x = M \ (N*x0 + f);
    X = [X x];
    iter = iter+1;
end