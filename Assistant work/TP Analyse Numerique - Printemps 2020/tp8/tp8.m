%% Run file for TP8

%% Exercise 1
f1 = @(x) x.^2 - 3*x - 4; % sign error
f2 = @(x) (x+1).*(x+5).^3 .* (x-5).^5;
[x1,X1] = BisectSolve(f1,-3.8,0.5,30,1e-10);
[x2,X2] = BisectSolve(f2,-3.8,0.5,30,1e-10);

figure(1)
plot(1:length(X1),X1,'bo',1:length(X2),X2,'r*')
xlabel('Iteration')
ylabel('x')
figure(2)
semilogy(1:length(X1),abs(f1(X1)),'bo',1:length(X2),abs(f2(X2)),'r*')
xlabel('Iteration')
ylabel('|f(x)|')
figure(3)
semilogy(1:length(X1),abs(X1-x1),'bo',1:length(X2),abs(X2-x2),'r*')
xlabel('Iteration')
ylabel('Error')

P = fit((1:length(X1))',log(abs(X1'-x1)),'poly1');
order1 = exp(P.p1);
P = fit((1:length(X2))',log(abs(X2'-x2)),'poly1');
order2 = exp(P.p1);

%% Exercise 2

n = 100;
lambda_min = 3;
lambda_max = 17;
[U,~] = qr(rand(n));
d = lambda_min + (lambda_max-lambda_min) * rand(n,1);
A = U * diag(d) * U';
x = 2*rand(n,1)-1;
f = A*x;

I = eye(n);

figure(1)
clf
for alpha = -0.1:0.05:0.1
    [~,X] = FixedPointSolve(A,f,zeros(n,1),alpha,50,1e-15);
    
    C = norm(I - alpha*A);
    
    [rowX,colX] = size(X);
    errX = zeros(colX,1); ind = 1:colX;
    for k=ind
        errX(k) = norm(X(:,k) - x)/norm(x);
    end
    figure(1)
    semilogy(ind,errX,'.--',ind,C.^ind,'-')
    hold on
end
xlabel('Iteration')
ylabel('Relative Error')
legend('-0.1, ex','-0.1, th','\alpha=-0.05','-0.05, th','\alpha=0',...
    '0, th','\alpha=0.05','0.05, th','\alpha=0.1','0.1,th')
hold off

figure(2)
clf
[~,XJ,CJ] = JacobiSolve(A,f,zeros(n,1),50,1e-15);
[~,XG,CG] = GSSolve(A,f,zeros(n,1),50,1e-15);
[~,colJ] = size(XJ); indJ = 1:colJ; errJ = zeros(colJ,1);
[~,colG] = size(XG); indG = 1:colG; errG = zeros(colG,1);
for k = indJ
    errJ(k) = norm(XJ(:,k) - x)/norm(x);
end
for k = indG
    errG(k) = norm(XG(:,k) - x)/norm(x);
end
semilogy(indJ,errJ,'r*--',indJ,CJ.^indJ,'r',indG,errG,'bo--',indG,CG.^indG,'b')
xlabel('Iteration')
ylabel('Relative error')

%% Exercise 3, partie 1

clf
clear
clc
f = @(x) x.^3 - x - 3;
df= @(x) 3*x.^2 - 1;

[x0,X0] = Newton(f,df,0,1e-15,50);
[x1,X1] = Newton(f,df,1,1e-15,50);

figure(1)
semilogy(1:length(X0),abs(X0 - x1),'r*',1:length(X1),abs(X1-x1),'bo')
xlabel('Iteration')
ylabel('Absolute error')

f = @(x) [ x(1)^3-3*x(1)*x(2)^2-1;
           3*x(1)^2*x(2)-x(2)^3 ];
df= @(x) [ 3*(x(1)^2 - x(2)^2), -6*x(1)*x(2);
                   6*x(1)*x(2), 3*(x(1)^2 - x(2)^2) ];

[x,X] = Newton(f,df,[-1;1],1e-10,100);
[~,col] = size(X); err = zeros(col,1);
for k = 1:col
    err(k) = norm(X(:,k) - x);
end
figure(2)
semilogy(1:col,err)
xlabel('Iteration')
ylabel('Absolute error')

dg= @(x) [2*(x(1) - 1);4*(x(2) + 1)^3];
Hg= @(x) [2, 0; 0, 12*(x(2)+1)^2];

[x,X] = Newton(dg,Hg,[0;0],1e-10,100);
[~,col] = size(X); err = zeros(col,1);
for k=1:col
    err(k) = norm(X(:,k) - x);
end
figure(3)
semilogy(1:col,err)
xlabel('Iteration')
ylabel('Absolute error')

%% Exercice 3, partie 2

clf
clear
clc
f = @(x) [ x(1)-1 ; (x(2)+1).^2 ];
df= @(x) [ 1, 0 ; 0, 2*(x(2)+1)];

[x,X] = GaussNewton(f,df,[0;0],eps,100);
[~,col] = size(X); err=zeros(col,1); nor=err;
for k = 1:col
    err(k) = norm(X(:,k) - [1;-1]);
    nor(k) = norm(f(X(:,k)));
end
figure(1)
subplot(1,2,1)
semilogy(1:col,err)
xlabel('Iteration')
ylabel('Absolute error')
subplot(1,2,2)
semilogy(1:col,nor)
xlabel('Iteration')
ylabel('||f(x_k)||')

[x,X] = GaussNewton(@ex32b,@gradex32b,[8000;15000;1000;0;-1;0;0],eps,100);
[~,col] = size(X); err=zeros(col,1); nor=err;
x = [9664,13115,4116,-0.043,-0.169,-0.032,-0.074];
for k = 1:col
    err(k) = norm(X(:,k) - x');
    nor(k) = norm(ex32b(X(:,k)));
end
figure(2)
subplot(1,2,1)
semilogy(1:col,err)
xlabel('Iteration')
ylabel('Absolute error')
subplot(1,2,2)
semilogy(1:col,nor)
xlabel('Iteration')
ylabel('||f(x_k)||')