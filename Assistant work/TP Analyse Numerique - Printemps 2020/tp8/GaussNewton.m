function [x,X] = GaussNewton(f,df,x0,tol,maxiter)
%GAUSSNEWTON minimizes norm(f) using the Gauss-Newton method.
%   Terminates if step length less than tol or number of iterations exceeds
%   maxiter. Output X contains the kth iterate in the kth column.

X = x0;
x = x0 - ( df(x0) \ f(x0) );
X = [X x];
iter = 2;
while norm(x - x0)>tol && iter < maxiter
    x0= x;
    x = x0 - ( df(x0) \ f(x0) ); % least squares
    X = [X x];
    iter = iter+1;
end