function [x,X] = BisectSolve(f,x1,x2,maxit,tol)
% BISECTSOLVE solves f(x)=0 using bisection method on interval [x1,x2].
%   Function terminates when interval is smaller than tol or number of
%   iterations is larger than maxit. Output X is a row vector of the
%   iterates.

iter = 1;
a = x1; b = x2;
sa=sign(f(a)); sb=sign(f(b));
X = [];

while iter < maxit && abs(b-a) > tol
    x = (a+b)/2;
    X = [X x];
    sf= sign(f(x));
    if sf==0
        a=x; b=x;
    elseif sf==sa
        a=x;
    elseif sf==sb
        b=x;
    end
    iter = iter+1;
end