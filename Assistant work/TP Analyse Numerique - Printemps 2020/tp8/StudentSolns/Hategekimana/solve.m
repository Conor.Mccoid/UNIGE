function y = solve(A, b)
	[Q R]= qr(A);
	y= R \ (inv(Q)*b)
end
