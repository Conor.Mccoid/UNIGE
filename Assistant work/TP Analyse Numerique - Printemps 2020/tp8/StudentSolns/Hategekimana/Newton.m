function [x, X]= Newton(f, df, x0, tol, maxiter)
	%NEWTON solves the equation equation f(x)=0 usig
	%Newton's method with initial guess x0
	%the function terminaites when the Newton step length is less than tol
	%or when the maximum number of iteration maxiter is reached.
	%The output argument X, when requested, is a matrix,
	%the kth column of which is the kth iterate.	
	X= [];
	maxiter = maxiter-1;	
	res= x0-(f(x0)/df(x0));
	X= [X res];
	while (abs(res-x0) > tol && maxiter > 0)
		x0=res;
		maxiter = maxiter-1;	
		res= x0-(f(x0)/df(x0));
		X= [X res];
	end
	x= res;
end
