function x= GaussNewton(f, df, x0, tol, maxiter)
	maxiter = maxiter-1;	
	fx= f(x0);
	dfx= df(x0);
	res= x0+solve(dfx, -fx);
	if norm([0;0]-f(res)') < tol || maxiter < 0
		x= res;		
	else
		x= GaussNewton(f, df, res, tol, maxiter);
	end
end
