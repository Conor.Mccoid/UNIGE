%% 1
%% a 
%%pour f(x)=(x^3)-x-3 impl�menter f(x) et f'(x)
f= @(x) (x.^3)-x-3;
df= @(x) 3*(x.^2)-1;

%%exemple pour voir si �a marche
%%f= @(x) (x.^2)-(5*x)+6;
%%df= @(x) (2*x)-5;



%% b 
%%impl�menter la m�thode de Newton: Newton(f, df, x, tol, maxiter
%%voir la fonction  Newton.m



%% c 
%%pour x0= 0 puis pour x0=1 représenter la fonction puis les approximations
x0= 1;
tol= 0.000000000001;
maxiter= 50000;
y= Newton(f, df, x0, tol, maxiter)

x= linspace(1.5, 2, 100);
plot(x, f(x), [y, y], [-3, 3])
%%avec x0= 0 l'approximation reste dans les alentour du zéro
%%avec x0= 1 l'approximation se rapproche bien du zéro de la fonction



%% d
X= [-1; 1];
tol= 1e-10;
maxiter= 100;

% G= @(x) [x(1)+x(2)-3;  2*x(1)-x(2)-6];
% dG= @(x) [1 1; 2 -1];

F= @(x) [(x(1).^3)-3*x(1)*x(2)-1; (3*(x(1)^2)*x(2))-(x(2)^3)];
dF= @(x) [3*x(1)^2-3*x(2)^2 -6*x(1)*x(2); 6*x(2)*x(1) 3*x(1)^2-3*x(2)^2];

% y= Newton2(G, dG, X, tol, maxiter)
y= Newton2(F, dF, X, tol, maxiter)




% h= @(x) [x(1)-1; (x(2)+1)^2]
% dh= @(x) [1 0; 0 x(2)+1]

% y= GaussNewton(G, dG, X, tol, maxiter)
y= GaussNewton(F, dF, X, tol, maxiter)
% y= GaussNewton(h, dh, X, tol, maxiter)
% fonction � impl�ment�
% y= Newton2(h, dh, X, tol, maxiter)
% h(y)