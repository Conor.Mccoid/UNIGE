function x= Newton2(F, dF, x0, tol, maxiter)
	%NEWTON solves the equation equation f(x)=0 usig
	%Newton's method with initial guess x0
	%the function terminaites when the Newton step length is less than tol
	%or when the maximum number of iteration maxiter is reached.
	%The output argument X, when requested, is a matrix,
	%the kth column of which is the kth iterate.	
	maxiter = maxiter-1;	
	res= x0-(inv(dF(x0))*(F(x0)));
	if abs(norm([0;0]-F(res)')) < tol || maxiter < 0
		x= res;		
	else
		x= Newton2(F, dF, res, tol, maxiter);
	end
end
