% Exo 2 Point 2
clear all;
clc
n = 100;
lambda_min = 3;
lambda_max = 17;
[U,~] = qr(rand(n));
d = lambda_min + (lambda_max-lambda_min) * rand(n,1);
A = U * diag(d) * U';
x = 2*rand(n,1)-1;
f = A*x;

alpha =-0.1:0.05:0.1;
x0=zeros(n,1);
maxit=50;
tol=1e-15;

Y= zeros(length(alpha)+1,maxit+1);
hold on;
set(gca,'yscale','log')
for i=1:length(alpha)
    [~,X] = FixedPointSolve(A,f,x0,alpha(i),maxit,tol);
    
    for j=1:length(X)
        Y(i,j) = (norm(X{j}-x,2))/norm(x,2);
    end
    plot(0:(length(Y(i,:))-1),Y(i,:))
end
alphaa=length(alpha);
L = norm(eye(n)-alpha(alphaa)*A,2);

for j=1:length(Y(alphaa,:))
    Y(alphaa+1,j) = L^j*norm(x);
end
plot(0:(length(Y(alphaa+1,:))-1),Y(alphaa+1,:),'b--')
hold off;
legend('-0.1','-0.05','0','0.05','0.1','theorical')
xlabel('Iterations number')
ylabel('Relative error')
% gr�ce au graphique on remarque qu'on obtient la convergence la plus rapide avec alpha = 0.1