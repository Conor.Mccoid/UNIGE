function [x,X] =  GaussSeidel_FixedPointSolve(A,f,x0,maxit,tol)
% FIXEDPOINTSOLVE solves a linear system Ax=f
% using a fixed-point iteration.
% The function terminates when
% the maximum number of iterations
% maxiter is reached
% or when the relative step becomes smaller than tol.
% The output argument X, when requested, is a matrix,
% the kth column of which is the kth iterate.
B=tril(A); % ici on utilise la m�thode de Gauss-Seidel o� B est la partie triangulaire inf�rieure de A
C= -triu(A,1);%et ?C est la partie triangulaire strictement sup�rieure
X=cell(1,maxit);
X{1} = x0;
i=1;
while  abs(norm(A*X{i}-f,2)) > tol && i< maxit
    x = B^-1*(C*X{i}+f);
    i=i+1;
    X{i}=x;
end
X(~cellfun('isempty',X));%ici on choisit de ne pas prendre les cellules vides de X
end