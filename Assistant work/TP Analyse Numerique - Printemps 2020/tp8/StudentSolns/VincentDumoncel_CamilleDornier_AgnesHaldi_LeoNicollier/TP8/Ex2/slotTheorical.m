function Y = slotTheorical(A,x,x0,maxit,methode)
%SLOTTHEORICAL va cr�er nos facteurs B�tas pour respectivement la m�thode
%de Jacobi et de Gauss-Seidel
if methode == "Jacobi"
    B=diag(diag(A));
    C= B-A; %on d�finit nos matrices de splitting B et C selon la m�thode de J
    beta = max(abs(eig(B^-1*C)));
    Y=cell(1,maxit);
    y=beta*norm(x0-x,2);
    Y{1}=y;
    for i=2:maxit
        y= beta*Y{i-1};
        Y{i}=y;
    end
    Y(~cellfun('isempty',Y));%ici on choisit de ne pas prendre les cellules vides de Y
elseif methode == "GaussSeidel"
    B=tril(A);
    C= -triu(A,1); %on d�finit nos matrices de splitting B et C selon la m�thode de GS
    beta = max(abs(eig(B^-1*C)));
    Y=cell(1,maxit);
    y=beta*norm(x0-x,2);
    Y{1}=y;
    for i=2:maxit
        y= beta*Y{i-1};
        Y{i}=y;
    end
    Y(~cellfun('isempty',Y));%ici on choisit de ne pas prendre les cellules vides de Y
    
else
    Y=[];
end
end

