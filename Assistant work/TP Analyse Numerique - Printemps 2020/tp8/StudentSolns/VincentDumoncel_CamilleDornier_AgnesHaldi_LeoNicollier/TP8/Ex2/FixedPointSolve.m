function [x,X] = FixedPointSolve(A,f,x0,alpha,maxit,tol)
% FIXEDPOINTSOLVE solves a linear system Ax=f
% using a fixed-point iteration.
% The function terminates when
% the maximum number of iterations
% maxiter is reached
% or when the relative step becomes smaller than tol.
% The output argument X, when requested, is a matrix,
% the kth column of which is the kth iterate.


X=cell(1,maxit);
X{1}=x0;
i=1;
while  abs(norm(A*X{i}-f,2)) > tol && i< maxit 
    x = X{i} -alpha *(A*X{i}-f);
    i=i+1;
    X{i}=x;
end
