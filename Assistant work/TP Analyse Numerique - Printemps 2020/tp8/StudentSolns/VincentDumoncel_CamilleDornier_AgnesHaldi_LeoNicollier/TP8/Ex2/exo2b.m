% Exo 2 Point 3
clear all;
clc
n = 100;
lambda_min = 3;
lambda_max = 17;
[U,~] = qr(rand(n));
d = lambda_min + (lambda_max-lambda_min) * rand(n,1);
A = U * diag(d) * U';
x = 2*rand(n,1)-1;
f = A*x;

alpha =-0.1:0.05:0.1;
x0=zeros(n,1);
maxit=50;
tol=1e-15;

Y= zeros(4,maxit);
hold on;
set(gca,'yscale','log')
%On s'int�resse d'abord aux valeurs avec la m�thode de Jacobi
[~,X] = Jacobi_FixedPointSolve(A,f,x0,maxit,tol);
    
for j=1:length(X(1,:))
    Y(1,j) = (norm(X{j}-x,2))/norm(x,2);
end
plot(0:(length(Y(1,:))-1),Y(1,:))
Y(2,:) = cell2mat(slotTheorical(A,x,x0,maxit,"Jacobi")); %on en appelle � la fonction slotTheorical pour plot nos facteurs B�ta J
plot(0:(length(Y(2,:))-1),Y(2,:),'--')
%puis aux valeurs avec la m�thode de Gauss Seidel
[~,X] = GaussSeidel_FixedPointSolve(A,f,x0,maxit,tol);
    
for j=1:length(X)
    Y(3,j) = (norm(X{j}-x,2))/norm(x,2);
end
plot(0:(length(Y(3,:))-1),Y(3,:))
Y(4,:) = cell2mat(slotTheorical(A,x,x0,maxit,"GaussSeidel"));%on en appelle � la fonction slotTheorical pour plot nos facteurs B�ta GS
plot(0:(length(Y(4,:))-1),Y(4,:),'--')
hold off;
legend('Jacobi','Slope Bj','Gauss Seidel','Slope BGS')
xlabel('Iterations number')
ylabel('error')

