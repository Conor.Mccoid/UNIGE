function [x,X] = Jacobi_FixedPointSolve(A,f,x0,maxit,tol)
% FIXEDPOINTSOLVE solves a linear system Ax=f
% using a fixed-point iteration.
% The function terminates when
% the maximum number of iterations
% maxiter is reached
% or when the relative step becomes smaller than tol.
% The output argument X, when requested, is a matrix,
% the kth column of which is the kth iterate.
X=cell(1,maxit);
X{1} = x0;
B=diag(diag(A));%ici on utilise la m�thode de jacobi o� B est la diagonale de A 
C= B-A;%et -C est la partie en dehors de la diagonale
i=1;
while  abs(norm(A*X{i}-f,2)) > tol && i< maxit
    x = B^-1*(C*X{i}+f);
    i=i+1;
    X{i}=x;
end
X(~cellfun('isempty',X));%ici on choisit de ne pas prendre les cellules vides de X
end