f1 = @(x) x.^2-3*x+4;
f2 = @(x) (x+1).*(x+5).^3*(x-5).^2;
[x,X] = BisectSolve(f1,-3.8,0.5,30,10^-10)
%f1 nest jamais egale a 0
x


[x,X] = BisectSolve(f2,-3.8,0.5,30,10^-10);
%la racine entre -3.8 et 0.5 de f2 est x= -1
x
% Notre code s'arrrete avec n=30 et abs(b-a)=4.0047e-09
% Pour atteindre une précision de 10^-10, on a besoin d'un n=36 et on a une
% tol de 6.2573e-11
%pour trouver les autres racines, il suffit de changer a et b 

% On a vu en cours qu'il fallait que n > log((b-a)/10^(-10))/log(2)=35.3236

% L'erreur absolue est au plus (b-a)/2^(n+1)
hold on;
scatter(1:length(X),abs(cell2mat(X)+1),'bo')
m=1;
Y= abs(cell2mat(X)+1).^m; Y(1)=[];
plot(1:length(Y),(Y),'-r')
set(gca,'yscale','log')
xlabel('Iteration Number') 
ylabel('Error')
legend('f2','theorical')
%ordre de la methode m=1;
