function [x,X] = BisectSolve(f,a,b,maxit,tol)
% BISECTSOLVE solves the equation f(x)=0 using
% the bisection method on the interval [x1,x2].
% The function terminates when the interval
% becomes smaller than tol
% or when the maximum number of iterations
% maxiter is reached.
% The output argument X, when requested, is a row,
% the kth entry of which is the kth iterate.
ok = 0;
X=[];
n=0;
r=f(a)*f(b);
if r < 0 
    X=cell(1,maxit);
    while  abs(b-a) > tol && ok==0 && n< maxit
        x = (a+b)/2;
        if f(a)*f(x) < 0
            b = x;
        elseif f(a)*f(x) > 0
            a=x;
        else 
            ok =1;
        end
        n=n+1;
        X{n}=x;
    end
    X(~cellfun('isempty',X))
    abs(b-a)
    tolobtenu = log(abs(b-a))/log(10)
    fprintf('Une des racines entre a et b est environ %f avec %.0f itérations et avec une précision de 10^%f',x,n,tolobtenu)
    
    %n
    %abs(b-a)
elseif r > 0
    disp('la fonction na pas de racine entre ses points')
    X = NaN;
    x= NaN;
else 
    disp('Votre fonction sannule en a ou en b')
    X = NaN;
    x= NaN;
end

