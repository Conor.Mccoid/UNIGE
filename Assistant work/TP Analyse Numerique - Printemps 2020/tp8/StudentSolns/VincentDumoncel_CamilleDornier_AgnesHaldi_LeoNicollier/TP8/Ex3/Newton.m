function [x,X] = Newton(f,df,x0,tol,maxiter)
% NEWTON solves the equation f(x)=0 using
% Newton's method with initial guess x0.
% The function terminates when the Newton step length is less than tol
% or when the maximum number of iterations maxiter is reached.
% The output argument X, when requested, is a matrix,
% the kth column of which is the kth iterate.

X=cell(1,maxiter);
X{1}=x0;
i=1;

while  norm(f(X{i}),2)>tol && i< maxiter
    x = X{i} - df(X{i})\f(X{i});
    i=i+1;
    X{i}=x;
end
X(~cellfun('isempty',X));%ici on choisit de ne pas prendre les cellules vides de X