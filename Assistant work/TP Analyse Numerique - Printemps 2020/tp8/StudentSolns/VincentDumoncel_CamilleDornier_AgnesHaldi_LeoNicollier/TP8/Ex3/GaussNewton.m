function x = GaussNewton(f,df,x0,tol,maxiter)
%GAUSSNEWTON 
x=x0;
i=1;
while  norm(f(x),2)>tol && i< maxiter
    p=(df(x)'*df(x))\(df(x)'*f(x));
    %p = df(x)\f(x);
    x = x - p;
    i=i+1;
end
end