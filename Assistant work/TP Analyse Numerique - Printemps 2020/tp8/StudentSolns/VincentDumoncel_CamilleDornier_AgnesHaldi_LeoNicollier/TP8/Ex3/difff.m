function df = difff(f, a, d)
x = a;
fa = f(a);
for j = 1:size(x,1)
    x(j) = x(j) + d;
    df(:,j) = (f(x) - fa)/d;
    x(j) = a(j);
end