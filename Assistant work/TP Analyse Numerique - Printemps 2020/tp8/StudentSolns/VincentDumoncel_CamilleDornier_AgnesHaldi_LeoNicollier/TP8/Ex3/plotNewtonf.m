function Y = plotNewtonf(f,df,x0,maxiter,tol,x)
 Y=zeros(1,maxiter);
[~,X] = Newton(f,df,x0,tol,maxiter);
for j=1:length(X(1,:))
    Y(j) = (norm(X{j}-x,2));
end
end