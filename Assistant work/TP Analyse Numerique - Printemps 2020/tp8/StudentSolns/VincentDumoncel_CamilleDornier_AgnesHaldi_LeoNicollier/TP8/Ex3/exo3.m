%% 3.1.c
clear all
clc
maxiter=25;

tol=10^-15;
root= roots([1 0 -1 -3]);
x=real(root(1));

f=@(x) x.^3-x-3;
df=@(x) 3*x.^2-1;
hold on 
%ici on choisit de plot l'erreur entre les approximations de newton 
%(avec pour condition initiales respectivement x0=0 et x0=1) et la courbe de notre fonction
x0=0; 
Y0 = plotNewtonf(f,df,x0, maxiter,tol,x);
x0=1;
Y1 = plotNewtonf(f,df,x0, maxiter,tol,x);
plot(0:length(Y0)-1,Y0)
plot(0:length(Y1)-1,Y1)
set(gca,'yscale','log')
legend('x0=0','x0=1')
xlabel('Iterations numbers')
ylabel('Error')
%on remarque que l'approximation avec la m�thode de Newton qui a pour
%condition initiale x0=1 converge relativement rapidement contrairement �
%celle qui s'initialise � x0=0. La raison est que la m�thode de Newton est
%efficace pour un x0 suffisamment proche de notre racine x*, ce qui dans
%notre cas correspont � x0=1.
%% 3.1.d
clear all
clc
f  = @(x) [x(1)^3-3*x(1)*x(2)^2-1; -x(2)^3+3*x(1)^2*x(2)];
df = @(x) [3*x(1)^2-3*x(2)^2    -6*x(1)*x(2);6*x(1)*x(2)     -3*x(2)^2+3*x(1)^2];
maxiter=100;
tol=10^-8;
x0=[-1 1]';
[x,X]=Newton(f,df,x0,tol,maxiter);
x
%on a bien les solutions voulues. 
%% 3.2.a

clear all 
clc
f  = @(x) [x(1) - 1; (x(2) + 1)^2];
df = @(x) [1      0;0      2*(x(2) + 1) ];
x0=[0;0];
tol=10^-10;
maxiter= 100;
x = GaussNewton(f,df,x0,tol,maxiter)

%% 3.2.b
clear all 
clc
Udata = [-0.0480 -0.0100  0.0490 -0.0190  0.0600  0.0125;0.0290  0.0305  0.0285  0.0115 -0.0005 -0.0270];
Xdata = [9855  8170  2885  8900  5700  8980;5680  5020   730  7530  7025 11120;3825  4013  4107  3444  3008  3412];
dx = 10^-10;
f  = @(x) fonction(Udata, Xdata, x);
df = @(x) difff(f, x, dx);
x0 = [8000; 15000; 1000; 0; -1; 0; 0];
tol = 10^-15; maxiter = 1000;
X=GaussNewton(f, df, x0, tol, maxiter);
x=X(1)
y=X(2)
z=X(3)
a=X(4)
b=X(5)
c=X(6)
theta=X(7)
%On remarque que c'est � l'aiguille verte que Gerhard a pris cette photo 
%Bravo G�rard clap clap 