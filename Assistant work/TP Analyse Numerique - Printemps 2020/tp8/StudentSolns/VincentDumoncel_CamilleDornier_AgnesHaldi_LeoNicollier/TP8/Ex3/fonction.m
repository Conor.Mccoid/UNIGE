function f = fonction(Udata, Xdata, x)
%x= [8000; 15000; 1000; 0; -1; 0; 0];
% notation locale
X = x(1:3);
a = x(4);
b = x(5);
c = x(6);
theta = x(7);

% vecteur horizontal
H = [b; -a; 0];
H = H/norm(H);

% vecteur vertical
G = [-a*c; -b*c; a^2 + b^2];
G = G/norm(G);

% les points
% rotation initiale
C = cos(theta); S = sin(theta);
Us = [C -S;S  C]*Udata;

% les vecteurs � aligner
Ws = [a;b;c] + [H G]*Us;
Qs = Xdata - X;

% les fonctions � annuler
f = cross(Ws,Qs);
%permet de tout mettre en une seule colonne
f = f(:);