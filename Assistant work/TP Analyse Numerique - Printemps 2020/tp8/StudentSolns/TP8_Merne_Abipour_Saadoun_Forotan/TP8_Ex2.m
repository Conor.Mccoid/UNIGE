%% Exercice 2: M�thode du point fixe

%% 1) Impl�mentation de l'it�ration du point fixe

%L'impl�mentation de la m�thode du point fixe est situ�e plus bas (Partie
%"Fonctions utilis�es dans l'exercices".


%% 2) Test de l'it�ration du point fixe
%Cr�ation du syst�me lin�aire
n = 100;
lambda_min = 3;
lambda_max = 17;
[U, ~] = qr(rand(n));
d = lambda_min + (lambda_max - lambda_min) * rand(n, 1);
A = U * diag(d) * U';
x = 2 * rand(n, 1) -1;
f = A * x;
 
%On d�finit nos variables donn�es dans l'�nonc�
x0 = zeros(n, 1);
alpha = (-.1:.05:.1);
maxit = 50;
tol = 1e-15;
nb_alpha = size(alpha,2); %Variable contenant le nombre d'alphas � tester

%On initialise notre tableau qui contient nos erreurs pour chaque it�ration
%pour les diff�rents alphas. On utilise un tableau de Nan plut�t qu'un
%tableau vide pour �viter d'avoir des z�ros si notre FixedPointSolve se
%termine avant d'atteindre le nombre d'it�rations maximal (maxit). Ces
%z�ros n'ont pas leur place sur le graphique.

err = zeros(nb_alpha, maxit);
err_th = zeros(nb_alpha, maxit);

%On initialise la boucle pour tester l'erreur de chaque alpha
for i = 1:nb_alpha
    %On appelle la fonction FixedPointSolve sur nos donn�es
    [~,XFixed] = FixedPointSolve(A,f,x0,alpha(1, i),maxit,tol);
    %On demande uniquement de retourner X qui contient chaque it�ration
    %On initialise la boucle qui va calculer l'erreur sur chaque it�ration
    for j = 1:size(XFixed,2)
        %Calcul de l'erreur relative 
        err(i,j) = norm(XFixed(:,j)-x)/norm(x);
        err_th(i,j) = norm((x0-x))*(norm(eye(size(A))-alpha(1,i).*A))^j;
    end
end

figure(1);

subplot(1,2,1); %Affichage de l'erreur relative et th�orique pour chaque alpha 
plot(1:maxit, err(:,:));
hold on
plot(1:maxit,err_th(:,:),'--') ;
hold off
xlim([0 50]); ylim([1e-10 1e15]); set(gca,'yscale','log');
xlabel('Iteration number'); ylabel('Relative error');
legend(num2str(alpha(1)),num2str(alpha(2)), num2str(alpha(3)), ...
    num2str(alpha(4)), num2str(alpha(5)), 'Theoretical', 'Location','northwest');
%On a choisi de laisser afficher l erreur theorique pour chaque alpha. 


%On voit que la valeur de convergence la plus rapide est pour alpha = 0,1.
%On voit aussi que les alphas n�gatifs font exploser l'erreur. 
 
%% 3) Splitting alg�brique
 
% Avec A = B - C, Ax=f equivaut� Bx=Cx+f

%Pour trouver notre facteur de convergence gamma
%On part de notre it�ration de point fixe 
% x(k+1)=(B^-1)*(C*x(k)+A*x)
%On soustrait x et on multiplie par B (inversible) des 2 c�t�s
% B*(x(k+1)-x)=C*x(k)+A*x-C*x=C*x(k)-C*x=C*(x(k)-x)
%On multiplie � nouveau par B^-1 des 2 c�t�s
% (x(k+1)-x)=(B^-1)*C*(x(k)-x)
%On applique la norme 2 et l'in�galit� fondamentale des op�rateurs (Cauchy-Schwartz)
%||(x(k+1)-x)||2<=||(B^-1)*C||2 * ||(x(k)-x)||2
%Notre facteur de convergence gamma=||(B^-1)*C||2
%On pour la m�thode de Jacobi et Gauss-Seidel
[BJacobi, CJacobi, XJacobi] = FixedPointSolve_Jacobi(A,f, x0, maxit,tol);
[BGauss, CGauss, XGauss] = FixedPointSolve_Gauss(A,f, x0, maxit,tol);
err_Jacobi=zeros(maxit,1);
err_Bj=zeros(maxit,1);
err_Gauss=zeros(maxit,1);
err_Bgs=zeros(maxit,1);

for j = 1:size(XJacobi,2)
        %Calcul de l'erreur relative 
        err_Jacobi(j) = norm((x0-x), 2)*(norm((BJacobi^-1)*CJacobi, 2))^j;
        err_Bj(j) = norm((x0-x), 2)*(max(abs(eig(((BJacobi^-1)*CJacobi)))))^j;
end
for j = 1:size(XGauss,2)
        %Calcul de l'erreur relative 
        err_Gauss(j) = norm((x0-x), 2)*(norm((BGauss^-1)*CGauss, 2))^j;
        err_Bgs(j) = norm((x0-x), 2)*(max(abs(eig(((BGauss^-1)*CGauss)))))^j;
end

subplot(1,2,2); %Affichage de l'erreur relative et th�orique pour chaque alpha avec le "splitting" alg�brique

hold on
plot(1:maxit, err_Jacobi, 'r');
plot(1:maxit, err_Bj, 'r--');
plot(1:maxit, err_Gauss, 'b');
plot(1:maxit, err_Bgs, 'b--');
hold off
legend("Jacobi", "Slope \beta_{J}", "Gauss-Seidel", "Slope \beta_{GS}");
xlim([0 40]); set(gca,'yscale','log');
xlabel('Iteration number'); ylabel('||x^{(k)}-x_{*}||_{2}');

%% Fonctions utilis�es dans l'exercice

 function [x,X] = FixedPointSolve(A,f,x0,alpha,maxit,tol)
% FIXEDPOINTSOLVE solves a linear system Ax=f
% using a fixed-point iteration.
% The function terminates when
% the maximum number of iterations
% maxiter is reached
% or when the relative step becomes smaller than tol.
% The output argument X, when requested, is a matrix,
% the kth column of which is the kth iterate.

 %initialisation de la matrice contenant nos iterations et de la variable
 %contenant le nombre d'it�rations
    x_iter = x0;
    nb_iter=0;
    % boucle d'it�rations
    for i = 1:maxit
        nb_iter = nb_iter+1; %Ajoute 1 au nb d'it�rations
        %Application de l'iteration du point fixe, on ajoute chaque
        %iteration a la matrice x_iter.
        x_iter =[x_iter (x_iter(:,i) - alpha.*(A*x_iter(:,i)-f))];
        %On calcule la difference entre deux iterations successives
        error = norm( x_iter(:,i) - x_iter(:,i+1) );
        %si l'erreur est inferieure a la tolerance on sort de la boucle. 
        if error < tol
            break;
        end
    end
    % On veut que la keme colone de X contienen la keme iteration. 
    X = x_iter(:,2:nb_iter);
    %La solution approch�e x est l'it�ration maxit ou celle qui rend error
    %inferieur a tol.
    x = x_iter(:,nb_iter);
    
 end




 function [B,C,X] = FixedPointSolve_Jacobi(A,f, x0, maxit,tol)
%pour la methode de Jacobi on extrait la diagonale de A 
 B = diag(diag(A)); 
%On stock la partie en dehors de la diagonale dans C. 
 C = B - A ;
 x_iter = x0;
    nb_iter=0;
    % boucle d'it�rations
    for i = 1:maxit
        nb_iter = nb_iter+1; %Ajoute 1 au nb d'it�rations
        %Application de l'iteration du point fixe, on ajoute chaque
        %iteration a la matrice x_iter.
        %x_iter =[x_iter (x_iter(:,i) - B^(-1)*(C*x_iter(:,i)+f))];
        x_iter =[x_iter (Dsol(B, (C*x_iter(:,i)+f)))];
        %On calcule la difference entre deux iterations successives
        error = norm( x_iter(:,i) - x_iter(:,i+1) );
        %si l'erreur est inferieure a la tolerance on sort de la boucle. 
        if error < tol
            break;
        end
    end
    % On veut que la keme colone de X contienne la keme iteration. 
    X = x_iter(:,2:nb_iter);   
 end




 function [B,C,X] = FixedPointSolve_Gauss(A,f, x0, maxit,tol)
%Pour la methode de Gauss on extrait la partie triangulaire inf�rieure de A 
 B = tril(A); 
%On prend le reste dans C. 
 C = B - A ;
 x_iter = x0;
    nb_iter=0;
    % boucle d'it�rations
    for i = 1:maxit
        nb_iter = nb_iter+1; %Ajoute 1 au nb d'it�rations
        %Application de l'iteration du point fixe, on ajoute chaque
        %iteration a la matrice x_iter.
        x_iter =[x_iter (lsol(B, (C*x_iter(:,i)+f)))];
        %On calcule la difference entre deux iterations successives
        error = norm( x_iter(:,i) - x_iter(:,i+1) );
        %si l'erreur est inferieure a la tolerance on sort de la boucle. 
        if error < tol
            break;
        end
    end
    % On veut que la keme colone de X contienen la keme iteration. 
    X = x_iter(:,2:nb_iter);   
 end
 
 
 function y = lsol(L,b)
% LSOL solves the system Lx = b for lower triangular L
%   y = lsol(L,b) solves the system Lx = b, where L is an
%   lower triangular matrix.
n = numel(b);
y = zeros(n,1);
for i =1:1:n
    y(i) = (b(i) - L(i,1:i-1)*y(1:i-1)) / L(i,i);
end
end


function y = Dsol(D,b)
n = numel(b);
y = zeros(n,1);
for i=n:-1:1
    y(i) = b(i)/ D(i,i);
end
end