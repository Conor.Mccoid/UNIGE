 function y = lsol(L,b)
% LSOL solves the system Lx = b for lower triangular L
%   y = lsol(L,b) solves the system Lx = b, where L is an
%   lower triangular matrix.
n = numel(b);
y = zeros(n,1);
for i =1:1:n
    y(i) = (b(i) - L(i,1:i-1)*y(1:i-1)) / L(i,i);
end
end