%% Exercice 3: M�thode de Newton

%% Point 1) Impl�mentation de la m�thode de Newton

%% Point 1 - Partie 1: R�solution d'un syst�me scalaire

tol = 1e-10;
maxiter = 100;
f = @(x) x.^3-x-3;
df = @(x) 3*x.^2-1;

[s0, S0]= NewtonScalaire(f, df, 0, tol, maxiter);
[s1, S1]= NewtonScalaire(f, df, 1, tol, maxiter);

k0 = zeros(length(S0), 1);
k1 = zeros(length(S1), 1);

for i = 1:1:length(S0)
    k0(i) = i-1;
end
for i = 1:1:length(S1)
    k1(i) = i-1;
end

x = linspace(-5, 5, 100);

figure(1);
subplot(1, 3, 1)
plot(x, f(x));
hold on
plot(s0, f(s0), '*');
plot(s1, f(s1), '*');
xlabel("x"); ylabel("y");
legend("f(x)", "Solution obtenue avec x0 = 0", "Solution obtenue avec x0 = 1")
hold off

subplot(1, 3, 2)
plot(k0, S0, "*");
axis([0, k0(length(S0)), -4, 4]);
title("x_0 = 0"); xlabel("Nombre d'it�rations (k)"); ylabel("Valeur de x_{k}");

subplot(1, 3, 3)
plot(k1, S1, "*");
axis([0, k1(length(S1)), -4, 4]);
title("x_0 = 1"); xlabel("Nombre d'it�rations (k)"); ylabel("Valeur de x_{k}");

% On constate que lorsque nous prenons comme condition initiale x0 = 1, la
% m�thode de Newton converge vers la solution du probl�me f(x) = 0, tandis
% que lorsque la condition initiale est x0 = 0, nous avons des oscillations
% entre quatre valeurs diff�rentes.

%% Point 1 - Partie 2: R�solution d'un syst�me vectoriel

g = @(X) [X(1).^3 - 3.*X(1).*X(2).^2 - 1; 3.*X(1).^2.*X(2) - X(2).^3];
dg = @(X) [3.*X(1).^2 - 3.*X(2).^2, -6.*X(1).*X(2); 6.*X(1).*X(2), 3.*X(1).^2 - 3.*X(2).^2]; %Matrice anti-sym�trique

[p1, P1] = NewtonVectorielle(g, dg, [-1;1], tol, maxiter); %Cela ne nous donne pas exactement le m�me r�sultat que dans le polycopi�
fprintf("Point 1 - Partie 2: R�solution dun syst�me 'vectorielle'\n");
fprintf("R�sultat obtenu avec la m�thode de Newton 'vectorielle'\n");
disp(p1);


[X1,Y1] = meshgrid(-1.5:0.1:0, 0:0.1:1.5);

figure (2)
quiver(X1,Y1, X1.^3 - 3.*X1.*Y1.^2 - 1, 3.*X1.^2.*Y1 - Y1.^3);
hold on
plot(-1, 1, "*"); %Point de d�part
plot(p1(1), p1(2), "*"); %Point d'arriv�e
hold off
title("Repr�sentation graphique de g"); xlabel("x"); ylabel("y");
legend("g(x,y)", "Point de d�part", "Point d'arriv�e");

%% Point 2) Impl�mentation de la m�thode de Gauss-Newton

%% Point 2 - a: Test de la fonction r�alisant la m�thode de Gauss-Newton

h = @(X) [X(1)-1; (X(2)+1)^2]; %La norme de cette fonction est minimale lorsque (x1,x2)=(1,-1)
dh = @(X) [1, 0; 0, 2*(X(2)+1)];

[p2GN, P2GN] = GaussNewton(h, dh, [0;0], tol, maxiter);

fprintf("Point 2 - a: Test de la m�thode de Gauss-Newton\n");
fprintf("R�sultat obtenu avec la m�thode de Gauss-Newton\n");
disp(p2GN); %Cela correspond au r�sultat attendu

%% Point 2 - b: Application de la m�thode de Newton au probl�me d'identification de param�tres de Ernst Hairer

%Donn�es provenant du polycopi� de Hairer p.147 (u,v,x,y,z)
u = [-0.0480 -0.0100 0.0490 -0.0190 0.0600 0.0125];
v = [0.0290 0.0305 0.0285 0.0115 -0.0005 -0.0270];
x = [9855 8170 2885 8900 5700 8980];
y = [5680 5020 730 7530 7025 11120];
z = [3825 4013 4107 3444 3008 3412];

alpha = @(k, theta) cos(theta) * u(k) + sin(theta) * v(k);
beta = @(k, theta) -sin(theta) * u(k) + cos(theta) * v(k);

h = @(a,b) 1 ./ (sqrt(a.^2 + b.^2)) .* [b; -a; 0];
g = @(a,b,c) 1 ./ (sqrt((a.^2 + b.^2) .* (a.^2 + b.^2 + c.^2))) .* [-a.*c; -b.*c; a.^2 + b.^2];
w = @(a,b,c,k,theta) [a; b; c] + alpha(k, theta) .* h(a,b) + beta(k,theta) .* g(a,b,c);

f = @(x_h, y_h, z_h, a, b, c, k, theta) cross(w(a,b,c,k,theta),[x(k) - x_h; y(k) - y_h; z(k) - z_h]);
%Faire le calcul du jacobien de cette fonction me para�t un peu long...

%D'apr�s le polycopi� de Ernst Hairer, la photo aurait �t� prise depuis
%l'Aiguille Verte, en sachant que les r�sultats qu'il obtient par la m�thode
%de Gauss-Newton sont: x_h = 9664, y_h = 13115, z_h = 4116

%% Code utilis� pour l'exercice

function y = SuiteNewtonScalaire(f, df, x)
y = x - f(x)/df(x);
end

function [x,X] = NewtonScalaire(f, df, x0, tol, maxiter)
% NEWTON solves the equation f(x)=0 using
% Newton's method with initial guess x0.
% The function terminates when the Newton step length is less than tol
% or when the maximum number of iterations maxiter is reached.
% The output argument X, when requested, is a matrix,
% the kth column of which is the kth iterate.

k = 1;
X = [x0]; %Valeur initiale
X = [x0, SuiteNewtonScalaire(f, df, X(k))]; %Valeur initiale, Premi�re it�r�e

while (abs(X(k+1) - X(k)) >= tol && k <= maxiter)
    k = k+1;
    X = [X, SuiteNewtonScalaire(f, df, X(k))];
end

x = X(k);
end

function y = SuiteNewtonVectorielle(f, df, x)
pk = (-1)* df(x) \ f(x);
y = x + pk;
end

function [x,X] = NewtonVectorielle(f, df, x0, tol, maxiter)
% NEWTON solves the equation f(x)=0 using
% Newton's method with initial guess x0.
% The function terminates when the Newton step length is less than tol
% or when the maximum number of iterations maxiter is reached.
% The output argument X, when requested, is a matrix,
% the kth column of which is the kth iterate.

k = 1;
X = [x0]; %Valeur initiale
X = [x0, SuiteNewtonVectorielle(f, df, X(:,k))]; %Valeur initiale, Premi�re it�r�e

while (norm(X(:,k+1) - X(:,k),2) >= tol && k <= maxiter)
    X = [X, SuiteNewtonVectorielle(f, df, X(:,k+1))];
    k= k+1;
end

x = X(:,k);
end


function [x, X] = GaussNewton(f,df,x0,tol,maxiter)
k = 1;
X = [x0]; %Valeur initiale

[Q,R] = Householder(df(X(:,k)));
DeltaX = usol(R, Q'*(-f(X(:,k))));
X = [X, X(:,k)+DeltaX]; %Valeur initiale, Premi�re it�r�e

while (norm(X(:,k+1) - X(:,k), 2) >= tol && k <= maxiter)
    k = k+1;
    
    [Q,R] = Householder(df(X(:,k)));
    DeltaX = usol(R, Q'*(-f(X(:,k))));
    
    X = [X,  X(:,k)+DeltaX];
end

x = X(:,k);
end

%% Code provenant du TP7

function [Q,R] = Householder(A)

r = rank(A);
[m, n] = size(A);

    Q1 = eye(m);
    R2 = A;
    for k = 1:n
        x = R2(k:m, k);
        Vk = x + sign(x(1))*norm(x)*[1 ; zeros(m - k, 1)];
        
        if norm(Vk) > 0
            Vk = Vk/norm(Vk);
        end
    
        % partie R
        for i = k:n
            R2(k:m, i) = R2(k:m, i) - 2*(Vk' * R2(k:m, i)) * Vk;
        end
        
        %partie Q
        if norm(Vk) > 0
            Hk = eye(m - k + 1) - 2* Vk * Vk';
            Qk = eye(m);
            Qk(k:m, k:m) = Hk;
            Q1 = Q1 * Qk;
        end
    end
    R = R2(1:n, 1:n); % R ; il n'y a que des 0 en-dessous
    Q = Q1(1:m, 1:n); %on prend la matrice de la dimension qui nous int�resse
      
end


function y = usol(U,b)
% USOL solves the system Ux = b for upper triangular U
%   y = usol(U,b) solves the system Ux = b, where U is a
%   upper triangular matrix.
n = numel(b);
y = zeros(n,1);
for i=n:-1:1
    y(i) = (b(i) - U(i,i+1:n)*y(i+1:n)) / U(i,i);
end
end