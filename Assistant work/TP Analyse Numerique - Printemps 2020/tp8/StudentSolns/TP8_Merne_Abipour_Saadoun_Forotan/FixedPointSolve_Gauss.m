 function [B,C,X] = FixedPointSolve_Gauss(A,f, x0, maxit,tol)
%Pour la methode de Gauss on extrait la partie triangulaire inférieure de A 
 B = tril(A); 
%On prend le reste dans C. 
 C = B - A ;
 x_iter = x0;
    nb_iter=0;
    % boucle d'itérations
    for i = 1:maxit
        nb_iter = nb_iter+1; %Ajoute 1 au nb d'itérations
        %Application de l'iteration du point fixe, on ajoute chaque
        %iteration a la matrice x_iter.
        x_iter =[x_iter (lsol(B, (C*x_iter(:,i)+f)))];
        %On calcule la difference entre deux iterations successives
        error = norm( x_iter(:,i) - x_iter(:,i+1) );
        %si l'erreur est inferieure a la tolerance on sort de la boucle. 
        if error < tol
            break;
        end
    end
    % On veut que la keme colone de X contienen la keme iteration. 
    X = x_iter(:,2:nb_iter);   
 end