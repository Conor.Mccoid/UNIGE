%% Exercice 1: M�thode de dichotomie/ m�thode de bissection

%% 1) BisectSolve function

%The function can be found in the functions section (below).

%% 2) Test of the BisectSolve function

f1 = @(x)(x.^2) - (3.*x) - 4;
%The roots of f1 are in the imaginary plane (x = (3+i*sqrt(7))/2 and
%x = (3-i*sqrt(7))/2) therefore we will not find its roots by using the
%dichotomy method.

f2 = @(x)(x+1) .* ((x+5).^3) .* ((x-5).^5);
%The roots of f2 are x=-1, x=-5, x=5. Since the roots are real we should
%be able to find them by using the dichotomy method.


%We will do a root search in the interval [-3.8, 0.5]. Therefore, we
%expect to find no roots for the function f1, and the root x=-1 for the
%function f2.
xIntMin = - 3.8;
xIntMax = 0.5;
maxiter = 30;
tol = 1e-10;

[xf1, Xf1] = BisectSolve(f1, xIntMin, xIntMax, maxiter, tol); %Not relevant
[xf2, Xf2] = BisectSolve(f2, xIntMin, xIntMax, maxiter, tol); %We find "-1"

%To be sure to find the other roots, we need to take intervals which are
%containing only one root of the list that we have defined before, and then
%we use our method BisectSolve on this interval, which permits us to get
%the root.

%% 3) Number of iterations necessary to obtain a precision of 1e-10

%From the course, we know that the number of steps (k) needed to obtain a
%result such that the error is smaller than the tolerance is given by:
%k >= log((b - a)/tol) / log(2)

fprintf("3) Number of iterations necessary to obtain a precision of 1e-10\n");

fprintf("Theoretical number of steps (k) necessary to obtain a precision of 1e-10\n");
fprintf("k >=");
disp(log((0.5 - (-3.8))/(10^(-10)))/log(2)); %~35.32 -> It needs 36 steps exactly

[xf2NbSteps, Xf2NbSteps] = BisectSolve(f2, xIntMin, xIntMax, 100, tol);
fprintf("Number of steps (k) required to obtain a precision of 1e-10\n");
fprintf("k =");
disp(length(Xf2NbSteps)); %36

%% 4) Order of the BissectSolve method

%As we can see in the following graph showing the error with respect to the
%number of iterations, the order of convergence of the dichotomy method is
%linear if we use it on the right function

x = linspace(-5, 5);
figure(1);
subplot(1,3,1);
plot(x, f1(x), 'r');
hold on
plot(xf1, f1(xf1), 'r*'); %Result of the dichotomy method
hold off
xlabel("x"); ylabel("F1(x)");

subplot(1,3,2);
plot(x, f2(x),'b');
hold on
plot(xf2, f2(xf2), 'b*'); %Result of the dichotomy method
hold off
xlabel("x"); ylabel("F2(x)");

subplot(1,3,3);
plot([1:maxiter],abs(f1(Xf1)), 'r.')
hold on;
plot([1:maxiter],abs(f2(Xf2)), 'bo')
legend("F1", "F2")
hold off;
set(gca, 'YScale', 'log');
xlabel("Number of iteration"); ylabel("Error");


%% Functions used in the exercise

function [x,X] = BisectSolve(f,x1,x2,maxit,tol)
% BISECTSOLVE solves the equation f(x)=0 using
% the bisection method on the interval [x1,x2].
% The function terminates when the interval
% becomes smaller than tol
% or when the maximum number of iterations
% maxiter is reached
% The output argument X, when requested, is a row,
% the kth entry of which is the kth iterate.

%Based on the pseudo-code of the course p.66
if f(x1)*f(x2) > 0
    fprintf('Bad choice of interval or function\n');
end

X = zeros(maxit);
k = 1; 
err = abs(x2 - x1);
ak = x1;
bk = x2;

    while ((k <= maxit) && (err > tol)) %Test of conditions
        X(k) = (ak + bk)/2;
        x = X(k);
        
        if (f(ak)*f(x) < 0)
            bk = X(k); % We don't need to pose ak = ak...
        elseif (f(ak)*f(x) > 0)
            ak = X(k);
        else
            break;
        end
        k = k+1;
        err = err/2;
        
    end
X = X(1:k-1); %We apply "-1" because we need to compensate the last addition of the while loop
end
