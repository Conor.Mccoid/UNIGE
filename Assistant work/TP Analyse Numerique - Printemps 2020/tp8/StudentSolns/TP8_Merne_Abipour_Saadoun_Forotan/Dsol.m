function y = Dsol(D,b)
n = numel(b);
y = zeros(n,1);
for i=n:-1:1
    y(i) = b(i)/ D(i,i);
end
end