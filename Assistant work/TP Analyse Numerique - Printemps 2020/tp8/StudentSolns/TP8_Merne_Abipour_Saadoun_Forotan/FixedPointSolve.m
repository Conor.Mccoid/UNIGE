 function [x,X] = FixedPointSolve(A,f,x0,alpha,maxit,tol)
% FIXEDPOINTSOLVE solves a linear system Ax=f
% using a fixed-point iteration.
% The function terminates when
% the maximum number of iterations
% maxiter is reached
% or when the relative step becomes smaller than tol.
% The output argument X, when requested, is a matrix,
% the kth column of which is the kth iterate.

 %initialisation de la matrice contenant nos iterations et de la variable
 %contenant le nombre d'itérations
    x_iter = x0;
    nb_iter=0;
    % boucle d'itérations
    for i = 1:maxit
        nb_iter = nb_iter+1; %Ajoute 1 au nb d'itérations
        %Application de l'iteration du point fixe, on ajoute chaque
        %iteration a la matrice x_iter.
        x_iter =[x_iter (x_iter(:,i) - alpha.*(A*x_iter(:,i)-f))];
        %On calcule la difference entre deux iterations successives
        error = norm( x_iter(:,i) - x_iter(:,i+1) );
        %si l'erreur est inferieure a la tolerance on sort de la boucle. 
        if error < tol
            break;
        end
    end
    % On veut que la keme colone de X contienen la keme iteration. 
    X = x_iter(:,2:nb_iter);
    %La solution approchée x est l'itération maxit ou celle qui rend error
    %inferieur a tol.
    x = x_iter(:,nb_iter);
    
 end





 
 



