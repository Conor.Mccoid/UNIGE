function [x,X] = BisectSolve(f,x1,x2,maxit,tol)
% BISECTSOLVE solves the equation f(x)=0 using
% the bisection method on the interval [x1,x2].
% The function terminates when the interval
% becomes smaller than tol
% or when the maximum number of iterations
% maxiter is reached
% The output argument X, when requested, is a row,
% the kth entry of which is the kth iterate.

%Based on the pseudo-code of the course p.66
if f(x1)*f(x2) > 0
    fprintf('Bad choice of interval or function\n');
end

X = zeros(maxit);
k = 1; 
err = abs(x2 - x1);
ak = x1;
bk = x2;

    while ((k <= maxit) && (err > tol)) %Test of conditions
        X(k) = (ak + bk)/2;
        x = X(k);
        
        if (f(ak)*f(x) < 0)
            bk = X(k); % We don't need to pose ak = ak...
        elseif (f(ak)*f(x) > 0)
            ak = X(k);
        else
            break;
        end
        k = k+1;
        err = err/2;
        
    end
X = X(1:k-1); %We apply "-1" because we need to compensate the last addition of the while loop
end