 function [B,C,X] = FixedPointSolve_Jacobi(A,f, x0, maxit,tol)
%pour la methode de Jacobi on extrait la diagonale de A 
 B = diag(diag(A)); 
%On stock la partie en dehors de la diagonale dans C. 
 C = B - A ;
 x_iter = x0;
    nb_iter=0;
    % boucle d'itérations
    for i = 1:maxit
        nb_iter = nb_iter+1; %Ajoute 1 au nb d'itérations
        %Application de l'iteration du point fixe, on ajoute chaque
        %iteration a la matrice x_iter.
        %x_iter =[x_iter (x_iter(:,i) - B^(-1)*(C*x_iter(:,i)+f))];
        x_iter =[x_iter (Dsol(B, (C*x_iter(:,i)+f)))];
        %On calcule la difference entre deux iterations successives
        error = norm( x_iter(:,i) - x_iter(:,i+1) );
        %si l'erreur est inferieure a la tolerance on sort de la boucle. 
        if error < tol
            break;
        end
    end
    % On veut que la keme colone de X contienne la keme iteration. 
    X = x_iter(:,2:nb_iter);   
 end




