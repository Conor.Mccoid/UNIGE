clc

f1 = @(x) (x^2 - 3*x - 4);
f2 = @(x) ((x + 1)*(x + 5)^3*(x-5)^5);
x1 = -3.8;
x2 = 0.5;
maxit = 30;
tol = 1e-10;

[sol1,X1]=BisectSolveTEST(f1,x1,x2,maxit,tol);
[sol2,X2]=BisectSolveTEST(f2,x1,x2,maxit,tol);


sol1 = fzero(f1,1);
sol2 = fzero(f2,1);

err1 = abs(X1 - sol1);
err2 = abs(X2 - sol2);

figure
hold on
set(gca, 'YScale', 'log')
plot(linspace(0,maxit,maxit),err1,'r.')
plot(linspace(0,maxit,maxit),err2,'bo')
xlabel('Iteration Number')
ylabel('Error')
legend('f1','f2')
hold off

%----QUESTION 2------------------------------------------------------------
% 2. Quelles sont les racines? Comment trouver les autres racines? 
% Une autre methode pour trouver les rcaines consiste a utiliser la forme 
% factoris�e comme pour f2, ses racines sont [2, 5, 6]. Sinon, on peut 
% changer d'intervale en trouvant de nouveaux x1 et x2 tq f(x1)*f(x2)<0.

%----QUESTION3-------------------------------------------------------------

% 3.Combien d'it�rations sont n�cessaires pour atteindre une pr�cision de
% tol=1e-10? Ce nombre correspond-il � nos attentes, au vu de l'analyse
% de convergence faite en cours? 
% Le nombre d'it�ration est atteint � 36. 
% Pour que Pour que |x*  - x^(k)| <= tol , avec x* la racine r�el et x^(k)
% la racine approxim�e � la ki�me it�ration, il faut que 
% k >= log2(x2-x1)-log2(1e-10)

k = log((x2-x1)/(1e-10))/log(2)

% On obtient k = 35.32.36... donc on est bien en dessous de 36. Ce nombre
% correspond bien � nos attentes. 

%----QUESTION 4------------------------------------------------------------
%Quel est l'ordre de cette m�thode?
%lim k -> infE(k+1)/E(k) est de derg� 1 donc cette m�thode est lin�aire. 
%----FONCTIONS-------------------------------------------------------------
function [x,X]=BisectSolveTEST(f,x1,x2,maxit,tol)
if (f(x1)==0)
        x=x1;
elseif  (f(x2)==0)
        x=x2;   
elseif (f(x1)*f(x2)>=0)
    fprintf("Les bornes sont de m�me signe donc pas de racine possible.")    
end

a=x1; b=x2;X=[];
nbriter = 0;

for k=1:maxit;
    x=(a+b)/2;
    X(k)=x;
    nbriter = nbriter + 1;
    if (f(x)*f(a)<0)
          b=x;
    elseif(f(x)*f(b)<0)
          a=x;
    else k=maxit;
    end
    if abs(b-a) <= tol;
        
        fprintf("Tol�rence atteinte")
       k=maxit;
    else
        k=k;
    end
end
end