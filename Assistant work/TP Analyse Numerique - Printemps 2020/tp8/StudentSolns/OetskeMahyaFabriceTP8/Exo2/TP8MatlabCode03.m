clc
clear
close all
n = 100;
lambda_min = 3;
lambda_max = 17;
[U,~] = qr(rand(n));
d = lambda_min + (lambda_max-lambda_min) * rand(n,1);
A = U * diag(d) * U';
x = 2*rand(n,1)-1;
f = A*x;

x0=zeros(n,1);
maxit=50;
tol=1e-15;
[xJ,XJ, ERRJ , ERRBJ] = JacobiSolve(A,f,x,x0,maxit,tol);
semilogy(ERRJ(1:40),'LineWidth',1.8);
hold on
semilogy(ERRBJ(1:40),'--','LineWidth',1.8);
[xGS,XGS, ERRGS , ERRBGS] = GaussSeidelSolve(A,f,x,x0,maxit,tol);
semilogy(ERRGS(1:40),'LineWidth',1.8);
semilogy(ERRBGS(1:40),'--','LineWidth',1.8);

ylabel('||x^{(k)}-x_{*}||-{2}');
xlabel('Iteration Number');
legend('Jacobi','Slop \beta_{J}','Gauss-Sidel','Slop \beta_{BG}','Location','southwest');