function [x1,X, ERROR, ERRB] = JacobiSolve(A,f,x,x0,maxit,tol)
% JacobiSolve solves a linear system Ax=f
% using a splitting A=B+C Where B is diagonal of A and C other array
% The function terminates when
% the maximum number of iterations
% maxiter is reached
% or when the relative step becomes smaller than tol.
% The output argument X, when requested, is a matrix,
% the kth column of which is the kth iterate.
% the input x is the exact solution
k=0;
ERR=10;
B=diag(diag(A));
C=A-B;
Beta=max(abs(eig(pinv(B)*C)));
while ERR>tol && k<maxit
    x1=pinv(B)*(-C*x0+f);
    k=k+1;
    ERR=norm(x-x1,2);
    ERROR(k)=ERR;
    x0=x1;
    X(:,k)=x1;
        if k==1
        ERRB(k)=norm(x1-x,2);
    else
        ERRB(k)=ERRB(k-1)*Beta;
    end

end