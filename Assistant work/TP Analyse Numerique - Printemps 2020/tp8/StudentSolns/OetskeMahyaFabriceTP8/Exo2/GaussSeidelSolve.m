function [x1,X, ERROR, ERRB] = GaussSeidelSolve(A,f,x,x0,maxit,tol)
% Gauss-Seidel solves a linear system Ax=f
% using a splitting A=B+C B est la partie triangulaire inférieure
% (diagonale inclue) de A, et
% C est la partie triangulaire strictement supérieure..
% The function terminates when
% the maximum number of iterations
% maxiter is reached
% or when the relative step becomes smaller than tol.
% The output argument X, when requested, is a matrix,
% the kth column of which is the kth iterate.
% the input x is the exact solution
k=0;
ERR=10;
C=zeros(size(A));
B=C;
for i=1:size(A,1)
    for j=1:size(A,2)
        if i>=j
            B(i,j)=A(i,j);
        else
            C(i,j)=A(i,j);
        end
    end
end
Beta=max(abs(eig(pinv(B)*C)));

while ERR>tol && k<maxit
    x1=pinv(B)*(-C*x0+f);
    k=k+1;
    ERR=norm(x1-x,2);
    ERROR(k)=ERR;
    x0=x1;
    X(:,k)=x1;
    if k==1
        ERRB(k)=norm(x1-x,2);
    else
        ERRB(k)=ERRB(k-1)*Beta;
    end
end