clc
clear
close all
n = 100;
lambda_min = 3;
lambda_max = 17;
[U,~] = qr(rand(n));
d = lambda_min + (lambda_max-lambda_min) * rand(n,1);
A = U * diag(d) * U';
x = 2*rand(n,1)-1;
f = A*x;

x0=zeros(n,1);
maxit=50;
tol=1e-15;
alpha=-0.1:.05:.1;
for i=1:length(alpha)
    [x,X,ERR] = FixedPointSolve(A,f,x0,alpha(i),maxit,tol);
    semilogy(ERR,'LineWidth',1.8)
    hold on
end
ylabel('Relative Error');
xlabel('Iteration Number');
legend('\alpha=-0.1','\alpha=-0.05','\alpha=0','\alpha=0.05','\alpha=0.1',...
        'Location','northwest');
title('convergence de litration de point fixe')
