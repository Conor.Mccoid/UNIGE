function [x,X, ERROR] = FixedPointSolve(A,f,x0,alpha,maxit,tol)
% FIXEDPOINTSOLVE solves a linear system Ax=f
% using a fixed-point iteration.
% The function terminates when
% the maximum number of iterations
% maxiter is reached
% or when the relative step becomes smaller than tol.
% The output argument X, when requested, is a matrix,
% the kth column of which is the kth iterate.
k=0;
ERR=10;
while ERR>tol && k<maxit
    x=x0-alpha*(A*x0-f);
    k=k+1;
    ERR=norm(A*x-f,2);
    ERROR(k)=ERR;
    x0=x;
    X(:,k)=x;
end