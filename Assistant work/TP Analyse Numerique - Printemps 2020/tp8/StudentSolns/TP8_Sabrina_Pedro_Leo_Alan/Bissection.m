function [x,X] = Bissection(f,a,b,maxiter,tol)
% BISECTSOLVE solves the equation f(x)=0 using
% the bisection method on the interval [x1,x2].
% The function terminates when the interval
% becomes smaller than tol
% or when the maximum number of iterations
% maxiter is reached.
% The output argument X, when requested, is a row,
% the kth entry of which is the kth iterate.
if f(a)*f(b)>0
    error('Cette fonction ne possede aucune racine reelle sur lintervalle donne')

end
if f(a)<0
     f=@(x) -f(x) ;
end
%Cas ou la racine est sur la borne des intervalles 
if f(a)==0 
    x=a;
    return
elseif f(b)==0
    x=b;
    return
end 
k=1; X=[];
while k<maxiter && abs(b-a)>tol 
    x=(a+b)/2;
    if f(a)*f(x)<0 
        b=x;
    elseif f(a)*f(x)>0 
        a=x;
    else
        break
    end
    X(k)=x;
    k=k+1;
end
end

