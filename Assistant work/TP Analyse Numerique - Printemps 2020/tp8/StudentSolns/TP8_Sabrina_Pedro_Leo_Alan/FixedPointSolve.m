function [X] = FixedPointSolve(x0,maxit,tol,Phi)
% FIXEDPOINTSOLVE trouve le point fixe de la fonction Phi en
% partant de x0. Cette fonction se termine quand le nombre d'iterations
% maximal maxit est atteint ou lorsque le pas relatif est inf�rieur � tol.
% L'argument de sorti est une matrice o� le ki�me colonne est le ki�me
% it�r�. NOTE : Cette fonction est une g�n�ralisation de la fonction
% demand�e en Ex2. 1 afin de pouvoir l'appliquer � l'exercice 2.3

%X= [x1, x2, x3, ..., xk]
X=[];k=1;
X(:,1)= Phi(x0);
X(:,2)=Phi(X(:,1));
while k+1 < maxit && norm(X(k+1)-X(k))/norm(X(k))>= tol
    [~,k]=size(X);
    X=[X Phi(X(:,k))];
end
