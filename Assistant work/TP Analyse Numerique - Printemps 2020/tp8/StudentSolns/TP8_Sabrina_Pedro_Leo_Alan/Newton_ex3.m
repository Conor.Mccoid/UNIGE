clear all
format longE

%% EXERCICE 3.1

%% 1a.
% Fonctions anonymes dont on aura besoin pour M�thode Newton
fs = @(x) x.^3-x-3;
dfs = @(x) 3*x.^2-1;

%% 1c. Newton scalaire
% Utilise notre fonction pour trouver racine approxim�e
[xs0,Xs0] = Newton(fs, dfs, 0, 10^(-10), 50);
fprintf('Valeur racine avec M�thode Newton scalaire d�part 0 est %.15f \n', xs0)
[xs1,Xs1] = Newton(fs, dfs, 1, 10^(-10), 50);
fprintf('Valeur racine avec M�thode Newton scalaire d�part 1 est %.15f \n', xs1)

% Afin de pouvoir comparer les approximations de notre racine avec la
% racine exacte du polyn�me, trouver cette racine exacte avec rootS
racine = roots([1 0 -1 -3]); % 1*x^3+0*x^2-1*x^1-3*x^0
% Remarque : on remarque que ce polyn�me a 3 racines par le th�or�me
% fondamental de l'alg�bre, mais qu'une seule d'entre elles est r�elle, on
% va donc vouloir nous concentrer sur celle-l� (1e affich�e)
xs = real(racine(1));

% Calculons les erreurs entre les approximations faites par notre fonction
% (xs0 et xs1) et notre racine exacte (xs)
for i = 1:length(Xs0(1, :)) % 1e ligne et prend toutes les colonnes
    erreur0(i) = norm(Xs0(:,i)-xs); % prend i�me vecteur et compare � xs
    % on prend la norme car si c'est un scalaire, on prendra juste la
    % valeur absolue, mais si on est dans Newton pour des vecteurs, on aura
    % bien la norme 2 (celle de base sur Matlab)
end
for i = 1:length(Xs1(1, :))
    erreur1(i) = norm(Xs1(:,i)-xs);
end

% Graphiques de la fonction et des erreurs
figure(1)
abscisses = 0:0.1:5;
image = fs(abscisses);
plot(abscisses, image)
ylim([-5, 5]) % limites axe y
xlabel('x')
ylabel('y')
title('Graphique de la fonction')
grid; % comme �a voit mieux la racine

figure(2)
hold on
nb_iter0 = 1:length(erreur0); % donne tableau allant de 1 � nb int�rations
plot(nb_iter0, erreur0, '*b')
nb_iter1 = 1:length(erreur1);
plot(nb_iter1, erreur1, '*r')
set(gca,'yscale','log') % bons axes sinon on ne voit rien sur le graphique
title('Graphique erreur approximation racine par M�thode Newton')
legend('x0 = 0','x0 = 1')
xlabel('Nombre it�rations')
ylabel('Erreur entre racine th�orique et racine trouv�e')
hold off

% COMMENTAIRES SUR LE GRAPHIQUE REPRESENTANT
% LES APPROXIMATIONS DE LA METHODE DE NEWTON
% Lorsque la valeur initiale est 0, on voit que l'erreur oscille, mais ne
% devient pas de plus en plus petite. De plus, la valeur de la racine n'est
% pas correcte et change selon le nombre d'it�rations comme oscille, c'est
% pourquoi si on choisit la donn�e initiale 0, ce n'est pas un bon choix.
% Tandis que quand la valeur initiale est 1,
% on voit clairement que l'erreur diminue jusqu'� atteindre la
% pr�cision de la machine. Ceci est d� au fait que l'approximation de la
% racine par la m�thode de Newton converge seulement si on choisit une
% donn�e initiale suffisamment proche de la racine th�orique, qui est, dans
% ce cas, environ 1.67. De plus, cette convergence est quadratique, comme
% on l'a vu en cours.


%% 1d. Newton vecteur

% x est un vecteur : x=(x1;x2)
% fv est la fonction et dfv la matrice jacobienne 2x2
fv = @(x) [x(1)^3-3*x(1)*x(2)^2-1; 3*x(1)^2*x(2)-x(2)^3];
dfv = @(x) [3*x(1)^2-3*x(2)^2, -6*x(1)*x(2) ;
    6*x(1)*x(2), 3*x(1)^2-3*x(2)^2];

% Utilise notre fonction pour trouver racine approxim�e
[xv,Xv] = Newton(fv, dfv, [-1;1], 10^(-10), 50); % x0 est vecteur colonne
fprintf('Valeur racine avec M�thode Newton vecteur avec d�part x=(-1,1)')
fprintf(' en x1 : %d \n et en x2 : %d \n',...
    xv(1), xv(2))


%% EXERCICE 3.2

%% 2a.

% Rappel, minimiser la norme d'une focnion revient � minimiser la norme au
% carr�
% fGN est la fonction et dfGN la matrice hessienne 2x2
fGN = @(x) [x(1)-1; (x(2)+1)^2];
dfGN = @(x) [1, 0 ; 0, 2*(x(2)+1)];

xGN = GaussNewton(fGN, dfGN, [0;0], 10^(-10), 50);
fprintf('Valeur racine avec M�thode GaussNewton vecteur')
fprintf(' en x1 : %d \n et en x2 : %d \n',...
    xGN(1), xGN(2))
% Ce qui est proche de la solution exacte qui est x=[1;-1]


%% 2b.

% Le probl�me consiste � trouver la position de la cam�ra,
% ses caract�ristiques (foyer) et les angles d�inclinaison.
% Pour la formulation math�matique de ce probl�me,
% on a des coordonn�es (u,v) et des coordonn�es (x,y,z) sur la carte, o� z
% est la hauteur.

UV = [-0.0480 -0.0100  0.0490 -0.0190  0.0600  0.0125;
    0.0290  0.0305  0.0285  0.0115 -0.0005 -0.0270];
XYZ = [9855  8170  2885  8900  5700  8980;
    5680  5020   730  7530  7025 11120;
    3825  4013  4107  3444  3008  3412];

pas = 10^-10; % pas pour approximer d�riv�e
F  = @(x) fct(UV, XYZ, x);
dF = @(x) jacob(F, x, pas);
x0 = [8000; 15000; 1000; 0; -1; 0; 0]; % donn�es initiales th�orie
X = GaussNewton(F, dF, x0, 10^(-15), 100);
x = X(1)
y = X(2)
z =X(3)
a = X(4);
b = X(5);
c = X(6);
theta = X(7);
% Gr�ce aux r�sultats trouv�s en (x,y,z), on a trouv� la position de la
% cam�ra.
% La photo a �t� prise depuis l'Aiguille verte.
% L'altitude de l'Aiguille Verte est 4122m et avec le code, on arrive �
% 4116 (coordonn�e z).
% Si on se positionne sur l'ar�te derri�re l'Aiguille du Moine (num�ro 6),
% on verra bien ce qu'on voit sur la photo. En plus, en 1e plan de la
% photo, tout devant, on voit un bout d'un D�me qui fait partie de
% l'Aiguille du Dru.


%% FONCTIONS

% Newton valable pour des scalaires, mais �galement pour des vecteurs
function [x,X] = Newton(f, df, x0, tol, nbmax)
% Newton r�soud l'�quation f(x)=0 en utilisant la m�thode Newton avec
% l'approximation initiale de la racine x0.
% O� f est la fonction, df sa d�riv�e/matrice jacobienne, x0
% l'approximation initiale, tol la tol�rance (pr�cision maximale de
% l'intervalle) et nbmax le nombre maximal d'it�rations.
% x est la valeur de la racine et X est un tableau avec les �tapes afin de
% trouver la bonne racine. La ki�me colonne de cette matrice est la ki�me
% it�ration.

% Initialiser les param�tres
X = []; % matrice vide qu'on rempliera petit � petit
X(:,1) = x0; % 1e colonne de matrice est valeur initiale
erreur_actuelle = 3*tol; % pour entrer dans le while

k=1; % avec boucle while parcoure le tableau colonne par colonne
while (erreur_actuelle>tol) && (k<nbmax) % quand rentre dans le while
    % pour des vecteurs, on ne veut pas calculer l'inverse de la matrice
    % jacobienne df, on va donc r�soudre df(x_k)*p_k=-f(x_k) pour p
    % l'inconnue (avec Ax=b --> x=A\b), car ensuite on aura x_k+1=x_k+p_k
    % Doonc avec la th�orie, on a (�quation 7.11 th�orie):
    X(:,k+1) = X(:,k) - (df(X(:,k)))\(f(X(:,k)));
    
    x = X(:, k+1); % C'est la valeur de la racine (en colonne)
    erreur_actuelle = norm(X(:,k+1)-X(:,k)); % erreur actuelle
    
    k=k+1; % pour passer au suivant comme est dans un while
end
end

% M�thode Gauss-Newton pour r�soudre probl�me moindres carr�s non-lin�aire
function x = GaussNewton(f, df, x0, tol, nbmax)
% Cette fonction r�soud le probl�me des moindres carr�s pour une fonction f
% allant de R^n dans R^m. Les arguments dans cette fonction ont la m�me
% description que pour Newton.

x = x0; % donn�e initiale

k=1;
% Equation 7.20 th�orie
while (norm(f(x))>tol) && (k<nbmax)
    x = x - (df(x)'*df(x))\(df(x)'*f(x));
    k=k+1;
end
end

function F = fct(UV, XYZ, x)
% Suit la th�orie du pdf Hairer p.143
% Pour fixer les inconnues, notons par (x_tilde, y_tilde, z_tilde) la
% position du foyer de la cam�ra, par le vecteur (a,b,c) la direction de
% vue avec norme correspondante � la distance du plan du film et par theta
% l�angle de rotation de la cam�ra autour du vecteur (a,b,c).

% On a mis un x en argument et on veut en extraire les x,y,z,a,b,c,theta
X = x(1:3); % x,y,z
a = x(4);
b = x(5);
c = x(6);
theta = x(7);

% calcul de h et g selon th�orie
% vecteur horizontal
h = [b; -a; 0];
h = h/norm(h);
% vecteur vertical
g = [-a*c; -b*c; a^2 + b^2];
g = g/norm(g);

% On a que (a,b,c)', h et g sont une base orthogonale et h et g engendrent
% le plan du film, avec h horizontal et g vertical.
% Alors le vecteur dans l'espace qui montre du centre de la lentille, not�
% (x_tilde, y_tilde, z_tilde), vers le point (u,v) sur le film est donn�
% par la formule suivante :

% Rotation initiale
% On construit notre matrice de rotation R
R_UV = [cos(theta) -sin(theta) ; sin(theta)  cos(theta)] * UV; % UV=(u,v)'
% Vecteurs � aligner
W = [a;b;c] + [h, g]*R_UV;
P = XYZ - X; % x_k-x_0,y_k-y_0,z_k-z_0

% Vecteurs � aligner
% On veut que vecteurs dans la matrice W et ceux dans la matrice P soient
% colin�aires => produit vectoriel
F = cross(W,P);
% Et on met tout dans une m�me colonne, comme fonction fonctionne
% pour des vecteurs, mais on ne veut pas une matrice � la fin
F = F(:);
end

function dF = jacob(f, a, pas) % jacobienne
x = a;
fa = f(a); % par d�finition de la fonction
for i = 1:size(x,1)
    x(i) = x(i) + pas;
    dF(:,i) = (f(x) - fa)/pas; % Calcul approx de la d�riv�e partielle en x_i
    x(i) = a(i);
end
end