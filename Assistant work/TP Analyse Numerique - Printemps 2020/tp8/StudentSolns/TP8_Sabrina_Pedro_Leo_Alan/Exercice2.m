%% 2.2

%syst�me lin�aire al�atoire Ax = f d'ordre n o� A est une matrice sym�trique dont le
%spectre est inclus dans [lambda_min; lambda_max]
%A est g�n�r�e avec gen_mat

n=100;
lambda_min = 3;
lambda_max = 17;

%Testons le code
a= [-0.1:0.05:0.1]; %alpgha
x0=zeros(n,1);
maxit=50;
tol=1e-15;
Plots=zeros(maxit,length(a));
[A,f,x_etoile]=gen_mat(n,lambda_min,lambda_max);

%On calcule les valeurs du graphique
for i=1:length(a)
    Phi = @(x) x - a(i)*(A*x-f) ;
    [X] = FixedPointSolve(x0,maxit,tol,Phi);
    [~,m]=size(X);
    for j=1:m
        C(j)=norm(X(:,j)-x_etoile)/norm(x_etoile);
    end
    Plots(:,i)=C';
end

%trouvons L tel que ||x(k+1)-x*||<=L||x(k)-x*||=L^k*||x(0)-x*||
%On sait que x(k+1) = x(k)-a*(A*x(k)-f) 
%       <=>  x(k+1) - x* = x(k)-a*(A*x(k)-f) -x*
%et on utilise Ax*=f 
%       <=>  x(k+1) - x* = (Id-alpha*A)(x(k)-x*)
%et on utilise la propri�t� sous-multiplicative de la norme op�rateur
%        =>||x(k+1)-x*|| <= ||(Id-alpha*A)||*||(x(k)-x*)||

L= norm(eye(n)-0.1*A);
X=1:50;
semilogy(X,Plots(:,1),X,Plots(:,2),X,Plots(:,3),'--',X,Plots(:,4),X,Plots(:,5),X,(L.^X)*norm(x_etoile),'k--')
legend('-0.1','-0.05','0','0.05','0.1','th�orique')
xlabel('Iteration number')
ylabel('Relative error')
%On voit que l'alpha qui donne la convergence la plus rapide est alpha=0.1
%% Ex 2.3
clear

%Donn�es 
n=100;
alpha = [-0.1:0.05:0.1];
x0=zeros(n,1);
maxit=40;
tol=1e-15;

lambda_min = 3;
lambda_max = 17;

%Matrice al�atoire 
[A,f,x_etoile]=gen_mat(n,lambda_min,lambda_max);


% Pour Ax=f, et  A=B-C
%x(k+1) = B^(-1)*(C*x(k) + f);

%(a) M�thode de Jacobi : B est la diagonale de A, et -C est la partie en
%dehors de la diagonale.

%phiJ = B^(-1)*(C*x(k) + f);
phiJ=@(x) diag(diag(A))\((diag(diag(A))-A)*x+f) ; 
[XJ] = FixedPointSolve(x0,maxit,tol,phiJ);

% (b) M�thode de Gauss-Seidel : B est la partie triangulaire inf�rieure
% (diagonale inclue) de A, et -C est la partie triangulaire strictement
% sup�rieure.

%phiG = B^(-1)*(C*x(k) + f);
phiG=@(x) tril(A)\((tril(A)-A)*x+f) ; 
[XG] =  FixedPointSolve(x0,maxit,tol,phiG);

%Construction du plot
%Y est le vecteur tq Y(i)=||x(k)-x*||, x(k) calcul� par M�th. Jacobi
%Z est le vecteur tq Z(i)=||x(k)-x*||, x(k) calcul� par M�th. Gauss-Seidel

[~,m]=size(XJ);
Y=zeros(1,m);
for j=1:m
        Y(j)=norm(XJ(:,j)-x_etoile);
end

[~,m]=size(XG);
Z=zeros(1,m);
for j=1:m
        Z(j)=norm(XG(:,j)-x_etoile);
end

%||x(k+1)-x*||_2 <= gamma*||x(k)-x*||_2
%Trouvons les facteurs de croissance beta=max(abs(eig(B^-1*C)))
beta_J  = max(abs(eig(diag(diag(A))\(diag(diag(A))-A))));
beta_GS = max(abs(eig(tril(A)\(tril(A)-A))));

%On peut approximer ces coefficients en regardant le rapport entre 
%||x(k+1)-x*||_2 et ||x(k)-x*||_2. Prenons par exemple la moyenne des 5
%derniers rapport de ce type
BETA_J=mean(Y(35:40)./Y(34:39));
BETA_GS=mean(Z(35:40)./Z(34:39));

%et on voit que les facteurs de croissance trouv�s sont tr�s proches de
%ceux calcul�s � partir du max de |valeurs propres de B^-1*C|
fprintf('Facteurs de croissance approxim� de la m�thode Jacobi est de %f \n',BETA_J);
fprintf('Facteurs de croissance approxim� de la m�thode Gauss-Seidel est de %f \n',BETA_GS);


%Plot
subplot(1,2,1)
X=1:maxit;
semilogy(X,Y,'r',X,(beta_J.^X)*norm(x_etoile) ,'r--',X,Z,'b', X,(beta_GS.^X)*norm(x_etoile) ,'b--')
legend('M�thode de Jacobi','Pente Jacobi','M�thode de Gauss-Seidel','Pente Gauss-Seidel')
title('Exercice 2.3')
ylabel('||x(k)-x_*||_2')
xlabel('Iteration number')

subplot(1,2,2)
semilogy(1:39,abs(beta_J-Y(2:40)./Y(1:39)),'r',1:39,abs(beta_GS-Z(2:40)./Z(1:39)),'b');
ylabel('||x(k+1)-x_*||/||x(k)-x_*||-beta')
xlabel('Iteration number')
title('Diff�rence entre le facteur de croissance et ||x(k+1)-x_*||/||x(k)-x_*||')
legend('M�thode de Jacobi','M�thode de Gauss-Seidel')

%
%% Fonction gen_mat
%Donn�e en �nonc� 
function [A,f,x]=gen_mat(n,lambda_min,lambda_max)
[U,~] = qr(rand(n));
d = lambda_min + (lambda_max-lambda_min) * rand(n,1);
A = U * diag(d) * U';
x = 2*rand(n,1)-1;
f = A*x;
end