f1 = @(x) x.^2-3*x-4 ;
f2 = @(x) (x+1).*(x+5).^3.*(x-5).^5 ;

x1=-3.8;x2=0.5;maxiter=30;tol=10^-10;

%Ces fonctions possede plusieurs racines
%Ici, on trouve toutes les racines :
% -1
[x,~] = Bissection(f1,x1,x2,maxiter,tol) %nous donne une racine de f1
[b,~] = Bissection(f2,x1,x2,maxiter,tol)

%Pour trouver toutes les racines: Changeons d'intervalle
%Pour f1: la fonction a un z�ro en -1 et 4: on applique Bissection sur une
%subdivision de l'intervalle (-6,6) assez petit pour que chaque
%sous-intervalle ne contienne qu'une racine. 

%Pour f2:Ici on peut voir que la fonction est telle que f(x)<0 pour x<-5
%et f(x)>0 pour x>5. Donc on peut diviser l`intervalle -6,6 
%en intervalles de longueur l assez petite afin quelle ne contienne qu'une
%seule racine

%Ici, on choisit un intervalle tel que les racines ne sont pas sur les
%bords des sous-intervalles. 

l=0.0023;
L=-6:l:6;
Racinesf1=[];
Racinesf2=[];
for i=1:length(L)-1
    a=L(i);
    b=L(i+1);
    if f1(b)*f1(a)<=0
        x=Bissection(f1,a,b,maxiter,tol);
    Racinesf1=[Racinesf1 x];
    end
    if f2(b)*f2(a)<=0
        x=Bissection(f2,a,b,maxiter,tol);
    Racinesf2=[Racinesf2 x];
    end
end 
Racinesf1
Racinesf2
%% 1.3:
f1 = @(x) x.^2-3*x-4 ;
f2 = @(x) (x+1).*(x+5).^3.*(x-5).^5 ;

a=-3.8;b=0.5;maxiter=30;tol=10^-10;

[~,X1]=Bissection(f1,a,b,50,tol);
[~,X2]=Bissection(f2,a,b,50,tol);
L1=length(X1) %le nombre d'it�ration n�cessaire pour une tol de 10^(-10)
L2=length(X2) %le nombre d'it�ration n�cessaire pour une tol de 10^(-10)
x1=1:L1; x2=1:L2;
y1=abs(X1+1); y2=abs(X2+1); %on calcule l'erreur entre la racine et la 
%valeur it�r�e 
semilogy(x1,y1,'.r',x2,y2,'ob',[0 50],[tol,tol],'--',x1,((1/2).^x1),'--');
xlabel('Iteration Number');
ylabel('Error');
legend('f1','f2','tol','0.5^i');
%k doit �tre plus grand que:
log((0.5+3.8)/tol)/(log(2)) %=35.32--> on prend k =36 =length(X2)
%cela correspond donc bien au cours.


%% 1.4:
%on dit que la convergence de la suite d'it�ration est d'ordre au moins m
%s'il existe une constante C telle que norm(x(k+1)-x*)<(ou
%�gale)Cnorm(x(k)-x*)^m. 

%la convergence est lin�aire: m=1 et c<1. 

%En effet, on a que |x*-x(k)| <=2^-k * (b-a), k>=1 et |x*-x(k)|<=1/2*|x*-x(k-1)|
%C=|x*-x(k)|/|x*-x(k-1)|=1/2

%On voit ici que le nombre exactes en plus ou moins propotionnelle au
%nombre d'it�rations, si l'on ignore les erreures d'arrondi. 
C=abs(X1+1)';
% C=
%    0.650000000000000
%    0.425000000000000
%    0.112500000000000
%    0.156250000000000
%    0.021875000000000
%    0.045312500000000
%    0.011718750000000
%    0.005078125000000
%    0.003320312500000
%    0.000878906250000
%    0.001220703125000
%    0.000170898437500
%    0.000354003906250
%    0.000091552734375
%    0.000039672851563
%    0.000025939941406
%    0.000006866455078
%    0.000009536743164
%    0.000001335144043
%    0.000002765655518
%    0.000000715255737
%    0.000000309944153
%    0.000000202655792
%    0.000000053644180
%    0.000000074505806
%    0.000000010430813
%    0.000000021606684
%    0.000000005587935
%    0.000000002421439
%    0.000000001583248
%    0.000000000419095
%    0.000000000582077
%    0.000000000081491
%    0.000000000168802
%    0.000000000043656
%    0.000000000018918











