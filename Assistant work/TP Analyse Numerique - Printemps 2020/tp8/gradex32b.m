function df = gradex32b(inputs)
udat= [-0.0480;-0.0100; 0.0490;-0.0190; 0.0600; 0.0125];
vdat= [ 0.0290; 0.0305; 0.0285; 0.0115;-0.0005;-0.0270];
xdat= [ 9855; 8170; 2885; 8900; 5700; 8980];
ydat= [ 5680; 5020;  730; 7530; 7025;11120];
zdat= [ 3825; 4013; 4107; 3444; 3008; 3412];

x0 = inputs(1);
y0 = inputs(2);
z0 = inputs(3);
a  = inputs(4);
b  = inputs(5);
c  = inputs(6);
th = inputs(7);

rac= sqrt(a^2 + b^2);
h1 = b/rac;
h2 =-a/rac;
h3 = 0;
drac=-[0,0,0, a,b,0,0]/rac;
dh1= [0,0,0, 0,1,0,0]/rac + h1*drac/rac;
dh2= [0,0,0,-1,0,0,0]/rac + h2*drac/rac;
dh3=zeros(1,7);

rac= sqrt((a*c)^2 + (b*c)^2 + (a^2 + b^2)^2);
g1 =-a*c/rac;
g2 =-b*c/rac;
g3 = (a^2 + b^2)/rac;
drac=-[0,0,0,a*c^2+2*a*(a^2+b^2),b*c^2+2*b*(a^2+b^2),c*(a^2+b^2),0]/rac^2;
dg1= [0,0,0,-c,0,-a,0]./rac + g1*drac;
dg2= [0,0,0,0,-c,-b,0]./rac + g2*drac;
dg3= [0,0,0,2*a,2*b,0,0]./rac + g3*drac;

df = zeros(18,7);
I  = eye(7);
for k = 1:6
    u = udat(k)*cos(th) + vdat(k)*sin(th);
    v =-udat(k)*sin(th) + vdat(k)*cos(th);
    du= [zeros(1,6), -udat(k)*sin(th)+vdat(k)*cos(th)];
    dv= [zeros(1,6), -udat(k)*cos(th)-vdat(k)*sin(th)];
    
    w1= a + h1*u + g1*v; dw1= I(4,:) + dh1*u + h1*du + dg1*v + g1*dv;
    w2= b + h2*u + g2*v; dw2= I(5,:) + dh2*u + h2*du + dg2*v + g2*dv;
    w3= c + h3*u + g3*v; dw3= I(6,:) + dh3*u + h3*du + dg3*v + g3*dv;
    q1= xdat(k)-x0;      dq1= -I(1,:);
    q2= ydat(k)-y0;      dq2= -I(2,:);
    q3= zdat(k)-z0;      dq3= -I(3,:);
    
    df(3*(k-1) + 1,:)= dw1*q2 + w1*dq2 - dw2*q1 - w2*dq1;
    df(3*(k-1) + 2,:)= dw2*q3 + w2*dq3 - dw3*q2 - w3*dq2;
    df(3*(k-1) + 3,:)= dw3*q1 + w3*dq1 - dw1*q3 - w1*dq3;
end
end