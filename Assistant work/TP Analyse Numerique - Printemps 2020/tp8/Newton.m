function [x,X] = Newton(f,df,x0,tol,maxiter)
%NEWTON sovles the equation f(x)=0 using Newton's method with initial guess
%x0.
%   Terminates if step length less than tol or number of iterations exceeds
%   maxiter. Output X contains the kth iterate in the kth column.

x = x0 - ( df(x0) \ f(x0) );
X = x;
iter = 2;
while norm(x - x0)>tol && iter < maxiter
    x0= x;
    x = x0 - ( df(x0) \ f(x0) );
    X = [X x];
    iter = iter+1;
end