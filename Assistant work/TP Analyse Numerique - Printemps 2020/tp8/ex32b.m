function f = ex32b(inputs)
udat= [-0.0480;-0.0100; 0.0490;-0.0190; 0.0600; 0.0125];
vdat= [ 0.0290; 0.0305; 0.0285; 0.0115;-0.0005;-0.0270];
xdat= [ 9855; 8170; 2885; 8900; 5700; 8980];
ydat= [ 5680; 5020;  730; 7530; 7025;11120];
zdat= [ 3825; 4013; 4107; 3444; 3008; 3412];

x0 = inputs(1);
y0 = inputs(2);
z0 = inputs(3);
a  = inputs(4);
b  = inputs(5);
c  = inputs(6);
th = inputs(7);

rac= sqrt(a^2 + b^2);
h1 = b/rac;
h2 =-a/rac;
h3 = 0;

rac= sqrt((a*c)^2 + (b*c)^2 + (a^2 + b^2)^2);
g1 =-a*c/rac;
g2 =-b*c/rac;
g3 = (a^2 + b^2)/rac;

f  = zeros(18,1);
for k = 1:6
    u = udat(k)*cos(th) + vdat(k)*sin(th);
    v =-udat(k)*sin(th) + vdat(k)*cos(th);
    
    w1= a + h1*u + g1*v;
    w2= b + h2*u + g2*v;
    w3= c + h3*u + g3*v;
    q1= xdat(k)-x0;
    q2= ydat(k)-y0;
    q3= zdat(k)-z0;
    
    f(3*(k-1) + 1) = w1*q2 - w2*q1;
    f(3*(k-1) + 2) = w2*q3 - w3*q2;
    f(3*(k-1) + 3) = w3*q1 - w1*q3;
end
end