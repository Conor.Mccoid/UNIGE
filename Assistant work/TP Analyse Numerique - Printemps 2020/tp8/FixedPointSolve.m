function [x,X] = FixedPointSolve(A,f,x0,alpha,maxit,tol)
% FIXEDPOINTSOLVE solves a linear system Ax=f using a fixed point iteraiton
%   The function terminates when number of iterations exceeds maxit or
%   when the relative step size becomes smaller than tol.
%   Output X is a matrix with the kth column the kth iterate.

X = x0;
x = x0 - alpha*(A*x0 - f);
iter = 2;
while norm(x-x0)/norm(x) > tol && iter < maxit
    x0= x;
    x = x0 - alpha*(A*x0 - f);
    X = [X x];
    iter = iter+1;
end