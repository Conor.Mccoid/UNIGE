%% Ex1

F1 = @(t,y) (1-4*t/3).*y;
dF1= @(t,y) (1-4*t/3).*eye(length(y));
Y1 = 1;
T1 = 5;
N1 = 50;
t  = linspace(0,T1,N1+1);

[yEE,yRK,yIE,yCN]= NonlinODE(F1,T1,Y1,N1,dF1);
yExact = exp(t-2*t.^2/3);

figure(1)
plot(t,yExact,'k-',t,yEE,'r--',t,yRK,'b.',t,yIE,'g-',t,yCN,'m.')
xlabel('t')
ylabel('y(t)')
legend('Exact','Explicit Euler','Runge Kutta','Implicit Euler','Crank-Nicholson')

eT = zeros(4,7); k=1; h=zeros(7,1); m=1;
for N = [50,100,200,400,800,1600,3200]
    t = linspace(0,T1,N+1);
    [yEE,yRK,yIE,yCN]= NonlinODE(F1,T1,Y1,N,dF1);
    yExact = exp(t-2*t.^2/3);
    
    eT(1,k) = norm(yEE-yExact,m)./norm(yExact,m);
    eT(2,k) = norm(yRK-yExact,m)./norm(yExact,m);
    eT(3,k) = norm(yIE-yExact,m)./norm(yExact,m);
    eT(4,k) = norm(yCN-yExact,m)./norm(yExact,m);
    h(k) = 1./(N+1);
    k=k+1;
end

figure(2)
loglog(h,eT(1,:),'g-',h,eT(2,:),'m-',h,eT(3,:),'r-',h,eT(4,:),'b-')
xlabel('h')
ylabel([num2str(m) '-norm error'])
legend('Explicit Euler','Runge Kutta','Implicit Euler','Crank-Nicholson')

F2 = @(~,y) [y(1)-0.7*y(1)*y(2);-y(2)+1.1*y(1)*y(2)];
dF2= @(~,y) [1-0.7*y(2),-0.7*y(1);1.1*y(2),-1+1.1*y(1)];
Y2 = [0.5;1];
T2 = 50;
N2 = 1000;
t  = linspace(0,T2,N2+1);

[yEE,yRK] = NonlinODE(F2,T2,Y2,N2);

figure(3)
plot(t,yRK(1,:),'b',t,yRK(2,:),'r')
xlabel('t')
legend('u','v')

%% Ex2

A = [0,1;-1,0];
F = @(~,x) A*x;
dF= @(~,~) A;
T = 500;
Y = [1;0];
N = T/0.05 - 1;

[xEE,xRK,xIE,xCN] = NonlinODE(F,T,Y,N,dF);

figure(4)
subplot(1,3,1)
plot(xEE(1,:),xEE(2,:))
xlabel('x_1')
ylabel('x_2')
title('Explicit Euler')
subplot(1,3,2)
plot(xIE(1,:),xIE(2,:))
xlabel('x_1')
ylabel('x_2')
title('Implicit Euler')
subplot(1,3,3)
plot(xCN(1,:),xCN(2,:),'r--',xRK(1,:),xRK(2,:),'b--')
xlabel('x_1')
ylabel('x_2')
legend('Crank-Nicholson','Runge Kutta')

N = T/0.8 - 1;

[xEE,xRK,xIE,xCN] = NonlinODE(F,T,Y,N,dF);

figure(5)
subplot(1,3,1)
plot(xEE(1,:),xEE(2,:))
xlabel('x_1')
ylabel('x_2')
title('Explicit Euler')
subplot(1,3,2)
plot(xIE(1,:),xIE(2,:))
xlabel('x_1')
ylabel('x_2')
title('Implicit Euler')
subplot(1,3,3)
plot(xCN(1,:),xCN(2,:),'r--',xRK(1,:),xRK(2,:),'b--')
xlabel('x_1')
ylabel('x_2')
legend('Crank-Nicholson','Runge Kutta')

%% Ex3

F = @(t,y) -50*(y - cos(t));
T = 2;
Y = 0;
dF= @(t,y) -50;

yExact = @(t) 2500*(cos(t)-exp(-50*t))/2501 + 50*sin(t)/2501;

figure(6)
clf
hold on
for N = [20,40,80,160]
    [yEE,~,yIE,~] = NonlinODE(F,T,Y,N,dF);
    t = linspace(0,T,N+1);
    yE= yExact(t);
    
    plot(t,abs(yE-yEE),'--',t,abs(yE-yIE),'.')
end
hold off
xlabel('t')
ylabel('Error')
legend('Explicit Euler N=20','Implicit Euler N=20',...
    'Explicit Euler N=40','Implicit Euler N=40',...
    'Explicit Euler N=80','Implicit Euler N=80',...
    'Explicit Euler N=160','Implicit Euler N=160')
set(gca,'YScale','log')

T = 1000;
D = ones(29,1); D = spdiags([D,-2*D,D],-1:1,29,29)/31^2;
F = @(t,y) D*y;
dF= @(t,y) D;
x = linspace(0,1,31)'; x = x(2:end-1); Y=x.^2.*(1-x);
for N = [10,20,40]
    h = T./(N+1); t=linspace(0,T,N+1);
    [yEE,~,yIE,~] = NonlinODE(F,T,Y,N,dF);
    
    for i=1:N
        figure(7)
        plot([0;x;1],[0;yEE(:,i);0],'r-',[0;x;1],[0;yIE(:,i);0],'b-')
        xlabel('x')
        ylabel('y(t)')
        title(num2str(t(i)))
        pause(0)
    end
end