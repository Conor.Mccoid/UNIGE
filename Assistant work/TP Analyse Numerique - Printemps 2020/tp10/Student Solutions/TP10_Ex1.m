%% Exercice 1
close all;
clear;
clc;
%% 1.1) Test des programmes de résolution d'équations différentielles

F = @(t,y)(1-4/3*t)*y;
dF = @(t,y) (1-4/3*t);

y0=1; Tmin=0; Tmax=5; N=50;

[TEE,YEulerExplicite] = EulerExplicite(F,Tmin,Tmax,N,y0);
[TEI,YEulerImplicite] = EulerImplicite(F,dF,Tmin,Tmax,N,y0);
[TCN,YCrankNicolson] = CrankNicolson(F,dF,Tmin,Tmax,N,y0);
[TRK,YRungeKutta] = RungeKutta(F,Tmin,Tmax,N,y0);
T=linspace(Tmin, Tmax, 50); SolExacte = @(t) exp(t-(2/3)*t.^2);



figure(1);
hold on
plot(TEE,YEulerExplicite);
plot(TEI,YEulerImplicite);
plot(TCN,YCrankNicolson);
plot(TRK,YRungeKutta);
plot(T, SolExacte(T))
hold off
xlabel('t') 
ylabel('y_{h}')
grid on
legend('EE', 'IE', 'CN', 'RK', 'exact');

%% 1.1.2) Comparer les erreurs des methodes pour plusieurs valeurs de N


%Erreur relative pour differentes valeurs de N


 eps_TEE = [];
 eps_TIE = [];
 eps_TCN = [];
 eps_TRK = [];
 err_absTEE = [];
 h = [];
 
 
%numerateur = 0;

% %Calcul des solutions discretes pour divers valeurs de N :
 for N=10:1:50
     T = linspace(Tmin,Tmax, N+1)';
     h(N) = Tmax/N;
     [TEE,YEulerExplicite] = EulerExplicite(F,Tmin,Tmax,N,y0);
     [TEI,YEulerImplicite] = EulerImplicite(F,dF,Tmin,Tmax,N,y0);
     [TCN,YCrankNicolson] = CrankNicolson(F,dF,Tmin,Tmax,N,y0);
     [TRK,YRungeKutta] = RungeKutta(F,Tmin,Tmax,N,y0);
     eps_TEE(N) = abs((SolExacte(T(N+1)) - YEulerExplicite(N+1))) /abs( SolExacte(T(N+1)));
     %err_absTEE(N) = norm((SolExacte(T(N)) - YEulerExplicite(N)), 2) /abs( SolExacte(T(N)));
     eps_TIE(N) = abs((SolExacte(T(N+1)) - YEulerImplicite(N+1))) /abs( SolExacte(T(N+1)));
     eps_TCN(N) = abs((SolExacte(T(N+1)) - YCrankNicolson(N+1))) /abs( SolExacte(T(N+1)));
     eps_TRK(N) = abs((SolExacte(T(N+1)) - YRungeKutta(N+1))) /abs( SolExacte(T(N+1)));
     
   
     

 end
% 
% 
figure(2);
hold on
%loglog(h,err_absTEE);
plot(h,eps_TIE);
plot(h,eps_TCN);
plot(h,eps_TRK);
plot(h,h);
hold off
xlabel('h')
ylabel('||y(T) - y_{N}||_{2}')
% xlim([1e-4,1e-2]);
% ylim([1e-20,1e-5]);
% % axis equal
% grid on
% legend('EE','IE','CN','RK','O(h)');

%% 1.2 Lotka-Volterra

% alpha = 1; beta = 2; gamma = 1.1; delta = 0.7;
% Tmin = 0; Tmax = 50;
% T = linspace(Tmin,Tmax,N+1);




%% Fonctions 

function [T,Y] = EulerExplicite(F,Tmin,Tmax,N,y0)
h = (Tmax-Tmin)/N; %Pas temporel
T = linspace(Tmin,Tmax, N+1)'; %intervalle de recherche 
Y = zeros(N+1,1); 

Y(1)=y0; 
for j=2:1:N+1
    Y(j) = Y(j-1)+h*F(T(j-1), Y(j-1));
end

end


function [T,Y] = EulerImplicite(F,dF,Tmin,Tmax,N,y0)
h = (Tmax-Tmin)/N;
T = linspace(Tmin,Tmax, N+1)';

Y = zeros(N+1, length(y0));
Y(1,:)=y0; maxiter = 100; tol = 1e-15;
    
for j=2:N+1
    G = @(y) -y+Y(j-1,:)+h*F(T(j), y);
    dG = @(y) -eye(length(y0))+h*dF(T(j), y);
    [Y(j,:), ~] = NewtonSolve(G, dG, Y(j-1,:), maxiter, tol);
end

end


function [T,Y] = CrankNicolson(F,dF,Tmin,Tmax,N,y0)
h = (Tmax-Tmin)/N;
T = linspace(Tmin,Tmax, N+1)';

Y = zeros(N+1, length(y0));
Y(1,:)=y0; maxiter = 100; tol = 1e-15;

for j=2:1:N+1
    G = @(y) -y+Y(j-1,:)+(h/2)*( F(T(j), y) + F(T(j-1), Y(j-1,:)) );
    dG = @(y) -eye(length(y0))+(h/2)*dF(T(j), y);
    [Y(j,:), ~] = NewtonSolve(G, dG, Y(j-1,:), maxiter, tol);
end

end


function [T,Y] = RungeKutta(F,Tmin,Tmax,N,y0)
h = (Tmax-Tmin)/N;
T = linspace(Tmin,Tmax, N+1)';
Y = zeros(N+1,1);

Y(1)=y0;
for j=2:1:N+1
    k1 = F(T(j-1), Y(j-1));
    k2 = F(T(j-1)+h/2, Y(j-1)+h/2*k1);
    k3 = F(T(j-1)+h/2, Y(j-1)+h/2*k2);
    k4 = F(T(j-1)+h, Y(j-1)+h*k3);
    Y(j) = Y(j-1)+(h/6)*(k1+2*k2+2*k3+k4);
end

end


function [x,X] = NewtonSolve(f, df, x0, maxit, tol)

  n = numel(x0);
  X = [];
  
  x = x0;
  for i = 1 : maxit
	s = df(x) \ f(x);
    
	if ( norm(s) <= tol * norm(x) )
		break
	end
    x = x - s;
    if ( nargout == 2 ) %nargout donne le nombre d'argument en retour de la fonction définie par l'utilisateur
      X = [X x];
    end
  end
 
  return
end
	  