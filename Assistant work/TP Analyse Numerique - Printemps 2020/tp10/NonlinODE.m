function [yEE,yRK,yIE,yCN] = NonlinODE(F,T,Y0,N,dF)
t = linspace(0,T,N+1);
yEE = ExplicitEuler(F,t,Y0);
yRK = RungeKutta(F,t,Y0);
if nargin>4
    yIE = ImplicitEuler(F,t,Y0,dF);
    yCN = CrankNicolson(F,t,Y0,dF);
end
end

function y = ExplicitEuler(f,t,y0)
y = zeros(length(y0),length(t));
y(:,1) = y0;
for i=2:length(t)
    ti=t(i-1);
    y(:,i) = (t(i)-ti)*f(ti,y(:,i-1)) + y(:,i-1);
end
end

function y = ImplicitEuler(f,t,y0,df)
y = zeros(length(y0),length(t));
y(:,1) = y0;
for i=2:length(t)
    ti=t(i-1); h=t(i)-ti; yi=y(:,i-1);
    F = @(y) y-h*f(ti+h,y)-yi; dF = @(y) eye(length(y0)) - h*df(ti+h,y);
    y(:,i) = NewtonSolve(F,dF,yi,100,1e-6);
end
end

function y = CrankNicolson(f,t,y0,df)
y = zeros(length(y0),length(t));
y(:,1) = y0;
for i=2:length(t)
    ti=t(i-1); h=t(i)-ti; yi=y(:,i-1);
    F = @(y) y - h*f(ti+h,y)/2 - h*f(ti,yi)/2 - yi;
    dF= @(y) eye(length(y0)) - h*df(ti+h,y)/2;
    y(:,i) = NewtonSolve(F,dF,yi,100,1e-6);
end
end

function y = RungeKutta(f,t,y0)
y = zeros(length(y0),length(t));
y(:,1) = y0;
for i= 2:length(t)
    ti=t(i-1); h=t(i)-ti; yi=y(:,i-1);
    k1=f(ti,yi);
    k2=f(ti+h/2,yi+h*k1/2);
    k3=f(ti+h/2,yi+h*k2/2);
    k4=f(ti+h  ,yi+h*k3);
    y(:,i) = h*(k1+2*k2+2*k3+k4)/6 + yi;
end
end