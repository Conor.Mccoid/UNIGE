% Ex1
T = 5;
h = 0.1;
N = T/h;
y = zeros(2,N+1); y(:,1) = [1;0];

for n=1:N
    y(:,n+1) = h*[y(2,n);-sin(y(1,n))] + y(:,n);
end

plot(0:h:T,y(1,:))
xlabel('t')
ylabel('y(t)')

%% Ex2
n = 100;
D = diag(linspace(10,1,n));
[Q,~] = qr(rand(n));
A = Q*D*Q';
P = rand(n);
B = P*(D/P);
x0A = Q(:,end) + rand(n,1)/n;
x0B = P(:,end) + rand(n,1)/n;
e0 = 1 + 0.04;

[~,~,~,ekA,sinA] = eigNewton(A,x0A,e0,10,eps,Q(:,end));
[~,~,~,ekB,sinB] = eigNewton(B,x0B,e0,10,eps,P(:,end));

figure(1)
subplot(1,2,1)
semilogy(1:length(ekA),abs(1-ekA),'--',1:length(ekA),sinA,'.-')
xlabel('Iteration')
ylabel('Error')
title('A=QDQ''')
legend('Eigenvalue','Eigenvector')

subplot(1,2,2)
semilogy(1:length(ekB),abs(1-ekB),'--',1:length(ekB),sinB,'.-')
xlabel('Iteration')
ylabel('Error')
title('B=PDP^{-1}')
legend('Eigenvalue','Eigenvector')

%% Ex3

[~,~,~,RQA,RQsinA] = eigNewton_Rayleigh(A,x0A,e0,10,eps,Q(:,end));
[~,~,~,RQB,RQsinB] = eigNewton_Rayleigh(B,x0B,e0,10,eps,P(:,end));

figure(2)
subplot(1,2,1)
semilogy(1:length(ekA),abs(1-ekA),'r--',1:length(ekA),sinA,'r.-',...
    1:length(RQA),abs(1-RQA),'b--',1:length(RQsinA),RQsinA,'b.-')
xlabel('Iteration')
ylabel('Error')
title('A=QDQ''')
legend('Newton val','Newton vec','RQI val','RQI vec')

subplot(1,2,2)
semilogy(1:length(ekB),abs(1-ekB),'r--',1:length(ekB),sinB,'r.-',...
    1:length(RQB),abs(1-RQB),'b--',1:length(RQsinB),RQsinB,'b.-')
xlabel('Iteration')
ylabel('Error')
title('B=PDP^{-1}')
legend('Newton val','Newton vec','RQI val','RQI vec')