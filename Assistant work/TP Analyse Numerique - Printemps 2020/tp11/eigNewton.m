function [v,e,vk,ek,sink] = eigNewton(A,v0,e0,maxit,tol,v_exact)
I = eye(size(A));
f = @(v,e) [ (A - e*I)*v ; 0.5*v'*v - 1];
df= @(v,e) [ (A - e*I), -v; v', 0];
v = v0; e = e0;
if nargout>2
    vk = zeros(length(v),maxit);
    ek = zeros(1,maxit);
    if nargout>4
        sink = zeros(1,maxit);
    end
end
for i = 1:maxit
    s = df(v,e) \ f(v,e);
    if ( norm(s) <= tol * norm([v;e]) )
		break
    end
    v = v - s(1:end-1);
    e = e - s(end);
    if nargout>2
        vk(:,i) = v;
        ek(i)   = e;
        if nargin==6
            sink(i) = subspace(vk,v_exact);
        end
    end
end
end