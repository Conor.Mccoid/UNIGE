function x = solveCholesky(A,b)
L = Cholesky(A);
n = length(b);
y = zeros(n,1);
y(1) = b(1) / L(1,1);
for k=2:n
    ind = 1:(k-1);
    y(k) = ( b(k) - L(k,ind)*y(ind) )/L(k,k);
end
x = zeros(n,1);
x(end) = y(end) / L(end,end);
for k=1:(n-1)
    ind = (n-k+1):n;
    x(n-k) = ( y(n-k) - L(ind,n-k)'*x(ind) )/L(n-k,n-k);
end