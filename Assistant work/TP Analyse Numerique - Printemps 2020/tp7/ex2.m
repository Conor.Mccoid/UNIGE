n = 50;
N = 20;
F = gallery('frank',n,n);
I = eye(n);
a = linspace(1,10,N);
err = zeros(N,4);
con = zeros(N,1);
for k=1:N
    A = F + a(k)*I;
    [Qg,Rg] = GramSchmidt(A);
    [Qm,Rm] = GramSchmidtMod(A);
    [Qh,Rh] = Householder(A);
    [Qt,Rt] = qr(A);
    err(k,:)= [ norm(Qg'*Qg - I), norm(Qm'*Qm - I), norm(Qh'*Qh - I), norm(Qt'*Qt - I) ];
    con(k)  = cond(A);
end

%%
semilogy(a,err,'*--',a,con,'o--')
xlabel('\alpha')
ylabel('Error')
legend('GS','GSmod','Hh','qr','\kappa(A)')