f = @(x) exp(sin(4*x));
m = 100;
n = 15;
t = linspace(0,1,m)';
A = zeros(m,n);
for j = 1:n
    A(:,j) = t.^(j-1);
end
b = f(t) ./ 2006.787453080206;

x1= A \ b; disp(x1(15)-1)
x2= solveCholesky(A'*A,A'*b); disp(x2(15)-1)
[Q,R] = GramSchmidtMod(A); x3= solveQR(Q,R,b); disp(x3(15)-1)
[Q,R] = Householder(A); x4= solveQR(Q,R,b); disp(x4(15)-1)

plot(t,b,'k-',t,A*x1,'.--',t,A*x2,'.--',t,A*x3,'.--',t,A*x4,'.--')
xlabel('t')
ylabel('f(t)')
legend('Exact','Backslash','Cholesky','GSmod','Householder')
axis([0,1,min(b),max(b)])