function [Q,R] = Householder(A)
[m,n] = size(A);
V = zeros(m,n);
for k = 1:n
    x = A(k:m,k);
    v = x;
    v(1) = v(1) + sign(x(1))*norm(x);
    if norm(v) > 0
        v = v / norm(v);
    end
    X = A(k:m,k:n);
    A(k:m,k:n) = X - 2 * (v * v') * X;
    V(k:m,k)   = v;
end
R = A(1:n,1:n);

Q = eye(m,n);
for k = n:-1:1
    Q(k:end,k:end) = Q(k:end,k:end) - 2 * (V(k:end,k) * V(k:end,k)') * Q(k:end,k:end);
end