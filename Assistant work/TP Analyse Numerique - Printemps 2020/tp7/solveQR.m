function x = solveQR(Q,R,b)
y = Q'*b;
n = length(y);
x = y;
x(end) = y(end) / R(end,end);
for k=1:(n-1)
    ind = (n-k+1):n;
    x(n-k) = ( y(n-k) - R(n-k,ind)*x(ind) )/R(n-k,n-k);
end