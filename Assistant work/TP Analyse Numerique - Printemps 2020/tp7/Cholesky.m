function L = Cholesky(A)
n = length(A);
L = zeros(n);
for j = 1:n
    L(j,j) = sqrt( A(j,j) - L(j,1:(j-1))*L(j,1:(j-1))' );
    for i = j+1:n
        L(i,j) = ( A(i,j) - L(i,1:(j-1))*L(j,1:(j-1))')/L(j,j);
    end
end