function [Q,R] = GramSchmidtMod(A)
[m,n] = size(A);
Q = A;
R = zeros(n,n);

R(1,1) = norm(Q(:,1));
Q(:,1) = Q(:,1) / R(1,1);

for k=2:n
    R(k-1,k:end) = Q(:,k-1)'*Q(:,k:end);
    Q(:,k:end) = Q(:,k:end) - Q(:,k-1)*R(k-1,k:end);
    R(k,k) = norm(Q(:,k));
    Q(:,k) = Q(:,k) / R(k,k);
end