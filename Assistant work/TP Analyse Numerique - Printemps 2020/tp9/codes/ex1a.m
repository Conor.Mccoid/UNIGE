% Example 2

clear
clc

v=[1:51]';
t=0.02*(v-1);

ue = @(t)exp(-2*t).*cos(20*t);
u = ue(t);

b = u + 0.2*(2*rand(size(u))-1);

x0 = [1.2,-1.9,18]';
tol = 1.0e-12;
max_iter = 15;

[x,iter,err] = gauss_newton(@A_jacob,@g_fun,b,x0,t,max_iter,tol);

uc = x(1)*exp(x(2)*t).*cos(x(3)*t);

figure(1)
plot(t,u,'-',t,b,'go',t,uc','r*')
legend('ground truth','observed data','Gauss Newton')
axis([0 1 -1 1.5])
