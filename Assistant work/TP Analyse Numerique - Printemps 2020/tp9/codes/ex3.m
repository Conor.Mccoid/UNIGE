n=24; h=1/(n+1); t=0:h:1; tol=1e-8;
A=diag(-2*ones(1,n))+diag(ones(1,n-1),1)+diag(ones(1,n-1),-1);
F=@(x)A*x+h^2*(exp(x)-5*cos(x));
dF=@(x)A+h^2*diag(exp(x)+5*sin(x));
v01=zeros(1,n)';
[v1,V1]=NewtonSolve(F,dF,v01,100,tol);
v02=8*t(2:n+1).*(1-t(2:n+1)); v02=v02';
[v2,V2]=NewtonSolve(F,dF,v02,100,tol);
v1=[0 v1' 0]; v2=[0 v2' 0];

figure(1)
plot(t,v1,'r',t,v2,'b','LineWidth',3);
xlabel('t'); ylabel('v(t)');
legend('\alpha=0','\alpha=20')

[~,V1col] = size(V1);
V1 = [zeros(1,V1col) ; V1 ; zeros(1,V1col)];
[~,V2col] = size(V2);
V2 = [zeros(1,V2col) ; V2 ; zeros(1,V2col)];
for k = 1:min(V1col,V2col)
    figure(2)
    plot(t,V1(:,k),'b--',t,v1,'b-',t,V2(:,k),'r--',t,v2,'r-')
    xlabel('t')
    ylabel('v(t)')
    pause(0.5)
end