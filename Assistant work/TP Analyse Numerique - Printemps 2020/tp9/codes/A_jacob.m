function A = A_jacob(x,t)
    m=length(x);
    n=length(t);
    A=zeros(n,m);
    A(:,1)=exp(x(2).*t).*cos(x(3).*t);
    A(:,2)=t.*x(1).*exp(x(2).*t).*cos(x(3).*t);
    A(:,3)=-t.*x(1).*exp(x(2).*t).*sin(x(3).*t);
    