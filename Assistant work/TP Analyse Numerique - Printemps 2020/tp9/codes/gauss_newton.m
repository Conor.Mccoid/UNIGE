function [x,iter,err] = gauss_newton(A,g,b,x,t,max_iter,tol)
    p=inf;
    iter=0;
    err=norm(p);
    while norm(p)>tol & iter<max_iter
        G = A(x,t)'*A(x,t);
        f = A(x,t)'*(b-g(x,t));
        p = G\f;
        x = x + p;
        iter=iter+1;
        err=[err;norm(p)];
    end
    err=err(2:end);
    diff = norm(g(x,t)-b);